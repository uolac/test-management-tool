﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;

using System.IO;
using System.Text.RegularExpressions;
using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.AspNetCore.Mvc.Formatters;
using System.Text;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace WebApplication6
{
    //Object to store a list which spans multiple pages
    public class PagedList<T> : List<T>
    {
        public int currentpage;
        public int total;
    }

    //Conversion from list or database query to PagedList
    public static class PagedListExtensions
    {
        public static PagedList<object> ToPagedList<T>(this IEnumerable<T> source, int page)
        {
            var s = source.Skip((page-1) * 10).Take(10).Cast<object>();
            var pl = new PagedList<object>();
            pl.AddRange(s);
            pl.currentpage = page;
            pl.total = source.Count();
            return pl;
        }

        public static PagedList<object> ToPagedList<T>(this IQueryable<T> source, int page)
        {
            var s = source.Skip((page - 1) * 10).Take(10).Cast<object>();
            var pl = new PagedList<object>();
            pl.AddRange(s);
            pl.currentpage = page;
            pl.total = source.Count();
            return pl;
        }

        public static PagedList<T> ToPagedListT<T>(this IEnumerable<T> source, int page)
        {
            var s = source.Skip((page - 1) * 10).Take(10).Cast<T>();
            var pl = new PagedList<T>();
            pl.AddRange(s);
            pl.currentpage = page;
            pl.total = source.Count();
            return pl;
        }

        public static PagedList<T> ToPagedListT<T>(this IQueryable<T> source, int page)
        {
            var s = source.Skip((page - 1) * 10).Take(10).Cast<T>();
            var pl = new PagedList<T>();
            pl.AddRange(s);
            pl.currentpage = page;
            pl.total = source.Count();
            return pl;
        }
    }

    public enum PageType { Index, Edit, Create, Delete, Archive }
    public static partial class PageBuilder
    {
        public static ContentResult GetBase(string title, RoleInfo role, int project, string controller, string action, string content, string script)
        {
            var cr = new ContentResult();
            cr.ContentType = "text/html";;
            cr.Content = Html.BasePage(title, role, controller, action, content, script);
            return cr;
        }



        //Blank page
        public static ContentResult GetBlank(string content)
        {
            var cr = new ContentResult();
            cr.ContentType = "text/html";
            cr.Content = content;
            return cr;
        }

        //Generate the links for a page from the Links object passed to it
        public static string GetLinks(Links links, RoleInfo role, int project, string controller, PageType type, string idcolumn, string id, bool divider, string id2="")
        {
            ;
            var linkspre = "";
            var deletestring = "Delete";

            if(controller == "requirements" || controller == "tests")
            {
                deletestring = "Archive";
            }

            foreach (var l in links.linkfirst)
            {
                //If you wouldn't have access to the page
                if (!role.IsAllowed(l.controller, l.action))
                {
                    //Don't generate the link
                    continue;
                }
                if (l.primary)
                {
                    linkspre += @"<a class=""btn btn-primary"" href=""/" + l.controller + "/" + l.action + "/" + project + "?" + idcolumn + "=" + id + @"""" + ">" + l.label + "</a>";
                }
                else
                {
                    linkspre += @"<a class=""btn btn-default"" href=""/" + l.controller + "/" + l.action + "/" + project + "?" + idcolumn + "=" + id + @"""" + ">" + l.label + "</a>";
                }

                linkspre += divider ? @"<div class=""divider""></div>" : "";
            }

            foreach (var l in links.links)
            {
                ;
                if(l.label == "Rerun this test")
                {
                    linkspre += @"<a class=""btn btn-default"" href= ""/" + l.controller + "/" + l.action + "/" + project + "?test_id=" + id2 + @"""" +">" + l.label + "</a>";
                }
                else
                {
                    //If you wouldn't have access to the page
                    if (!role.IsAllowed(l.controller, l.action))
                    {
                        //Don't generate the link
                        continue;
                    }
                    if (l.primary)
                    {
                        linkspre += @"<a class=""btn btn-primary"" href= ""/" + l.controller + "/" + l.action + "/" + project + "?" + idcolumn + "=" + id + @"""" + ">" + l.label + "</a>";
                    }
                    else
                    {
                        linkspre += @"<a class=""btn btn-default"" href= ""/" + l.controller + "/" + l.action + "/" + project + "?" + idcolumn + "=" + id + @"""" + ">" + l.label + "</a>";
                    }
                }

                linkspre += divider ? @"<div class=""divider""></div>" : "";
            }
            linkspre += ((links.edit && type != PageType.Edit && !(!role.IsSupplierDOrAbove() && controller == "defects")) ? @"<a class=""btn btn-default"" href = ""/" + controller + "/Edit/" + project + "/" + id + @"""" + $">{(role.IsAllowed(controller, PageType.Edit) ? "Edit" : "View")}</a>" + (divider ? @"<div class=""divider""></div>" : "") : "");
            linkspre += ((links.delete && role.IsAllowed(controller, PageType.Delete) && type != PageType.Delete) ? Html.LinkButton(deletestring, controller, deletestring, project, Convert.ToInt32(id)) + (divider ? @"<div class=""divider""></div>" : "") : "");
            return linkspre;
        }

        //Get the content for a table cell based on a property of the object passed to it
        public static string GetTableContent(Column c, object firstobject, int test_cycle_id, bool links = false, int project = 0, string controller = "")
        {
            object rt = "";
            switch (c.name)
            {
                case "derived_requirement_state": return ((requirements)firstobject).DerivedState(DateTime.Now, test_cycle_id).ToEnumString();
                case "derived_test_state": return ((tests)firstobject).DerivedState(DateTime.Now, test_cycle_id).ToEnumString();
                case "scheduled": return ((tests)firstobject).Scheduled(test_cycle_id) ? "Yes" : "No";
                case "defect_descriptions": return ((runs)firstobject).defects.Select(d => d.name + ": " + d.description).Any() ? ((runs)firstobject).defects.Select(d => d.name + ": " + d.description).Aggregate((x, y) => x + "; " + y) : "";
            }
            //Two levels of indirection
            if (c.indirect == 2)
            {
                ;
                var secondobject = firstobject.BindObject(c.before);
                //If the column is an aggregate list of linked objects
                if (secondobject is IEnumerable<object>)
                {
                    var secondobjectaslist = ((IEnumerable<object>)secondobject);
                    var thirdobject = secondobjectaslist.Select(b => b?.BindObject(c.after));
                    var property = thirdobject?.Select(b2 => (links ? b2?.BindLink(c.after2, project, controller) : b2?.Bind(c.after2)) ?? c.nullval);
                    var aggregate = property.Any() ? property.Aggregate((x, y) => x + ", " + y) : "(None)";
                    rt = aggregate;
                }
                else
                {
                    ;
                    var thirdobject = secondobject?.BindObject(c.after);
                    var property = ((links ? thirdobject?.BindLink(c.after2, project, controller) : thirdobject?.Bind(c.after2)) ?? c.nullval);
                    rt = property;
                }
            }
            //One level of indirection
            else if (c.indirect == 1)
            {
                var secondobject = firstobject.BindObject(c.before);
                //If the column is an aggregate list of linked objects
                if (secondobject is IEnumerable<object>)
                {
                    var secondobjectaslist = ((IEnumerable<object>)secondobject);
                    var property = secondobjectaslist?.Select(b => (links ? b?.BindLink(c.after, project, controller) : b?.Bind(c.after)) ?? c.nullval);
                    var aggregate = property.Any() ? property.Aggregate((x, y) => x + ", " + y) : "(None)";
                    rt = aggregate;
                }
                else
                {
                    var property = ((links ? secondobject?.BindLink(c.after, project, controller) : secondobject?.Bind(c.after)) ?? c.nullval);
                    rt = property;
                }
            }
            //No indirection
            else if (c.indirect == 0)
            {
                var property = ((links ? firstobject?.BindLink(c.name, project, controller) : firstobject?.Bind(c.name)) ?? c.nullval);
                rt = property;
            }
            else
            {
                throw new NotImplementedException();
            }

            if((rt is string) && (c.name == "name" || c.name == "description" || c.name == "status_comment"))
            {
                rt = System.Net.WebUtility.HtmlDecode(rt.ToString());
            }
            
            return rt.ToString();
        }

        public static ContentResult Error403(int project, RoleInfo role = null)
        {
            var content = $"You do not have the role assigned to you on this project to view this page." + $@"<br><br><p>
        <a class=""btn btn-default"" href=""/"">Back to Homepage</a>
    </p>";
            return PageBuilder.GetBase("Error403", new RoleInfo() { user_id = 0, type = RoleType.None, project = project }, project, "", "", content, "");
        }

        public static ContentResult ModelStateError(int project, RoleInfo role, string screen, ModelStateDictionary m, string text = "")
        {

            Email.HandleError(m, role, screen);
            var content = text + $@"<br><br><p>
        <a class=""btn btn-default"" href=""/"">Back to Homepage</a>
    </p>";
            return PageBuilder.GetBase("Model State Error", new RoleInfo() { user_id = 0, type = RoleType.None, project = project }, project, "", "", content, "");
        }

        public static ContentResult Back(RoleInfo role, string title, string text, string controller, string action, int project, bool backtoindex = false)
        {
            var content = text + $@"<br><br><p>
        {Html.LinkButton(backtoindex ? "Back to Index" : "Back", controller, action, project)}
    </p>";
            return PageBuilder.GetBase(title, role, project, "", "", content, "");
        }

        //Get a named property of an object
        public static string Bind(this object o, string field)
        {
            var rt = "";

            if (o.GetType().GetProperties().Any(p => p.Name == field))
            {
                var rts = o.GetType().GetProperty(field).GetValue(o);
              
                if(rts is DateTime)
                {
                    rt = ((DateTime)rts).ToString("dd/MM/yyyy");
                }
                else if (rts == null && field == "due_date")
                {
                    rt = "(None)";
                }
                else if (rts is bool)
                {
                    rt = (bool)rts ? "Yes" : "No";
                }
                else
                {
                    rt = rts?.ToString() ?? "";
                }

                if (field == "due_date")
                {
                    ;
                }
            }
            else
            {
                throw new Exception("Property does not exist on this entity");
            }

            return rt;
        }

        //Get a link to the page of the named property of an object
        public static string BindLink(this object o, string field, int project, string controller)
        {
            var rt = "";
            var of = o.Bind(field);
            if (o is requirements && (controller != "requirements" || field == "name")) { rt = $"/requirements/Edit/{project}/{o.Bind("requirement_id")}"; }
            else if (o is tests && (controller != "tests" || field == "name")) { rt = $"/tests/Edit/{project}/" + o.Bind("test_id"); }
            else if (o is runs && controller != "runs") { rt = $"/testruns/Edit/{project}/" + o.Bind("run_id"); }
            else if (o is defects && (controller != "defects" || field == "name")) { rt = $"/defects/AssignOrUpdate/{project}?defect_id=" + o.Bind("defect_id"); }
            else if (o is WebApplication6.DefectContainer && (controller != "defects" || field == "name") && field != "due_date") { rt = $"/defects/AssignOrUpdate/{project}?defect_id=" + o.Bind("defect_id"); }
            /*else if (o is users && (controller == "requirements" || controller == "tests")) {
                rt = $"/{controller}/Index/{project}?owner_id=" + o.Bind("user_id");
            }
            else if (o is users && (controller == "testruns"))
            {
                rt = $"/{controller}/Index/{project}?run_by_id=" + o.Bind("user_id");
            }
            else if (o is users && (controller == "defects"))
            {
                rt = $"/{controller}/Index/{project}?assigned_to_id=" + o.Bind("user_id");
            }
           From	Subject	Received	Size	Categories	
Marcus Harris	Marcus Harris has shared 'SMS'	15:00	27 KB		 else if (o is modules && (controller == "requirements" || controller == "tests" || controller == "defects"))
            {
                rt = $"/{controller}/Index/{project}?module_id=" + o.Bind("module_id");
            }*/

            if (rt == "")
            {
                return of;
            }
            else
            {
                return $"<a href={rt}>{of}</a>";
            }
        }

        //Get a named property of an object as an object
        public static object BindObject(this object o, string field)
        {
            object rt = null;

            if (o.GetType().GetProperties().Any(p => p.Name == field))
            {
                ;
                rt = o.GetType().GetProperty(field).GetValue(o);
            }
            else
            {
                throw new Exception("Property does not exist on this entity");
            }

            return rt;
        }

        

    }
}


