﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Mail;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace WebApplication6
{
    public static class Email
    {
        public static void Send(users user, string subject, string message)
        {
            Send(new List<users>() { user }, subject, message);
        }

        public static void Send(IEnumerable<users> users, string subject, string message)
        {
            using (var smtp = new SmtpClient("aexccas02.network.uni"))
            {
                smtp.UseDefaultCredentials = true;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                //For each distinct email in the users passed through where they've elected to recieve emails
                foreach(var email in users.Where(u => u.email_preference_id == 2).Select(u => u.email).Distinct())
                {
                    var mail = new MailMessage
                    {
                        Subject = subject,
                        From = new MailAddress("TestManagementTool@lincoln.ac.uk"), //Would like to have a generic account set up for this
                        Body = message
                    };

                    mail.To.Add(email);

                    try
                    {
                        smtp.SendMailAsync(mail).Wait();
                    }
                    catch
                    {
                    }
                }           
            }
        }

        public static void SendError(string message, Exception ex)
        {
            using (var smtp = new SmtpClient("aexccas02.network.uni"))
            {
                smtp.UseDefaultCredentials = true;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                var mail = new MailMessage
                {
                    Subject = "Error",
                    From = new MailAddress("TestManagementTool@lincoln.ac.uk"),
                    Body = message + " " + ex.ToString()
                    };

                    mail.To.Add("aCoady@lincoln.ac.uk");

                try
                {
                    smtp.SendMailAsync(mail).Wait();
                }
                catch
                {

                }

            }
        }

        public static void SendErrorText(string message)
        {
            using (var smtp = new SmtpClient("aexccas02.network.uni"))
            {
                smtp.UseDefaultCredentials = true;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                var mail = new MailMessage
                {
                    Subject = "Error",
                    From = new MailAddress("TestManagementTool@lincoln.ac.uk"),
                    Body = message
                };

                mail.To.Add("aCoady@lincoln.ac.uk");

                try
                {
                    smtp.SendMailAsync(mail).Wait();
                }
                catch
                {

                }

            }
        }

        public static void HandleError(ModelStateDictionary m, RoleInfo r, string screen)
        {
            foreach (var e in m.Values.SelectMany(v => v.Errors))
            {
                if (e.Exception == null)
                {
                    Email.SendError(screen+" error" + r.user_name + " " + e.ErrorMessage, new Exception());
                }
                else
                {
                    Email.SendError(screen + " error" + r.user_name + " " + e.ErrorMessage, e.Exception);
                }

            }
        }
    }
}
