﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace WebApplication6
{
    [Controller]
    public class tasktestdataController : Controller
    {
        private testing2Context _context;

        public tasktestdataController(testing2Context context)
        {
            _context = context;
        }

        //Generate test data for the database
        public IActionResult Create(int project)
        {

            _context.Database.ExecuteSqlCommand("ClearDB");
            _context.SaveChanges();

            var tc = new test_cycles() { name = "Default" };

            var project_ = _context.projects.First();

            var m1 = new modules() { name = "Curriculum" };
            var m2 = new modules() { name = "Admissions" };
            project_.modules.Add(new modules() { name = "System & Reference Data" });
            project_.modules.Add(m1);
            project_.modules.Add(new modules() { name = "Fee Configuration" });
            project_.modules.Add(new modules() { name = "Applications & Offers" });
            project_.modules.Add(m2);
            project_.modules.Add(new modules() { name = "Student Financial Management" });
            project_.modules.Add(new modules() { name = "Studentships & Scholarships" });
            project_.modules.Add(new modules() { name = "Advanced Standing" });
            project_.modules.Add(new modules() { name = "Agents" });
            project_.modules.Add(new modules() { name = "Third-Party Portal" });
            project_.modules.Add(new modules() { name = "Student Self-Service" });
            project_.modules.Add(new modules() { name = "Access & Security" });
            project_.modules.Add(new modules() { name = "Student Records Administration" });
            project_.modules.Add(new modules() { name = "Enquiries" });
            project_.modules.Add(new modules() { name = "Recruitment & Attraction" });
            project_.modules.Add(new modules() { name = "UK Visas & Immigration" });
            project_.modules.Add(new modules() { name = "Statutory Reporting" });
            project_.modules.Add(new modules() { name = "Assessment, Progressions & Results" });
            project_.modules.Add(new modules() { name = "Completions" });
            project_.modules.Add(new modules() { name = "Graduation" });
            project_.modules.Add(new modules() { name = "Enrolment" });
            project_.modules.Add(new modules() { name = "Candidature Management" });
            project_.modules.Add(new modules() { name = "Reporting & Data Analysis" });
            project_.modules.Add(new modules() { name = "Workflow" });
            project_.modules.Add(new modules() { name = "Correspondence" });
            project_.modules.Add(new modules() { name = "Testing" });
            project_.modules.Add(new modules() { name = "Training" });
            project_.modules.Add(new modules() { name = "Data Migration" });
            project_.modules.Add(new modules() { name = "Change Management" });
            project_.modules.Add(new modules() { name = "Configuration" });
            project_.modules.Add(new modules() { name = "T1 Cloud" });
            project_.modules.Add(new modules() { name = "Interfaces & Integration" });
            project_.modules.Add(new modules() { name = "T1 Consulting" });
            project_.modules.Add(new modules() { name = "T1 R&D" });
            project_.modules.Add(new modules() { name = "Project Management" });
            project_.modules.Add(new modules() { name = "Technical" });
            project_.modules.Add(new modules() { name = "Customer Development" });
            project_.modules.Add(new modules() { name = "Personal Tutor" });

            project_.test_cycles.Add(tc);

            var user1 = _context.users.Where(u => u.user_id == 1).First();

            var userhm = new users() { name = "Hannah Marchant", email = "aCoady@lincoln.ac.uk", email_preference_id = 2, default_project_id = 1, ad_identifier=@"NETWORK\mgarbutt" };
            var userlqb = new users() { name = "Luke Quintana Baker", email = "lQuintanabaker@lincoln.ac.uk", email_preference_id = 2, default_project_id = 1, ad_identifier = @"NETWORK\lQuintanabaker" };
            var usermh = new users() { name = "Marcus Harris", email = "MHarris@lincoln.ac.uk", email_preference_id = 2, default_project_id = 1, ad_identifier = @"NETWORK\MHarris" };
            var usersgr = new users() { name = "Steve Green", email = "StGreen@lincoln.ac.uk", email_preference_id = 2, default_project_id = 1, ad_identifier = @"NETWORK\StGreen" };
            var useraj = new users() { name = "Alex Jones", email = "Ajones@lincoln.ac.uk", email_preference_id = 2, default_project_id = 1, ad_identifier = @"NETWORK\Ajones" };
            var userbs = new users() { name = "Ben Samuels", email = "BSamuels@lincoln.ac.uk", email_preference_id = 2, default_project_id = 1, ad_identifier = @"NETWORK\BSamuels" };
            var userar = new users() { name = "Amy Richards", email = "AmRichards@lincoln.ac.uk", email_preference_id = 2, default_project_id = 1, ad_identifier = @"NETWORK\AmRichards" };
            var usersf = new users() { name = "Sarah Fry", email = "sarah_fry@technologyonecorp.com", email_preference_id = 2, default_project_id = 1, ad_identifier = @"NETWORK\SFry" };
            var userew = new users() { name = "Evon Weir", email = "evon_weir@technologyonecorp.com", email_preference_id = 2, default_project_id = 1, ad_identifier = @"NETWORK\EWeir" };
            var usertt = new users() { name = "T1 Technical Resource", email = "aCoady@lincoln.ac.uk", email_preference_id = 2, default_project_id = 1, ad_identifier = @"NETWORK\T1Technical" };
            var userpg = new users() { name = "Pam Guy", email = "pam_guy@technologyonecorp.com", email_preference_id = 2, default_project_id = 1, ad_identifier = @"NETWORK\PGuy" };
            var userjg = new users() { name = "Jonathan Gaze", email = "JGaze@lincoln.ac.uk", email_preference_id = 2, default_project_id = 1, ad_identifier = @"NETWORK\JGaze" };
            var usersa = new users() { name = "Steven Audis", email = "SAudis@lincoln.ac.uk", email_preference_id = 2, default_project_id = 1, ad_identifier = @"NETWORK\SAudis" };
            var userhmr = new users() { name = "Helen Moody-Reed", email = "HMReed@lincoln.ac.uk", email_preference_id = 2, default_project_id = 1, ad_identifier = @"NETWORK\HMReed" };
            var usersgi = new users() { name = "Steve Gilliatt", email = "Sgilliatt@lincoln.ac.uk", email_preference_id = 2, default_project_id = 1, ad_identifier = @"NETWORK\Sgilliatt" };
            var userlp = new users() { name = "Lucy Prescott", email = "LPrescott@lincoln.ac.uk", email_preference_id = 2, default_project_id = 1, ad_identifier = @"NETWORK\LPrescott" };
            var userjb = new users() { name = "Jenny Beighton", email = "jbeighton@lincoln.ac.uk", email_preference_id = 2, default_project_id = 1, ad_identifier = @"NETWORK\jbeighton" };
            var usermy = new users() { name = "Maureen Young", email = "MYoung@lincoln.ac.uk", email_preference_id = 2, default_project_id = 1, ad_identifier = @"NETWORK\MYoung" };
            var userrh = new users() { name = "Rebecca Helliwell", email = "RHelliwell@lincoln.ac.uk", email_preference_id = 2, default_project_id = 1, ad_identifier = @"NETWORK\RHelliwell" };
            var userec = new users() { name = "Erika Coggins", email = "ECoggins@lincoln.ac.uk", email_preference_id = 2, default_project_id = 1, ad_identifier = @"NETWORK\ECoggins" };

            _context.SaveChanges();

            user1.users_projects_roles.Add(new users_projects_roles() { project = project_, role_id = 3 });

            usermh.users_projects_roles.Add(new users_projects_roles() { project = project_, role_id = 4 });
            usersgr.users_projects_roles.Add(new users_projects_roles() { project = project_, role_id = 4 });

            userhm.users_projects_roles.Add(new users_projects_roles() { project = project_, role_id = 1 });
            userlqb.users_projects_roles.Add(new users_projects_roles() { project = project_, role_id = 1 });
            useraj.users_projects_roles.Add(new users_projects_roles() { project = project_, role_id = 1 });
            userbs.users_projects_roles.Add(new users_projects_roles() { project = project_, role_id = 1 });
            userar.users_projects_roles.Add(new users_projects_roles() { project = project_, role_id = 1 });
            usersf.users_projects_roles.Add(new users_projects_roles() { project = project_, role_id = 1 });
            userew.users_projects_roles.Add(new users_projects_roles() { project = project_, role_id = 1 });
            usertt.users_projects_roles.Add(new users_projects_roles() { project = project_, role_id = 1 });
            userpg.users_projects_roles.Add(new users_projects_roles() { project = project_, role_id = 1 });
            userjg.users_projects_roles.Add(new users_projects_roles() { project = project_, role_id = 1 });
            usersa.users_projects_roles.Add(new users_projects_roles() { project = project_, role_id = 1 });
            userhmr.users_projects_roles.Add(new users_projects_roles() { project = project_, role_id = 1 });
            usersgi.users_projects_roles.Add(new users_projects_roles() { project = project_, role_id = 1 });
            userlp.users_projects_roles.Add(new users_projects_roles() { project = project_, role_id = 1 });
            userjb.users_projects_roles.Add(new users_projects_roles() { project = project_, role_id = 1 });
            usermy.users_projects_roles.Add(new users_projects_roles() { project = project_, role_id = 1 });
            userrh.users_projects_roles.Add(new users_projects_roles() { project = project_, role_id = 1 });
            userec.users_projects_roles.Add(new users_projects_roles() { project = project_, role_id = 1 });


            _context.users.Update(user1);
            _context.users.Add(userhm);
            _context.users.Add(userlqb);
            _context.users.Add(useraj);
            _context.users.Add(userbs);
            _context.users.Add(userar);
            _context.users.Add(usersf);
            _context.users.Add(userew);
            _context.users.Add(usertt);
            _context.users.Add(userpg);
            _context.users.Add(userjg);
            _context.users.Add(usersa);
            _context.users.Add(userhmr);
            _context.users.Add(usersgi);
            _context.users.Add(usermh);
            _context.users.Add(usersgr);
            _context.users.Add(userlp);
            _context.users.Add(userjb);
            _context.users.Add(usermy);
            _context.users.Add(userrh);
            _context.users.Add(userec);

            _context.SaveChanges();

            project_.current_test_cycle = tc;

            _context.SaveChanges();

            var defects = new List<defects>();      

            defects.Add(new defects() { test_run = null, project_id = project_.project_id, test_run_id = null, module = m1,
                name = "Curriculum structure decision"
                ,description = "Decide on structure"
                ,found_byNavigation = usersgi
                ,found_date = new DateTime(2016, 11, 9)
                ,due_date = new DateTime(2016, 11, 20)
            });

            defects[0].defect_history.Add(new defect_history() { priority_id = 3, severity_id = 5, defect_status_id = 2, closure_reason_id = 1, effective_date = DateTime.Now.AddDays(-1), assigned_toNavigation = userjg });

            defects.Add(new defects()
            {
                test_run = null,
                project_id = project_.project_id,
                test_run_id = null,
                module = m2,
                name = "Admissions master spreadsheet"
                ,
                description = "Create spreadsheet"
                ,
                found_byNavigation = userew
                ,
                found_date = new DateTime(2016, 11, 6)
                ,
                due_date = new DateTime(2016, 12, 10)
            });

            defects[1].defect_history.Add(new defect_history() { priority_id = 3, severity_id = 5, defect_status_id = 1, closure_reason_id = 1, effective_date = DateTime.Now.AddDays(-3) });

            _context.defects.AddRange(defects);

            _context.SaveChanges();

            return RedirectToAction("Index", "requirements", new { project = project_.project_id });
        }
    }
}
