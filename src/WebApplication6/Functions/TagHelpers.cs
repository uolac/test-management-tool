﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Http;

namespace WebApplication6
{ 

    public class MultiFieldTagHelper : TagHelper
    {
        [ViewContext]
        public ViewContext vc { get; set; }
        public MultiField mf { get; set; }
        public bool rdonly { get; set; } = false;

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "";
            var script = "";
            output.Content.SetHtmlContent(ParseMultiField(mf, vc.controller(), vc.projectint(), ref script, rdonly)+script);
        }

        public static string ParseMultiField(MultiField mf, string controller, int project, ref string script, bool rdonly)
        {
            string control = "";

            //Setup for the multi-field Add/Remove buttons
            var outer = Html.multiouter;
            var inner1 = new List<string>();
            var inner2 = "";
            var scriptinner = "";
            var multiinnertext = rdonly ? Html.multiinnerrdonly : (mf.removebuttons ? Html.multiinner : Html.multiinnernoremove);
            var multioutertext = rdonly ? Html.multiouterrdonly : Html.multiouter;

            //Static setup of javascript and new empty controls
            foreach (Field f in mf.fields)
            {
                if (f is Textbox)
                {
                    //Create a javascript section for creating new empty textboxes
                    scriptinner += Html.TextboxForMultiField(mf.hiddenname.Replace("_id", "_") + f.name, f.label, "", rdonly, f.helptext);
                }
                else if (f is Textarea)
                {
                    //Create a javascript section for creating new empty textboxes
                    scriptinner += Html.TextareaForMultiField(mf.hiddenname.Replace("_id", "_") + f.name, f.label, "", rdonly);
                }
                else if (f is Dropdown)
                {

                    var fmd = (Dropdown)f;

                    //Create list of options for dropdown
                    List<string> options = new List<string>();
                    foreach (object o in fmd.list)
                    {
                        var text = o.Bind(fmd.namefield);
                        var idvalue = o.Bind(fmd.idfield);
                        options.Add(Html.DropdownOption(idvalue, text, idvalue == (fmd.defaultvalue ?? "0")));
                    }

                    //Create a javascript section for creating new empty dropdowns
                    scriptinner += Html.DropdownForMultiField(mf.hiddenname.Replace("_id", "_") + fmd.name, fmd.label, options, rdonly, fmd.haslabel, fmd.helptext);

                }
                else if (f is AttachmentUpload)
                {
                    var fau = (AttachmentUpload)f;
                    var fileupload = Html.FileUpload(fau.name, controller, project, rdonly);
                    scriptinner += fileupload;
                }

                scriptinner += @"<div class=""divider""></div>";
            }

            //for each existing value, create a set of controls
            foreach (object oe in mf.list)
            {
                inner2 = "";
                var hiddenvalue = oe.Bind(mf.hiddenname);

                foreach (Field f in mf.fields)
                {

                    if (f is Textbox)
                    {
                        var value = oe.Bind(f.name);
                        inner2 += Html.TextboxForMultiField(mf.hiddenname.Replace("_id", "_") + f.name, f.label, value, rdonly, f.helptext);
                    }
                    else if (f is Textarea)
                    {
                        var value = oe.Bind(f.name);
                        inner2 += Html.TextareaForMultiField(mf.hiddenname.Replace("_id", "_") + f.name, f.label, value, rdonly);
                    }
                    else if (f is Dropdown)
                    {
                        var fmd = (Dropdown)f;
                        //Find currently selected value
                        var existing_idfield = oe.Bind(fmd.idfield);

                        //Create list of options for dropdown
                        var options = new List<string>();
                        foreach (object o in fmd.list)
                        {
                            var text = o.Bind(fmd.namefield);
                            var idvalue = o.Bind(fmd.idfield);
                            options.Add(Html.DropdownOption(idvalue, text, idvalue == existing_idfield));
                        }

                        inner2 += Html.DropdownForMultiField(mf.hiddenname.Replace("_id", "_") + fmd.name, fmd.label, options, rdonly, fmd.haslabel, fmd.helptext);
                    }
                    else if (f is AttachmentUpload)
                    {
                        var fau = (AttachmentUpload)f;
                        inner2 += Html.FileUpload(fau.name, controller, project, rdonly, Convert.ToInt32(hiddenvalue), oe.Bind("attachment_filename"), oe.Bind("attachment_mimetype"), (byte[])oe.BindObject("attachment_content"));
                        ;
                    }
                    inner2 += @"<div class=""divider""></div>";

                }

                //Put the collection of fields for each row in the enclosing HTML
                inner1.Add(multiinnertext.Replace("@inner2@", inner2)
                    .Replace("@hiddenname@", mf.hiddenname)
                    .Replace("@hiddenvalue@", hiddenvalue));
            }

            //Add the created script to the page
            script += Html.multiscript.Replace("@mdinner@",
                    multiinnertext
                    .Replace("@inner2@", scriptinner)
                    .Replace("@hiddenname@", mf.hiddenname)
                    .Replace("@hiddenvalue@", "0"))
                   .Replace("@mdlabel@", mf.label)
                   .Replace("@mdscriptname@", mf.label.Replace(" ", "_"));


            //Add the created controls with existing values to the page
            control = (multioutertext
                .Replace("@inner1@", inner1.Any() ? inner1.Aggregate((x, y) => x + y) : "")
                .Replace("@mdscriptname@", mf.label.Replace(" ", "_"))
                .Replace("@mdlabel@", mf.label)
                );

            return control;
        }
    }

        public class TableRowTagHelper : TagHelper
    {
        public object For { get; set; }
        public List<Column> Columns { get; set; }
        public int Rownum { get; set; }
        public int TestCycleId { get; set; }
        [ViewContext]
        public ViewContext vc { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "";
            var tabledef = new List<string>();

                if (Rownum % 2 == 0)
                {
                    tabledef.Add("<tr>");
                }
                else
                {
                    tabledef.Add(@"<tr style=""background-color:rgb(240,240,240)"">");
                }


                //Get the values to go in this row of the table
                foreach (var c in Columns)
                {
                    tabledef.Add("<td>" + PageBuilder.GetTableContent(c, For, TestCycleId, true, vc.projectint(), vc.controller()) + "</td>");
                }

                output.Content.SetHtmlContent(tabledef.Any() ? tabledef.Aggregate((x, y) => x + y) : "");
            }
    }

    [HtmlTargetElement("form-self", TagStructure = TagStructure.WithoutEndTag)]
    public class FormSelfTagHelper : TagHelper
    {
        [ViewContext]
        public ViewContext vc { get; set; }
        public bool index { get; set; }
        public IHtmlGenerator generator { get; set; }
     

    public bool multipartencoding { get; set; } = false;

        public FormSelfTagHelper(IHtmlGenerator generator)
        {
            this.generator = generator;

        }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "";
            var action = vc.action();
            if(action == "Archive" || action == "Delete")
            {
                action = "DeleteConfirmed";
            }

            var antiforgeryTag = generator.GenerateAntiforgery(vc);
            output.PostContent.AppendHtml(antiforgeryTag);

            output.Content.SetHtmlContent($@"<form {(index ? @"class=""form-horizontal"" method=""get""" : @" method=""post"" ")}action=""/{vc.controller()}/{action}/{vc.project()}/{vc.id()}"" {(multipartencoding ? @"enctype=""multipart/form-data""" : "")}>");
        }
    }

    [HtmlTargetElement("searchdiv", TagStructure = TagStructure.WithoutEndTag)]
    public class SearchDivHelper : TagHelper
    {
        [ViewContext]
        public ViewContext vc { get; set; }
        public string text { get; set; }
        public bool showbox { get; set; } = true;

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "";
            output.Content.SetHtmlContent($@"<div id=""search"" class=""collapse{(vc.HttpContext.Request.QueryString.HasValue && showbox ? " in" : "")}"">");
        }
    }

    public class UserNameTagHelper : TagHelper
    {
        public HttpContext hc;

        public UserNameTagHelper(IHttpContextAccessor hc)
        {
            this.hc = hc.HttpContext;
        }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "";
            output.Content.SetHtmlContent($@"--{hc.User.Identity.Name ?? "Null"}--");
        }
    }

    public class TestRunStepsTagHelper : TagHelper
    {
        [ViewContext]
        public ViewContext vc { get; set; }
        public TestRunsSteps steps { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "";

            var control = "";
            ;
            control += ((steps.mvctest.description == "" || steps.mvctest.description == null) ? "" : $@"<h4>Test Summary/Preconditions</h4><br><p style=""max-width: 800px; font-size:16px;"" class=""form-control-static"">{steps.mvctest.description}</p><br>");
            control += steps.steps.Any() ? @"<h4>Test Steps</h4><table class=""table""><th class=""col-md-1""></th><th class=""col-md-4"">step</th><th class=""col-md-3"">expected result</th><th class=""col-md-2"">status</th><th class=""col-md-3"">attachment</th>"
            + steps.steps.Select((s, i) =>
            $@"{(i % 2 == 1 ? "<tr>" : @"<tr style=""background-color:rgb(240,240,240)"">")}<td style=""vertical-align:middle;"">{(i + 1).ToString() + "."}</td><td style=""vertical-align:middle;"">{s.Item3}</td><td style=""vertical-align:middle;"">{s.Item4}</td><td align=""left"" style=""vertical-align:middle;""><input type=""hidden"" name=""runstepid[{i}]"" value=""{s.Item1}"">
<input type=""hidden"" name=""stepid[{i}]"" value=""{s.Item2}"">
<ul style=""list-style-type: none;"">
<li><input type=""radio"" name=""stepcheck[{i}]"" value=""0"" {(s.Item5 == 0 ? "checked" : "")}>Not Started</li>
<li><input type=""radio"" name=""stepcheck[{i}]"" value=""1"" {(s.Item5 == 1 ? "checked" : "")}>Failed</li>
<li><input type=""radio"" name=""stepcheck[{i}]"" value=""2"" {(s.Item5 == 2 ? "checked" : "")}>Passed</li>
</ul>
{((s.Item6 != null) ? $@"
</td>
<td><a style=""vertical-align:middle;"" href=""/testruns/ViewImage/{vc.project()}/{s.Item2}"" target""_blank"">
{((s.Item7 == "image/bmp" || s.Item7 == "image/jpg" || s.Item7 == "image/jpeg" || s.Item7 == "image/png") ? $@"<img style=""width:100%; height:auto; border: 1px solid black;""src=""data:{s.Item7};base64,{Convert.ToBase64String(s.Item6)}"">" : "Download")}</a></td>" : "<td></td>")}</tr>"
).Aggregate((x, y) => x + y) : "";
            control += steps.steps.Any() ? @"<td></td><td></td><td></td><td><button type=""button"" class=""btn btn-default"" onclick=""var elements = document.getElementsByTagName('input'); for(var i = 0; i < elements.length; i++) {if(elements[i].type == 'radio' && elements[i].value == '2') {elements[i].checked = true;}}"">Check All</button></td><td></td>​" : "";
            control += "</table>";

            output.Content.SetHtmlContent(control);
        }
    }

    

    public class PageNumberTagHelper : TagHelper
    {
        public PagedList<object> modellist { get; set; }
        public List<Field> Filters { get; set; }
        [ViewContext]
        public ViewContext vc { get; set; }
        public RoleInfo role { get; set; }
        public bool History { get; set; } = false;

        public PageNumberTagHelper(IRoleService rs)
        {
            this.role = rs.GetRole();

        }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "";
            var pages = new List<int>();

            for (int i = Math.Max(1, modellist.currentpage - 4); i <= Math.Min(((modellist.total - 1) / 10) + 1, modellist.currentpage + 5); i++)
            { pages.Add(i); }

            var filterstring = Filters.Any() ? Filters.Select(f => "&" + f.name + "=" + f.defaultvalue).Aggregate((x, y) => x + y) : "";
            if (History) { filterstring += vc.HistoryQueryString(); }

            if (pages.Any())
            {
                output.Content.SetHtmlContent($@"<ul class=""pagination""><li>{Html.Link("&laquo;", vc.controller(), vc.action(), vc.projectint(), "page=1" + filterstring)}</li>{pages.Select(p => $@"<li{(p == modellist.currentpage ? @" class=""active""" : "")}>{Html.Link(p.ToString(), vc.controller(), vc.action(), vc.projectint(), "page=" + p.ToString() + filterstring)}</li>").Aggregate((x, y) => x + y)}<li>{Html.Link("&raquo;", vc.controller(), vc.action(), vc.projectint(), "page=" + (((modellist.total - 1) / 10) + 1) + filterstring)}</li></ul>");
            }
            else
            {
                output.Content.SetHtmlContent("");
            }

        }
    }

    public class TableRowFooterTagHelper : TagHelper
    {
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "";
            output.Content.SetHtmlContent("</tr>");
        }
    }

    public class MetricTagHelper : TagHelper
    {
        public MetricGroup Type { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "";
            output.Content.SetHtmlContent(Type.metriccontent);
        }
    }

    public class ChartTagHelper : TagHelper
    {
        public DashboardElement Type { get; set; }
        public bool Content { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "";
            output.Content.SetHtmlContent(Content ? Type.content : Type.script);
        }
    }

    public class NotificationTagHelper : TagHelper
    {
        [ViewContext]
        public ViewContext vc { get; set; }
        public IRoleService rs { get; set; }
        public InboxElement Type { get; set; }
        public int TestCycleId { get; set; }
        public int Num { get; set; }

        public NotificationTagHelper(IRoleService rs)
        {
            this.rs = rs;
        }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "";
            output.Content.SetHtmlContent(Type.GetContent((vc.projectint() == 0 ? rs.GetDefaultProject() : vc.projectint()),TestCycleId, (vc.projectint() == 0 ? rs.GetRole(rs.GetDefaultProject()) : rs.GetRole()), Num));
        }

        public class InboxElement
        {
            public List<object> modellist;
            public string controller;
            public List<Field> fields;
            public Links links;
            public string title;
            public RoleType role;
            public bool onbydefault;

            public InboxElement(RoleType role, string title, List<object> model, string controller, List<Field> fields, Links links, bool onbydefault)
            {
                this.modellist = model.ToList();
                this.controller = controller;
                this.fields = fields;
                this.links = links;
                this.title = title;
                this.role = role;
                this.onbydefault = onbydefault;
            }

            public string GetContent(int project, int test_cycle_id, RoleInfo roleinfo, int iecount)
            {
                if(role == RoleType.Manager && !roleinfo.IsSupplierDOrAbove()) { return ""; }
                if (!modellist.Any()) { return ""; }
                var idcolumn = controller == "testruns" ? "run_id" : (controller == "scheduletestruns" ? "test_cycle_test_id" : controller.Substring(0, controller.Length - 1) + "_id");
                //Create index page table columns
                var tabledef = new List<string>();
                var columns = fields.Cast<Column>();
                tabledef.Add("<tr>");
                foreach (var c in columns)
                {
                    tabledef.Add("<th" + (c.width == 0 ? "" : " class=col-md-" + c.width) + ">" + c.label + "</th>");
                }
                tabledef.Add("<th class=col-md-2></th></tr>");
                ;
                int rownum = 0;
                //Create the table
                foreach (var i in modellist)
                {
                    rownum++;
                    if (rownum % 2 == 0)
                    {
                        tabledef.Add("<tr>");
                    }
                    else
                    {
                        tabledef.Add(@"<tr style=""background-color:rgb(240,240,240)"">");
                    }

                    //Get the values to go in this row of the table
                    foreach (var c in columns)
                    {
                        tabledef.Add("<td>" + PageBuilder.GetTableContent(c, i, test_cycle_id, true, project, "Homepage") + "</td>");
                    }

                    var linkstext = PageBuilder.GetLinks(links, roleinfo, project, controller, PageType.Index, idcolumn, i.Bind(idcolumn), false);

                    tabledef.Add(@"<td><div class=""btn-group-vertical"">" + linkstext + "</div></td>");

                    tabledef.Add("</tr>");
                }

                return $@"<h4>{title} ({modellist.Count})<div class=""divider""></div><button type=""button"" class=""btn btn-default"" data-toggle=""collapse"" data-target=""#search{iecount}"">Show/Hide</button></h4><br><div id=""search{iecount}"" class=""collapse{(onbydefault ? " in" : "")}""><table class=""table""{tabledef.Aggregate((x, y) => x + y)}</table></div><br>";
            }
        }
    }

    public class DropdownOptionsTagHelper : TagHelper
    {

        public Dropdown For { get; set; }
        public int? Value { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "";
            var ddoptions = new List<string>();

            //Default option in menu for tables where null is a default value
            if (For.idfield == "module_id" || For.name == "owner_id")
            {
                ddoptions.Add(Html.DropdownOption("-99", "(None)", false));
            }
         
            if (For.defaultvalue == "-97")
            {
                ddoptions.Add(Html.DropdownOption("-97", "(Choose One)", true));
            }


            //Create list of options for dropdown
            foreach (object o in For.list)
            {
                var text = o.Bind(For.namefield);
                var idvalue = o.Bind(For.idfield);
                ddoptions.Add(Html.DropdownOption(idvalue, text, idvalue == ((For.defaultvalue == null || For.defaultvalue == "") ? Value.ToString() : For.defaultvalue)));

            }

            output.Content.SetHtmlContent(ddoptions.Any() ? ddoptions.Aggregate((x,y) => x+y) : "");
             
        }
    }

    public class HeaderLinksTagHelper : TagHelper
    {
        [ViewContext]
        public ViewContext vc { get; set; }
        public IRoleService rs { get; set; }

        public HeaderLinksTagHelper(IRoleService rs)
        {
            this.rs = rs;       
        }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "";
            output.Content.SetHtmlContent(Html.Header((vc.projectint() == 0 ? rs.GetRole(rs.GetDefaultProject()) : rs.GetRole()), vc.controller(),vc.action()));
        }
    }

    public class ProjectDropdownOptionsTagHelper : TagHelper
    {
        public RoleInfo role { get; set; }

        public ProjectDropdownOptionsTagHelper(IRoleService rs)
        {
            this.role = rs.GetRole();
        }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "";
            var options = role.available_projects.Select(ap => Html.DropdownOption("/dashboard/Homepage/" + ap.project_id.ToString(), ap.name, ap.project_id == role.project)).ToList();
            output.Content.SetHtmlContent(options.Any() ? options.Aggregate((x,y) => x+y) : "");
        }
    }

    public class FilterOptionsTagHelper : TagHelper
    {

        public Dropdown For { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "";
            var options = new List<string>();

                if (For.name == "test_cycle_id" || For.name == "due_date_status" || For.name == "derived_requirement_state" || For.name == "derived_test_state" || For.name == "scheduled" || For.name == "draft_status_id")
                {

                }
                else
                {
                    options.Add(Html.DropdownOption("0", "(All)", For.defaultvalue == "0"));
                }

                if (For.name == "derived_requirement_state")
                {
                    options.AddRange(Enum.GetNames(typeof(RequirementState)).Select((e, i) => (e != "DoesNotYetExist" && e != "Draft" && e != "Archived") ? Html.DropdownOption(i.ToString(), i == 0 ? "(All)" : Extension.ToEnumString(e), For.defaultvalue == e) : ""));
                    output.Content.SetHtmlContent(options.Any() ? options.Aggregate((x, y) => x + y) : "");
                    return;
                }
            else if (For.name == "due_date_status")
            {
                options.AddRange(Enum.GetNames(typeof(DueDateStatus)).Select((e, i) => Html.DropdownOption(i.ToString(), i == 0 ? "(All)" : Extension.ToEnumString(e), For.defaultvalue == e)));
                output.Content.SetHtmlContent(options.Any() ? options.Aggregate((x, y) => x + y) : "");
                return;
            }
            else if (For.name == "derived_test_state")
                {
                    options.AddRange(Enum.GetNames(typeof(TestState)).Select((e, i) => (e != "DoesNotYetExist" && e != "Draft" && e != "Archived") ? Html.DropdownOption(i.ToString(), i == 0 ? "(All)" : Extension.ToEnumString(e), For.defaultvalue == e) : ""));
                output.Content.SetHtmlContent(options.Any() ? options.Aggregate((x, y) => x + y) : "");
                return;
            }
                else if (For.name == "scheduled")
                {
                    options.Add(Html.DropdownOption("-1", "(All)", For.defaultvalue == "-1"));
                    options.Add(Html.DropdownOption("1", "Yes", For.defaultvalue == "1"));
                    options.Add(Html.DropdownOption("0", "No", For.defaultvalue == "0"));
                output.Content.SetHtmlContent(options.Any() ? options.Aggregate((x, y) => x + y) : "");
                return;
            }
                else if (For.name == "draft_status_id")
                {
                    options.Add(Html.DropdownOption("99", "Draft/Live", For.defaultvalue == "99"));
                }     
            else if (For.name == "defect_status_id")
            {
                options.Add(Html.DropdownOption("-95", "All Open", For.defaultvalue == "-95"));
            }       

            //Create dropdown options
            if (For.name == "owner_id" || For.name == "assigned_to_id" || For.idfield == "module_id")
            {
                options.Add(Html.DropdownOption("-99", "(None)", For.defaultvalue == "-99"));
            }

            foreach (object o in For.list)
            {
                var text = o.Bind(For.namefield);
                var idvalue = o.Bind(For.idfield);
                options.Add(Html.DropdownOption(idvalue, text, idvalue == (For.defaultvalue ?? "0")));
            }

            output.Content.SetHtmlContent(options.Any() ? options.Aggregate((x, y) => x + y) : "");

        }
    }

    public static class ContextExtensions
    {
        public static string controller(this ViewContext vc)
        {
            return (vc.RouteData.Values["controller"] ?? "").ToString();
        }

        public static string action(this ViewContext vc)
        {
            return (vc.RouteData.Values["action"] ?? "").ToString();
        }

        public static string project(this ViewContext vc)
        {
            return (vc.RouteData.Values["project"] ?? "").ToString();
        }

        public static string HistoryQueryString(this ViewContext vc)
        {

            return "&defect_id="+(vc.HttpContext.Request.Query["defect_id"].FirstOrDefault() ?? "").ToString();
        }

        public static int projectint(this ViewContext vc)
        {
            var p = vc.RouteData.Values["project"];

                if (p == null)
            { return 0; }

            int r;

            if(Int32.TryParse(p.ToString(),out r))
            {
                return r;
            }
            else
            {
                return 0;
            }
                

        }

        public static string id(this ViewContext vc)
        {
            return (vc.RouteData.Values["id"] ?? "").ToString();
        }
    }
}

