﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;

using System.IO;
using System.Text.RegularExpressions;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;


using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;


namespace WebApplication6
{

    public static partial class DerivedStateExtensions
    {
        //Function to get the overall state of a requirements based on a particular date and test cycle
        public static RequirementState DerivedState(this requirements r, DateTime? dn, int test_cycle_id)
        {
            DateTime d = dn ?? DateTime.Now;



            if (test_cycle_id == 0) { throw new Exception(); }

            if (r.effective_date > d)
            {
                return RequirementState.DoesNotYetExist;
            }
            if (r.accepted_will_not_be_met)
            {
                return RequirementState.AcceptedWillNotBeMet;
            }

            if (r.draft_status_id == 1 || r.date_made_live > d) { return RequirementState.Draft; }
            else if (r.draft_status_id == 3) { return RequirementState.Archived; }

            var tests = r.requirements_tests.Select(rt => rt.test).Where(t => t.draft_status_id != 3).ToList().Select(t => new { test = t, state = t.DerivedState(d, test_cycle_id) })
               .Where(t => !t.state.IsIn(TestState.DoesNotYetExist, TestState.Archived));

            if (tests.All(t => t.state == TestState.Draft))
            {
                return RequirementState.NoTestsLive;
            }

            if (tests.All(t => t.state == TestState.Passed))
            {
                return RequirementState.AllPassed;
            }

            if (tests.All(t => t.state.IsIn(TestState.NotStarted, TestState.Draft)))
            {
                return RequirementState.NotStarted;
            }

            return RequirementState.InProgress;

        }

        //Helper function for if a requirement is in one of several states
        public static bool IsIn(this RequirementState source, params RequirementState[] values)
        {
            return values.Contains(source);
        }

        //Helper function for if a test is in one of several states
        public static bool IsIn(this TestState source, params TestState[] values)
        {
            return values.Contains(source);
        }


        //Function to get the overall state of a requirements based on a particular date and test cycle
        public static TestState DerivedState(this tests t, DateTime? dn, int test_cycle_id)
        {
            DateTime d = dn ?? DateTime.Now;

            if (test_cycle_id == 0) { throw new Exception(); }
            if (t.effective_date > d)
            {
                return TestState.DoesNotYetExist;
            }

            if (t.draft_status_id == 1 || t.date_made_live > d) { return TestState.Draft; }
            else if (t.draft_status_id == 3) { return TestState.Archived; }

            var latest_run = t.runs.Where(r => r.outcome_id != 4 && r.run_date < d && (r.test_cycle_id == test_cycle_id)).OrderByDescending(r => r.run_date).ThenByDescending(r => r.run_id).FirstOrDefault();

            var scheduled = t.test_cycles_tests.Any(tct => tct.scheduled_status_id == 1 && tct.scheduled_end_date > d);
            if (latest_run == null)
            {
                return TestState.NotStarted;
            }
            else
            {
                switch (latest_run.outcome_id)
                {
                    case 1: return TestState.Passed;
                    case 2: return TestState.Failed;
                    case 3: return TestState.Blocked;
                }
            }



            return TestState.None;
        }

        //Function to get whether a test is scheduled for a particular test cycle
        public static bool Scheduled(this tests t, int test_cycle_id)
        {
            var scheduled = t.test_cycles_tests.Any(tct => (tct.scheduled_status_id == 1 && tct.scheduled_end_date > DateTime.Now && tct.test_cycle_id == test_cycle_id));
            return scheduled;
        }

    }

    public class Field
    {
        public string name;
        public string label;
        public string defaultvalue;
        public string nullval;
        public string helptext;

        /// <summary>
        /// Creates a control on the page based on a field of the entity or entities passed to the page
        /// </summary>
        /// <param name="name">The database field assigned to the control</param>
        /// <param name="defaultvalue">Value to display when the page is loaded without the value set</param>
        /// <param name="nullval">Value to display if the database field is null</param>
        public Field(string name, string defaultvalue = "", string nullval = "", string label = "", string helptext="")
        {
            this.label = label == "" ? name.Replace("_id", "").Replace("_", " ") : label;
            this.name = name;
            this.defaultvalue = defaultvalue;
            this.nullval = nullval;
            this.helptext = helptext;
        }
    }

    public class Textbox : Field
    {
        /// <summary>
        /// Creates a textbox control on the page based on a field of the entity or entities passed to the page
        /// </summary>
        /// <param name="name">The database field assigned to the control</param>
        /// <param name="defaultvalue">Value to display when the page is loaded without the value set</param>
        public Textbox(string name, string defaultvalue = "", string label="", string helptext="") : base(name, defaultvalue?.ToString(),"",label,helptext) { }
    }

    public class Textarea : Field
    {
        /// <summary>
        /// Creates a textarea control on the page based on a field of the entity or entities passed to the page
        /// </summary>
        /// <param name="name">The database field assigned to the control</param>
        /// <param name="defaultvalue">Value to display when the page is loaded without the value set</param>
        public Textarea(string name, string defaultvalue = "", string label = "") : base(name, defaultvalue?.ToString(), "", label) { }
    }

   
    

    public class Datetime : Field
    {
        /// <summary>
        /// Creates a datetime control on the page based on a field of the entity or entities passed to the page
        /// </summary>
        /// <param name="name">The database field assigned to the control</param>
        /// <param name="defaultvalue">Value to display when the page is loaded without the value set</param>
        public Datetime(string name, string defaultvalue = "", string label="") : base(name, defaultvalue?.ToString(), "", label) {}
    }

    public class TestRunsSteps : Field
    {
        /// <summary>
        /// Creates the test runs control
        /// </summary>
        public List<Tuple<int,int,string,string,int,byte[],string>> steps;
        public tests mvctest;
        public TestRunsSteps(IEnumerable<test_runs_test_steps> runs_steps, IEnumerable<test_steps> steps) : base("teststeps")
        {
            this.steps = steps.Select(s => System.Tuple.Create(runs_steps.SingleOrDefault(rs => rs.test_step_id == s.test_step_id)?.test_runs_test_steps_id ?? 0,s.test_step_id, s.description, s.expected_result, runs_steps.SingleOrDefault(rs => rs.test_step_id == s.test_step_id)?.completed ?? 0, s.attachment_content, s.attachment_mimetype)).ToList();
        }

        public TestRunsSteps(IEnumerable<test_runs_test_steps> runs_steps, IEnumerable<test_steps> steps, tests mvctest) : base("teststeps")
        {
            this.steps = steps.Select(s => System.Tuple.Create(runs_steps.SingleOrDefault(rs => rs.test_step_id == s.test_step_id)?.test_runs_test_steps_id ?? 0, s.test_step_id, s.description, s.expected_result, runs_steps.SingleOrDefault(rs => rs.test_step_id == s.test_step_id)?.completed ?? 0, s.attachment_content, s.attachment_mimetype)).ToList();
            this.mvctest = mvctest;
        }
    }

    public class AttachmentUpload : Field
    {
        /// <summary>
        /// Creates a file upload control on the page
        /// </summary>
        public AttachmentUpload() : base("fileuploaded", null) { }
    }

    //Class for transferring attachment information internally
    public class attachment_container
    {
        public byte[] attachment_content { get; set; }
        public string attachment_mimetype { get; set; }
        public string attachment_filename { get; set; }
    }
   

    public class Dropdown : Field
    {

        public IEnumerable<object> list; 
        public string idfield;
        public string namefield;
        public bool haslabel = true;
        public bool hashelptext = false;

        /// <summary>
        /// Creates a dropdown control on the page based on a field of the entity or entities passed to the page
        /// </summary>
        /// <param name="name">The database field assigned to the control</param>
        /// <param name="list">List of model objects to populate with</param>
        /// <param name="idfield">Primary key field in the reference object if its name is different to the entity field name</param>
        /// <param name="defaultvalue">Value to display when the page is loaded without the value set</param>
        public Dropdown(string name, IEnumerable<object> list, string idfield = null, object defaultvalue = null) : base(name, defaultvalue?.ToString())
        {
            this.list = list.ToList();
            this.idfield = idfield ?? name;
            this.namefield = "name";
        }

        /// <summary>
        /// Creates a dropdown control on the page based on a field of the entity or entities passed to the page
        /// </summary>
        /// <param name="name">The database field assigned to the control</param>
        /// <param name="list">List of model objects to populate with</param>
        /// <param name="idfield">Primary key field in the reference object if its name is different to the entity field name</param>
        /// <param name="defaultvalue">Value to display when the page is loaded without the value set</param>
        public Dropdown(string name, IEnumerable<object> list, bool haslabel) : base(name, "")
        {
            this.list = list.ToList();
            this.idfield = idfield ?? name;
            this.namefield = "name";
            this.haslabel = haslabel;
        }

        public Dropdown(string name, IEnumerable<object> list, bool hashelptext, string helptext) : base(name, "", "", "", helptext)
        {
            this.list = list.ToList();
            this.idfield = idfield ?? name;
            this.namefield = "name";
            this.hashelptext = hashelptext;
            this.helptext = helptext.Replace("<", "&lt;").Replace(">", "&gt;");
        }

        public Dropdown(string name, IEnumerable<object> list, bool hashelptext, string helptext, object defaultvalue) : base(name, defaultvalue?.ToString(), "", "", helptext)
        {
            this.list = list.ToList();
            this.idfield = idfield ?? name;
            this.namefield = "name";
            this.hashelptext = hashelptext;
            this.helptext = helptext.Replace("<", "&lt;").Replace(">", "&gt;");
        }



        /// <summary>
        /// Creates a dropdown control on the page based on a field of the entity or entities passed to the page
        /// </summary>
        /// <param name="name">The database field assigned to the control</param>
        /// <param name="list">List of model objects to populate with</param>
        /// <param name="defaultvalue">Value to display when the page is loaded without the value set</param>
        /// <param name="sameid">Dummy variable used when you want to use the defaultvalue without setting an idfield</param>
        public Dropdown(string name, IEnumerable<object> list, object defaultvalue, bool sameid) : base(name, defaultvalue?.ToString())
        {
            this.list = list;
            this.idfield = name;
            this.namefield = "name";
        }

        public string Get(bool filters = false)
        {

            //Create dropdown options
            var options = new List<string>();


            //Add special values
            if (filters)
            {
                if (this.name == "test_cycle_id" || this.name == "derived_requirement_state" || this.name == "derived_test_state" || this.name == "scheduled" || this.name == "draft_status_id")
                {

                }
                else
                {
                    options.Add(Html.DropdownOption("0", "(All)", defaultvalue == "0"));
                }

                if (this.name == "derived_requirement_state")
                {
                    options.AddRange(Enum.GetNames(typeof(RequirementState)).Select((e, i) => (e != "DoesNotYetExist" && e != "Draft" && e != "Archived") ? Html.DropdownOption(i.ToString(), i == 0 ? "(All)" : Extension.ToEnumString(e), defaultvalue == e) : ""));
                    return Html.Dropdown(this.name, "status", options, false, true);
                }
                else if (this.name == "derived_test_state")
                {
                    options.AddRange(Enum.GetNames(typeof(TestState)).Select((e,i) => (e != "DoesNotYetExist" && e != "Draft" && e != "Archived") ? Html.DropdownOption(i.ToString(), i == 0 ? "(All)" : Extension.ToEnumString(e), defaultvalue == e) : ""));
                    return Html.Dropdown(this.name, "status", options, false, true);
                }
                else if (this.name == "due_date_status")
                {
                    options.AddRange(Enum.GetNames(typeof(DueDateStatus)).Select((e, i) => Html.DropdownOption(i.ToString(), i == 0 ? "(All)" : Extension.ToEnumString(e), defaultvalue == e)));
                    return Html.Dropdown(this.name, "status", options, false, true);
                }
                else if (this.name == "scheduled")
                {
                    options.Add(Html.DropdownOption("-1", "(All)", defaultvalue == "-1"));
                    options.Add(Html.DropdownOption("1", "Yes", defaultvalue == "1"));
                    options.Add(Html.DropdownOption("0", "No", defaultvalue == "0"));
                    return Html.Dropdown(this.name, "scheduled", options, false, true);
                }
                else if (this.name == "draft_status_id")
                {
                    options.Add(Html.DropdownOption("99", "Draft/Live", defaultvalue == "99"));
                }
                else if (this.name == "defect_status_id")
                {
                    options.Add(Html.DropdownOption("-97", "All Open", defaultvalue == "-97"));
                }


            }

            //Create dropdown options
            if (this.name == "owner_id" || this.name == "assigned_to_id" || this.idfield == "module_id")
            {
                options.Add(Html.DropdownOption("-99", "(None)", defaultvalue == "-99"));
            }

            foreach (object o in this.list)
            {
                var text = o.Bind(this.namefield);
                var idvalue = o.Bind(this.idfield);
                options.Add(Html.DropdownOption(idvalue, text, idvalue == (this.defaultvalue ?? "0")));
            }

            return Html.Dropdown(this.name, this.label, options, false, true);
        }
    }

    public class MultiField : Field
    {
        public List<Field> fields; //List of fields used
        public IEnumerable<object> list;
        public string hiddenname;
        public bool removebuttons;

        /// <summary>
        /// Contains a set of fields for a linked entity type for a one-to-many relationship between the page entity and the linked entity
        /// </summary>
        /// <param name="name">The field label</param>
        /// /// <param name="idfield">The id field of the linked entity</param>
        /// <param name="list">The existing list of linked entity objects</param>
        /// <param name="fields">The fields to show for the linked entity</param>
        public MultiField(string name, string idfield, IEnumerable<object> list, params Field[] fields) : base("")
        {
            this.label = name;
            this.fields = fields.ToList();
            this.list = list?.ToList();
            this.hiddenname = idfield;
            this.removebuttons = true;
        }

        public MultiField(string name, string idfield, bool removebuttons, IEnumerable<object> list, params Field[] fields) : base("")
        {
            this.label = name;
            this.fields = fields.ToList();
            this.list = list?.ToList();
            this.hiddenname = idfield;
            this.removebuttons = removebuttons;
        }
    }

    public class Column : Field
    {
        public int indirect;
        public string before;
        public string after;
        public string after2;
        public int width;
        public bool excelint = false;

        /// <summary>
        /// Creates a column on the page containing the values for a particular field of the entity
        /// </summary>
        /// <param name="name">The database field assigned to the column</param>
        /// <param name="width">Column width</param>
        /// <param name="excelint">Is an number for the purposes of Excel report generation</param>
        public Column(string name, int width, bool excelint, string label = null) : this(name, width)
        {
            this.excelint = excelint;
        }

        /// <summary>
        /// Creates a column on the page containing the values for a particular field of the entity
        /// </summary>
        /// <param name="name">The database field assigned to the column</param>
        /// <param name="width">Column width</param>
        /// <param name="nullval">Value to display if the database field is null</param>
        /// <param name="label">Column label</param>
        public Column(string name, int width = 0, string nullval = "", string label = "") : base(name.Replace(".name", "").Replace(".", " "), null, nullval)
        {
            //Indirect property binding if the string contains dots e.g. "name" or "test.name" or "requirement.test.name"
            if (label != "") { this.label = label; }
            this.width = width;
            if (name.Contains('.'))
            {
                before = name.Substring(0, name.IndexOf('.'));
                after = name.Substring(name.IndexOf('.') + 1, name.Length - name.IndexOf('.') - 1);
                if (after.Contains('.'))
                {
                    after2 = after.Substring(after.IndexOf('.') + 1, after.Length - after.IndexOf('.') - 1);
                    after = after.Substring(0, after.IndexOf('.'));
                    indirect = 2;
                }
                else
                {
                    indirect = 1;
                }
            }
            else
            {
                indirect = 0;
            }
        }
    }

    //Object to store a list of page filter fields
    public class Filters
    {
        public List<Field> filters;
        public bool defaultids = false;

        public Filters(bool defaultids, params Field[] filters)
        {
            this.filters = filters.ToList();
            this.defaultids = defaultids;
        }

        public Filters(params Field[] filters)
        {
            this.filters = filters.ToList();
        }
    }

    //Object to store a list of page links
    public class Links
    {
        public bool edit = true;
        public bool delete = true;
        public List<Link> links;
        public List<Link> linkfirst = new List<Link>();

        public Links(bool edit, bool delete, params Link[] links)
        {
            this.edit = edit;
            this.delete = delete;
            this.links = links.ToList();
        }

        public Links(Link first, bool edit, bool delete, params Link[] links)
        {
            linkfirst.Add(first);
            this.edit = edit;
            this.delete = delete;
            this.links = links.ToList();
        }

    }

    //Object to store a page link
    public class Link
    {
        public string controller;
        public string action;
        public string label;
        public bool primary;

        public Link(string controller, string action, string label, bool primary = false)
        {
            this.controller = controller;
            this.action = action;
            this.label = label;
            this.primary = primary;
        }

        public Link(string controller, string action)
        {
            this.controller = controller;
            this.action = action;
            this.label = controller.Substring(0, 1).ToUpper() + controller.Substring(1, controller.Length - 1);
        }
    }

    //Stores the overall state of a requirement
    public enum RequirementState { None, DoesNotYetExist, Draft, AcceptedWillNotBeMet, NoTestsLive, NotStarted, InProgress, AllPassed, Archived }

    //Stores the overall state of a test
    public enum TestState { None, DoesNotYetExist, Draft, NotStarted, Passed, Failed, Blocked, Archived }

    //Stores the overall state of a test
    public enum DueDateStatus { All, NoDueDate, Overdue, DueInTheNextMonth }



}
