﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;

using System.IO;
using System.Text.RegularExpressions;
using System;
using System.Collections.Generic;

namespace WebApplication6
{
    public static class Html
    {
        public static string LinkInner(string controller, string action, int project)
        {
            return $@"/{controller}/{action}/{project}/";
        }

        public static string Link(string label, string controller, string action, int project, string querystring)
        {
            return $@"<a href=""/{controller}/{action}/{project}{(querystring == "" ? "" : $"?{querystring}")}"">{label}</a>";
        }

        public static string LinkButton(string label, string controller, string action, int project, int id = -1, string querystring = "")
        {
            return $@"<a class=""btn btn-default"" href=""/{controller}/{action}/{project}/{(id == -1 ? "" : $"{id}")}{(querystring == "" ? "" : $"?{querystring}")}"">{label}</a>";
        }


        public static string StaticText(string value)
        {
            return $@"<p class=""form-control-static"">{value}</p>";
        }

        public static string FormGroup(string field, string label, string control, bool inline=false)
        {
            return $@"<div class=""form-group"">
            <label class=""control-label col-md-2"" for=""{field}"">{label}</label>
            <div class=""col-md-10{(inline ? " form-inline" : "")}"">{control}</div>
            </div>";
        }


        public static string DropdownOption(string value, string text, bool selected)
        {
            return $@"<option {(selected ? @"selected=""selected""" : "")} value=""{value}"">{text}</option>";
        }

        public static string Helptext(string text)
        {
            return $@"<a class=""btn popovertext"" href=""#"" data-html=""true"" data-content=""{text}"" rel=""popover"" data-placement=""bottom"" data-trigger=""hover""><span style=""color: black;"" class=""glyphicon glyphicon-question-sign""></span></a>";
        }
        public static string Dropdown(string field, string label, List<string> options, bool rdonly, bool onchange = false, string helptext = "")
        {
            var hashelptext = (helptext != "");
            return rdonly ? Html.FormGroup(field, label, Html.StaticText(options.Where(o => o.Contains(@"selected=""selected""")).FirstOrDefault() ?? "")) : Html.FormGroup(field, label, $@"<select class=""form-control""  {(onchange ? @"onchange=""this.form.submit()""" : "")} id=""{field}"" name=""{field}"">{(options.Any() ? options.Aggregate((x, y) => x + y) : "")}</select>"+(hashelptext ? Helptext(helptext) : ""), hashelptext);
        }

        public static string HasLabel(bool haslabel, string text)
        {
            return haslabel ? $@"<label>{text}</label><div class=""divider""></div>" : "";
        }

        public static string DropdownForMultiField(string field, string label, List<string> options, bool rdonly, bool haslabel, string helptext = "")
        {
            var hashelptext = (helptext != "");
            return rdonly ? $@"{HasLabel(haslabel, label)}{options.Where(o => o.Contains(@"selected=""selected""")).FirstOrDefault() ?? ""}" : $@"{HasLabel(haslabel, label)}<select class=""form-control""  name = ""{field}"" class=""form-control"" id=""sel"">{(options.Any() ? options.Aggregate((x, y) => x + y) : "")}</select>" + (hashelptext ? Helptext(helptext) : "");
        }

        public static string ProjectDropdown(RoleInfo role)
        {
            var options = role.available_projects.Select(ap => Html.DropdownOption("/dashboard/Homepage/" + ap.project_id.ToString(), ap.name, ap.project_id == role.project)).ToList();
            return $@"<select style=""height:26px !important; font-size:10px; display: inline-block;"" class=""form-control"" onchange=""var url = $(this).val(); if (url) {{window.location = url;}} return false;"" id=""changeproject"" name=""changeproject"">{(options.Any() ? options.Aggregate((x, y) => x + y) : "")}</select>";
        }


        public static string TextboxForMultiField(string field, string label, string value, bool rdonly, string helptext="")
        {
            var hashelptext = (helptext != "");
            return rdonly ? $@"<label for=""txt1"">{label}</label>{" "+value}" : $@"<label for=""txt1"">{label}:</label><div class=""divider""></div><input class=""form-control"" type=""text"" maxlength=""{(field == "name" ? 50 : 255)}"" name=""{field}"" value=""{value}"">" + (hashelptext ? Helptext(helptext) : "");
        }

        public static string TextareaForMultiField(string field, string label, string value, bool rdonly)
        {
            return rdonly ? $@"<label for=""txt1"">{label}</label>{" "+value}" : $@"<label for=""txt1"">{label}:</label><div class=""divider""></div><textarea class=""form-control"" rows=""6""  name=""{field}"">{value}</textarea>";
        }

        public static string FileUpload(string field, string controller, int project, bool rdonly, int attachment_id = 0, string attachment_filename = "", string attachment_mimetype = "", byte[] attachment_content = null)
        {
            var img = (attachment_content != null && (attachment_mimetype == "image/bmp" || attachment_mimetype == "image/jpg" || attachment_mimetype == "image/jpeg" || attachment_mimetype == "image/png")) ? $@"<br><br><div style=""border: 1px solid black; float: left;""><img src=""data:{attachment_mimetype};base64,{Convert.ToBase64String(attachment_content)}""></div><br style=""clear:both;""><br>" : "";
            var dl = (attachment_content != null) ? $@"{Html.LinkButton("Download",controller,"DownloadAttachment",project,attachment_id)}" : "";
            return rdonly ? $@"<label>attachment:</label><div>{attachment_filename + img + dl}</div>" : $@" <div class=""divider""></div><label>"+((attachment_content != null) ? attachment_filename : "")+$@"<input style=""width: 200px; {((attachment_content == null) ? attachment_filename : "display:none;")}"" name=""{field}"" value=""{attachment_filename}"" type=""file""/></label>" + ((attachment_content == null) ? "" : @"<div class=""divider""></div>") + img + dl;
        }

        public static string BasePage(string pagetitle, RoleInfo role, string controller, string action, string content, string script)
        {
            var project_options = Html.ProjectDropdown(role);
            var header = Html.Header(role, controller, action);

            return $@"
<html>
    <head>
<meta http-equiv=""X-UA-Compatible"" content=""IE=edge""> 
              <meta charset = ""utf-8"" />
        <meta name=""viewport"" content=""width=device-width, initial-scale=1.0"" />
        <title>Test Management Tool</title>

        
            
<link rel=""stylesheet"" href=""//cwd5.web01.lincoln.ac.uk/cwd-min.css"">
<link rel=""shortcut icon"" href=""/favicon.png"" type=""image/x-icon"">
<link rel=""icon"" href=""/favicon.png"" type=""image/x-icon"" >
          

<style>
table {{
    table-layout: fixed;
    word-wrap: break-word;
    font-size: 13px;
}}

.form-control {{
    max-width: 400px;
}}

.form-inline .form-group {{
    margin-left: 0;
    margin-right: 0;
    margin-top: 0;
    margin-bottom: 0;
}}

.divider{{
    width:8px;
    height:auto;
    display:inline-block;
}}

.divider-s{{
    width:0px;
    height:auto;
    display:inline-block;
}}

</style>
    </head>
    <body>
        <header id=""cwd-header"">
					<div class=""container"">
						<div id=""cwd-header-title"">       
                                                <div style=""float:right;"">
<img style=""margin-top: 8px; width: 50px; height: 50px; float:left;"" src=""/logo.png"">
<p style=""font-size: 24px; font-weight: bold; color: white; margin-left:10px; margin-right:10px; margin-top:16px; float:left;"">Test Management Tool</p> 
                            
</div>
 
                        </div>    
                 			
						<div id=""cwd-navbar"" class=""navbar navbar-default"" role=""navigation"">
							<ul class=""nav navbar-nav"">
								{header}	
					
							</ul>
								<div style=""float:right; margin-top:3px;"">
{project_options}
</div>			
					</div> 

				</header>

            <script src=""/lib/jquery/dist/jquery.js""></script>
           <script src=""//cwd5.web01.lincoln.ac.uk/cwd-min.js""></script>

        <div class=""container body-content"">
 <br>           
<h2>{pagetitle}</h2>
<br>

{content}

{script}

<br>
<br>
<br>
<br>
<br>
<br>
<br>
            <footer class=""container"" id=""cwd-footer"">
			<div class=""row"">
				<div class=""col-sm-6 col-lg-8"">
					<!-- do not remove the copyright logo. -->
					&copy; University of Lincoln
					<!-- Leave this here - the CWD version is written here using JavaScript on page load. -->
					<div id=""cwd-version"">version</div>
				</div>
				
				<div class=""col-xs-6 col-sm-3 col-lg-2"">
					<ul class=""nav nav-pills nav-stacked"">
						<li>
						<a href=""http://support.lincoln.ac.uk/"" target=""_blank"">ICT Support Desk</a>
						</li>
						<li>
						<a href=""http://lincoln.ac.uk/home/termsconditions/"" target=""_blank"">Policy Statements</a>
						</li>
					</ul>
				</div>
				<div class=""col-xs-6 col-sm-3 col-lg-2"">
					<ul class=""nav nav-pills nav-stacked"">
						<li><a href=""http://gateway.lincoln.ac.uk/"" target=""_blank"">Gateway</a></li>
						<li><a href=""http://lincoln.ac.uk/"" target=""_blank"">www.lincoln.ac.uk</a></li>
					</ul>
				</div>
			</div>
		</footer>

        </div>

      

    
    <script src = ""/lib/jquery/dist/jquery.min.js""></script>
<script src = ""/js/tooltip.js""></script>
<script src = ""/js/popover.js""></script>
    <script>
$(document).ready(function() {{
    $('.popovertext').popover();
}});</script>



</body>
</html>";
    
    }
     
        //Test selection page for test runs
        public static string testselectpage(int project, string dropdownhtml)
        {
            return $@"<h4>Select Test</h4>
<br>
<form class=""form-group"" action=""/testruns/Create/{project}"" method=""get"">
<div class=""form-inline"">
{dropdownhtml}
<input type=""submit"" value=""Create"" class=""btn btn-default"" />
</div>
</form>
<br>
<br>
";
        }

      

        //Get page header links based on role
        public static string Header(RoleInfo role, string controller, string action)
        {
            var istester = role.IsTester();
            var isadmin = role.IsAdministrator();
            var project = role.project;

            Func<string, string, string> active2 = (c, a) => { return (c == controller && a == action) ? @" class=""active""" : ""; };

            Func<string, string> active = (c) => { return (c == controller) ? @" class=""active""" : ""; };

            Func<string, string, string> activenot = (c, a) => { return (c == controller && a != action) ? @" class=""active""" : ""; };


            if (istester)
            {

                /*<li{active2("testruns", "CreateSelect")}><a href=""/testruns/CreateSelect/{project}"">Execute New Test Run</a></li>
<li{active("tests")}><a href=""/tests/Index/{project}"">View Tests</a></li>
<li{activenot("testruns", "CreateSelect")}><a href=""/testruns/Index/{project}"">View Test Runs</a></li>
                 * 
*/
                return $@"
<li{active2("dashboard", "Homepage")}><a href = ""/dashboard/Homepage/{project}"">Home</a></li>
<li{active2("defects", "Create")}><a href=""/defects/Create/{project}"">Create Issue</a></li>
<li{activenot("defects", "Create")}><a href=""/defects/Index/{project}"">Search Issues</a></li>
<li{active2("dashboard", "Index")}><a href = ""/dashboard/Index/{project}"">Dashboard</a></li>
<li{active2("dashboard", "Help")}><a href = ""/dashboard/Help/{project}"">Help</a></li>
";
            }
            else
            {
                if(isadmin)
                {
                    return $@"
<li{active2("dashboard", "Homepage")}><a href = ""/dashboard/Homepage/{project}"">Home</a></li>
<li{active2("dashboard", "Index")}><a href = ""/dashboard/Index/{project}"">Dashboard</a></li>
<li{active("requirements")}><a href = ""/requirements/Index/{project}"">Requirements</a></li>
<li{active("tests")}><a href=""/tests/Index/{project}"">Tests</a></li>
<li{active("scheduletestruns")}><a href=""/scheduletestruns/Index/{project}"">Scheduled Runs</a></li>
<li{active("testruns")}><a href=""/testruns/Index/{project}"">Test Runs</a></li>
<li{active("defects")}><a href=""/defects/Index/{project}"">Defects</a></li>
<li{active("projects")}><a href=""/projects/Index/{project}"">Projects</a></li>
<li{active("users")}><a href=""/users/Index/{project}"">Users</a></li>
<li{active2("dashboard", "Help")}><a href = ""/dashboard/Help/{project}"">Help</a></li>
";
                }
                else
                {
                    return $@"
<li{active2("dashboard", "Homepage")}><a href = ""/dashboard/Homepage/{project}"">Home</a></li>
<li{active2("dashboard", "Index")}><a href = ""/dashboard/Index/{project}"">Dashboard</a></li>
<li{active("requirements")}><a href = ""/requirements/Index/{project}"">Requirements</a></li>
<li{active("tests")}><a href=""/tests/Index/{project}"">Tests</a></li>
<li{active("scheduletestruns")}><a href=""/scheduletestruns/Index/{project}"">Scheduled Runs</a></li>
<li{active("testruns")}><a href=""/testruns/Index/{project}"">Test Runs</a></li>
<li{active("defects")}><a href=""/defects/Index/{project}"">Defects</a></li>
<li{active2("dashboard", "Help")}><a href = ""/dashboard/Help/{project}"">Help</a></li>
";
                }

            }

        }


        //Multifield templates

        public static readonly string multiinner = @"<div class=""form-group""><label class=""col-md-2 control-label"">@mdlabel@</label><div class=""col-md-10""><div class=""form-inline""><div class=""form-group"">@inner2@<input type=hidden name=""@hiddenname@"" value=""@hiddenvalue@""><button type=""button"" class=""btn btn-default"" onclick=""fnRemove@mdscriptname@(this);"">Remove</button></div></div></div></div>";

        public static readonly string multiinnernoremove = @"<div class=""form-group""><label class=""col-md-2 control-label"">@mdlabel@</label><div class=""col-md-10""><div class=""form-inline""><div class=""form-group"">@inner2@<input type=hidden name=""@hiddenname@"" value=""@hiddenvalue@""></div></div></div></div>";

        public static readonly string multiinnerrdonly = @"<div class=""form-group"">@inner2@</div>";

        public static readonly string multiouter = @"<div>@inner1@<div class=""form-group""><div class=""col-md-2""></div><div class=""col-md-10""><button type=""button"" class=""btn btn-default"" onclick=""fnAdd@mdscriptname@(this);"">Add @mdlabel@</button></div></div></div>";

        public static readonly string multiouterrdonly = @"<div>@inner1@</div>";

        public static readonly string multiscript = @"<script>""use strict"" 

function fnAdd@mdscriptname@(btn) {var newdiv = document.createElement(""div""); newdiv.id = ""multi_inner""; newdiv.innerHTML = '@mdinner@'; btn.parentNode.parentNode.parentNode.insertBefore(newdiv, btn.parentNode.parentNode); $('.popovertext').popover();};

function fnRemove@mdscriptname@(btn) {
    btn.parentNode.parentNode.parentNode.parentNode.parentNode.removeChild(btn.parentNode.parentNode.parentNode.parentNode)
};
</script>
";
    }


}
 