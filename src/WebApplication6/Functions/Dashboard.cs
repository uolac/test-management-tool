﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;

using System.IO;
using System.Text.RegularExpressions;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;


using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;


namespace WebApplication6
{
    //Represents a dashboard metric or chart
    public class DashboardElement
    {
        public string content;
        public string script;
        public int id;
    }

    public static class DashboardFunctions
    {
        //Get chart colour based on a number
        public static string basecolours(int i)
        {

            switch (i)
            {
                default: return basecolours(i - 9);
                case 0: return "#F7464A"; //red
                case 1: return "#FDB45C"; //orange
                case 2: return "#46BFBD"; //green
                case 3: return "#6495ED"; //blue
                case 4: return "#FBDB0C"; //yellow
                case 5: return "#C71585"; //purple
                case 6: return "#6464ed"; //blue2
                case 7: return "#d3d3d3"; //grey
                case 8: return "#696969"; //grey2
            }


        }

        //Get chart colour based on a DashColor parameter
        public static string basecolours(DashColor dc)
        {

            return basecolours((int)dc);
        }

        public static string highlightcolours(DashColor dc)
        {

            return highlightcolours((int)dc);
        }

        public static string highlightcolours(int i)
        {

            switch (i)
            {
                default: return basecolours(i - 9);
                case 0: return "#F7464A"; //red
                case 1: return "#FDB45C"; //orange
                case 2: return "#46BFBD"; //green
                case 3: return "#6495ED"; //blue
                case 4: return "#FBDB0C"; //yellow
                case 5: return "#C71585"; //purple
                case 6: return "#6464ed"; //blue2
                case 7: return "#d3d3d3"; //grey
                case 8: return "#696969"; //grey2
            }

        }
    }

    //Represents a chart colour
    public enum DashColor { Red, Orange, Green, Blue, Yellow, Purple, Blue2, None }

    public class Chart : DashboardElement { }

    //Represents a line chart
    public class LineChart : Chart
    {
        public LineChart(string label, LineChartData model, ref int idcounter, bool stacked = false)
        {

            //If stacked line chart
            if (stacked && model.datasets.Any())
            {
                for (int i = 0; i < model.datasets.Count(); i++)
                {
                    model.datasets[i].fillColor = model.datasets[i].strokeColor;
                }

                for (int i = 1; i < model.datasets.Count(); i++)
                {
                    for (int j = 0; j < model.datasets[i].data.Count(); j++)
                    {
                        model.datasets[i].data[j] += model.datasets[i - 1].data[j];
                    }
                }

                var datasets2 = model.datasets.Select(d => d.fillColor + "").ToList();
                model.datasets.Reverse();

            }
            idcounter++;
            this.id = idcounter;

            //Chart HTML
            this.content = $@"<div class=""col-md-4"">
<h2 class=""text-center"">{label}</h2>
<div>
<canvas id=""myChart{this.id}"" class=""center-block"" width=""300"" height=""300""/>
</div>
</div>
<div id=""legend{this.id}"" class=""legend col-md-2"">
</div>";

            //Chart Javascript
            this.script = $@"
<script type=""text/javascript"">


    // Get the context of the canvas element we want to select
    var ctx = document.getElementById(""myChart{this.id}"").getContext(""2d"");

    var data = {JsonConvert.SerializeObject(model)};

    var myLineChart = new Chart(ctx).Line(data, {{animation: false, bezierCurve: false, legendTemplate : '<table style=""margin-top: 130px;"">'
                            +'<% for (var i=0; i<datasets.length; i++) {{ %>'
                            +'<tr><td><div class=\""boxx\"" style=\""width: 20px; height: 20px; background-color:<%=datasets[i].strokeColor %>\""></div></td>'
                            + '<% if (datasets[i].label) {{ %><td><div class=""divider""></div><span style=""font-size: 13px;"">  <%= datasets[i].label %></span></td><% }} %></tr><tr height=""5""></tr>'
                            + '<% }} %>'
                            + '</table>'}});
    
    document.getElementById('legend{this.id}').innerHTML = myLineChart.generateLegend();


</script>";

            //Two charts per row
            if (idcounter % 2 == 1)
            {
                this.content = @"<div class=""row"">" + this.content;
            }
            else
            {
                this.content = this.content + "</div>";
            }
        }
    }

    public class MetricGroup : DashboardElement
    {

        public string text;
        public string metriccontent { get; set; }

        public MetricGroup(string text, ref int idcounter, params Metric[] metrics)
        {
            idcounter++;
            this.text = text;
            var metricstable = metrics.Select(m => m.content).Aggregate((x, y) => x + y);

            this.content = $@"<div><button type=""button"" class=""btn btn-primary"" data-toggle=""collapse"" data-target=""#search{idcounter}"">Show/Hide {text} Metrics</button>
    <div id=""search{idcounter}"" class=""collapse""><table class=""table""><th class=""col-md-8"">metric</th><th class=""col-md-2"">number</th><th class=""col-md-2"">percentage</th>{metricstable}</table></div></div>";

            this.metriccontent = metricstable;
        }
    }


        public class Metric : DashboardElement
    {
        public Metric(string label, string bookmark, int model, int total = -1)
        {
            if (total == -1)
            {
                //Number
                this.content = $@"<tr><td><a href=""#{bookmark}"">{label}</a></td><td>{model}</td><td>-</td></tr>";
            }
            else
            {
                //Percentage
                this.content = $@"<tr><td><a href=""#{bookmark}"">{label}</a></td><td>{model} of {total}</td><td>{Math.Round(total == 0 ? 0 : (float)model / total * 100, 1)}%</td></tr>";
            }

        }
    }

    public class PieChart : Chart
    {
        public PieChart(string label, IEnumerable<PieChartData> model_pre, ref int idcounter, string controller, string action, int project, string filter)
        {
            idcounter++;
            this.id = idcounter;

            var model = model_pre.Select(p => new PieChartDataInner(p));

            //Get the links when you click the chart
            var filtermap = model_pre.Any() ? model_pre.Select(p => $@"if (label == ""{p.label}"") {{ navid = {p.idfield}; }};").Aggregate((x, y) => x + y) : "";
      

            //Chart HTML
            this.content = $@"<div class=""col-md-4"">
<h2 class=""text-center"" id=""{label}"">{label}</h2>
<div>
<canvas id=""myChart{this.id}"" class=""center-block"" width=""300"" height=""300""/>
</div>
</div>
<div id=""legend{this.id}"" class=""legend col-md-2"">
</div>";

            //Chart Javascript
            this.script = $@"
<script type=""text/javascript"">


    // Get the context of the canvas element we want to select
    var ctx = document.getElementById(""myChart{this.id}"").getContext(""2d"");


    var data = {JsonConvert.SerializeObject(model.Any() ? model.ToList() : new List<PieChartDataInner>() { new PieChartDataInner(new PieChartData("None", 1, DashColor.Green, 0) ) })};

    var myPieChart{this.id} = new Chart(ctx).Pie(data, {{animation: false, legendTemplate: ""<ul style=\""list-style:none; margin-left:-30px; margin-top:170px;\""; class=\""<%=name.toLowerCase()%>-legend\""><% for (var i=0; i<segments.length; i++){{%><li><div style=\""width:200px\""><div style=\""float:left; margin-right:10px; width:15px; height:15px; background-color:<%=segments[i].fillColor%>\""></div><div style=\""float:left margin-right:100px;\""><%if(segments[i].label){{%><%=segments[i].label%><%}}%></div></div></li><%}}%></ul>""}});
    
       $(""#myChart{this.id}"").click( 
                        function(evt){{
                    var activePoints = myPieChart{this.id}.getSegmentsAtEvent(evt);
                    var navid = -4;
                    var label = activePoints[0].label;
                            {filtermap}
                    var url = '{Html.LinkInner(controller,action,project)}{filter}' + navid;

                    if (navid != -4) {{ window.location = url; }}; 
                        }}
                    );   

    document.getElementById('legend{this.id}').innerHTML = myPieChart{this.id}.generateLegend();


</script>";

            //Two charts per row
            if (idcounter % 2 == 1)
            {
                this.content = @"<div class=""row"">" + this.content;
            }
            else
            {
                this.content = this.content + "</div>";
            }

            ;
        }
    }

    //Stores a pie chart segment plus a link on clicking a segment
    public class PieChartData
    {
        public int value { get; set; }
        public string color { get; set; }
        public string highlight { get; set; }
        public string label { get; set; }
        public int idfield { get; set; }


        public PieChartData(string label, int value, int idfield)
        {
            this.label = label;
            this.value = value;
            this.idfield = idfield;
        }

        public PieChartData(string label, int value, DashColor num, int idfield)
        {
            this.label = label;
            this.value = value;
            this.color = DashboardFunctions.basecolours(num);
            this.highlight = DashboardFunctions.highlightcolours(num);
            this.idfield = idfield;
        }

        public PieChartData(string label, int value, int num, int idfield)
        {
            this.label = label;
            this.value = value;
            this.color = DashboardFunctions.basecolours(num);
            this.highlight = DashboardFunctions.highlightcolours(num);
            this.idfield = idfield;
        }
    }

    //Stores a pie chart segment
    public class PieChartDataInner
    {
        public int value { get; set; }
        public string color { get; set; }
        public string highlight { get; set; }
        public string label { get; set; }

        public PieChartDataInner(PieChartData d)
        {
            this.label = d.label;
            this.value = d.value;
            this.highlight = d.highlight;
            this.color = d.color;
        }
    }

    //Stores a line chart's data
    public class LineChartData
    {
        public List<string> labels { get; set; }
        public List<LineChartSeries> datasets { get; set; }
        public LineChartData(IEnumerable<string> label, params LineChartSeries[] datasets)
        {
            this.datasets = datasets.ToList();
            this.labels = label.ToList();
        }


    }

    //Stores a line chart series
    public class LineChartSeries
    {
        public LineChartSeries(string label, IEnumerable<int> data, int color)
        {
            this.label = label;
            this.data = data.ToList();
            this.pointColor = DashboardFunctions.basecolours(color);
            this.strokeColor = DashboardFunctions.basecolours(color);
        }

        public string label { get; set; }
        public string fillColor { get; set; } = "rgba(220,220,220,0)";
        public string strokeColor { get; set; } = "rgba(220,220,220,1)";
        public string pointColor { get; set; } = "rgba(220,220,220,1)";
        public string pointStrokeColor { get; set; } = "#fff";
        public string pointHighlightFill { get; set; } = "#fff";
        public string pointHighlightStroke { get; set; } = "rgba(220,220,220,1)";
        public List<int> data { get; set; }
    }

    public static partial class DerivedStateExtensions
    {
        //Default colours for certain statuses
        public static DashColor GetColor(this RequirementState source)
        {
            switch (source)
            {
                default: return DashColor.None;
                case RequirementState.Draft: return DashColor.Blue;
                case RequirementState.AcceptedWillNotBeMet: return DashColor.Blue2;
                case RequirementState.NoTestsLive: return DashColor.Red;
                case RequirementState.NotStarted: return DashColor.Yellow;
                case RequirementState.InProgress: return DashColor.Orange;
                case RequirementState.AllPassed: return DashColor.Green;
            }
        }

        //Default colours for certain statuses
        public static DashColor GetColor(this TestState source)
        {
            switch (source)
            {
                default: return DashColor.None;
                case TestState.Draft: return DashColor.Blue;
                case TestState.NotStarted: return DashColor.Red;
                case TestState.Failed: return DashColor.Orange;
                case TestState.Blocked: return DashColor.Yellow;
                case TestState.Passed: return DashColor.Green;
            }
        }

        //Helper functions that generates a pie chart from grouped Linq queries

        public static IEnumerable<PieChartData> GeneratePieChart<T1, T2>(this IEnumerable<IGrouping<T1, T2>> gr, Func<IGrouping<T1, T2>, string> f, Func<T1, DashColor> color, Func<T1,int> idfunc)
        {
            var pc = gr.Select((g, i) => new PieChartData(f(g) ?? "None", g.Count(), color(g.Key), idfunc(g.Key)));
            return pc;
        }

        public static IEnumerable<PieChartData> GeneratePieChart<T1, T2>(this IEnumerable<IGrouping<T1, T2>> gr, Func<IGrouping<T1, T2>, string> f, Func<T1, int> color, Func<T1, int> idfunc)
        {
            var pc = gr.Select((g, i) => new PieChartData(f(g) ?? "None", g.Count(), color(g.Key), idfunc(g.Key)));
            return pc;
        }

        public static IEnumerable<PieChartData> GeneratePieChart<T1, T2>(this IEnumerable<IGrouping<T1, T2>> gr, Func<IGrouping<T1, T2>, string> f, Func<T1, int> idfunc)
        {
            var pc = gr.Select((g, i) => new PieChartData(f(g) ?? "None", g.Count(), i, idfunc(g.Key)));
            return pc;
        }


    }
}
