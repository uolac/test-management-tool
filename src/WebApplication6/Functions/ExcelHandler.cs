﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;

using System.IO;
using System.Text.RegularExpressions;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;


using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;


namespace WebApplication6
{
    public static partial class PageBuilder
    {
        //Generates a report from the page information

        public static IActionResult GenerateReport(int project, int test_cycle_id, IEnumerable<object> model, params Column[] columns)
        {
            return GenerateReport(project, test_cycle_id, model, null, "Report", "", columns);
        }

        public static IActionResult GenerateReport(int project, int test_cycle_id, IEnumerable<object> model, IEnumerable<object> model2, string nm, string nm2, params Column[] columns)
        {

            ActionResult result = PageBuilder.Back(new WebApplication6.RoleInfo(), "Report generation failed", "", "", "", project);


            using (var ms = new MemoryStream())
            {

                //Uses DocumentFormat.OpenXml to create a new spreadsheet
                var spreadSheet = SpreadsheetDocument.Create(ms, SpreadsheetDocumentType.Workbook);

                spreadSheet.AddWorkbookPart();
                spreadSheet.WorkbookPart.Workbook = new Workbook();
                var wsp = spreadSheet.WorkbookPart.AddNewPart<WorksheetPart>();
                wsp.Worksheet = new Worksheet();
                wsp.Worksheet.AppendChild(new SheetData());

                if (model.Any())
                {

                    var toprow = wsp.Worksheet.First().AppendChild(new Row());

                    var modelexample = model.First();


                    //Create first row
                    foreach (var c in columns)
                    {
                        toprow.AppendChild(new Cell()
                        {
                            DataType = CellValues.InlineString,
                            InlineString = new InlineString() { Text = new Text(c.label) }
                        });
                    }

                    //For each object in the model
                    foreach (var i in model)
                    {
                        var row = wsp.Worksheet.First().AppendChild(new Row());
                        foreach (var c in columns)
                        {
                            //Set the cell content
                            var cellcontent = GetTableContent(c, i, test_cycle_id);
                            if (c.excelint)
                            {
                                row.AppendChild(new Cell() { CellValue = new CellValue(cellcontent.ToString()) });
                            }
                            else
                            {
                                row.AppendChild(new Cell()
                                {
                                    DataType = CellValues.InlineString,
                                    InlineString = new InlineString() { Text = new Text(cellcontent.ToString()) }
                                });
                            }
                        }
                    }
                }


                // save worksheet
                wsp.Worksheet.Save();
                ;
                // create the worksheet to workbook relation
                spreadSheet.WorkbookPart.Workbook.AppendChild(new Sheets());
                spreadSheet.WorkbookPart.Workbook.GetFirstChild<Sheets>().AppendChild(new Sheet()
                {
                    Id = spreadSheet.WorkbookPart.GetIdOfPart(wsp),
                    SheetId = 1,
                    Name = nm
                });

                
                if (model2 != null)
                {
                    wsp = spreadSheet.WorkbookPart.AddNewPart<WorksheetPart>();
                    wsp.Worksheet = new Worksheet();
                    wsp.Worksheet.AppendChild(new SheetData());
                    
                    if (model2.Any())
                    {

                        var toprow = wsp.Worksheet.First().AppendChild(new Row());

                        var modelexample = model2.First();


                        //Create first row
                        foreach (var c in columns)
                        {
                            toprow.AppendChild(new Cell()
                            {
                                DataType = CellValues.InlineString,
                                InlineString = new InlineString() { Text = new Text(c.label) }
                            });
                        }

                        //For each object in the model
                        foreach (var i in model2)
                        {
                            var row = wsp.Worksheet.First().AppendChild(new Row());
                            foreach (var c in columns)
                            {
                                //Set the cell content
                                var cellcontent = GetTableContent(c, i, test_cycle_id);
                                if (c.excelint)
                                {
                                    row.AppendChild(new Cell() { CellValue = new CellValue(cellcontent.ToString()) });
                                }
                                else
                                {
                                    row.AppendChild(new Cell()
                                    {
                                        DataType = CellValues.InlineString,
                                        InlineString = new InlineString() { Text = new Text(cellcontent.ToString()) }
                                    });
                                }
                            }
                        }
                    }
                    


                    // save worksheet
                    wsp.Worksheet.Save();
                    ;
                    // create the worksheet to workbook relation
                    //spreadSheet.WorkbookPart.Workbook.AppendChild(new Sheets());
                    spreadSheet.WorkbookPart.Workbook.GetFirstChild<Sheets>().AppendChild(new Sheet()
                    {
                        Id = spreadSheet.WorkbookPart.GetIdOfPart(wsp),
                        SheetId = 2,
                        Name = nm2
                    });
                }
                



                spreadSheet.WorkbookPart.Workbook.Save();

                spreadSheet.Close();

                ms.Seek(0, SeekOrigin.Begin);


                var array = ms.ToArray();

                //Return the created file to download
                result = new FileContentResult(array, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                {
                    FileDownloadName = "report.xlsx"
                };


            }

            return result;
        }
    }

    public static class Validation
    {
        //Checks a bulk imported field matches a database ID or name
        public static int ValidateID(string field, string value, IEnumerable<object> collection, ref string error_string, string colname = "")
        {
            int result = 0;
            colname = (colname == "" ? field : colname);

            int.TryParse(value, out result);

            if(!collection.Any()) { throw new Exception("ID required but no records exist"); }

            var idmatch = collection.Any(m => (int)m.GetType().GetProperty(field).GetValue(m) == result);
            var names = collection.Select(m => (string)m.GetType().GetProperty("name").GetValue(m));
            var namematch = collection.Where(m => ((string)m.GetType().GetProperty("name").GetValue(m)).ToLower() == value.ToLower()).FirstOrDefault();

            if (!idmatch && namematch == null)
            {
                error_string += colname + " must match a current " + field + " in this project; ";
            }
            if (namematch != null)
            {
                result = (int)namematch.GetType().GetProperty(field).GetValue(namematch);
            }
            
            return result;
        }

        //Checks a bulk imported field is a certain length
        public static string ValidateLength(string field, string value, int length, int lengthmax, ref string error_string, string colname = "")
        {

            colname = (colname == "" ? field : colname);

            if(value.Length <= length) { error_string += colname + " must be greater than " + length + " characters."; }
            if(value.Length > lengthmax) { error_string += colname + " must be less than " + lengthmax + " characters."; }
            return value;
        }

        //Checks a bulk imported field matches a database ID or name or is blank
        public static int? ValidateIDOrNull(string field, string value, IEnumerable<object> collection, ref string error_string, string colname = "")
        {
            int resultnotnull = 0;
            int? result = resultnotnull;
            colname = (colname == "" ? field : colname);

            int.TryParse(value, out resultnotnull);

            if (value == "" || !collection.Any()) { result = null; }
            else
            {
                var idmatch = collection.Any(m => (int)m.GetType().GetProperty(field).GetValue(m) == resultnotnull);
                var nmtest = collection.First();
                var nmtest2 = (string)nmtest.GetType().GetProperty("name").GetValue(nmtest);
                var namematch = collection.Where(m => ((string)m.GetType().GetProperty("name").GetValue(m)).ToLower() == value.ToLower()).FirstOrDefault();        
                if (!idmatch && namematch == null)
                {
                    error_string += colname + " must match a current " + field + " in this project or be left blank; ";
                }
                if (namematch != null)
                {
                    result = (int)namematch.GetType().GetProperty(field).GetValue(namematch);
                }
                else { result = resultnotnull; }
            }


            return result;
        }

        //Checks a bulk imported field matches a database ID or name or sets a default
        public static int ValidateIDOrDefault(string field, string value, IEnumerable<object> collection, ref string error_string, int default_value, string colname = "")
        {
            int resultnotnull = 0;
            int? result = resultnotnull;
            colname = (colname == "" ? field : colname);

            int.TryParse(value, out resultnotnull);

            if (value == "" || !collection.Any()) { result = null; }
            else
            {
                var idmatch = collection.Any(m => (int)m.GetType().GetProperty(field).GetValue(m) == resultnotnull);
                var nmtest = collection.First();
                var nmtest2 = (string)nmtest.GetType().GetProperty("name").GetValue(nmtest);
                var namematch = collection.Where(m => ((string)m.GetType().GetProperty("name").GetValue(m)).ToLower() == value.ToLower()).FirstOrDefault();
                if (!idmatch && namematch == null)
                {
                    error_string += colname + " must match a current " + field + " in this project or be left blank; ";
                }
                if (namematch != null)
                {
                    result = (int)namematch.GetType().GetProperty(field).GetValue(namematch);
                }
                else { result = resultnotnull; }
            }


            return result ?? default_value;
        }
    }

    //Intermediate object for bulk importing
    public class ExcelCell
    {
        public int row;
        public int column;
        public string value;

        public ExcelCell(Cell c, IEnumerable<string> sst)
        {

            var refr = c.CellReference.InnerText;

            var col = refr.Substring(0, 1);
            int shift = 0;
            if (col == "A")
            {
                switch (refr.Substring(0, 2))
                {
                    case "AA": column = 27; shift = 1; break;
                    case "AB": column = 28; shift = 1; break;
                    case "AC": column = 29; shift = 1; break;
                    case "AD": column = 30; shift = 1; break;
                    case "AE": column = 31; shift = 1; break;
                    case "AF": column = 32; shift = 1; break;
                    case "AG": column = 33; shift = 1; break;
                    case "AH": column = 34; shift = 1; break;
                    case "AI": column = 35; shift = 1; break;
                    case "AJ": column = 36; shift = 1; break;
                    case "AK": column = 37; shift = 1; break;
                    case "AL": column = 38; shift = 1; break;
                    case "AM": column = 39; shift = 1; break;
                    case "AN": column = 40; shift = 1; break;
                    case "AO": column = 41; shift = 1; break;
                    case "AP": column = 42; shift = 1; break;
                    case "AQ": column = 43; shift = 1; break;
                    case "AR": column = 44; shift = 1; break;
                    case "AS": column = 45; shift = 1; break;
                    case "AT": column = 46; shift = 1; break;
                    case "AU": column = 47; shift = 1; break;
                    case "AV": column = 48; shift = 1; break;
                    case "AW": column = 49; shift = 1; break;
                    case "AX": column = 50; shift = 1; break;
                    case "AY": column = 51; shift = 1; break;
                    case "AZ": column = 52; shift = 1; break;
                    default: column = 1; break;
                }
            }
            else
            if (col == "B")
            {
                switch (refr.Substring(0, 2))
                {
                    case "BA": column = 53; shift = 1; break;
                    case "BB": column = 54; shift = 1; break;
                    case "BC": column = 55; shift = 1; break;
                    case "BD": column = 56; shift = 1; break;
                    case "BE": column = 57; shift = 1; break;
                    case "BF": column = 58; shift = 1; break;
                    case "BG": column = 59; shift = 1; break;
                    case "BH": column = 60; shift = 1; break;
                    case "BI": column = 61; shift = 1; break;
                    case "BJ": column = 62; shift = 1; break;
                    case "BK": column = 63; shift = 1; break;
                    case "BL": column = 64; shift = 1; break;
                    case "BM": column = 65; shift = 1; break;
                    case "BN": column = 66; shift = 1; break;
                    case "BO": column = 67; shift = 1; break;
                    case "BP": column = 68; shift = 1; break;
                    case "BQ": column = 69; shift = 1; break;
                    case "BR": column = 70; shift = 1; break;
                    case "BS": column = 71; shift = 1; break;
                    case "BT": column = 72; shift = 1; break;
                    case "BU": column = 73; shift = 1; break;
                    case "BV": column = 74; shift = 1; break;
                    case "BW": column = 75; shift = 1; break;
                    case "BX": column = 76; shift = 1; break;
                    case "BY": column = 77; shift = 1; break;
                    case "BZ": column = 78; shift = 1; break;
                    default: column = 2; break;
                }
            }
            else
            if (col == "C")
            {
                switch (refr.Substring(0, 2))
                {
                    case "CA": column = 79; shift = 1; break;
                    case "CB": column = 80; shift = 1; break;
                    case "CC": column = 81; shift = 1; break;
                    case "CD": column = 82; shift = 1; break;
                    case "CE": column = 83; shift = 1; break;
                    case "CF": column = 84; shift = 1; break;
                    case "CG": column = 85; shift = 1; break;
                    case "CH": column = 86; shift = 1; break;
                    case "CI": column = 87; shift = 1; break;
                    case "CJ": column = 88; shift = 1; break;
                    case "CK": column = 89; shift = 1; break;
                    case "CL": column = 90; shift = 1; break;
                    case "CM": column = 91; shift = 1; break;
                    case "CN": column = 92; shift = 1; break;
                    case "CO": column = 93; shift = 1; break;
                    case "CP": column = 94; shift = 1; break;
                    case "CQ": column = 95; shift = 1; break;
                    case "CR": column = 96; shift = 1; break;
                    case "CS": column = 97; shift = 1; break;
                    case "CT": column = 98; shift = 1; break;
                    case "CU": column = 99; shift = 1; break;
                    case "CV": column = 100; shift = 1; break;
                    case "CW": column = 101; shift = 1; break;
                    case "CX": column = 102; shift = 1; break;
                    case "CY": column = 103; shift = 1; break;
                    case "CZ": column = 104; shift = 1; break;
                    default: column = 3; break;
                }
            }
            else
                 if (col == "D")
            {
                switch (refr.Substring(0, 2))
                {
                    case "DA": column = 105; shift = 1; break;
                    case "DB": column = 106; shift = 1; break;
                    case "DC": column = 107; shift = 1; break;
                    case "DD": column = 108; shift = 1; break;
                    case "D": column = 3; break;
                    default: column = 999; break;
                }
            }
            else
            {
                switch (col)
                {
                    case "D": column = 4; break;
                    case "E": column = 5; break;
                    case "F": column = 6; break;
                    case "G": column = 7; break;
                    case "H": column = 8; break;
                    case "I": column = 9; break;
                    case "J": column = 10; break;
                    case "K": column = 11; break;
                    case "L": column = 12; break;
                    case "M": column = 13; break;
                    case "N": column = 14; break;
                    case "O": column = 15; break;
                    case "P": column = 16; break;
                    case "Q": column = 17; break;
                    case "R": column = 18; break;
                    case "S": column = 19; break;
                    case "T": column = 20; break;
                    case "U": column = 21; break;
                    case "V": column = 22; break;
                    case "W": column = 23; break;
                    case "X": column = 24; break;
                    case "Y": column = 25; break;
                    case "Z": column = 26; break;
                    default: column = 999; break;
                }
            }
            if(column == 999)
            {
                return;
            }
            row = Convert.ToInt32(refr.Substring(1 + shift, refr.Length - 1 - shift));
            value = (c.DataType == null ? (c?.CellValue?.InnerText ?? "") : sst.ElementAt(Convert.ToInt32((c?.CellValue?.InnerText ?? "0"))));
        }

    }
}
