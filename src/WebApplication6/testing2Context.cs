using Microsoft.EntityFrameworkCore;
using System.Linq;
using Microsoft.EntityFrameworkCore.Metadata;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Utilities;
using Microsoft.Extensions.Logging;
using System.Text.RegularExpressions;
using System.Net.Http.Headers;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace WebApplication6
{

    public partial class testing2Context : DbContext
    {
        private IHostingEnvironment env;

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            string cs;

            // options.UseSqlServer(@"Server=(localdb)\v11.0;Database=testing2;Trusted_Connection=True;");
            if(env.ContentRootPath.Contains("aCoady"))
            {
                cs = @"Server=(localdb)\v11.0;Database=testing2;Trusted_Connection=True;";
            }
            else
            {
                //cs = @"Server=(localdb)\v11.0;Database=testing2;Trusted_Connection=True;";
                cs = @"Server=ASQL02;Database=TestManagementTool;Trusted_Connection=True;";
            }
          
            // var cs = @"Server=ASQL02;Database=TestManagementTool;User Id=NETWORK\TestManagementTool;Password='q)#4t(m>b)5jR;)~';";
            // throw new Exception(cs);
            options.UseSqlServer(cs);
            ;
        }

        public testing2Context(DbContextOptions options, IHostingEnvironment ienv) : base(options) {
            env = ienv;
        }


        public testing2Context(DbContextOptions options) : base(options)
        {
            throw new Exception();
        }

        //Create the database model
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<runs_attachments>(entity =>
            {
                entity.HasKey(e => e.run_attachment_id);

               

                entity.Property(e => e.run_attachment_id).UseSqlServerIdentityColumn();

                entity.Property(e => e.attachment_content);

                entity.Property(e => e.attachment_mimetype)
                    .HasMaxLength(255);



                entity.Property(e => e.attachment_filename)
                    .HasMaxLength(255);
                   

                entity.Property(e => e.last_modified_by).HasDefaultValue(1);

                entity.Property(e => e.last_modified_date)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.HasOne(d => d.last_modified_byNavigation).WithMany(p => p.runs_attachments).HasForeignKey(d => d.last_modified_by).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.runs).WithMany(p => p.attachments).HasForeignKey(d => d.run_id).OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<defects_attachments>(entity =>
            {
                entity.HasKey(e => e.defect_attachment_id);



                entity.Property(e => e.defect_attachment_id).UseSqlServerIdentityColumn();

                entity.Property(e => e.attachment_content);

                entity.Property(e => e.attachment_mimetype)
                    .HasMaxLength(255);


                entity.Property(e => e.attachment_filename)
                    .HasMaxLength(255);
                   

                entity.Property(e => e.last_modified_by).HasDefaultValue(1);

                entity.Property(e => e.last_modified_date)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.HasOne(d => d.last_modified_byNavigation).WithMany(p => p.defects_attachments).HasForeignKey(d => d.last_modified_by).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.defects).WithMany(p => p.defects_attachments).HasForeignKey(d => d.defect_id).OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<audit>(entity =>
            {
                entity.HasKey(e => e.audit_id);

                entity.Property(e => e.audit_id).UseSqlServerIdentityColumn();

                entity.Property(e => e.entity);
                entity.Property(e => e.entity_pk);
                entity.Property(e => e.property);
                entity.Property(e => e.original);
                entity.Property(e => e.@new);
                entity.Property(e => e.modified_by);
                entity.Property(e => e.modified_date);
            });
                modelBuilder.Entity<defect_history>(entity =>
            {
                entity.HasKey(e => e.defect_history_id);

                entity.Property(e => e.defect_history_id).UseSqlServerIdentityColumn();

                entity.Property(e => e.defect_status_id).HasDefaultValue(0);

                entity.Property(e => e.effective_date)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.last_modified_by).HasDefaultValue(1);

                entity.Property(e => e.last_modified_date)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.priority_id).HasDefaultValue(3);

                entity.Property(e => e.status_comment);

                entity.HasOne(d => d.assigned_toNavigation).WithMany(p => p.defect_history).HasForeignKey(d => d.assigned_to);

                entity.HasOne(d => d.defect).WithMany(p => p.defect_history).HasForeignKey(d => d.defect_id).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.defect_status).WithMany(p => p.defect_history).HasForeignKey(d => d.defect_status_id).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.closure_reason).WithMany(p => p.defect_history).HasForeignKey(d => d.closure_reason_id).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.last_modified_byNavigation).WithMany(p => p.defect_historyNavigation).HasForeignKey(d => d.last_modified_by).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.priority).WithMany(p => p.defect_history).HasForeignKey(d => d.priority_id).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.severity).WithMany(p => p.defect_history).HasForeignKey(d => d.severity_id);
            });

            modelBuilder.Entity<defect_status>(entity =>
            {
                entity.HasKey(e => e.defect_status_id);

                entity.Property(e => e.defect_status_id).UseSqlServerIdentityColumn();

                entity.Property(e => e.name)
                    .HasMaxLength(50);
                   
            });

            modelBuilder.Entity<phases>(entity =>
            {
                entity.HasKey(e => e.phase_id);

                entity.Property(e => e.phase_id).UseSqlServerIdentityColumn();

                entity.Property(e => e.name)
                    .HasMaxLength(50);

            });

            modelBuilder.Entity<closure_reason>(entity =>
            {
                entity.HasKey(e => e.closure_reason_id);

                entity.Property(e => e.closure_reason_id).UseSqlServerIdentityColumn();

                entity.Property(e => e.name)
                    .HasMaxLength(50);
                   
            });

            modelBuilder.Entity<defect_types>(entity =>
            {
                entity.HasKey(e => e.defect_type_id);

                entity.Property(e => e.defect_type_id).UseSqlServerIdentityColumn();

                entity.Property(e => e.name)
                    .HasMaxLength(50);

            });

            modelBuilder.Entity<test_runs_test_steps>(entity =>
            {
                entity.HasKey(e => e.test_runs_test_steps_id);

                entity.Property(e => e.test_runs_test_steps_id).UseSqlServerIdentityColumn();

                entity.Property(e => e.completed)
                    .HasColumnType("int");


                entity.HasOne(e => e.run).WithMany(e => e.test_runs_test_steps).HasForeignKey(e => e.run_id);
                entity.HasOne(e => e.test_steps).WithMany(e => e.test_runs_test_steps).HasForeignKey(e => e.test_step_id);
            });

            var t = modelBuilder.Model;

            modelBuilder.Entity<defects>(entity =>
            {
                entity.HasKey(e => e.defect_id);

                entity.Property(e => e.defect_id).UseSqlServerIdentityColumn();

                entity.Property(e => e.name);

                entity.Property(e => e.description);

                entity.Property(e => e.external_ref);

                entity.Property(e => e.last_modified_by).HasDefaultValue(1);

                entity.Property(e => e.found_date).HasColumnType("datetime");

                entity.Property(e => e.due_date).HasColumnType("datetime");

                entity.Property(e => e.last_modified_date)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.tags)
                    .HasMaxLength(255);
                   

                entity.HasOne(d => d.test_run).WithMany(p => p.defects).HasForeignKey(d => d.test_run_id).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.found_byNavigation).WithMany(p => p.defects).HasForeignKey(d => d.found_by).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.defect_type).WithMany(p => p.defects).HasForeignKey(d => d.defect_type_id).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.phase).WithMany(p => p.defects).HasForeignKey(d => d.phase_id).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.last_modified_byNavigation).WithMany(p => p.defectsNavigation).HasForeignKey(d => d.last_modified_by).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.module).WithMany(p => p.defects).HasForeignKey(d => d.module_id);

                entity.HasOne(d => d.project).WithMany(p => p.defects).HasForeignKey(d => d.project_id).OnDelete(DeleteBehavior.Restrict);
            });

            var t2 = modelBuilder.Model;

            modelBuilder.Entity<draft_status>(entity =>
            {
                entity.HasKey(e => e.draft_status_id);

                entity.Property(e => e.draft_status_id).UseSqlServerIdentityColumn();

                entity.Property(e => e.name)
                    .HasMaxLength(50);
                   
            });

            modelBuilder.Entity<email_preferences>(entity =>
            {
                entity.HasKey(e => e.email_preference_id);

                entity.Property(e => e.email_preference_id).UseSqlServerIdentityColumn();

                entity.Property(e => e.name)
                    .HasMaxLength(50);
                   
            });

            modelBuilder.Entity<modules>(entity =>
            {
                entity.HasKey(e => e.module_id);

                entity.Property(e => e.module_id).UseSqlServerIdentityColumn();

                entity.Property(e => e.description);

                entity.Property(e => e.last_modified_by).HasDefaultValue(1);

                entity.Property(e => e.last_modified_date)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.name)
                    .HasMaxLength(50)
                    
                    .HasDefaultValue("New Module");

                entity.HasOne(d => d.last_modified_byNavigation).WithMany(p => p.modules).HasForeignKey(d => d.last_modified_by).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.project).WithMany(p => p.modules).HasForeignKey(d => d.project_id).OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<outcomes>(entity =>
            {
                entity.HasKey(e => e.outcome_id);

                entity.Property(e => e.outcome_id).UseSqlServerIdentityColumn();

                entity.Property(e => e.name)
                    .HasMaxLength(50);
                   
            });

            modelBuilder.Entity<priorities>(entity =>
            {
                entity.HasKey(e => e.priority_id);

                entity.Property(e => e.priority_id).UseSqlServerIdentityColumn();

                entity.Property(e => e.name)
                    .HasMaxLength(50);
                   
            });

            modelBuilder.Entity<projects>(entity =>
            {
                entity.HasKey(e => e.project_id);

                entity.Property(e => e.project_id).UseSqlServerIdentityColumn();

                entity.Property(e => e.description);

                entity.Property(e => e.last_modified_by).HasDefaultValue(0);

                entity.Property(e => e.last_modified_date)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

              /*  entity.Property(e => e.start_date)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.end_date)
                    .HasColumnType("datetime"); */

                entity.Property(e => e.name)
                    .HasMaxLength(50)
                    
                    .HasDefaultValue("New Project");

                entity.HasOne(d => d.current_test_cycle).WithMany(p => p.projects).HasForeignKey(d => d.current_test_cycle_id);

                entity.HasOne(d => d.last_modified_byNavigation).WithMany(p => p.projects).HasForeignKey(d => d.last_modified_by).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.owner).WithMany(p => p.projectsNavigation).HasForeignKey(d => d.owner_id);
            });

            modelBuilder.Entity<requirements>(entity =>
            {
                entity.HasKey(e => e.requirement_id);

                entity.Property(e => e.requirement_id).UseSqlServerIdentityColumn();

                entity.Property(e => e.cross_reference)
                    .HasMaxLength(255);
                   

                entity.Property(e => e.description);

                entity.Property(e => e.draft_status_id).HasDefaultValue(1);

                entity.Property(e => e.date_made_live)
                    .HasColumnType("datetime");

                entity.Property(e => e.effective_date)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.last_modified_by).HasDefaultValue(1);

                entity.Property(e => e.last_modified_date)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.name)
                    .HasMaxLength(50)
                    
                    .HasDefaultValue("New Requirement");

                entity.Property(e => e.tags)
                    .HasMaxLength(255);
                   

                entity.Property(e => e.accepted_will_not_be_met)
                   .HasColumnType("bit").HasDefaultValue(false);

                entity.Property(e => e.compulsory_yn)
                .HasColumnType("bit").HasDefaultValue(true);

                entity.HasOne(d => d.draft_status).WithMany(p => p.requirements).HasForeignKey(d => d.draft_status_id).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.last_modified_byNavigation).WithMany(p => p.requirements).HasForeignKey(d => d.last_modified_by).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.module).WithMany(p => p.requirements).HasForeignKey(d => d.module_id);

                entity.HasOne(d => d.owner).WithMany(p => p.requirementsNavigation).HasForeignKey(d => d.owner_id);

                entity.HasOne(d => d.project).WithMany(p => p.requirements).HasForeignKey(d => d.project_id).OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<roles>(entity =>
            {
                entity.HasKey(e => e.role_id);

                entity.Property(e => e.role_id).UseSqlServerIdentityColumn();

                entity.Property(e => e.name)
                    .HasMaxLength(50);
                   
            });

            modelBuilder.Entity<runs>(entity =>
            {
                entity.HasKey(e => e.run_id);

                entity.Property(e => e.run_id).UseSqlServerIdentityColumn();

                entity.Property(e => e.description);

                entity.Property(e => e.last_modified_by).HasDefaultValue(1);

                entity.Property(e => e.last_modified_date)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.run_date)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.HasOne(d => d.last_modified_byNavigation).WithMany(p => p.runs).HasForeignKey(d => d.last_modified_by).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.outcome).WithMany(p => p.runs).HasForeignKey(d => d.outcome_id).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.run_byNavigation).WithMany(p => p.runsNavigation).HasForeignKey(d => d.run_by).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.test_cycle).WithMany(p => p.runs).HasForeignKey(d => d.test_cycle_id).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.test).WithMany(p => p.runs).HasForeignKey(d => d.test_id).OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<scheduled_status>(entity =>
            {
                entity.HasKey(e => e.scheduled_status_id);

                entity.Property(e => e.scheduled_status_id).UseSqlServerIdentityColumn();

                entity.Property(e => e.name)
                    .HasMaxLength(50);
                   
            });

            modelBuilder.Entity<severities>(entity =>
            {
                entity.HasKey(e => e.severity_id);

                entity.Property(e => e.severity_id).UseSqlServerIdentityColumn();

                entity.Property(e => e.name)
                    .HasMaxLength(50);
                   
            });

            modelBuilder.Entity<test_cycles>(entity =>
            {
                entity.HasKey(e => e.test_cycle_id);

                entity.Property(e => e.test_cycle_id).UseSqlServerIdentityColumn();

                entity.Property(e => e.description);

                entity.Property(e => e.last_modified_by).HasDefaultValue(0);

                entity.Property(e => e.last_modified_date)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.name)
                    .HasMaxLength(50)
                    
                    .HasDefaultValue("New Test Cycle");

                entity.HasOne(d => d.last_modified_byNavigation).WithMany(p => p.test_cycles).HasForeignKey(d => d.last_modified_by).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.project).WithMany(p => p.test_cycles).HasForeignKey(d => d.project_id).OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<requirements_tests>(entity =>
            {
                entity.HasKey(e => e.requirements_tests_id);

                entity.Property(e => e.requirements_tests_id).UseSqlServerIdentityColumn();

                entity.HasOne(d => d.requirement).WithMany(p => p.requirements_tests).HasForeignKey(d => d.requirement_id).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.test).WithMany(p => p.requirements_tests).HasForeignKey(d => d.test_id).OnDelete(DeleteBehavior.Restrict);

            });

            modelBuilder.Entity<test_cycles_tests>(entity =>
            {
                entity.HasKey(e => e.test_cycle_test_id);

                entity.Property(e => e.test_cycle_test_id).UseSqlServerIdentityColumn();

                entity.Property(e => e.last_modified_by).HasDefaultValue(0);

                entity.Property(e => e.last_modified_date)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.scheduled_end_date).HasColumnType("datetime");

                entity.Property(e => e.scheduled_start_date).HasColumnType("datetime");

                entity.HasOne(d => d.last_modified_byNavigation).WithMany(p => p.test_cycles_tests).HasForeignKey(d => d.last_modified_by).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.scheduled_status).WithMany(p => p.test_cycles_tests).HasForeignKey(d => d.scheduled_status_id).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.scheduled_userNavigation).WithMany(p => p.test_cycles_testsNavigation).HasForeignKey(d => d.scheduled_user);

                entity.HasOne(d => d.test_cycle).WithMany(p => p.test_cycles_tests).HasForeignKey(d => d.test_cycle_id).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.test).WithMany(p => p.test_cycles_tests).HasForeignKey(d => d.test_id).OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<test_steps>(entity =>
            {
                entity.HasKey(e => e.test_step_id);

                entity.Property(e => e.test_step_id).UseSqlServerIdentityColumn();

                entity.Property(e => e.description);

                entity.Property(e => e.expected_result);

                entity.Property(e => e.last_modified_by).HasDefaultValue(1);

                entity.Property(e => e.last_modified_date)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.seq_no).HasDefaultValue(1);

                entity.HasOne(d => d.last_modified_byNavigation).WithMany(p => p.test_steps).HasForeignKey(d => d.last_modified_by).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.test).WithMany(p => p.test_steps).HasForeignKey(d => d.test_id).OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<tests>(entity =>
            {
                entity.HasKey(e => e.test_id);

                entity.Property(e => e.test_id).UseSqlServerIdentityColumn();

                entity.Property(e => e.description);

                entity.Property(e => e.draft_status_id).HasDefaultValue(1);

                entity.Property(e => e.effective_date)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.last_modified_by).HasDefaultValue(1);

                entity.Property(e => e.last_modified_date)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.date_made_live)
                 .HasColumnType("datetime");

                entity.Property(e => e.name)
                    .HasMaxLength(50)
                    
                    .HasDefaultValue("New Test");

                entity.Property(e => e.priority_id).HasDefaultValue(3);

                entity.Property(e => e.tags)
                    .HasMaxLength(255);


                entity.Property(e => e.test_item)
                    .HasMaxLength(255);
                   

                entity.HasOne(d => d.draft_status).WithMany(p => p.tests).HasForeignKey(d => d.draft_status_id).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.last_modified_byNavigation).WithMany(p => p.tests).HasForeignKey(d => d.last_modified_by).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.owner).WithMany(p => p.testsNavigation).HasForeignKey(d => d.owner_id);

                entity.HasOne(d => d.project).WithMany(p => p.tests).HasForeignKey(d => d.project_id);

                entity.HasOne(d => d.module).WithMany(p => p.tests).HasForeignKey(d => d.module_id);

                entity.HasOne(d => d.priority).WithMany(p => p.tests).HasForeignKey(d => d.priority_id).OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<users>(entity =>
            {
                entity.HasKey(e => e.user_id);

                entity.Property(e => e.user_id).UseSqlServerIdentityColumn();

                entity.Property(e => e.ad_identifier)
                    .HasMaxLength(50);


                entity.Property(e => e.email)
                    .HasMaxLength(255);
                   

                entity.Property(e => e.email_preference_id).HasDefaultValue(0);

                entity.Property(e => e.last_modified_by).HasDefaultValue(1);

                entity.Property(e => e.last_modified_date)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.is_administrator).HasColumnType("bit").HasDefaultValue(false);

                entity.Property(e => e.name)
                    .HasMaxLength(50)
                    
                    .HasDefaultValue("New User");

                entity.HasOne(d => d.default_project).WithMany(p => p.users).HasForeignKey(d => d.default_project_id);

                entity.HasOne(d => d.email_preference).WithMany(p => p.users).HasForeignKey(d => d.email_preference_id);

                entity.HasOne(d => d.last_modified_byNavigation).WithMany(p => p.Inverselast_modified_byNavigation).HasForeignKey(d => d.last_modified_by).OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<users_projects_roles>(entity =>
            {
                entity.HasKey(e => e.users_projects_roles_id);

                entity.Property(e => e.users_projects_roles_id).UseSqlServerIdentityColumn();

                entity.Property(e => e.last_modified_by).HasDefaultValue(1);

                entity.Property(e => e.last_modified_date)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.HasOne(d => d.last_modified_byNavigation).WithMany(p => p.users_projects_roles_lmb_Navigation).HasForeignKey(d => d.last_modified_by).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.user).WithMany(p => p.users_projects_roles).HasForeignKey(d => d.user_id).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.project).WithMany(p => p.users_projects_roles).HasForeignKey(d => d.project_id).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.role).WithMany(p => p.users_projects_roles).HasForeignKey(d => d.role_id).OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<entities>(entity =>
            {
                entity.HasKey(e => e.entity_id);

                entity.Property(e => e.entity_id).UseSqlServerIdentityColumn();

                entity.Property(e => e.entity_name)
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<fields>(entity =>
            {
                entity.HasKey(e => e.field_id);

                entity.Property(e => e.field_id).UseSqlServerIdentityColumn();

                entity.Property(e => e.description)
                    .HasMaxLength(255);

                entity.Property(e => e.field_name)
                    .HasMaxLength(50);

                entity.Property(e => e.field_type)
                    .HasMaxLength(50);

                entity.Property(e => e.front_end_field)
                    .HasMaxLength(50)
                   ;

                entity.HasOne(d => d.entity).WithMany(p => p.fields).HasForeignKey(d => d.entity_id);

                entity.HasOne(d => d.table).WithMany(p => p.fields).HasForeignKey(d => d.table_id);
            });

            modelBuilder.Entity<interfaces>(entity =>
            {
                entity.HasKey(e => e.interface_id);

                entity.Property(e => e.interface_id).UseSqlServerIdentityColumn();

                entity.Property(e => e.argument1)
                    .HasMaxLength(255)
                   ;

                entity.Property(e => e.external_id)
                    .HasMaxLength(50)
                   ;

                entity.Property(e => e.interface_name)
                    .HasMaxLength(50)
                   ;

                entity.Property(e => e.procedure_name)
                    .HasMaxLength(255)
                   ;

                entity.Property(e => e.schedule)
                    .HasMaxLength(255)
                   ;

                entity.Property(e => e.script);

                entity.Property(e => e.future_script);

                entity.Property(e => e.servers)
                    .HasMaxLength(255)
                   ;
            });

            modelBuilder.Entity<links>(entity =>
            {
                entity.HasKey(e => e.link_id);

                entity.Property(e => e.batch);

                entity.Property(e => e.link_id).UseSqlServerIdentityColumn();

                entity.Property(e => e.business_logic)
                    .HasMaxLength(255)
                   ;

                entity.Property(e => e.is_future).HasDefaultValue(false);

                entity.Property(e => e.transformation)
                    .HasMaxLength(255)
                   ;

                entity.Property(e => e.where_conditions)
                 ;

                entity.HasOne(d => d.destination_field).WithMany(p => p.links).HasForeignKey(d => d.destination_field_id);

                entity.HasOne(d => d._interface).WithMany(p => p.links).HasForeignKey(d => d.interface_id);

                entity.HasOne(d => d.source_field).WithMany(p => p.linksNavigation).HasForeignKey(d => d.source_field_id);

            });

            modelBuilder.Entity<systems>(entity =>
            {
                entity.HasKey(e => e.system_id);

                entity.Property(e => e.system_id).UseSqlServerIdentityColumn();

                entity.Property(e => e.system_name)
                    .HasMaxLength(50)
                   ;
            });

            modelBuilder.Entity<tables>(entity =>
            {
                entity.HasKey(e => e.table_id);

                entity.Property(e => e.table_id).UseSqlServerIdentityColumn();

                entity.Property(e => e.table_name)
                    .HasMaxLength(100)
                   ;

                entity.HasOne(d => d.system).WithMany(p => p.tables).HasForeignKey(d => d.system_id);
            });
        }

        //Creates specific types for database retrieval
        public virtual DbSet<runs_attachments> runs_attachments { get; set; }
        public virtual DbSet<defects_attachments> defects_attachments { get; set; }
        public virtual DbSet<defect_history> defect_history { get; set; }
        public virtual DbSet<defect_status> defect_status { get; set; }
        public virtual DbSet<closure_reason> closure_reason { get; set; }
        public virtual DbSet<defects> defects { get; set; }
        public virtual DbSet<draft_status> draft_status { get; set; }
        public virtual DbSet<email_preferences> email_preferences { get; set; }
        public virtual DbSet<modules> modules { get; set; }
        public virtual DbSet<outcomes> outcomes { get; set; }
        public virtual DbSet<priorities> priorities { get; set; }
        public virtual DbSet<projects> projects { get; set; }
        public virtual DbSet<requirements> requirements { get; set; }
        public virtual DbSet<requirements_tests> requirements_tests { get; set; }
        public virtual DbSet<roles> roles { get; set; }
        public virtual DbSet<runs> runs { get; set; }
        public virtual DbSet<scheduled_status> scheduled_status { get; set; }
        public virtual DbSet<severities> severities { get; set; }
        public virtual DbSet<defect_types> defect_types { get; set; }
        public virtual DbSet<phases> phases { get; set; }
        public virtual DbSet<test_cycles> test_cycles { get; set; }
        public virtual DbSet<test_cycles_tests> test_cycles_tests { get; set; }
        public virtual DbSet<test_runs_test_steps> test_runs_test_steps { get; set; }
        public virtual DbSet<test_steps> test_steps { get; set; }
        public virtual DbSet<tests> tests { get; set; }
        public virtual DbSet<users> users { get; set; }
        public virtual DbSet<audit> audit { get; set; }
        public virtual DbSet<users_projects_roles> users_projects_roles { get; set; }
        public virtual DbSet<entities> entities { get; set; }
        public virtual DbSet<fields> fields { get; set; }
        public virtual DbSet<interfaces> interfaces { get; set; }
        public virtual DbSet<links> links { get; set; }
        public virtual DbSet<systems> systems { get; set; }
        public virtual DbSet<tables> tables { get; set; }

        //Non-system users
        public virtual System.Linq.IQueryable<users> users_ { get { return users.Where(u => u.user_id != 0); } }

        //Objects only from this project
        public System.Linq.IQueryable<requirements> project_requirements(int project) { return requirements.AsNoTracking().Where(r => r.project_id == project); }
        public System.Linq.IQueryable<tests> project_tests(int project) { return tests.AsNoTracking().Where(r => r.project_id == project); }
        public System.Linq.IQueryable<test_cycles> project_testcycles(int project) { return test_cycles.AsNoTracking().Where(r => r.project_id == project); }
        public System.Linq.IQueryable<test_cycles_tests> project_testcycles_tests(int project) { return test_cycles_tests.AsNoTracking().Include(t => t.test).Where(r => r.test.project_id == project); }
        public System.Linq.IQueryable<modules> project_modules(int project) { return modules.AsNoTracking().Where(r => r.project_id == project); }
        public System.Linq.IQueryable<defects> project_defects(int project) { return defects.AsNoTracking().Where(r => r.project_id == project); }
        public IEnumerable<users> project_users(int project) { return users.Include(u => u.users_projects_roles).AsNoTracking().ToList().OrderBy(u => u.name).Where(u => u.user_id != 0 && u.users_projects_roles.Any(upr => upr.project_id == project)); }              

        //Get the uploaded files from the request, so that blank files are not skipped
        public async Task<List<attachment_container>> GetFormFilesAsync(HttpContext hc)
        {
            var request = hc.Request;
            var postedFiles = new List<attachment_container>();
            if (request.HasFormContentType)
            {
                var form = await request.ReadFormAsync();

                foreach (var file in form.Files)
                {
                    ContentDispositionHeaderValue parsedContentDisposition;
                    ContentDispositionHeaderValue.TryParse(file.ContentDisposition, out parsedContentDisposition);

                    // If there is an <input type="file" ... /> in the form and is left blank.
                    if (parsedContentDisposition == null ||
                        (file.Length == 0 &&
                         string.IsNullOrEmpty(RemoveQuotes(parsedContentDisposition.FileName))))
                    {
                        postedFiles.Add(null);
                    }
                    else
                    {
                        using (var ms = new MemoryStream())
                        {
                            using (var sf = file.OpenReadStream())
                            {
                                sf.CopyTo(ms);
                                var attachment = new attachment_container() { attachment_filename = System.IO.Path.GetFileName(ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"')), attachment_mimetype = file.ContentType, attachment_content = ms.ToArray() };
                                postedFiles.Add(attachment);
                            }
                        }

                    }
                }
            }

            return postedFiles;
        }

        public static string RemoveQuotes(string input)
        {
            if (!string.IsNullOrEmpty(input) && input.Length >= 2 && input[0] == '"' && input[input.Length - 1] == '"')
            {
                input = input.Substring(1, input.Length - 2);
            }
            return input;
        }
    }

    public static class Extension
    {      
        //Change CamelCase to spaces
        public static string ToEnumString(this Enum val)
        {
            return Regex.Replace(val.ToString(), "(\\B[A-Z])", " $1");
        }

        public static string ToEnumString(string val)
        {
            return Regex.Replace(val.ToString(), "(\\B[A-Z])", " $1");
        }
    }


    public class DefectContainer
    {
        public int defect_id { get; set; }
        public string name { get; set; }
        public string tags { get; set; }
        public string description { get; set; }
        public string status_comment { get; set; }
        public int found_by { get; set; }
        public users found_byNavigation { get; set; }
        public int? priority_id { get; set; }
        public int? severity_id { get; set; }
        public IEnumerable<defects_attachments> defects_attachments { get; set; }
        public DateTime found_date { get; set; }
        public int? assigned_to { get; set; }
        public users assigned_toNavigation { get; set; }
        public DateTime effective_date { get; set; }
        public defect_status defect_status { get; set; }
        public int defect_status_id { get; set; }
        public int test_id { get; set; }
        public int? module_id { get; set; }
        public modules module { get; set; }
        public priorities priority { get; set; }
        public severities severity { get; set; }
        public int? test_run_id { get; set; }
        public runs test_run { get; set; }
        public users last_modified_byNavigation { get; set; }
        public DateTime last_modified_date { get; set; }
        public DateTime? due_date { get; set; }
        public string external_ref { get; set; }
        public defects original { get; set; }
        public closure_reason closure_reason { get; set; }
        public int closure_reason_id { get; set; }
        public int defect_type_id { get; set; }
        public defect_types defect_type { get; set; }
        public int phase_id { get; set; }
        public phases phase { get; set; }

        public DefectContainer()
        {
        }

        public DefectContainer(defects d, testing2Context c, DateTime? dn = null)
        {
            var as_of_date = dn ?? DateTime.Now;

            var dhall = d.defect_history.Where(dhi => dhi.effective_date <= as_of_date).OrderByDescending(dhi => dhi.effective_date);

            var dh = dhall.First();

            /*
            List<defect_history> dhl;
            if (included = false)
            {
                dhl = c.defect_history.Include(dha => dha.defect_status).Include(dha => dha.priority).Include(dha => dha.severity).Include(dha => dha.assigned_toNavigation).Include(dha => dha.closure_reason).ToList();
            }
            else
            {

            }
          
            
            var dh = dhl.Single(dha => dha.defect_history_id == dhp.defect_history_id);*/
            
            defect_id = d.defect_id;
            name = d.name;
            tags = d.tags;
            description = d.description;
            found_by = d.found_by;
            found_byNavigation = d.found_byNavigation;
            priority_id = dh.priority_id;
            priority = dh.priority;
            severity_id = dh.severity_id;
            severity = dh.severity;
            defects_attachments = d.defects_attachments;
            closure_reason = dh.closure_reason;
            closure_reason_id = dh.closure_reason_id;
            found_date = d.found_date;
            found_byNavigation = d.found_byNavigation;
            assigned_to = dh.assigned_to;
            assigned_toNavigation = dh.assigned_toNavigation;
            effective_date = dh.effective_date;
            defect_status_id = dh.defect_status_id;
            defect_status = dh.defect_status;
            phase = d.phase;
            phase_id = d.phase_id;
            defect_type_id = d.defect_type_id;
            defect_type = d.defect_type;
            module_id = d.module_id;
            module = d.module;
            status_comment = dh.status_comment;
            test_run_id = d.test_run_id;
            test_run = d.test_run;
            last_modified_byNavigation = dh.last_modified_byNavigation;
            last_modified_date = dh.last_modified_date;
            external_ref = d.external_ref;
            original = d;
            due_date = d.due_date;
        }
    }
}

