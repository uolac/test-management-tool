using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApplication6;
using System.Collections.Generic;
using System;
using System.IO;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SqlClient;

using MoreLinq;


namespace WebApplication6.Controllers
{
    public class linksController : Controller
    {
        private testing2Context _context;

        public linksController(testing2Context context)
        {
            _context = context;    
        }

        [ActionName("Canvas")]
        public IActionResult Canvas(int param1, string param2)
        {
            return View(new CanvasInitialViewModel() { initial_id = param1, initial_type = param2 });
        }

        [HttpPost, ActionName("GetModel")]
        public CanvasViewModel GetModel([FromBody] CanvasRecieveParams input)
        {
            var viewmodel = new CanvasViewModel();


            var maintable = new tables();
            var link = new links();
            var field = new fields();
            var forwardtables2 = new List<tables>();
            var backtables2 = new List<tables>();
            var forwardlinks = new List<links>();
            var backlinks = new List<links>();
            var linkforwardtable = new tables();

            var forwardtables1test = new List<links>();
            var forwardlinkstest = new List<links>();

            var include_interfaces = new List<int>();
            var all_interfaces = new List<int>();
            var all_interfaces_names = new List<string>();

            var id_ = input.new_id;

            int tbl = 0;

            var intfnames = _context.interfaces.Select(intf => new { intf.interface_id, intf.interface_name }).ToList();

            viewmodel.links = new List<CVMLink>();

            if (input.new_type == "link")
            {



                maintable = _context.links.Include(l => l.source_field).ThenInclude(f => f.table).ThenInclude(t => t.fields).ThenInclude(f => f.entity).Single(l => l.link_id == id_).source_field.table;

                link = _context.links.Include(l => l.source_field).ThenInclude(f => f.table).Include(l => l.destination_field).ThenInclude(f => f.table).Include(l => l._interface).Single(m => m.link_id == id_);

                include_interfaces.Add(link.interface_id ?? 0);

                field = null;

                linkforwardtable = _context.links.Include(l => l.destination_field).ThenInclude(f => f.table).ThenInclude(t => t.fields).ThenInclude(t2 => t2.entity).Single(l => l.link_id == id_).destination_field.table;

                var forwardtablescontext = _context.links.Include(l => l.source_field).ThenInclude(f => f.table).ThenInclude(t => t.fields).ThenInclude(f => f.linksNavigation).ThenInclude(l2 => l2.destination_field).ThenInclude(f2 => f2.table).ThenInclude(t2 => t2.fields).ThenInclude(t2 => t2.entity).Single(l => l.link_id == id_);

                if (forwardtablescontext?.source_field != null)
                {


                    var forwardtables1 = forwardtablescontext.source_field.table.fields.ToList();

                    forwardtables1test = forwardtables1.SelectMany(f => f.linksNavigation).ToList();

                    forwardtables2 = forwardtables1.SelectMany(f => f.linksNavigation).Where(l => l.link_id != id_ && include_interfaces.Contains(l.interface_id ?? 0)).Select(l => l.destination_field.table).ToList();

                    forwardlinks = forwardtables1.SelectMany(f => f.linksNavigation).Where(l => l?.source_field != null && l?.destination_field != null).ToList();

                }


                var backtablescontext = _context.links.Include(l => l.source_field).ThenInclude(f => f.table).ThenInclude(t => t.fields).ThenInclude(f => f.links).ThenInclude(l2 => l2.source_field).ThenInclude(f2 => f2.table).ThenInclude(t2 => t2.fields).ThenInclude(t2 => t2.entity).Single(l => l.link_id == id_);


                if (backtablescontext?.source_field != null)
                {
                    var backtables1 = backtablescontext.source_field.table.fields.ToList();

                    backtables2 = backtables1.SelectMany(f => f.links).Where(l => l.link_id != id_ && include_interfaces.Contains(l.interface_id ?? 0)).Select(l => l.source_field.table).ToList();



                    backlinks = backtables1.SelectMany(f => f.links).Where(l => l?.source_field != null && l?.destination_field != null).ToList();
                }

                all_interfaces = forwardlinks.Select(f => f.interface_id ?? 0).Union(backlinks.Select(f => f.interface_id ?? 0)).OrderBy(intf => intf).ToList();

                all_interfaces.Add(link.interface_id ?? 0);

                all_interfaces_names = all_interfaces.Select(intf => intfnames.Single(names => names.interface_id == intf).interface_name).ToList();



            }
            else
            {
                include_interfaces = input.including_interface_ids;

                maintable = _context.fields.Include(f => f.table).ThenInclude(t => t.fields).Single(f => f.field_id == id_).table;

                link = null;

                field = _context.fields.Single(f => f.field_id == input.new_id);

                linkforwardtable = null;

                var forwardtablescontext = _context.fields

                    .Include(f3 => f3.table).ThenInclude(t4 => t4.fields).ThenInclude(f2x => f2x.linksNavigation).ThenInclude(l2 => l2.destination_field).ThenInclude(f2 => f2.table).ThenInclude(t2 => t2.fields).ThenInclude(f5 => f5.entity).Single(f => f.field_id == id_);

                var forwardtables1 = forwardtablescontext.table.fields.ToList();

                forwardtables2 = forwardtables1.SelectMany(f => f.linksNavigation).Where(l => l.is_future == !input.is_current && include_interfaces.Contains(l.interface_id ?? 0)).Select(l => l?.destination_field?.table).Where(t => t != null).ToList();

                var backtablescontext = _context.fields.Include(f => f.table).ThenInclude(t => t.fields).ThenInclude(f => f.links).ThenInclude(l2 => l2.source_field).ThenInclude(f2 => f2.table).ThenInclude(t2 => t2.fields).ThenInclude(t2 => t2.entity).Single(f => f.field_id == id_);

                var backtables1 = backtablescontext.table.fields.ToList();

                backtables2 = backtables1.SelectMany(f => f.links).Where(l => l.is_future == !input.is_current && include_interfaces.Contains(l.interface_id ?? 0)).Select(l => l?.source_field?.table).Where(t => t != null).ToList();

                forwardlinks = forwardtables1.SelectMany(f => f.linksNavigation).Where(l => l?.source_field != null && l?.destination_field != null && l.is_future == !input.is_current).ToList();

                forwardlinkstest = forwardtables1.SelectMany(f => f.linksNavigation).ToList();

                backlinks = backtables1.SelectMany(f => f.links).Where(l => l?.source_field != null && l?.destination_field != null && l.is_future == !input.is_current).ToList();


                all_interfaces = forwardlinks.Select(f => f.interface_id ?? 0).Union(backlinks.Select(f => f.interface_id ?? 0)).Distinct().OrderBy(intf => intf).ToList();

                all_interfaces_names = all_interfaces.Select(intf => intfnames.Single(names => names.interface_id == intf).interface_name).ToList();

            }

            var start_x = 1750;
            var start_y = 1500;

            var validfields1 = _context.links.Select(l => l.source_field_id).ToList();
            var validfields2 = _context.links.Select(l => l.destination_field_id).ToList();
            var validfields = validfields1.Union(validfields2);

            var test = maintable.fields.Where(f => validfields.Contains(f.field_id));

            viewmodel.maintable = new CVMTable() { table_id = maintable.table_id, x = input.original_type == "new" ? start_x : input.new_x, y = input.original_type == "new" ? start_y : input.new_y, table_name = maintable.table_name, fields = maintable.fields.Where(f => validfields.Contains(f.field_id)).Select(f => GetField(f)).OrderBy(f => f.column_order).ThenBy(f => f.field_name).ToList() };
            if (link != null) { viewmodel.link = new CVMLink() { link_id = link.link_id, x1 = 0, x2 = 0, y1 = 0, y2 = 0, interface_id = link?.interface_id ?? -1, interface_name = intfnames.Single(names => names.interface_id == link.interface_id).interface_name, source_field = link?.source_field?.table.table_name + "." + link?.source_field?.field_name + " " + link?.source_field?.field_type + " " + ((link?.source_field?.field_length ?? 0) == 0 ? "" : "(" + link?.source_field?.field_length + ")"), dest_field = link?.destination_field?.table.table_name + "." + link?.destination_field?.field_name + " " + link?.destination_field?.field_type + " " + ((link?.destination_field?.field_length ?? 0) == 0 ? "" : "(" + link?.destination_field?.field_length + ")"), source_field_id = link.source_field_id ?? -1, dest_field_id = link.destination_field_id ?? -1 }; }
            if (field != null) { viewmodel.field = new CVMField() { field_id = field.field_id, field_name = field.field_name }; }

            var ftables = forwardtables2
                .DistinctBy(t => t.table_id)
                .Select(t => new CVMTable() { table_id = t.table_id, x = input.original_type == "new" ? start_x + 250 : input.new_x + 250, y = 0, table_name = t.table_name, isforward = true, fields = t.fields.Where(f => validfields.Contains(f.field_id)).Select(f => GetField(f)).OrderBy(f => f.column_order).ThenBy(f => f.field_name).ToList() }).ToList();
            var btables = backtables2
                .DistinctBy(t => t.table_id)
                .Select(t => new CVMTable() { table_id = t.table_id, x = input.original_type == "new" ? start_x - 250 : input.new_x - 250, y = 0, table_name = t.table_name, isforward = false, fields = t.fields.Where(f => validfields.Contains(f.field_id)).Select(f => GetField(f)).OrderBy(f => f.column_order).ThenBy(f => f.field_name).ToList() }).ToList();

            if (linkforwardtable != null)
            {
                if (!ftables.Any(t => t.table_id == linkforwardtable.table_id))
                {
                    ftables.Add(new CVMTable() { table_id = linkforwardtable.table_id, x = input.original_type == "new" ? start_x + 250 : input.new_x + 250, y = 0, table_name = linkforwardtable.table_name, fields = linkforwardtable.fields.Where(f => validfields.Contains(f.field_id)).Select(f => GetField(f)).OrderBy(f => f.column_order).ThenBy(f => f.field_name).ToList() });
                }
            }

            int f_lowerhalfstart = 0;
            int f_upperhalfstart = 0;
            int b_lowerhalfstart = 0;
            int b_upperhalfstart = 0;

            int original_table_id = -1;
            var originalftable = new CVMTable() { y = viewmodel.maintable.y };
            var originalbtable = new CVMTable() { y = viewmodel.maintable.y };

            if (ftables.Any(t => t.table_id == input.original_id))
            {
                originalftable = ftables.Single(t => t.table_id == input.original_id);
                original_table_id = originalftable.table_id;
                originalftable.x = input.original_x;
                originalftable.y = input.original_y;
                f_lowerhalfstart = input.original_y + originalftable.fields.Count() * 16 + 40;
                f_upperhalfstart = input.original_y;
            }
            else
            {
                f_lowerhalfstart = viewmodel.maintable.y + viewmodel.maintable.fields.Count() * 8;
                f_upperhalfstart = viewmodel.maintable.y + viewmodel.maintable.fields.Count() * 8;
            }


            if (btables.Any(t => t.table_id == input.original_id))
            {
                originalbtable = btables.Single(t => t.table_id == input.original_id);
                original_table_id = originalbtable.table_id;
                originalbtable.x = input.original_x;
                originalbtable.y = input.original_y;
                b_lowerhalfstart = input.original_y + originalbtable.fields.Count() * 16 + 40;
                b_upperhalfstart = input.original_y;
            }
            else
            {
                b_lowerhalfstart = viewmodel.maintable.y + viewmodel.maintable.fields.Count() * 8;
                b_upperhalfstart = viewmodel.maintable.y + viewmodel.maintable.fields.Count() * 8;
            }

            int secondhalf = 0;
            int fldcount = (int)Math.Ceiling((float)ftables.Sum(t => t.fields.Count()) / 2);
            int flditerator = 0;

            for (int i = 0; i < ftables.Count; i++)
            {
                flditerator += ftables[i].fields.Count();
                if (flditerator > fldcount)
                {
                    secondhalf = i;
                }
            }

            if (viewmodel.maintable.y - originalftable.y < -200) { secondhalf = ftables.Count(); }
            if (viewmodel.maintable.y - originalftable.y > 200) { secondhalf = 0; }

            tbl = f_lowerhalfstart;
            for (int i = 0; i < secondhalf; i++)
            {
                var v = ftables[i];
                if(v.table_id != original_table_id)
                {
                    v.y = tbl;
                    tbl += v.fields.Count() * 16 + 40;
                }
            }


            tbl = f_upperhalfstart;
            for (int i = secondhalf; i < ftables.Count(); i++)
            {
                var v = ftables[i];
                if (v.table_id != original_table_id)
                {
                    tbl -= v.fields.Count() * 16 + 40;
                    v.y = tbl;
                }
                
            }

            secondhalf = 0;
            fldcount = (int)Math.Ceiling((float)btables.Sum(t => t.fields.Count()) / 2);
            flditerator = 0;

            for (int i = 0; i < btables.Count; i++)
            {
                flditerator += btables[i].fields.Count();
                if (flditerator > fldcount)
                {
                    secondhalf = i;
                }
            }

            if (viewmodel.maintable.y - originalbtable.y < -200) { secondhalf = ftables.Count(); }
            if (viewmodel.maintable.y - originalbtable.y > 200) { secondhalf = 0; }

            tbl = b_lowerhalfstart;
            for (int i = 0; i < secondhalf; i++)
            {
                var v = btables[i];
                if (v.table_id != original_table_id)
                {
                    v.y = tbl;
                    tbl += v.fields.Count() * 16 + 40;
                }
            }


            tbl = b_upperhalfstart;
            for (int i = secondhalf; i < btables.Count(); i++)
            {
                var v = btables[i];
                if (v.table_id != original_table_id)
                {             
                    tbl -= v.fields.Count() * 16 + 40;
                    v.y = tbl;
                }
            }

            if(forwardlinks.Any())
            {
                var flinks = forwardlinks
              .Where(l => include_interfaces.Contains(l.interface_id ?? 0))
              .Select(l => new CVMLink() { link_id = l.link_id, x1 = 0, x2 = 0, y1 = 0, y2 = 0, interface_id = l?._interface?.interface_id ?? -1, interface_name = intfnames.Single(names => names.interface_id == l.interface_id).interface_name, isforward = true,
                  source_field_table = (l?.source_field?.table?.table_name ?? ""),
                source_field = (l?.source_field?.field_name ?? ""),
                source_field_type = (l?.source_field?.field_type ?? "") + " " +
                ((l?.source_field?.field_length ?? 0) == 0 ? "" : "(" + l?.source_field?.field_length + ")"),
                  dest_field_table = (l?.destination_field?.table?.table_name ?? ""),
                  dest_field = (l?.destination_field?.field_name ?? ""),
                  dest_field_type  = (l?.destination_field?.field_type ?? "") + " " +
                 ((l?.destination_field?.field_length ?? 0) == 0 ? "" : "(" + l?.destination_field?.field_length + ")")
                  , source_field_id = l.source_field?.field_id ?? -1, dest_field_id = l.destination_field?.field_id ?? -1 })
              .ToList();

                for (int i = 0; i < flinks.Count(); i++)
                {
                    var l = flinks[i];
                    var dt = ftables.Single(t => t.fields.Select(f => f.field_id).Contains(l.dest_field_id));
                    var df = dt.fields.Select(f => f.field_id).ToList().IndexOf(l.dest_field_id);
                    var st = viewmodel.maintable;
                    var sf = st.fields.Select(f => f.field_id).ToList().IndexOf(l.source_field_id);

                    l.x1 = st.x + 125;
                    l.y1 = st.y + 16 + sf * 16;
                    l.x2 = dt.x;
                    l.y2 = dt.y + 16 + df * 16;
                }

                viewmodel.links.AddRange(flinks);
               
            }
            if (backlinks.Any())
            {
                var blinks = backlinks
               .Where(l => include_interfaces.Contains(l.interface_id ?? 0))
              .Select(l => new CVMLink()
              {
                  link_id = l.link_id,
                  x1 = 0,
                  x2 = 0,
                  y1 = 0,
                  y2 = 0,
                  interface_id = l?._interface?.interface_id ?? -1,
                  interface_name = intfnames.Single(names => names.interface_id == l.interface_id).interface_name,
                  isforward = true,
                  source_field_table = (l?.source_field?.table?.table_name ?? ""),
                  source_field = (l?.source_field?.field_name ?? ""),
                  source_field_type = (l?.source_field?.field_type ?? "") + " " +
                ((l?.source_field?.field_length ?? 0) == 0 ? "" : "(" + l?.source_field?.field_length + ")"),
                  dest_field_table = (l?.destination_field?.table?.table_name ?? ""),
                  dest_field = (l?.destination_field?.field_name ?? ""),
                  dest_field_type = (l?.destination_field?.field_type ?? "") + " " +
                 ((l?.destination_field?.field_length ?? 0) == 0 ? "" : "(" + l?.destination_field?.field_length + ")")
                  ,
                  source_field_id = l.source_field?.field_id ?? -1,
                  dest_field_id = l.destination_field?.field_id ?? -1
              })
               .ToList();


                for (int i = 0; i < blinks.Count(); i++)
                {
                    var l = blinks[i];
                    var st = btables.Single(t => t.fields.Select(f => f.field_id).Contains(l.source_field_id));
                    var sf = st.fields.Select(f => f.field_id).ToList().IndexOf(l.source_field_id);
                    var dt = viewmodel.maintable;
                    var df = dt.fields.Select(f => f.field_id).ToList().IndexOf(l.dest_field_id);

                    l.x1 = st.x + 125;
                    l.y1 = st.y + 16 + sf * 16;
                    l.x2 = dt.x;
                    l.y2 = dt.y + 16 + df * 16;
                }


                viewmodel.links.AddRange(blinks);
            }

 

            if(viewmodel.field == null)
            {
                viewmodel.num_field_selected = viewmodel.maintable.fields.IndexOf(viewmodel.maintable.fields.Single(f => f.field_id == link.source_field_id));
                if(viewmodel.num_field_selected == -1) { throw new Exception(); }
            }
            else
            {
                viewmodel.num_field_selected = viewmodel.maintable.fields.IndexOf(viewmodel.maintable.fields.Single(f => f.field_id == field.field_id));
                if (viewmodel.num_field_selected == -1) { throw new Exception(); }
            }

            
            viewmodel.tables = ftables;
            viewmodel.tables.AddRange(btables);
            viewmodel.included_interfaces = include_interfaces;
            viewmodel.all_interfaces = all_interfaces.Distinct().ToList();
            viewmodel.all_interfaces_names = all_interfaces_names.Distinct().ToList();

            //if(input.original_type=="filter")
            //{
            //    var a = 1;
            //}

            //Debug.WriteLine(input.original_type);


            return viewmodel;
        }

        public CVMField GetField(fields f)
        {
            var ff = new CVMField() { field_id = f.field_id, field_name = f.field_name, column_order = f.column_position.ToString() ?? "-", datatype = f.field_type + " " + (f.field_length > 0 ? "(" + f.field_length + ")" : ""), entity = f.entity?.entity_name + " " + (f.is_entity_master ? "(Source)" : ""), table = f.table?.table_name, system = f.table?.system?.system_name };
            return ff;
        }

        [HttpPost, ActionName("MinDist")]
        public MinDistSend MinDist([FromBody] MinDistRecieve input)
        {
            var id_ = -1;
            CVMLink li = new CVMLink();
            float md = 100000;


            //var select = input.links.Select(obj => MinDistSend.minimum_distance(
                   // new Vector2(obj.x1, obj.y1)
                   // , new Vector2(obj.y1, obj.y2), new Vector2(input.x, input.y)));

            //var min = input.links.Min(obj => MinDistSend.minimum_distance(
                   // new Vector2(obj.x1, obj.y1)
                  //  , new Vector2(obj.y1, obj.y2), new Vector2(input.x, input.y)));

            foreach (var obj in input.links)
            {  
                //Debug.WriteLine(obj.link_id);
                
                var md_ = MinDistSend.minimum_distance(
                    new Vector2(obj.x1, obj.y1)
                    , new Vector2(obj.x2,obj.y2), new Vector2(input.x,input.y));

                if (md_ < 40 && md_ < md)
                {
                    md = md_;
                    li = obj;
                }
            }

            if (md < 1000)
            {
                id_ = li.link_id;
            }

            return new MinDistSend() { id = id_,  x = input.x, y = input.y };
        }

        public class MinDistRecieve
        {
            public IEnumerable<CVMLink> links { get; set; }
            public float x { get; set; }
            public float y { get; set; }
        }

        public class MinDistSend
        {
            public int id;
            public float x, y;

            public static float minimum_distance(Vector2 v, Vector2 w, Vector2 p)
            {
                float l2 = (w - v).Length() * (w - v).Length();
                
                if (l2 == 0.0) return (v - p).Length();
                float t = Vector2.Dot(p - v, w - v) / l2;

               // Debug.WriteLine("v:" + v.x + "," + v.y);
                //Debug.WriteLine("w:" + w.x + "," + w.y);
               // Debug.WriteLine("p:" + p.x + "," + p.y);
               // Debug.WriteLine("t:" + t);

                if (t < 0.0) return (p - v).Length();
                else if (t > 1.0) return (p - w).Length();
                Vector2 projection = v + t * (w - v);

               // Debug.WriteLine("projection:" + projection.x + "," + projection.y);
               // Debug.WriteLine("plength:" + (p - projection).Length());

                return (p - projection).Length();
            }
        }

        public IActionResult Index(int? id)
        {
            var testing2Context = _context.links.Where(f => f.interface_id == id).Take(50).Include(l => l._interface).Include(l => l.destination_field).ThenInclude(t => t.table).Include(l => l.source_field).ThenInclude(t => t.table).OrderBy(l => l.batch).ThenBy(l => l.link_column_num);
            ViewData["source_id"] = id;
            return View(testing2Context.ToList());
        }

        [HttpPost]
        
        public IActionResult Index(string sourceid, string name)
        {
            var s_id = System.Convert.ToInt32(sourceid);
            var testing2Context = _context.links.Include(l => l._interface).Include(l => l.destination_field).Include(l => l.source_field).Where(f => (f.interface_id == s_id || s_id == -1) && (f.source_field.field_name + f.destination_field.field_name).Contains(name)).OrderBy(l => l.batch).ThenBy(l => l.link_column_num);
            ViewData["source_id"] = sourceid;
            return View(testing2Context.ToList());
        }


        // GET: links/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var links = _context.links.Include(l => l.destination_field).ThenInclude(f => f.table).Include(l => l.source_field).ThenInclude(f => f.table).Single(m => m.link_id == id);

            if (links == null)
            {
                return NotFound();
            }
            ViewData["source_id"] = id;
            return View(links);
        }

        // GET: links/Details/5

        
       

        // GET: links/Exception
        public IActionResult ExceptionReport()
        {
            IEnumerable<ExceptionReportViewModel> viewmodel = new List<ExceptionReportViewModel>();

            var linksmodel = _context.links
                .Include(l => l.source_field).ThenInclude(f => f.table)
                .Include(l => l.destination_field).ThenInclude(f => f.table)
                .Include(l => l._interface)
                .ToList();

            SqlConnection con = new SqlConnection();
            con.ConnectionString = "Server=ASQL02;Database=testmanagementtool;Integrated Security=true;";
            con.Open();
            var com = new SqlCommand();
            com.Connection = con;
            com.CommandText = @"
SELECT ilv.*, ISNULL(f.entity_id,-1) AS entity,ISNULL(f.is_entity_master,-1) AS is_entity_master FROM
(SELECT 
testmanagementtool.dbo.links.link_id
, COALESCE(l8.link_id,l7.link_id,l6.link_id,l5.link_id,l4.link_id,l3.link_id,l2.link_id,-1) AS source_link
, CASE 
WHEN l8.link_id IS NOT NULL THEN 7
WHEN l7.link_id IS NOT NULL THEN 6
WHEN l6.link_id IS NOT NULL THEN 5
WHEN l5.link_id IS NOT NULL THEN 4
WHEN l4.link_id IS NOT NULL THEN 3
WHEN l3.link_id IS NOT NULL THEN 2
WHEN l2.link_id IS NOT NULL THEN 1 
ELSE -1 END AS length
FROM links 
LEFT OUTER JOIN testmanagementtool.dbo.links l2 ON links.source_field_id = l2.destination_field_id
LEFT OUTER JOIN testmanagementtool.dbo.links l3 ON l2.source_field_id = l3.destination_field_id
LEFT OUTER JOIN testmanagementtool.dbo.links l4 ON l3.source_field_id = l4.destination_field_id
LEFT OUTER JOIN testmanagementtool.dbo.links l5 ON l4.source_field_id = l5.destination_field_id
LEFT OUTER JOIN testmanagementtool.dbo.links l6 ON l5.source_field_id = l6.destination_field_id
LEFT OUTER JOIN testmanagementtool.dbo.links l7 ON l6.source_field_id = l7.destination_field_id
LEFT OUTER JOIN testmanagementtool.dbo.links l8 ON l7.source_field_id = l8.destination_field_id
) ilv
LEFT OUTER JOIN testmanagementtool.dbo.links sl ON source_link = sl.link_id
LEFT OUTER JOIN testmanagementtool.dbo.fields f ON sl.source_field_id = f.field_id";

            SqlDataReader r = com.ExecuteReader();
            
            var dte = new DataTable();

            dte.Load(r);
            con.Close();
            var dt = dte.AsEnumerable();

            var linkspre = linksmodel.Select(l => new
            {
                l,
                a =
                (GetExceptionReport.DatatypeMatch(l) ? 0 : GetExceptionReport.Exceptions.Datatype) |
                (GetExceptionReport.LengthMatch(l) ? 0 : GetExceptionReport.Exceptions.Length) |
                (GetExceptionReport.TraceEntityMatch(l, dt) ? 0 : GetExceptionReport.Exceptions.TraceEntity) |
                (GetExceptionReport.TraceLength(l, dt) ? 0 : GetExceptionReport.Exceptions.TraceLength)
            }).ToList();


             var links = linkspre.Where(la => la.a > 0).OrderBy(la => la.l.interface_id);



            viewmodel = links.Select(la => new ExceptionReportViewModel()
            {

                link_id = la.l.link_id,
                interface_name = la.l._interface.interface_name,
                source_field = (la.l?.source_field?.table?.table_name ?? "") + "." +
                (la.l?.source_field?.field_name ?? "") + " " +
                (la.l?.source_field?.field_type ?? "") + " " +
                ((la.l?.source_field?.field_length ?? 0) == 0 ? "" : "("+la.l?.source_field?.field_length+")") ,
                dest_field = (la.l?.destination_field?.table?.table_name ?? "") + "." +
                (la.l?.destination_field?.field_name ?? "") + " " +
                (la.l?.destination_field?.field_type ?? "") + " " +
                ((la.l?.destination_field?.field_length ?? 0) == 0 ? "" : "(" + la.l?.destination_field?.field_length + ")"),
                is_future = la.l.is_future,
                exception = ((la.a.HasFlag(GetExceptionReport.Exceptions.Datatype) ? "Datatype " : "") +
 (la.a.HasFlag(GetExceptionReport.Exceptions.Length) ? "Length " : "") +
  (la.a.HasFlag(GetExceptionReport.Exceptions.TraceEntity) ? "TraceEntity " : "") +
   (la.a.HasFlag(GetExceptionReport.Exceptions.TraceLength) ? "TraceLength " : ""))
            }
            )/*.Take(20)*/;


            return View(viewmodel);
        }

        // GET: links/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            links links = _context.links.Single(m => m.link_id == id);
            if (links == null)
            {
                return NotFound();
            }
            ViewData["interface_id"] = links.interface_id;
            ViewData["destination_field_id"] = links.destination_field_id;
            ViewData["source_field_id"] = links.source_field_id;
            return View(links);
        }

        // POST: links/Edit/5
        [HttpPost]
        
        public IActionResult Edit(links links)
        {
            if (ModelState.IsValid)
            {
                _context.Update(links);
                _context.SaveChanges();
                return RedirectToAction("Index", new { id = links.interface_id });
            }
            ViewData["interface_id"] = links.interface_id;
            ViewData["destination_field_id"] = links.destination_field_id;
            ViewData["source_field_id"] = links.source_field_id;
            return View(links);
        }

        // GET: links/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            links links = _context.links.Single(m => m.link_id == id);
            if (links == null)
            {
                return NotFound();
            }

            return View(links);
        }

        // POST: links/Delete/5
        [HttpPost, ActionName("Delete")]
        
        public IActionResult DeleteConfirmed(int id)
        {
            links links = _context.links.Single(m => m.link_id == id);
            var return_ = links.interface_id;
            _context.links.Remove(links);
            _context.SaveChanges();
            return RedirectToAction("Index", new { id = return_ });
        }
    }

    public static class GetExceptionReport
    {
        public enum Exceptions { Datatype = 1 , Length = 2, TraceLength = 4, TraceEntity = 8}

        public static bool DatatypeMatch(links lnk)
        {
            var dest_datatype = lnk?.destination_field?.field_type;
            var src_datatype = lnk?.source_field?.field_type;

            if (dest_datatype == null || src_datatype == null)
            {
                return false;
            }

            bool matchconvert = false;
            Regex match = new Regex("(CONVERT\\(" + dest_datatype.Replace("varchar", "char") + ".*,[0-9]*\\))", RegexOptions.IgnoreCase);

            var m_ = (from Match m in match.Matches(lnk.transformation ?? "") select m.Value);

            if (m_.Count() >= 1)
            {
                matchconvert = true;
            }

            return src_datatype.Replace("varchar", "char") == dest_datatype.Replace("varchar", "char") || src_datatype == "Unknown" || dest_datatype == "Unknown" || matchconvert;
        }

        public static bool LengthMatch(links lnk)
        {
            Regex match1 = new Regex("(LEFT\\([0-9]*\\))|(RIGHT\\([0-9]*\\))");
            Regex match2 = new Regex("(SUBSTRING\\(.*,[0-9]*\\))");
            var transformok1 = (from Match m in match1.Matches(lnk.transformation ?? "") select m.Value).Where(m => Int32.Parse(m.Substring(m.IndexOf('(') + 1, m.IndexOf(')') - m.IndexOf('(') - 1)) <= lnk?.destination_field?.field_length).Any();
            var transformok2 = (from Match m in match2.Matches(lnk.transformation ?? "") select m.Value).Where(m => Int32.Parse(GetNum(m)) <= lnk?.destination_field?.field_length).Any();
            return (lnk?.destination_field?.field_length >= lnk?.source_field?.field_length || transformok1 || transformok2 || lnk?.destination_field?.field_length == 0 || lnk?.source_field?.field_length == 0);
        }

        public static int SecondIndex(string s, char c)
        {
            var substr = s.Substring(s.IndexOf(c));
            return substr.IndexOf(c) + s.IndexOf(c) + 2;
        }

        public static string GetNum(string m)
        {
            var rt = m.Substring(SecondIndex(m, ',') + 1, m.IndexOf(')') - SecondIndex(m, ',') - 1);
            return rt;
        }


        public static bool TraceLength(links lnk, IEnumerable<DataRow> dt)
        {
            var matched = dt.Where(r => (int)r["link_id"] == lnk.link_id);

            return matched.Max(m => (int)m["length"]) > 3;
        }

        public static bool TraceEntityMatch(links lnk, IEnumerable<DataRow> dt)
        {
            var matched = dt.Where(r => (int)r["link_id"] == lnk.link_id);

            var rt = lnk?.source_field?.entity_id == null ||
                matched.Any(m => (int)m["entity"] == lnk?.source_field?.entity_id && (bool)m["is_entity_master"]);

            return rt;
        }

    }

    public struct Vector2
    {
        public float x, y;

        public static Vector2 Zero { get { return new Vector2(0, 0); } }

        public Vector2(float x, float y)
        {
            this.x = x;
            this.y = y;
        }

        public static Vector2 operator +(Vector2 v1, Vector2 v2)
        {
            return new Vector2(v1.x + v2.x, v1.y + v2.y);
        }

        public static Vector2 operator -(Vector2 v1, Vector2 v2)
        {
            return new Vector2(v1.x - v2.x, v1.y - v2.y);
        }

        public static Vector2 operator *(float num, Vector2 v1)
        {
            return new Vector2(v1.x * num, v1.y * num);
        }

        public static Vector2 operator *(Vector2 v1, float num)
        {
            return new Vector2(v1.x * num, v1.y * num);
        }

        public static Vector2 operator /(Vector2 v1, float num)
        {
            return new Vector2(v1.x / num, v1.y / num);
        }

        public float Distance(Vector2 v2)
        {
            return (v2 - this).Length();
        }

        public float Length()
        {
                return (float)Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2));
        }

        public Vector2 Normalise()
        {
            if (Math.Abs(x) < 0.1 && Math.Abs(y) < 0.1) { return Zero; }
            else if (Math.Abs(x) < 0.1) { return new Vector2(0, y); }
            else if (Math.Abs(y) < 0.1) { return new Vector2(x, 0); }
            else return this / this.Length();
        }

        public static float Dot(Vector2 v1, Vector2 v2)
        {
            return v1.x * v2.x + v1.y * v2.y;
        }

    }


}
