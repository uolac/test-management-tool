﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using System.IO;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Http;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.Data;



using System.IO.Compression;
using System.Xml.Serialization;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;



namespace WebApplication6.Controllers
{
    public class requirementsController : Controller
    {
        //Database context
        private testing2Context _context;
        //Object that stores authenticated role information
        private RoleInfo role;
        //Service that authenticates the user
        private IRoleService _rs;
        //Database access layer for auditing
        private IDatabase database;
        //HTML encoding service
        private IHtmlEncoderService encoder;

        public requirementsController(testing2Context context, IRoleService rs, IDatabase database, IHtmlEncoderService encoder)
        {
            //Controller constructor: set fields for dependency injected services so that controller actions can access them
            _context = context;
            _rs = rs;
            role = rs.GetRole();
            this.database = database;
            this.encoder = encoder;
        }

        public IActionResult Index(int project = 0, string name = "", int module_id = 0, int owner_id = 0, int draft_status_id = 99, RequirementState derived_requirement_state = RequirementState.None, int page = 1, string button_name = "")
        {
            //Index page

            //Controller action parameters are project (filters every page by default), page number if it's on the page, and other filters.

            //HTML Encode string parameters
            name = encoder.Encode(name ?? "");
            button_name = encoder.Encode(button_name ?? "");

            //Set project to default if the user hasn't selected one
            project = role.project;
            var prj = _context.projects.SingleOrDefault(p => p.project_id == project);

            //If user does not have a role record against this project, deny access
            if (!role.IsAuthenticated() || prj == null) { return PageBuilder.Error403(project, role); }

            var test_cycle_id = prj.current_test_cycle_id ?? 0;        

            //Perform a database query on this type of object, including any linked objects with relevant information in the query, filtered by project and any filters from the page, ordered by last modified date       
            var testingContextpre = _context.requirements.Include(r => r.draft_status)
                .Include(r => r.module)
                .Include(r => r.owner)
                .Include(r => r.project)
                .Include(r => r.draft_status)
                .Include(r => r.last_modified_byNavigation)
                .Include(r => r.requirements_tests).ThenInclude(rt => rt.test).ThenInclude(t => t.runs).ThenInclude(r => r.outcome)
                .Where(r => (r.project_id == project)
                && (r.name.ToLower().Contains(name.ToLower()) || r.tags.ToLower().Contains(name.ToLower()) || name == "")
                && (r.owner_id == owner_id || owner_id == 0 || (owner_id == -99 && r.owner_id == null))
                && (r.module_id == module_id || module_id == 0 || (module_id == -99 && r.module_id == null))
                && (r.draft_status_id == draft_status_id || (draft_status_id == 99 && (r.draft_status_id == 1 || r.draft_status_id == 2)) || (draft_status_id == 0))
                ).ToList();

            var testingContext = testingContextpre
                .Where(r => (r.DerivedState(DateTime.Now, test_cycle_id) == derived_requirement_state || derived_requirement_state == RequirementState.None)
                ).OrderBy(r => r.name, new NaturalSortComparer<string>()).ToList();


            //If generate report button was clicked
            if (button_name == "Generate Report")
            {
                //Generate an excel report based on the filters
                return PageBuilder.GenerateReport(project, test_cycle_id, testingContext.ToList()
                    , new Column("requirement_id", 1, true)
                    , new Column("name", 3)
                    , new Column("derived_requirement_state", 1, null, "state")
                 , new Column("module.name", 2, "(None)")
                 , new Column("description", 2)
                 , new Column("draft_status.name", 1)
                 , new Column("owner.name", 2, "(None)")
                 , new Column("cross_reference", 2)
                 , new Column("tags", 2)
                 , new Column("compulsory_yn", 1)
                 , new Column("accepted_will_not_be_met", 1)
                 , new Column("last_modified_byNavigation.name", 2, "", "last modified by")
                 , new Column("last_modified_date", 2));
            }
            return View("requirements_index", new IndexViewModel<requirements>(role, project, test_cycle_id, Request.QueryString.HasValue, testingContext, page
                , new Filters(
                       new Dropdown("module_id", _context.project_modules(project), module_id, true)
                    , new Dropdown("owner_id", _context.project_users(project), "user_id", owner_id)
                    , new Dropdown("draft_status_id", _context.draft_status, draft_status_id, true)
                    , new Dropdown("derived_requirement_state", null, derived_requirement_state, true))
                , new Column("name", 2)
                 , new Column("derived_requirement_state", 2, null, "state")
                 , new Column("module.name", 2, "(None)")
                 , new Column("draft_status.name", 1)
                 , new Column("compulsory_yn", 2, "", "compulsory")
                 , new Column("owner.name", 3, "(None)")));
        }       

     
        public IActionResult Create(int project = 0)
        {
            //Create Requirement page
            if (!role.IsManager()) { return PageBuilder.Error403(project); }

            //Create a new requirements and send it to the page
            return View("requirements_create", new ViewModel<requirements>(project,new requirements() { project_id = project }
            , new Dropdown("module_id", _context.project_modules(project))
            , new Dropdown("draft_status_id", _context.draft_status, true, "")
            , new Dropdown("owner_id", _context.project_users(project), "user_id")));
        }



        [HttpPost]
        public IActionResult Create(int project, requirements requirements)
        {
            //Action to put the created requirement in the database

            encoder.Encode(requirements);

            var r_project = requirements?.project_id ?? 0;

            //If the user is not authorised, or the incorrect arguments were passed to the page, deny access
            ;
            if (requirements == null || !role.IsManager() || project != r_project || requirements?.project_id == 0) { return PageBuilder.Error403(project); }

            //Represents null value for module and owner
            if (requirements.module_id == -99) { requirements.module_id = null; }
            if (requirements.owner_id == -99) { requirements.owner_id = null; }

            if (ModelState.IsValid)
            {
                //Set internally maintained default values for the object
                requirements.effective_date = DateTime.Now;

                if(requirements.draft_status_id == 2) { requirements.date_made_live = DateTime.Now; }
                requirements.last_modified_by = role.user_id;
                requirements.last_modified_date = DateTime.Now;
                ;
                //Save the object to the database
                database.Add(requirements);

                //Set the permanent id of the object to the newly 
                requirements.lineage_id = requirements.requirement_id;

                database.Update(requirements);

                return RedirectToAction("Index", new { project = project });
            }

            return PageBuilder.ModelStateError(project, role, "Requirements create", ModelState);
        }



        public IActionResult Edit(int project = 0, int id = 0, bool isdelete = false)
        {
            //Edit requirement page
            requirements requirements = _context.requirements.Include(r => r.draft_status)
                .Include(r => r.module)
                .Include(r => r.owner)
                .Include(r => r.project)
                .Include(r => r.draft_status)
                .Include(r => r.last_modified_byNavigation).SingleOrDefault(m => m.project_id == project && m.requirement_id == id);

            //If the user is not authorised, or the incorrect arguments were passed to the page, deny access
            if (requirements == null || !role.IsManager())
            {
                return PageBuilder.Error403(project);
            }

            return View("requirements_edit", new ViewModel<requirements>(project, requirements
            , new Dropdown("module_id", _context.project_modules(project))
            , new Dropdown("draft_status_id", _context.draft_status, true, "")
            , new Dropdown("owner_id", _context.project_users(project), "user_id")));
        }

        [HttpPost]
        public IActionResult Edit(int project = 0, requirements requirements = null)
        {
            //Action to update the edited requirement in the database
            encoder.Encode(requirements);

            //If the user is not authorised, or the incorrect arguments were passed to the page, deny access
            if (requirements == null || !role.IsManager() || project != requirements?.project_id || requirements?.project_id == 0)
            {
                return PageBuilder.Error403(project);
            }

            //Represents null value for module and owner
            if (requirements.module_id == -99) { requirements.module_id = null; }
            if (requirements.owner_id == -99) { requirements.owner_id = null; }
            
            if (ModelState.IsValid)
            {
                var existing = _context.requirements.AsNoTracking().Single(r => r.requirement_id == requirements.requirement_id);

                requirements.last_modified_by = role.user_id;
                requirements.last_modified_date = DateTime.Now;
                if (requirements.draft_status_id == 2 && existing.draft_status_id != 2) { requirements.date_made_live = DateTime.Now; }

                database.Update(requirements);
                return RedirectToAction("Index", new { project = project });

            }
            return PageBuilder.ModelStateError(project, role, "Requirements edit", ModelState);
        }

        public IActionResult Archive(int project = 0, int id = 0)
        {
            //Edit requirement page
            requirements requirements = _context.requirements.Include(r => r.draft_status)
                .Include(r => r.module)
                .Include(r => r.owner)
                .Include(r => r.project)
                .Include(r => r.draft_status)
                .Include(r => r.last_modified_byNavigation).SingleOrDefault(m => m.project_id == project && m.requirement_id == id);

            //If the user is not authorised, or the incorrect arguments were passed to the page, deny access
            if (requirements == null || !role.IsManager())
            {
                return PageBuilder.Error403(project);
            }
            ;
            return View("requirements_archive", new ViewModel<requirements>(project, requirements));
        }      

        public IActionResult DeleteConfirmed(int project = 0, int id = 0)
        {
            //Action to delete/archive the requirement
            var requirements = _context.requirements.Include(r => r.requirements_tests).SingleOrDefault(m => m.requirement_id == id);

            if (requirements == null || !role.IsManager() || requirements?.project_id != project)
            {
                return PageBuilder.Error403(project);
            }

            database.RemoveRange(requirements.requirements_tests);
            requirements.draft_status_id = 3;
            database.Update(requirements);

            return RedirectToAction("Index", new { project = project });
        }

        public IActionResult UploadBulkImport(int project)
        {
            return View("requirements_bulkimport", project);
        }

        [HttpPost]
        public IActionResult UploadBulkImport(int project, IFormFile fileuploaded)
        {
            //Processes uploaded bulk import spreadsheet

            if (fileuploaded == null) { return PageBuilder.Back(role, "File is required. Click Choose File on the previous page.", "", "requirements", "UploadBulkImport", project, true); }

            if (!role.IsManager() || project == 0) { return PageBuilder.Error403(project); }

            //If the file uploaded is an Excel spreadsheet
            if (fileuploaded.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            {

                var rs = fileuploaded.OpenReadStream();

                using (var ms = new MemoryStream())
                {
                    rs.CopyTo(ms);

                    //Use DocumentFormat.OpenXml library to extract the data from the uploaded spreadsheet

                    SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(rs, false);

                    var sheet = spreadsheetDocument.WorkbookPart.GetPartsOfType<WorksheetPart>().First();

                    var sheetdata = sheet.Worksheet.GetFirstChild<SheetData>();

                    SharedStringTable sst_ = spreadsheetDocument.WorkbookPart.GetPartsOfType<SharedStringTablePart>().First().SharedStringTable;

                    var sst = sst_.ChildElements.Select(u => u.InnerText);

                    var rows = sheetdata.Descendants<Row>();

                    var newrequirements = new List<requirements>();
                    string error_strings = "";
                    string error_string = "";
                    var project_id = project;

                    //For each row in the spreadsheet
                    foreach (var r in rows)
                    {
                        //Put the data in the correct form - an array of strings

                        var allcells2 = r.Elements<Cell>();
                        
                        var allcells = allcells2.Select(c => new ExcelCell(c, sst)).Where(c => c.column < 999);

                        var rownum = allcells.FirstOrDefault()?.row;
                         
                        //If row is not blank
                        if (allcells.Any() && rownum >= 5)
                        {                                                  

                            error_string = "";
                            var requirement = new requirements();

                            var cells = new string[7];

                            for (int k = 0; k < cells.Length; k++)
                            {
                                cells[k] = "";
                            }

                            foreach (var c in allcells)
                            {
                                cells[Math.Min(c.column - 1,cells.Length-1)] = c.value;
                            }

                            encoder.Encode(cells);

                            //With a new requirement object, populate its fields with the validated input from the sheet

                            requirement.project_id = project;
                            requirement.module_id = Validation.ValidateIDOrNull("module_id", cells[0], _context.project_modules(project), ref error_string);
                            requirement.owner_id = Validation.ValidateIDOrNull("user_id", cells[1], _context.project_users(project), ref error_string, "owner_id");
                            requirement.draft_status_id = Validation.ValidateID("draft_status_id", cells[2], _context.draft_status.Where(ds => ds.draft_status_id == 1 || ds.draft_status_id == 2), ref error_string);
                            requirement.name = Validation.ValidateLength("name", cells[3], 4, 255, ref error_string);
                            requirement.description = cells[4];
                            requirement.cross_reference = cells[5];
                            requirement.tags = cells[6];
                            requirement.effective_date = DateTime.Now;
                            requirement.last_modified_by = role.user_id;
                            requirement.last_modified_date = DateTime.Now;
                            if(requirement.draft_status_id == 2) { requirement.date_made_live = DateTime.Now; }

                            //If there's an error, add the row number
                            if (error_string != "")
                            {
                                error_string = "Row " + rownum + ": " + error_string + "<br>";
                                error_strings += error_string;
                            }
                            else { newrequirements.Add(requirement); }
                            
                        }
                        

                    }

                    //If there's no errors
                    if (ModelState.IsValid && error_strings == "")
                    {
                        try
                        {
                            foreach (var r in newrequirements)
                            {
                                //Add the new requirement to the database
                                database.Add(r);
                            }

                            //Return the success page
                            return PageBuilder.Back(role, "Bulk Import Requirements", newrequirements.Count() + @" records successfully created.","requirements","Index",project);
                        }
                        catch (Exception e)
                        {
                            //Return the exception page
                            Email.SendError("requirements bulk import", e);
                            return PageBuilder.Back(role, "Bulk Import Requirements", e.ToString(), "requirements", "Index", project);
                        }
                    }
                    else
                    {
                        //Return the page that shows the errors
                        return PageBuilder.Back(role, "Bulk Import Requirements", "File contained the following errors and was not loaded: <br><br>" + error_strings, "requirements", "Index", project);
                    }
                }


            }
            //Return the exception page
            return PageBuilder.Back(role,"Invalid file","","requirements","Index",project,true);
        }

        public IActionResult GenerateBulkImport(int project)
        {
            if (!role.IsManager()) { return PageBuilder.Error403(project); }

            //Create a new file response that's a copy of requirements.xlsx under wwwroot/template, and send it to the user to download

            HttpContext.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            try
            { 
            FileContentResult result = new FileContentResult(System.IO.File.ReadAllBytes("wwwroot/templates/requirements_template.xlsx"), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            {
                FileDownloadName = "requirements_template.xlsx"
            };
            return result;
            }
            catch
            {
                return PageBuilder.Back(role, "I/O error", "", "requirements", "Index", project, true);
            }
            
        }


    }

   
}
