﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;


namespace WebApplication6.Controllers
{

    public class dashboardController : Controller
    {
        private testing2Context _context;
        private RoleInfo role;
        private IHtmlEncoderService encoder;

        public dashboardController(testing2Context context, IRoleService rs, IHtmlEncoderService encoder)
        {
            _context = context;
            role = rs.GetRole(rs.GetDefaultProject());
            this.encoder = encoder;
        }

        public IActionResult Index(string name = "", int project = 0, int draft_status_id = 99, int priority_id = 0, int severity_id = 0
            , int test_cycle_id = 0, int module_id = 0, int owner_id = 0, DateTime? as_of_date_n = null, int defect_status_id = 0, int page = 1)
        {
            name = encoder.Encode(name ?? "");

            //Set project to default if the user hasn't selected one
            project = role.project;

            var prj = _context.projects.SingleOrDefault(p => p.project_id == project);

            //If user does not have a role record against this project, deny access
            if (!role.IsAuthenticated() || prj == null) { return PageBuilder.Error403(project); }

            if (test_cycle_id == 0) { test_cycle_id = prj.current_test_cycle_id ?? 0; }

            var psd = prj.start_date;
            var project_start_date = new DateTime(psd.Year, psd.Month, 1);
            DateTime as_of_date = as_of_date_n ?? DateTime.Now;

            as_of_date = as_of_date.Date.AddHours(23).AddMinutes(59);

            //Create initial models for requirements, tests and defects based on the filter criteria

            var requirementsmodel = _context.project_requirements(project)
                .Include(r => r.module)
                .Include(t => t.owner)
                .Include(r => r.requirements_tests).ThenInclude(rt => rt.test).ThenInclude(t => t.runs).ThenInclude(r => r.outcome)
                .Include(r => r.requirements_tests).ThenInclude(rt => rt.test).ThenInclude(t => t.test_cycles_tests)
                .Where(r =>
                (r.draft_status_id == draft_status_id || (draft_status_id == 99 && (r.draft_status_id == 1 || r.draft_status_id == 2)) || draft_status_id == 0)
                && (r.draft_status_id != 3)
                && (r.module_id == module_id || module_id == 0)
                && (r.owner_id == owner_id || owner_id == 0)).ToList()
                .Where(r => r.effective_date <= as_of_date)
                .ToList();

            var testsmodel = _context.project_tests(project)
              .Include(t => t.module)
              .Include(t => t.priority)
              .Include(t => t.owner)
              .Include(t => t.runs).ThenInclude(r => r.outcome)
              .Include(t => t.test_cycles_tests)
              .Where(t =>
              (t.draft_status_id == draft_status_id || (draft_status_id == 99 && (t.draft_status_id == 1 || t.draft_status_id == 2)) || draft_status_id == 0)
               && (t.draft_status_id != 3)
              && (t.module_id == module_id || module_id == 0)
              && (t.owner_id == owner_id || owner_id == 0)
              && (t.priority_id == priority_id || priority_id == 0)).ToList()
                .Where(t => t.effective_date <= as_of_date)
                .ToList();

            var defectsmodelpre = _context.project_defects(project)
                .Include(d => d.module)
                .Include(d => d.found_byNavigation)
                .Include(r => r.last_modified_byNavigation)
                .Include(d => d.defects_attachments)
                .Include(d => d.defect_history).ThenInclude(dha => dha.defect_status)
                .Include(d => d.defect_history).ThenInclude(dha => dha.priority)
                .Include(d => d.defect_history).ThenInclude(dha => dha.severity)
                .Include(d => d.defect_history).ThenInclude(dha => dha.assigned_toNavigation)
                .Include(d => d.defect_history).ThenInclude(dha => dha.closure_reason)
                .Include(d => d.defect_type)
                .Include(d => d.phase).ToList();

            var defectsmodel = defectsmodelpre.Where(d => d.defect_history.Any(dhi => dhi.effective_date <= as_of_date)).Select(d => new DefectContainer(d, _context, as_of_date))
            .Where(t =>
            (t.module_id == module_id || module_id == 0)
            && (t.assigned_to == owner_id || owner_id == 0)
            && (t.priority_id == priority_id || priority_id == 0)
            && (t.severity_id == severity_id || severity_id == 0)
            && (t.defect_status_id == defect_status_id || defect_status_id == 0)).ToList()
                .Where(t => t.effective_date <= as_of_date)
                .ToList();

            //Create the list of dates at a 1 month interval based on the project dates for line chart series

            var dates = new List<DateTime>();

            if (project_start_date < as_of_date)
            {

                for (DateTime i = project_start_date; i < as_of_date; i = i.AddMonths(1))
                {
                    dates.Add(i);
                }

                if (as_of_date.Day != 1)
                {
                    dates.Add(as_of_date);
                }
            }

            //For each line chart, create the series data from the models

            var requirementsbydatemodel = new LineChartData(
                dates.Select(d => d.ToString("dd/MM/yyyy"))
                        , new LineChartSeries("Planned", dates.Select(d => requirementsmodel.Where(r => r.DerivedState(d, test_cycle_id) != RequirementState.DoesNotYetExist).Count()), (int)DashColor.Red)
                        , new LineChartSeries("Designed with Tests", dates.Select(d => requirementsmodel.Where(r => r.DerivedState(d, test_cycle_id).IsIn(RequirementState.NotStarted, RequirementState.InProgress, RequirementState.AllPassed)).Count()), (int)DashColor.Orange)
                        , new LineChartSeries("Completed", dates.Select(d => requirementsmodel.Where(r => r.DerivedState(d, test_cycle_id) == RequirementState.AllPassed).Count()), (int)DashColor.Green)


                );


            var testsbydatemodel = new LineChartData(
             dates.Select(d => d.ToString("dd/MM/yyyy"))
              , new LineChartSeries("Planned", dates.Select(d => testsmodel.Where(t => t.DerivedState(d, test_cycle_id) != TestState.DoesNotYetExist).Count()), (int)DashColor.Red)
              , new LineChartSeries("Designed", dates.Select(d => testsmodel.Where(t => t.DerivedState(d, test_cycle_id).IsIn(TestState.NotStarted, TestState.Failed, TestState.Blocked, TestState.Passed)).Count()), (int)DashColor.Orange)
              , new LineChartSeries("Run at least once", dates.Select(d => testsmodel.Where(t => t.date_made_live <= d && t.DerivedState(d, test_cycle_id).IsIn(TestState.Failed, TestState.Blocked, TestState.Passed)).Count()), (int)DashColor.Yellow)
              , new LineChartSeries("Completed", dates.Select(d => testsmodel.Where(t => t.date_made_live <= d && t.DerivedState(d, test_cycle_id) == TestState.Passed).Count()), (int)DashColor.Green)
             );

            var testsbydatemodel2 = new LineChartData(
            dates.Select(d => d.ToString("dd/MM/yyyy"))
             , new LineChartSeries("Passed", dates.Select(d => testsmodel.Where(t => t.DerivedState(d, test_cycle_id) == TestState.Passed).Count()), (int)TestState.Passed.GetColor())
             , new LineChartSeries("Failed", dates.Select(d => testsmodel.Where(t => t.DerivedState(d, test_cycle_id) == TestState.Failed).Count()), (int)TestState.Failed.GetColor())
             , new LineChartSeries("Blocked", dates.Select(d => testsmodel.Where(t => t.DerivedState(d, test_cycle_id) == TestState.Blocked).Count()), (int)TestState.Blocked.GetColor())
             , new LineChartSeries("Not Started", dates.Select(d => testsmodel.Where(t => t.DerivedState(d, test_cycle_id) == TestState.NotStarted).Count()), (int)TestState.NotStarted.GetColor())
            );

            var defectsbydatemodel1 = new LineChartData(
            dates.Select(d => d.ToString("dd/MM/yyyy"))
             , new LineChartSeries("Defects reported", dates.Select(d => defectsmodel.Where(df => df.found_date <= d).Count()), 0)
             , new LineChartSeries("Defects closed", dates.Select(d => defectsmodel.Where(df => df.found_date <= d && df.defect_status_id == 4).Count()), 1)
            );

            var defectsbydatemodel2 = new LineChartData(
           dates.Select(d => d.ToString("dd/MM/yyyy"))
            , new LineChartSeries("Resolved", dates.Select(d => defectsmodel.Where(df => df.found_date <= d && df.defect_status_id == 3).Count()), 2)
            , new LineChartSeries("On Hold", dates.Select(d => defectsmodel.Where(df => df.found_date <= d && df.defect_status_id == 5).Count()), 3)
            , new LineChartSeries("Assigned", dates.Select(d => defectsmodel.Where(df => df.found_date <= d && df.defect_status_id == 2).Count()), 1)
            , new LineChartSeries("New", dates.Select(d => defectsmodel.Where(df => df.found_date <= d && df.defect_status_id == 1).Count()), 0)


           );

            var open_defects = defectsmodel.Where(df => df.found_date <= as_of_date && df.defect_status_id != 4);
            var closed_defects = defectsmodel.Where(df => df.found_date <= as_of_date && df.defect_status_id == 4);

            var defectsbypersonpre = open_defects.GroupBy(d => d.assigned_toNavigation).Select(g => g.GroupBy(g2 => g2.defect_status).Select(g2 => new { user = g.Key, status = g2.Key, count = g2.Count(), countall = g.Count() })).SelectMany(a => a).OrderByDescending(d => d.countall).ThenBy(d => (d.status.defect_status_id == 3 ? 6 : d.status.defect_status_id));

            //).OrderBy(g2 => g2.Key.defect_status_id)

            var people = defectsbypersonpre.Select(d => d.user).Distinct();

            var status = defectsbypersonpre.Select(d => d.status).Distinct();

            var defectsbypersonmodel = new BarChartData(people.Select(p => p?.name ?? "Unassigned").ToList()
                , status.Select(s =>
                Tuple.Create(
                    s.name
                    , people.Select(p => defectsbypersonpre.SingleOrDefault(d => d.status == s && d.user == p)?.count.ToString() ?? "0").ToList()
                    )
                   ).ToList()
                  );
                  

            var livetests = testsmodel.Where(t => !t.DerivedState(as_of_date, test_cycle_id).IsIn(TestState.Draft, TestState.DoesNotYetExist, TestState.None, TestState.Archived));
            var liverequirements = requirementsmodel.Where(r => r.DerivedState(as_of_date, test_cycle_id).IsIn(RequirementState.NotStarted, RequirementState.NoTestsLive, RequirementState.InProgress, RequirementState.AllPassed, RequirementState.AcceptedWillNotBeMet));


            //Chart ID for individual uniqueness
            int idcounter = 0;
            int idcounter_m = 0;

            ;
            //Create the dashboard with a number of filters, metrics to display, pie charts and line charts

            return View("dashboard_Index", new DashboardViewModel(project, defectsbypersonmodel, 

             new MetricGroup("Requirements", ref idcounter_m,
                  new Metric("Number of requirements", "Requirements by status", requirementsmodel.Where(r => !r.DerivedState(as_of_date, test_cycle_id).IsIn(RequirementState.DoesNotYetExist)).Count())
                  , new Metric("Number of requirements designed", "Requirements by status", liverequirements.Count(), requirementsmodel.Where(r => !r.DerivedState(as_of_date, test_cycle_id).IsIn(RequirementState.DoesNotYetExist)).Count())
                  , new Metric("Number of designed requirements for which no tests are live", "Requirements by status", requirementsmodel.Where(r => r.DerivedState(as_of_date, test_cycle_id) == RequirementState.NoTestsLive).Count(), liverequirements.Count())
                  , new Metric("Number of designed requirements not started yet", "Requirements by status", requirementsmodel.Where(r => r.DerivedState(as_of_date, test_cycle_id) == RequirementState.NotStarted).Count(), liverequirements.Count())
                  , new Metric("Number of designed requirements in progress", "Requirements by status", requirementsmodel.Where(r => r.DerivedState(as_of_date, test_cycle_id) == RequirementState.InProgress).Count(), liverequirements.Count())
                  , new Metric("Number of designed requirements completed", "Requirements by status", requirementsmodel.Where(r => r.DerivedState(as_of_date, test_cycle_id) == RequirementState.AllPassed).Count(), liverequirements.Count())

                  , new Metric("Number of designed requirements for which there is an SDD reference", "Requirements by SDD reference present", liverequirements.Where(r => r.cross_reference != null && r.cross_reference != "").Count(), liverequirements.Count())
                  )
                  , new MetricGroup("Test", ref idcounter_m,


                  new Metric("Number of tests", "Tests by status", testsmodel.Where(t => !t.DerivedState(as_of_date, test_cycle_id).IsIn(TestState.DoesNotYetExist)).Count())
                  , new Metric("Number of tests designed", "Tests by status", livetests.Count(), testsmodel.Where(t => !t.DerivedState(as_of_date, test_cycle_id).IsIn(TestState.DoesNotYetExist)).Count())
                  , new Metric("Number of designed tests not run yet", "Tests by status", testsmodel.Where(t => t.DerivedState(as_of_date, test_cycle_id) == TestState.NotStarted).Count(), livetests.Count())
                  , new Metric("Number of designed tests run at least once", "Tests by status", testsmodel.Where(t => t.DerivedState(as_of_date, test_cycle_id).IsIn(TestState.Blocked, TestState.Failed, TestState.Passed)).Count(), livetests.Count())
                  , new Metric("Number of designed tests passed", "Tests by status", testsmodel.Where(t => t.DerivedState(as_of_date, test_cycle_id) == TestState.Passed).Count(), livetests.Count())
                  , new Metric("Number of designed tests not completed", "Tests by status", testsmodel.Where(t => t.DerivedState(as_of_date, test_cycle_id).IsIn(TestState.Failed, TestState.Blocked, TestState.NotStarted)).Count(), livetests.Count())
                  , new Metric("Number of designed tests failed", "Tests by status", testsmodel.Where(t => t.DerivedState(as_of_date, test_cycle_id) == TestState.Failed).Count(), livetests.Count())
                  , new Metric("Number of designed tests blocked", "Tests by status", testsmodel.Where(t => t.DerivedState(as_of_date, test_cycle_id) == TestState.Blocked).Count(), livetests.Count())

                  , new Metric("Number of designed tests incomplete and scheduled", "Tests by status", testsmodel.Where(t => t.DerivedState(as_of_date, test_cycle_id).IsIn(TestState.Failed, TestState.Blocked, TestState.NotStarted) && t.Scheduled(test_cycle_id)).Count(), livetests.Count())
                  , new Metric("Number of designed tests incomplete and not scheduled", "Incomplete tests by scheduled status", testsmodel.Where(t => t.DerivedState(as_of_date, test_cycle_id).IsIn(TestState.Failed, TestState.Blocked, TestState.NotStarted) && !t.Scheduled(test_cycle_id)).Count(), livetests.Count())
                  )
                  , new MetricGroup("Defect", ref idcounter_m,
                  new Metric("Number of open defects", "Defects by status", open_defects.Count())
                  , new Metric("Number of open defects with status new", "Defects by status", open_defects.Where(df => df.defect_status_id == 1).Count(), open_defects.Count())
                  , new Metric("Number of open defects with status assigned", "Defects by status", open_defects.Where(df => df.defect_status_id == 2).Count(), open_defects.Count())
                  , new Metric("Number of open defects with status resolved", "Defects by status", open_defects.Where(df => df.defect_status_id == 3).Count(), open_defects.Count())
                  , new Metric("Number of open defects with status on hold", "Defects by status", open_defects.Where(df => df.defect_status_id == 5).Count(), open_defects.Count())
                  , new Metric("Number of closed defects", "Defects by closure reason", closed_defects.Count())
                  ),
                   new Filters(
                    new Dropdown("draft_status_id", _context.draft_status.Where(d => d.draft_status_id < 3), draft_status_id, true)
                  , new Dropdown("priority_id", _context.priorities, priority_id, true)
                  , new Dropdown("test_cycle_id", _context.project_testcycles(project), test_cycle_id, true)
                  , new Dropdown("module_id", _context.project_modules(project), module_id, true)
                  , new Dropdown("owner_id", _context.project_users(project), "user_id", owner_id)
                  , new Datetime("as_of_date_n", as_of_date.ToString("yyyy-MM-dd"), "as of date")
                  , new Dropdown("defect_status_id", _context.defect_status, defect_status_id, true)
                  , new Dropdown("severity_id", _context.severities, severity_id, true)
                  )
                , new PieChart("Requirements by status", requirementsmodel.GroupBy(r => r.DerivedState(as_of_date, test_cycle_id)).GeneratePieChart(g => g?.Key.ToEnumString(), rs => rs.GetColor(), rs => (int)rs), ref idcounter, "requirements", "Index", project, "?derived_requirement_state=")
                , new PieChart("Tests by status", testsmodel.GroupBy(t => t.DerivedState(as_of_date, test_cycle_id)).GeneratePieChart(g => g?.Key.ToEnumString(), ts => ts.GetColor(), ts => (int)ts), ref idcounter, "tests", "Index", project, "?derived_test_state=")
                , new PieChart("Requirements by module", requirementsmodel.GroupBy(r => r.module).GeneratePieChart(g => g?.Key?.name, m => m?.module_id ?? -99), ref idcounter, "requirements", "Index", project, "?module_id=")
                , new PieChart("Requirements by owner", requirementsmodel.GroupBy(r => r.owner).GeneratePieChart(g => g?.Key?.name, u => u?.user_id ?? -99), ref idcounter, "requirements", "Index", project, "?owner_id=")
                , new PieChart("Tests by module", testsmodel.GroupBy(t => t.module).GeneratePieChart(g => g?.Key?.name, m => m?.module_id ?? -99), ref idcounter, "tests", "Index", project, "?module_id=")
                , new PieChart("Tests by owner", testsmodel.GroupBy(t => t.owner).GeneratePieChart(g => g?.Key?.name, u => u?.user_id ?? -99), ref idcounter, "tests", "Index", project, "?owner_id=")
                , new PieChart("Tests by priority", testsmodel.GroupBy(t => t.priority).GeneratePieChart(g => g?.Key?.name, p =>
                {
                    switch (p.priority_id)
                    {
                        case 1: return DashColor.Green;
                        case 2: return DashColor.Blue;
                        case 3: return DashColor.Orange;
                        case 4: return DashColor.Yellow;
                        case 5: return DashColor.Red;
                        default: return DashColor.Purple;
                    }
                }
                , p => p.priority_id), ref idcounter, "tests", "Index", project, "?priority_id=")
                , new PieChart("Incomplete tests by scheduled status", testsmodel.Where(t => t.DerivedState(as_of_date, test_cycle_id).IsIn(TestState.Failed, TestState.Blocked, TestState.NotStarted)).GroupBy(r => r.Scheduled(test_cycle_id)).GeneratePieChart(g => g.Key ? "Yes" : "No", s => s ? 1 : 0), ref idcounter, "tests", "Index", project, "?scheduled=")
                , new LineChart("Requirements trend", requirementsbydatemodel, ref idcounter)
                , new LineChart("Tests trend", testsbydatemodel, ref idcounter)
                , new LineChart("Test status trend", testsbydatemodel2, ref idcounter, true) 
                , new LineChart("Defects trend", defectsbydatemodel1, ref idcounter)
                , new LineChart("Open defects trend", defectsbydatemodel2, ref idcounter, true)
                , new PieChart("Requirements by SDD reference present", liverequirements.GroupBy(r => r.cross_reference != null && r.cross_reference != "").GeneratePieChart(g => g.Key ? "Yes" : "No", sdd => -4), ref idcounter, "requirements", "Index", project, "") 
                , new PieChart("Defects by closure reason", closed_defects.Where(d => d.closure_reason_id != 1).GroupBy(d => d.closure_reason).GeneratePieChart(g => g?.Key?.name, cr => cr.closure_reason_id), ref idcounter, "defects", "Index", project, "?closure_reason_id=")
                , new PieChart("Open defects by status", open_defects.GroupBy(d => d.defect_status).OrderBy(g => (g?.Key?.name == "New" ? 1 : (g?.Key?.name == "Assigned" ? 2 : 3))).GeneratePieChart(g => g?.Key?.name, ds => ds.defect_status_id), ref idcounter, "defects", "Index", project, "?defect_status_id=")
                , new PieChart("Open defects by priority", open_defects.GroupBy(d => d.priority).GeneratePieChart(g => g?.Key?.name, p =>
                {
                    switch (p.priority_id)
                    {
                        case 1: return DashColor.Green;
                        case 2: return DashColor.Blue;
                        case 3: return DashColor.Orange;
                        case 4: return DashColor.Yellow;
                        case 5: return DashColor.Red;
                        default: return DashColor.Purple;
                    }
                }
                , p => p.priority_id), ref idcounter, "defects", "Index", project, "?priority_id=")
                , new PieChart("Open defects by severity", open_defects.GroupBy(d => d.severity).GeneratePieChart(g => g?.Key?.name, s => s.severity_id), ref idcounter, "defects", "Index", project, "?severity_id=")
                , new PieChart("Open defects by module", open_defects.GroupBy(d => d.module).GeneratePieChart(g => g?.Key?.name, m => m?.module_id ?? -99), ref idcounter, "defects", "Index", project, "?module_id=")
                , new PieChart("Open defects by user assigned to", open_defects.GroupBy(t => t.assigned_toNavigation).GeneratePieChart(g => g?.Key?.name, u => u?.user_id ?? -99), ref idcounter, "defects", "Index", project, "?assigned_to_id=")
                ));
        }

        public IActionResult Homepage(int project = 0)
        {

            //Set project to default if the user hasn't selected one
            project = role.project;

            var prj = _context.projects.SingleOrDefault(p => p.project_id == project);
            
            //If user does not have a role record against this project, deny access
            if (!role.IsAuthenticated() || prj == null) { return PageBuilder.Error403(project); }

            var user = role.user_id;

            var test_cycle_id = prj.current_test_cycle_id ?? 0;

            //Get the notifications content

            var defectsmodel = _context.project_defects(project)
                .Include(d => d.module)
                .Include(d => d.found_byNavigation)
                .Include(r => r.last_modified_byNavigation)
                .Include(d => d.defects_attachments)
                .Include(d => d.defect_history).ThenInclude(dha => dha.defect_status)
                .Include(d => d.defect_history).ThenInclude(dha => dha.priority)
                .Include(d => d.defect_history).ThenInclude(dha => dha.severity)
                .Include(d => d.defect_history).ThenInclude(dha => dha.assigned_toNavigation)
                .Include(d => d.defect_history).ThenInclude(dha => dha.closure_reason)
                .Include(d => d.defect_type)
                .Include(d => d.phase).ToList();

            var incomplete_requirements_you_own = _context.project_requirements(project)
                .Include(r => r.requirements_tests).ThenInclude(rt => rt.test).ThenInclude(t => t.runs).ThenInclude(r => r.outcome)
                .Where(r => r.owner_id == user && r.draft_status_id != 3).ToList().Where(r => !new List<RequirementState>() { RequirementState.Archived, RequirementState.AllPassed }.Contains(r.DerivedState(DateTime.Now, test_cycle_id))).ToList<object>();

            var defects_for_assignment = defectsmodel.Where(d => d.defect_history.OrderByDescending(dh => dh.effective_date).First().defect_status_id == 1).ToList().Select(d => new DefectContainer(d, _context)).ToList<object>();

            var tests_scheduled_for_you = _context.project_testcycles_tests(project).Include(t => t.test).Include(t => t.test_cycle).Where(t => t.scheduled_user == user && t.scheduled_status_id == 1).ToList<object>();

            var defects_assigned_to_you = defectsmodel.Where(d => d.defect_history.OrderByDescending(dh => dh.effective_date).First().assigned_to == user && d.defect_history.OrderByDescending(dh => dh.effective_date).First().defect_status_id != 4).ToList().Select(d => new DefectContainer(d, _context)).ToList<object>();

            var defects_overdue = defectsmodel.Where(d => d.defect_history.OrderByDescending(dh => dh.effective_date).First().assigned_to == user && d.defect_history.OrderByDescending(dh => dh.effective_date).First().defect_status_id != 4 && d.due_date < DateTime.Today).ToList().Select(d => new DefectContainer(d, _context)).ToList<object>();

            var tt = _context.project_tests(project).Include(t => t.runs).ThenInclude(r => r.defects).ToList();

            var tests_with_runs = _context.project_tests(project).Include(t => t.runs).ThenInclude(r => r.defects).ThenInclude(d => d.defect_history).ToList();

            var ts = tests_with_runs.Select(t => Tuple.Create(t.test_id,t.DerivedState(DateTime.Now, test_cycle_id)));
            var ts2 = tests_with_runs.SelectMany(t => t.runs);

            var tx = tests_with_runs.Select(t => t.DerivedState(DateTime.Now, test_cycle_id));
            var t1 = tests_with_runs.Where(t => t.DerivedState(DateTime.Now, test_cycle_id).IsIn(TestState.Failed, TestState.Blocked)).ToList();
            var t2 = t1.Where(t => t.runs.SelectMany(r => r.defects).Any()).ToList();
            var t3 = t2.Where(t => t.runs.SelectMany(r => r.defects).All(d => d.defect_history.OrderByDescending(dh => dh.effective_date).First().defect_status_id == 3 || d.defect_history.OrderByDescending(dh => dh.effective_date).First().defect_status_id == 4)).ToList();

            ;
            var tests_with_resolved_defects = tests_with_runs.Where(t => t.DerivedState(DateTime.Now, test_cycle_id).IsIn(TestState.Failed,TestState.Blocked) && t.runs.SelectMany(r => r.defects).Any() && t.runs.SelectMany(r => r.defects).All(d => d.defect_history.OrderByDescending(dh => dh.effective_date).First().defect_status_id == 3 || d.defect_history.OrderByDescending(dh => dh.effective_date).First().defect_status_id == 4)).ToList<object>();
            ;
            var resolved_issues = defectsmodel.Where(d => d.defect_history.OrderByDescending(dh => dh.effective_date).First().defect_status_id == 3).ToList().Select(d => new DefectContainer(d, _context)).ToList<object>();

            var overdue_tests = _context.test_cycles_tests.Include(tct => tct.test).Where(tct => tct.test.project_id == project && tct.scheduled_status_id == 1 && DateTime.Now > tct.scheduled_end_date).ToList<object>();

            var open_defects_with_test_passed = _context.project_defects(project).Include(d => d.test_run).ThenInclude(t => t.test).ThenInclude(t => t.runs).Include(d => d.defect_history).Where(d => d.defect_history.OrderByDescending(dh => dh.effective_date).First().defect_status_id != 4).ToList().Where(d => d.test_run?.test?.DerivedState(DateTime.Now, test_cycle_id) == TestState.Passed).Select(d => new { defect_id = d.defect_id, defect = d, run = d.test_run.test.runs.OrderByDescending(r => r.run_date).First() }).ToList<object>();
            ;

            if (user == 1 || user == 14 || user == 8)
            {
                var defects_assigned_to_you_assignphase1 = defectsmodel.Where(d => d.defect_history.OrderByDescending(dh => dh.effective_date).First().assigned_to == user && d.phase_id == 1 && d.defect_history.OrderByDescending(dh => dh.effective_date).First().defect_status_id < 4).ToList().Select(d => new DefectContainer(d, _context)).ToList<object>();
                var defects_assigned_to_you_assignphase2 = defectsmodel.Where(d => d.defect_history.OrderByDescending(dh => dh.effective_date).First().assigned_to == user && d.phase_id == 2 && d.defect_history.OrderByDescending(dh => dh.effective_date).First().defect_status_id < 4).ToList().Select(d => new DefectContainer(d, _context)).ToList<object>();
                var defects_assigned_to_t1_assignphase1 = defectsmodel.Where(d => d.defect_history.OrderByDescending(dh => dh.effective_date).First().assigned_to == 24 && d.phase_id == 1 && d.defect_type_id != 7 && d.defect_history.OrderByDescending(dh => dh.effective_date).First().defect_status_id < 4).ToList().Select(d => new DefectContainer(d, _context)).ToList<object>();
                var defects_assigned_to_t1_assignphase1_enhance = defectsmodel.Where(d => d.defect_history.OrderByDescending(dh => dh.effective_date).First().assigned_to == 24 && d.phase_id == 1 && d.defect_type_id == 7 && d.defect_history.OrderByDescending(dh => dh.effective_date).First().defect_status_id < 4).ToList().Select(d => new DefectContainer(d, _context)).ToList<object>();
                var defects_assigned_to_t1_assignphase2 = defectsmodel.Where(d => d.defect_history.OrderByDescending(dh => dh.effective_date).First().assigned_to == 24 && d.phase_id == 2 && d.defect_type_id != 7 && d.defect_history.OrderByDescending(dh => dh.effective_date).First().defect_status_id < 4).ToList().Select(d => new DefectContainer(d, _context)).ToList<object>();
                var defects_assigned_to_t1_assignphase2_enhance = defectsmodel.Where(d => d.defect_history.OrderByDescending(dh => dh.effective_date).First().assigned_to == 24 && d.phase_id == 2 && d.defect_type_id == 7 && d.defect_history.OrderByDescending(dh => dh.effective_date).First().defect_status_id < 4).ToList().Select(d => new DefectContainer(d, _context)).ToList<object>();
                var defects_assigned_to_mc_assignphase1 = defectsmodel.Where(d => d.defect_history.OrderByDescending(dh => dh.effective_date).First().assigned_to == 82 && d.defect_history.OrderByDescending(dh => dh.effective_date).First().defect_status_id < 4).ToList().Select(d => new DefectContainer(d, _context)).ToList<object>();
                var defects_assigned_to_you_onholdphase1 = defectsmodel.Where(d => d.defect_history.OrderByDescending(dh => dh.effective_date).First().assigned_to == user && d.phase_id == 1 && d.defect_history.OrderByDescending(dh => dh.effective_date).First().defect_status_id == 5).ToList().Select(d => new DefectContainer(d, _context)).ToList<object>();
                var defects_assigned_to_you_onholdphase2 = defectsmodel.Where(d => d.defect_history.OrderByDescending(dh => dh.effective_date).First().assigned_to == user && d.phase_id == 2 && d.defect_history.OrderByDescending(dh => dh.effective_date).First().defect_status_id == 5).ToList().Select(d => new DefectContainer(d, _context)).ToList<object>();
                var defects_assigned_to_cutover = defectsmodel.Where(d => (d.defect_type_id == 12 || d.defect_history.OrderByDescending(dh => dh.effective_date).First().assigned_to == 85) && d.defect_history.OrderByDescending(dh => dh.effective_date).First().defect_status_id < 4).Select(d => new DefectContainer(d, _context)).ToList<object>();

                return View("dashboard_Homepage", new HomepageViewModel(project, test_cycle_id, role.user_id,
              new NotificationTagHelper.InboxElement(RoleType.Tester, "Issues assigned to you - phase 1", defects_assigned_to_you_assignphase1, "defects", new List<Field>() {
                    new Column("name",1)
                    ,new Column("defect_status.name", 1)
                , new Column("due_date", 1)}
               , new Links(true, false, new Link("defects", "AssignOrUpdate", "Update status")), false)
              , new NotificationTagHelper.InboxElement(RoleType.Tester, "Issues assigned to you - phase 2", defects_assigned_to_you_assignphase2, "defects", new List<Field>() {
                    new Column("name",1)
                    ,new Column("defect_status.name", 1)
                , new Column("due_date", 1)}
               , new Links(true, false, new Link("defects", "AssignOrUpdate", "Update status")), false)
                      , new NotificationTagHelper.InboxElement(RoleType.Tester, "Issues assigned to Master Config", defects_assigned_to_mc_assignphase1, "defects", new List<Field>() {
                    new Column("name",1)
                    ,new Column("defect_status.name", 1)
                    ,new Column("external_ref", 1)
                , new Column("due_date", 1)}
               , new Links(true, false, new Link("defects", "AssignOrUpdate", "Update status")), false)
               , new NotificationTagHelper.InboxElement(RoleType.Tester, "Issues assigned to TechOne - phase 1", defects_assigned_to_t1_assignphase1, "defects", new List<Field>() {
                    new Column("name",1)
                    ,new Column("defect_status.name", 1)
                    ,new Column("external_ref", 1)
                , new Column("due_date", 1)}
               , new Links(true, false, new Link("defects", "AssignOrUpdate", "Update status")), false)
                              , new NotificationTagHelper.InboxElement(RoleType.Tester, "Enhancements assigned to TechOne - phase 1", defects_assigned_to_t1_assignphase1_enhance, "defects", new List<Field>() {
                    new Column("name",1)
                    ,new Column("defect_status.name", 1)
                    ,new Column("external_ref", 1)
                , new Column("due_date", 1)}
               , new Links(true, false, new Link("defects", "AssignOrUpdate", "Update status")), false)
              , new NotificationTagHelper.InboxElement(RoleType.Tester, "Issues assigned to TechOne - phase 2", defects_assigned_to_t1_assignphase2, "defects", new List<Field>() {
                    new Column("name",1)
                    ,new Column("defect_status.name", 1)
                    ,new Column("external_ref", 1)
                , new Column("due_date", 1)}
               , new Links(true, false, new Link("defects", "AssignOrUpdate", "Update status")), false)
               , new NotificationTagHelper.InboxElement(RoleType.Tester, "Enhancements assigned to TechOne - phase 2", defects_assigned_to_t1_assignphase2_enhance, "defects", new List<Field>() {
                    new Column("name",1)
                    ,new Column("defect_status.name", 1)
                    ,new Column("external_ref", 1)
                , new Column("due_date", 1)}
               , new Links(true, false, new Link("defects", "AssignOrUpdate", "Update status")), false)
                 , new NotificationTagHelper.InboxElement(RoleType.Tester, "Cutover tasks", defects_assigned_to_cutover, "defects", new List<Field>() {
                    new Column("name",1)
                    ,new Column("defect_status.name", 1)
                , new Column("due_date", 1)}
               , new Links(true, false, new Link("defects", "AssignOrUpdate", "Update status")), false)
              , new NotificationTagHelper.InboxElement(RoleType.Tester, "Issues on hold - phase 1", defects_assigned_to_you_onholdphase1, "defects", new List<Field>() {
                    new Column("name",1)
                    ,new Column("defect_status.name", 1)
                , new Column("due_date", 1)}
               , new Links(true, false, new Link("defects", "AssignOrUpdate", "Update status")), false)
              , new NotificationTagHelper.InboxElement(RoleType.Tester, "Issues on hold - phase 2", defects_assigned_to_you_onholdphase2, "defects", new List<Field>() {
                    new Column("name",1)
                    ,new Column("defect_status.name", 1)
                , new Column("due_date", 1)}
               , new Links(true, false, new Link("defects", "AssignOrUpdate", "Update status")), false)
              , new NotificationTagHelper.InboxElement(RoleType.Tester, "Overdue issues", defects_overdue, "defects", new List<Field>() {
                    new Column("name",1)
                    ,new Column("defect_status.name", 1)
                , new Column("due_date", 1)}
               , new Links(true, false, new Link("defects", "AssignOrUpdate", "Update status")), false)
              , new NotificationTagHelper.InboxElement(RoleType.Manager, "Issues for assignment", defects_for_assignment, "defects", new List<Field>() {
                    new Column("name",1)
                    ,new Column("defect_status.name", 1)}
               , new Links(true, false
                  , new Link("defects", "AssignOrUpdate", "Reassign or update status"))
                  , false)
            , new NotificationTagHelper.InboxElement(RoleType.Manager, "Issues marked as resolved", resolved_issues, "defects", new List<Field>() {
                    new Column("name",1)
                    ,new Column("defect_status.name", 1)}
               , new Links(true, false
                  , new Link("defects", "AssignOrUpdate", "Reassign or update status"))
                  , false)
              , new NotificationTagHelper.InboxElement(RoleType.Tester, "Tests scheduled for you", tests_scheduled_for_you, "scheduletestruns", new List<Field>() {
                    new Column("test.name",1)
                    ,new Column("scheduled_start_date", 1, "", "between")
                    ,new Column("scheduled_end_date", 1, "", "and") }
                  , new Links(false, false, new Link("testruns", "Create", "Run test")), true)
                    , new NotificationTagHelper.InboxElement(RoleType.Manager, "Overdue Tests", overdue_tests, "scheduletestruns", new List<Field>() {
                       new Column("test.name",1)
                    ,new Column("scheduled_start_date", 1, "", "between")
                    ,new Column("scheduled_end_date", 1, "", "and") }
                  , new Links(true, false), false)
                  , new NotificationTagHelper.InboxElement(RoleType.Manager, "Incomplete requirements you own", incomplete_requirements_you_own, "requirements", new List<Field>() {
                    new Column("name",1)
                    ,new Column("derived_requirement_state", 1)}
               , new Links(true, false
               , new Link("tests", "Index", "View Tests")), false)
                  , new NotificationTagHelper.InboxElement(RoleType.Manager, "Tests with resolved defects", tests_with_resolved_defects, "tests", new List<Field>() {
                     new Column("name",1) }
                  , new Links(false, false, new Link("testruns", "Create", "Run test"), new Link("scheduletestruns", "Create", "Schedule a test run")), false)
                  , new NotificationTagHelper.InboxElement(RoleType.Manager, "Open defects with passed tests", open_defects_with_test_passed, "defects", new List<Field>() {
                     new Column("defect.name",1, "", "defect")
                    ,new Column("run.run_date",1, "", "linked test run passed")}
                  , new Links(false, false, new Link("defects", "AssignOrUpdate", "Reassign or update status")), false)
                  ));
            }
            //Return one table for each kind of notification
            return View("dashboard_Homepage",new HomepageViewModel(project, test_cycle_id, role.user_id,
                new NotificationTagHelper.InboxElement(RoleType.Tester, "Issues assigned to you", defects_assigned_to_you, "defects", new List<Field>() {
                    new Column("name",1)
                    ,new Column("defect_status.name", 1)
                , new Column("due_date", 1)}
                 , new Links(false, false, new Link("defects", "AssignOrUpdate", "Update status")), false)
                , new NotificationTagHelper.InboxElement(RoleType.Tester, "Overdue issues", defects_overdue, "defects", new List<Field>() {
                    new Column("name",1)
                    ,new Column("defect_status.name", 1)
                , new Column("due_date", 1)}
                 , new Links(false, false, new Link("defects", "AssignOrUpdate", "Update status")), false)
                , new NotificationTagHelper.InboxElement(RoleType.Manager, "Issues for assignment", defects_for_assignment, "defects", new List<Field>() {
                    new Column("name",1)
                    ,new Column("defect_status.name", 1)}
                 , new Links(false, false
                    , new Link("defects", "AssignOrUpdate", "Reassign or update status"))
                    , false)
              , new NotificationTagHelper.InboxElement(RoleType.Manager, "Issues marked as resolved", resolved_issues, "defects", new List<Field>() {
                    new Column("name",1)
                    ,new Column("defect_status.name", 1)}
                 , new Links(false, false
                    , new Link("defects", "AssignOrUpdate", "Reassign or update status"))
                    , false)
                , new NotificationTagHelper.InboxElement(RoleType.Tester, "Tests scheduled for you", tests_scheduled_for_you, "scheduletestruns", new List<Field>() {
                    new Column("test.name",1)
                    ,new Column("scheduled_start_date", 1, "", "between")
                    ,new Column("scheduled_end_date", 1, "", "and") }
                    , new Links(false, false, new Link("testruns", "Create", "Run test")), true)
                      , new NotificationTagHelper.InboxElement(RoleType.Manager, "Overdue Tests", overdue_tests, "scheduletestruns", new List<Field>() {
                       new Column("test.name",1)
                    ,new Column("scheduled_start_date", 1, "", "between")
                    ,new Column("scheduled_end_date", 1, "", "and") }
                    , new Links(true, false), false)
                    , new NotificationTagHelper.InboxElement(RoleType.Manager, "Incomplete requirements you own", incomplete_requirements_you_own, "requirements", new List<Field>() {
                    new Column("name",1)
                    ,new Column("derived_requirement_state", 1)}
                 , new Links(true, false
                 , new Link("tests", "Index", "View Tests")), false)
                    , new NotificationTagHelper.InboxElement(RoleType.Manager, "Tests with resolved defects", tests_with_resolved_defects, "tests", new List<Field>() {
                     new Column("name",1) }
                    , new Links(false, false, new Link("testruns", "Create", "Run test"), new Link("scheduletestruns", "Create", "Schedule a test run")), false)
                    , new NotificationTagHelper.InboxElement(RoleType.Manager, "Open defects with passed tests", open_defects_with_test_passed, "defects", new List<Field>() {
                     new Column("defect.name",1, "", "defect")
                    ,new Column("run.run_date",1, "", "linked test run passed")}
                    , new Links(false, false, new Link("defects", "AssignOrUpdate", "Reassign or update status")), false)
                    ));
        }

        public IActionResult Help(int project = 0)
        {
            return PageBuilder.Back(role, "Help", @"View issues log guide: <a href=""/IssuesLogGuidance.docx"">Download</a><br><br>View general user guide: <a href=""/UserGuide.docx"">Download</a><br><br>If you have any issues, comments or suggestions please email <a href=""mailto:aCoady@lincoln.ac.uk"">aCoady@lincoln.ac.uk</a>", "dashboard", "Homepage", project, true);
        }

        public IActionResult Debug()
        {
            return PageBuilder.Back(role, "Debug", "#" + (role.user_name ?? "NULL") + "#", "dashboard", "Debug", 0);
        }
    }
}
