﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using Microsoft.AspNetCore.Http;
using System.Net.Http.Headers;
using System.IO;
using System.Reflection;


using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;


namespace WebApplication6.Controllers
{  

    public class testsController : Controller
    {
        private testing2Context _context;
        private RoleInfo role;
        private IRoleService _rs;
        private IDatabase database;
        private IHtmlEncoderService encoder;

        public testsController(testing2Context context, IRoleService rs, IDatabase database, IHtmlEncoderService encoder)
        {
            _context = context;
            _rs = rs;
            role = rs.GetRole();
            this.database = database;
            this.encoder = encoder;
        }
        public IActionResult Index(int project = 0, string name = "", int requirement_id = 0, int module_id = 0, int owner_id = 0, int draft_status_id = 99, int priority_id = 0, int page = 1, TestState derived_test_state = TestState.None, int scheduled = -1, string button_name = "")
        {
            name = encoder.Encode(name ?? "");
            button_name = encoder.Encode(button_name ?? "");
            project = role.project;
            var prj = _context.projects.SingleOrDefault(p => p.project_id == project);
         
            if (!role.IsAuthenticated() || prj == null) { return PageBuilder.Error403(project); }

            var test_cycle_id = prj.current_test_cycle_id ?? 0;

            var testingContextpre = _context.tests
                .Include(t => t.draft_status)
                .Include(t => t.requirements_tests).ThenInclude(rt => rt.requirement)
                .Include(t => t.project)
                .Include(t => t.owner)
                .Include(t => t.@module)
                .Include(t => t.draft_status)
                .Include(t => t.priority)
                .Include(t => t.runs)
                .Include(t => t.test_cycles_tests)
                .Include(r => r.last_modified_byNavigation)
                .Where(t => (t.project_id == project)
                && (t.name.ToLower().Contains(name.ToLower()) || t.tags.ToLower().Contains(name.ToLower()) || name == "")
                && (t.owner_id == owner_id || owner_id == 0 || (owner_id == -99 && t.owner_id == null))
                && (t.module_id == module_id || module_id == 0 || (module_id == -99 && t.module_id == null))
                && (t.priority_id == priority_id || priority_id == 0)
                && (t.draft_status_id == draft_status_id || (draft_status_id == 99 && (t.draft_status_id == 1 || t.draft_status_id == 2)) || (draft_status_id == 0))
                );

            var testingContext = testingContextpre.ToList()
                .Where(t => (t.DerivedState(DateTime.Now,test_cycle_id) == derived_test_state || derived_test_state == TestState.None)
                && (Convert.ToInt32(t.Scheduled(test_cycle_id)) == scheduled || scheduled == -1)
                && (t.requirements_tests.Any(rt => rt.requirement_id == requirement_id) || requirement_id == 0)).OrderBy(r => r.name, new NaturalSortComparer<string>()).ToList();

            if(button_name == "Generate Report")
            {
                return PageBuilder.GenerateReport(project, test_cycle_id, testingContext.ToList()
                    , new Column("test_id", 1, true)
                    , new Column("name", 1)
                    , new Column("derived_test_state", 1, null, "state")
                 , new Column("scheduled", 2)
                 , new Column("requirements_tests.requirement.name", 2, "", "requirements")
                 , new Column("module.name", 2, "(None)")
                 , new Column("description", 3)
                 , new Column("draft_status.name", 1)
                 , new Column("priority.name", 1)
                 , new Column("owner.name", 2, "(None)")
                 , new Column("tags", 2)
                 , new Column("last_modified_byNavigation.name", 2, "", "last modified by")
                 , new Column("last_modified_date", 2));
            }

            return View("tests_index", new IndexViewModel<tests>(role, project, test_cycle_id, Request.QueryString.HasValue, testingContext, page
               , new Filters(
                     new Textbox("name", name)
                    , new Dropdown("requirement_id", _context.project_requirements(project).Where(r => r.draft_status_id != 3), requirement_id, true)
                    , new Dropdown("module_id", _context.project_modules(project), module_id, true)
                    , new Dropdown("owner_id", _context.project_users(project), "user_id", owner_id)
                    , new Dropdown("draft_status_id", _context.draft_status, draft_status_id, true)
                    , new Dropdown("priority_id", _context.priorities, priority_id, true)
                    , new Dropdown("derived_test_state", null, derived_test_state, true)
                    , new Dropdown("scheduled", null, scheduled, true)
                    )
                 , new Column("name", 2)
                 , new Column("derived_test_state", 2, null, "state")
                 , new Column("scheduled", 1, "", "sched.")
                 , new Column("requirements_tests.requirement.name", 2, "", "requirements")
                 , new Column("module.name", 2, "(None)")
                 , new Column("owner.name", 2, "(None)")
                ));
        }

        public IActionResult Create(int project = 0, int requirement_id = 0)
        {
            if (!role.IsManager()) { return PageBuilder.Error403(project); }

            var existing_requirements = new List<requirements>();

            var new_requirement = _context.requirements.SingleOrDefault(r => r.requirement_id == requirement_id);
            if (new_requirement != null) { existing_requirements.Add(new_requirement); }

            return View("tests_create", new ViewModel<tests>(project, new tests() { project_id = project }
                , new MultiField("requirement","requirement_id", existing_requirements, new Dropdown("requirement_id", _context.requirements.Where(r => r.project_id == project)))
                , new MultiField("test step", "test_step_id", new List<test_steps>(), new Textarea("description"), new Textarea("expected_result"), new AttachmentUpload())
                , new Dropdown("module_id", _context.project_modules(project))
                , new Dropdown("draft_status_id", _context.draft_status, true, @"
<b>Draft</b>: The requirement is still being designed. Does not count for reporting purposes. <br/>
<b>Live</b>: The requirement is current <br/>
<b>Archived</b>: The requirement is no longer in use.
")
                , new Dropdown("priority_id", _context.priorities)
                , new Dropdown("owner_id", _context.project_users(project), "user_id")
                ));
            //_context.test_step_attachments.Include(a => a.test_step).Where(a => a.test_step.test_id == id)

        }

        [HttpPost]
        public IActionResult Create(int project, tests tests, List<int> requirement_requirement_id, List<int> test_step_id, List<string> test_step_description, List<string> test_step_expected_result)
        {
            encoder.Encode(tests);
            encoder.Encode(test_step_description);
            encoder.Encode(test_step_expected_result);

            var files = _context.GetFormFilesAsync(HttpContext).Result;

            if (tests == null || !role.IsManager() || project != tests.project_id) { return PageBuilder.Error403(project); }

            if (tests.module_id == -99) { tests.module_id = null; }
            if (tests.owner_id == -99) { tests.owner_id = null; }

            if (ModelState.IsValid)
            {
                tests.effective_date = DateTime.Now;
                if (tests.draft_status_id == 2) { tests.date_made_live = DateTime.Now; }
                tests.last_modified_by = role.user_id;
                tests.last_modified_date = DateTime.Now;

                database.Add(tests);

                foreach (var rt in requirement_requirement_id.Distinct())
                {
                    var rtn = new requirements_tests() { requirement_id = rt, test_id = tests.test_id };
                    database.Add(rtn);
                }

                var seq_no = 1;
                for(int i=0; i<test_step_id.Count(); i++)
                {
                    var tsn = new test_steps() {
                        seq_no = seq_no, test_id = tests.test_id, test_step_id = test_step_id[i], description = test_step_description[i], expected_result = test_step_expected_result[i]
                    , attachment_content = files[i]?.attachment_content, attachment_filename = encoder.Encode(files[i]?.attachment_filename) ?? "", attachment_mimetype = encoder.Encode(files[i]?.attachment_mimetype ?? "")};
                    database.Add(tsn);
                    seq_no++;
                }

                
                return RedirectToAction("Index", new { project = project });
            }

            return PageBuilder.ModelStateError(project, role, "Tests create", ModelState);
        }

        public IActionResult Edit(int project = 0, int id = 0, bool isdelete = false)
        {
            var tests = _context.tests.Include(t => t.requirements_tests).Include(t => t.test_steps)
                 .Include(t => t.project)
                .Include(t => t.owner)
                .Include(t => t.@module)
                .Include(t => t.draft_status)
                .Include(t => t.priority)
                .Include(t => t.draft_status)
                .SingleOrDefault(m => m.project_id == project && m.test_id == id);

            if (tests == null || !role.IsManager())
            {
                return PageBuilder.Error403(project);
            }

            return View("tests_Edit", new ViewModel<tests>(project, tests
   , new MultiField("requirement", "requirements_tests_id", tests.requirements_tests, new Dropdown("requirement_id", _context.requirements.Where(r => r.project_id == project), false))
   , new MultiField("test step", "test_step_id", tests.test_steps.OrderBy(ts => ts.seq_no), new Textarea("description"), new Textarea("expected_result"), new AttachmentUpload())
   , new Dropdown("module_id", _context.project_modules(project))
    , new Dropdown("draft_status_id", _context.draft_status, true, @"
<b>Draft</b>: The requirement is still being designed. Does not count for reporting purposes. <br/>
<b>Live</b>: The requirement is current <br/>
<b>Archived</b>: The requirement is no longer in use.
")
    , new Dropdown("priority_id", _context.priorities)
    , new Dropdown("owner_id", _context.project_users(project), "user_id")));
        }


        public IActionResult View(int project = 0, int id = 0, bool isdelete = false)
        {
            var tests = _context.tests.Include(t => t.requirements_tests).Include(t => t.test_steps)
                                 .Include(t => t.project)
                .Include(t => t.owner)
                .Include(t => t.@module)
                .Include(t => t.draft_status)
                .Include(t => t.priority)
                .Include(t => t.draft_status)
                .SingleOrDefault(m => m.project_id == project && m.test_id == id);

            if (tests == null || !role.IsAuthenticated())
            {
                return PageBuilder.Error403(project);
            }

            return View("tests_View", new ViewModel<tests>(project, tests
   , new MultiField("requirement", "requirements_tests_id", tests.requirements_tests, new Dropdown("requirement_id", _context.requirements.Where(r => r.project_id == project), false))
   , new MultiField("test step", "test_step_id", tests.test_steps.OrderBy(ts => ts.seq_no), new Textarea("description"), new Textarea("expected_result"), new AttachmentUpload())
   , new Dropdown("module_id", _context.project_modules(project))
    , new Dropdown("draft_status_id", _context.draft_status, true, @"
<b>Draft</b>: The requirement is still being designed. Does not count for reporting purposes. <br/>
<b>Live</b>: The requirement is current <br/>
<b>Archived</b>: The requirement is no longer in use.
")
    , new Dropdown("priority_id", _context.priorities)
    , new Dropdown("owner_id", _context.project_users(project), "user_id")));
        }

        [HttpPost]
        public IActionResult Edit(int project, tests tests, List<int> requirements_tests_requirement_id, List<int> test_step_id, List<string> test_step_description, List<string> test_step_expected_result)
        {
            encoder.Encode(tests);
            encoder.Encode(test_step_description);
            encoder.Encode(test_step_expected_result);

            if (tests == null || !role.IsManager() || project != tests.project_id)
            {
                return PageBuilder.Error403(project);
            }
            var files = _context.GetFormFilesAsync(HttpContext).Result;

            if (tests.module_id == -99) { tests.module_id = null; }
            if (tests.owner_id == -99) { tests.owner_id = null; }

            if (ModelState.IsValid)
            {
                var existing_test = _context.tests.AsNoTracking().Single(r => r.test_id == tests.test_id);
                if (tests.draft_status_id == 2 && existing_test.draft_status_id != 2) { tests.date_made_live = DateTime.Now; }
                database.Update(tests);

                var distinct_reqs = requirements_tests_requirement_id.Distinct().ToList();

                database.UpdateLinked<requirements_tests>(_context.requirements_tests.Where(ts => ts.test_id == tests.test_id)
                    , "requirements_tests_id", distinct_reqs.Select(x => 0).ToList(),
                    (i) => new requirements_tests() { requirement_id = distinct_reqs[i], test_id = tests.test_id },
                    (existing, i) => { existing.requirement_id = distinct_reqs[i]; }
                    );

                var test_step_seq_no = test_step_id.Select((ts, i) => i+1).ToArray();

                database.UpdateLinked<test_steps>(_context.test_steps.Where(ts => ts.test_id == tests.test_id)
                    , "test_step_id", test_step_id,
                    (i) => new test_steps() {
                        test_id = tests.test_id
                    , description = test_step_description[i]
                    , expected_result = test_step_expected_result[i]
                    , attachment_content = files[i]?.attachment_content
                    , attachment_filename = encoder.Encode(files[i]?.attachment_filename)
                    , attachment_mimetype = encoder.Encode(files[i]?.attachment_mimetype)
                    , seq_no = test_step_seq_no[i]
                    },
                    (existing, i) => {
                        existing.description = test_step_description[i];
                        existing.expected_result = test_step_expected_result[i];
                        existing.seq_no = test_step_seq_no[i];

                        if (files[i] != null && files[i]?.attachment_content != null)
                        {
                            existing.attachment_content = files[i]?.attachment_content;
                            existing.attachment_filename = encoder.Encode(files[i]?.attachment_filename);
                            existing.attachment_mimetype = encoder.Encode(files[i]?.attachment_mimetype);
                        }
                    }
                    );

                ;

                return RedirectToAction("Index", new { project = project });
            }
            return PageBuilder.ModelStateError(project, role, "Tests edit", ModelState);
        }        

        public IActionResult Archive(int project = 0, int id = 0)
        {
            var tests = _context.tests.Include(t => t.requirements_tests).Include(t => t.test_steps)
                                 .Include(t => t.project)
                .Include(t => t.owner)
                .Include(t => t.@module)
                .Include(t => t.draft_status)
                .Include(t => t.priority)
                .Include(t => t.draft_status)
                .SingleOrDefault(m => m.project_id == project && m.test_id == id);

            if (tests == null || !role.IsManager())
            {
                return PageBuilder.Error403(project);
            }

            return View("tests_Archive", new ViewModel<tests>(project, tests, 
                new MultiField("requirement", "requirements_tests_id", tests.requirements_tests, new Dropdown("requirement_id", _context.requirements.Where(r => r.project_id == project), false))
   , new MultiField("test step", "test_step_id", tests.test_steps.OrderBy(ts => ts.seq_no), new Textarea("description"), new Textarea("expected_result"), new AttachmentUpload())));
        }
       

        public IActionResult DeleteConfirmed(int project = 0, int id = 0)
        {
            var tests = _context.tests.Include(t => t.requirements_tests).Include(t => t.test_cycles_tests).SingleOrDefault(m => m.test_id == id);

            if (tests == null || !role.IsManager() || project != tests.project_id)
            {
                return PageBuilder.Error403(project);
            }

            database.RemoveRange(tests.test_cycles_tests);
            database.RemoveRange(tests.requirements_tests);
            tests.draft_status_id = 3;
            database.Update(tests);

            return RedirectToAction("Index", new { project = project });
        }      

        public IActionResult GenerateBulkImport(int project)
        {
            if (!role.IsManager()) { return PageBuilder.Error403(project); }

            try
            {
                HttpContext.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                FileContentResult result = new FileContentResult(System.IO.File.ReadAllBytes("wwwroot/templates/tests_template.xlsx"), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                {
                    FileDownloadName = "tests_template.xlsx"
                };
                return result;
            }
            catch
            {
                return PageBuilder.Back(role, "I/O error", "", "tests", "Index", project, true);
            }

          
        }

        public IActionResult DownloadAttachment(int project, int id)
        {
            var attachment = _context.test_steps.SingleOrDefault(t => t.test_step_id == id);

            if (!role.IsAuthenticated() || attachment == null) { return PageBuilder.Error403(project); }

            HttpContext.Response.ContentType = attachment.attachment_mimetype;
            FileContentResult result = new FileContentResult(attachment.attachment_content, attachment.attachment_mimetype)
            {
                FileDownloadName = attachment.attachment_filename
            };

            return result;
        }


        public IActionResult UploadBulkImport(int project)
        {
            return View("tests_bulkimport", project);
        }

       [HttpPost]
        public IActionResult UploadBulkImport(int project, IFormFile fileuploaded)
        {

            if (fileuploaded == null) { return PageBuilder.Back(role, "File is required. Click Choose File on the previous page.", "", "tests", "UploadBulkImport", project, true); }


            if (!role.IsManager() || project == 0) { return PageBuilder.Error403(project); }

            if (fileuploaded.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            {

                var rs = fileuploaded.OpenReadStream();

                using (var ms = new MemoryStream())
                {
                    rs.CopyTo(ms);

                    SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(rs, false);

                    var sheet = spreadsheetDocument.WorkbookPart.GetPartsOfType<WorksheetPart>().First();

                    var sheetdata = sheet.Worksheet.GetFirstChild<SheetData>();

                    SharedStringTable sst_ = spreadsheetDocument.WorkbookPart.GetPartsOfType<SharedStringTablePart>().First().SharedStringTable;

                    var sst = sst_.ChildElements.Select(u => u.InnerText);

                    var rows = sheetdata.Descendants<Row>();

                    var newtests = new List<Tuple<tests,List<int?>>>();
                    var new_test_steps = new List<Tuple<tests,string,string,int>>();
                    string error_strings = "";
                    string error_string = "";
                    var project_id = project;

                    foreach (var r in rows)
                    {
                        var allcells2 = r.Elements<Cell>();

                        var allcells = allcells2.Select(c => new ExcelCell(c, sst)).Where(c => c.column < 999);

                        var rownum = allcells.FirstOrDefault()?.row;

                        if (allcells.Any() && rownum >= 5)
                        {



                            error_string = "";
                            var test = new tests();

                            var cells = new string[105];

                            for (int k = 0; k < cells.Length; k++)
                            {
                                cells[k] = "";
                            }

                            foreach (var c in allcells)
                            {
                                cells[Math.Min(c.column - 1,cells.Length-1)] = c.value;
                            }

                            encoder.Encode(cells);

                            if (cells[0] == cells[1] || (cells[1] == null && cells[1] == cells[2]) || cells[0] == cells[2]) { error_string += "Requirement IDs must be unique";  }
                          
                            var req_1 = Validation.ValidateID("requirement_id", cells[0], _context.project_requirements(project).Where(t => t.draft_status_id != 3), ref error_string, "requirement_id 1");
                            var req_2 = Validation.ValidateIDOrNull("requirement_id", cells[1], _context.project_requirements(project).Where(t => t.draft_status_id != 3), ref error_string, "requirement_id 2");
                            var req_3 = Validation.ValidateIDOrNull("requirement_id", cells[2], _context.project_requirements(project).Where(t => t.draft_status_id != 3), ref error_string, "requirement_id 3");
                            var req_4 = Validation.ValidateIDOrNull("requirement_id", cells[3], _context.project_requirements(project).Where(t => t.draft_status_id != 3), ref error_string, "requirement_id 4");
                            var req_5 = Validation.ValidateIDOrNull("requirement_id", cells[4], _context.project_requirements(project).Where(t => t.draft_status_id != 3), ref error_string, "requirement_id 5");
                            var req_6 = Validation.ValidateIDOrNull("requirement_id", cells[5], _context.project_requirements(project).Where(t => t.draft_status_id != 3), ref error_string, "requirement_id 6");
                            var req_7 = Validation.ValidateIDOrNull("requirement_id", cells[6], _context.project_requirements(project).Where(t => t.draft_status_id != 3), ref error_string, "requirement_id 7");
                            var req_8 = Validation.ValidateIDOrNull("requirement_id", cells[7], _context.project_requirements(project).Where(t => t.draft_status_id != 3), ref error_string, "requirement_id 8");
                            var reqs = new List<int?>() { req_1, req_2, req_3, req_4, req_5, req_6, req_7, req_8 };
                            test.project_id = project;
                            test.module_id = Validation.ValidateIDOrNull("module_id", cells[8], _context.project_modules(project), ref error_string);
                            test.owner_id = Validation.ValidateIDOrNull("user_id", cells[9], _context.project_users(project), ref error_string, "owner_id");
                            test.draft_status_id = Validation.ValidateID("draft_status_id", cells[10], _context.draft_status.Where(ds => ds.draft_status_id == 1 || ds.draft_status_id == 2), ref error_string);
                            test.name = Validation.ValidateLength("name", cells[11], 4, 255, ref error_string);
                            test.description = cells[12];
                            test.priority_id = Validation.ValidateID("priority_id", cells[13], _context.priorities, ref error_string);
                            test.tags = cells[14];
                            test.effective_date = DateTime.Now;
                            test.last_modified_by = role.user_id;
                            test.last_modified_date = DateTime.Now;
                            if (test.draft_status_id == 2) { test.date_made_live = DateTime.Now; }

                            for (int i=15;i<105;i+=2)
                            {
                                if(cells[i] != "" || cells[i+1] != "")
                                {
                                    new_test_steps.Add(System.Tuple.Create(test,cells[i], cells[i+1], i));
                                }
                            }

                            if (error_string != "")
                            {
                                error_string = "Row " + rownum + ": " + error_string + "<br>";
                                error_strings += error_string;
                            }
                            else { newtests.Add(System.Tuple.Create(test,reqs)); }
                        }

                    }
                    ;
                    if (ModelState.IsValid && error_strings == "")
                    {
                        try
                        {
                            foreach (var t in newtests)
                            {

                                database.Add(t.Item1);

                                foreach(var r in t.Item2)
                                {
                                    if (r != null) { database.Add(new requirements_tests { test_id = t.Item1.test_id, requirement_id = r }); }
                                }
                          
                            }

                            var new_test_steps_t = new_test_steps.Select(s => new test_steps { test_id = s.Item1.test_id, description = s.Item2, expected_result = s.Item3, seq_no = s.Item4 });
                            foreach (var ts in new_test_steps_t)
                            {
                                database.AddNoAudit(ts);
                            }

                            foreach (var t in newtests.Select(t => t.Item1))
                            {
                                t.lineage_id = t.test_id;
                                database.Update(t);
                            }

                            return PageBuilder.Back(role, "Bulk Import Tests", newtests.Count() + @" records successfully created.", "tests", "Index", project);
                        }
                        catch (Exception e)
                        {
                            return PageBuilder.Back(role, "Bulk Import Tests", e.ToString(), "tests", "Index", project);
                        }
                    }
                    else
                    {
                        return PageBuilder.Back(role, "Bulk Import Tests", "File contained the following errors and was not loaded: <br><br>" + error_strings, "tests", "Index", project);
                    }
                }


            }

            return PageBuilder.ModelStateError(project, role, "Tests bulk import", ModelState);
        }

    }
}
