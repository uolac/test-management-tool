using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApplication6;
using System.Data;
using System.Data.SqlClient;

namespace WebApplication6.Controllers
{
    public class interfacesController : Controller
    {
        private testing2Context _context;

        public interfacesController(testing2Context context)
        {
            _context = context;    
        }

        // GET: interfaces
        public IActionResult Index()
        {
            return View(_context.interfaces.ToList());
        }

        [HttpPost]
        
        public IActionResult Index(string name)
        {
            return View(_context.interfaces.Where(s => (s.interface_name+s.external_id).Contains(name)).ToList());
        }

        // GET: interfaces/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            interfaces interfaces = _context.interfaces.Single(m => m.interface_id == id);
            if (interfaces == null)
            {
                return NotFound();
            }

            return View(interfaces);
        }

        // GET: interfaces/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: interfaces/Create
        [HttpPost]
        
        public IActionResult Create(interfaces interfaces)
        {
            if (ModelState.IsValid)
            {
                _context.interfaces.Add(interfaces);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(interfaces);
        }

        // GET: entities/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            interfaces interfaces = _context.interfaces.Single(m => m.interface_id == id);
            if (interfaces == null)
            {
                return NotFound();
            }
            return View(interfaces);
        }

        // POST: interfaces/Edit/5
        [HttpPost]
        
        public IActionResult Edit(interfaces interfaces)
        {
            if (ModelState.IsValid)
            {
                _context.Update(interfaces);
                _context.SaveChanges();



                var connectionstring_ = "Server=ASQL02;Database=testmanagementtool;Integrated Security=true;";
                var sqlcon_ = new SqlConnection(connectionstring_);
                sqlcon_.Open();

                SqlCommand sqlcmd_;
                int rows;

                if (interfaces.script != null)
                {
                    if (interfaces.script.Length > 5)
                    {

                        if (interfaces.script.Substring(0, 5) == "<?xml")
                        {

                            T1ETL.Program.Start(interfaces.interface_id, true, interfaces.argument1);

                        }
                        else if (interfaces.script != "")
                        {
                            Interfaces.Program.Start(interfaces.interface_id, true);
                        }
                        sqlcmd_ = new SqlCommand(@"SELECT COUNT(*) FROM testmanagementtool.dbo.holding_table", sqlcon_);

                        rows = (int)sqlcmd_.ExecuteScalar();

                        if (rows > 0)
                        {
                            sqlcmd_ = new SqlCommand(@"testmanagementtool.dbo.process_script", sqlcon_);
                            sqlcmd_.CommandType = CommandType.StoredProcedure;
                            sqlcmd_.Parameters.Add("@is_future", SqlDbType.Int).Value = 0;
                            sqlcmd_.Parameters.Add("@filesystemtable", SqlDbType.VarChar).Value = interfaces.argument1 ?? " ";

                            sqlcmd_.ExecuteNonQuery();

                        }

                    }
                }
                if (interfaces.future_script != null)
                {
                    if (interfaces.future_script.Length > 5)
                    {
                        if (interfaces.future_script.Substring(0, 5) == "<?xml")
                        {

                            T1ETL.Program.Start(interfaces.interface_id, false, interfaces.argument1);

                        }
                        else if (interfaces.future_script != "")
                        {
                            Interfaces.Program.Start(interfaces.interface_id, false);
                        }

                        sqlcmd_ = new SqlCommand(@"SELECT COUNT(*) FROM testmanagementtool.dbo.holding_table", sqlcon_);
                        rows = (int)sqlcmd_.ExecuteScalar();
                        if (rows > 0)
                        {
                            sqlcmd_ = new SqlCommand(@"testmanagementtool.dbo.process_script", sqlcon_);
                            sqlcmd_.CommandType = CommandType.StoredProcedure;
                            sqlcmd_.Parameters.Add("@is_future", SqlDbType.Int).Value = 1;
                            sqlcmd_.Parameters.Add("@filesystemtable", SqlDbType.VarChar).Value = interfaces.argument1 ?? " ";
                            sqlcmd_.ExecuteNonQuery();
                        }
                    }
                }


                return RedirectToAction("Index");
            }
            return View(interfaces);
        }

        // GET: interfaces/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            interfaces interfaces = _context.interfaces.Single(m => m.interface_id == id);
            if (interfaces == null)
            {
                return NotFound();
            }

            return View(interfaces);
        }

        // POST: interfaces/Delete/5
        [HttpPost, ActionName("Delete")]
        
        public IActionResult DeleteConfirmed(int id)
        {
            interfaces interfaces = _context.interfaces.Single(m => m.interface_id == id);
            _context.interfaces.Remove(interfaces);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }


    }
}
