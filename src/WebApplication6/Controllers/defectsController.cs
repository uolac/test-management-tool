﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using Microsoft.AspNetCore.Http;
using System.IO;
using System.Net.Http.Headers;


using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Data.SqlClient;

namespace WebApplication6.Controllers
{  

    public class defectsController : Controller
    {
        private testing2Context _context;
        private RoleInfo role;
        private IRoleService _rs;
        private IDatabase database;
        private IHtmlEncoderService encoder;

        public defectsController(testing2Context context, IRoleService rs, IDatabase database, IHtmlEncoderService encoder)
        {
            _context = context;
            _rs = rs;
            role = rs.GetRole();
            this.database = database;
            this.encoder = encoder;
        }

        public IActionResult Index(int project = 0, string name = "", int found_by_id = 0, int assigned_to_id = 0, int defect_status_id = -95, int priority_id = 0, int severity_id = 0, int defect_type_id = 0, int module_id = 0, int phase_id = 0, int closure_reason_id = 0, DueDateStatus due_date_status = DueDateStatus.All, DateTime? start_date = null, DateTime? end_date = null, DateTime? due_start_date = null, DateTime? due_end_date = null, string button_name = "", int page = 1)
        {
            name = encoder.Encode(name ?? "");
            button_name = encoder.Encode(button_name ?? "");
            project = role.project;

            if (!role.IsAuthenticated()) { return PageBuilder.Error403(project); }

            var t1 = _context.project_defects(project)
                 .Include(d => d.module)
                 .Include(d => d.found_byNavigation)
                 .Include(r => r.last_modified_byNavigation)
                 .Include(d => d.defects_attachments)
                 .Include(d => d.defect_history).ThenInclude(dha => dha.defect_status)
                 .Include(d => d.defect_history).ThenInclude(dha => dha.priority)
                 .Include(d => d.defect_history).ThenInclude(dha => dha.severity)
                 .Include(d => d.defect_history).ThenInclude(dha => dha.assigned_toNavigation)
                 .Include(d => d.defect_history).ThenInclude(dha => dha.closure_reason)
                 .Include(d => d.defect_type)
                 .Include(d => d.phase)
            .Include(d => d.test_run).ThenInclude(t => t.test).ToList();

            var t1a = t1.Select(d => new DefectContainer(d, _context));

            var t2 = t1a.Where(r => (r.name.ToLower().Contains(name.ToLower()) || (r.external_ref?.ToLower()?.Contains(name.ToLower()) ?? false)  || (r.tags?.ToLower()?.Contains(name.ToLower()) ?? false) || r.defect_id.ToString() == name || name == "")
                && (r.found_byNavigation.user_id == found_by_id || found_by_id == 0)
                && (r.module_id == module_id || module_id == 0 || (module_id == -99 && r.module_id == null))).ToList();
                var t3 = t2;
            ;
            if(t2.Any())
            {
                   t3 = t2.Where(r => (r.closure_reason_id == closure_reason_id || closure_reason_id == 0)
                && (r.priority_id == priority_id || priority_id == 0)
                && (r.severity_id == severity_id || severity_id == 0)
                && (r.defect_status_id == defect_status_id || defect_status_id == 0 || (defect_status_id == -95 && r.defect_status_id != 4))
                && (r.assigned_to == assigned_to_id || assigned_to_id == 0 || (assigned_to_id == -99 && r.assigned_to == null))
               && (r.module_id == module_id || module_id == 0 || (module_id == -99 && r.module_id == null))
                && (r.found_date >= start_date || start_date == null)
                && (r.found_date <= end_date || end_date == null)
                && (r.due_date >= due_start_date || due_start_date == null)
                && (r.due_date <= due_end_date || due_end_date == null)
                && (r.defect_type_id == defect_type_id || defect_type_id == 0)
                && (r.phase_id == phase_id || phase_id == 0)
                && ((due_date_status == DueDateStatus.Overdue && r.due_date < DateTime.Today) || (due_date_status == DueDateStatus.DueInTheNextMonth && r.due_date < DateTime.Today.AddMonths(1))  || (due_date_status == DueDateStatus.NoDueDate && r.due_date == null) || due_date_status == DueDateStatus.All)
                ).OrderByDescending(r => r.last_modified_date) /*.OrderBy(r => r.name, new NaturalSortComparer<string>())*/.ToList();
            }

            ;

            page = Math.Min(page, ((t3.Count - 1) / 10) + 1);

            var testingContext = t3;

            if (button_name == "Generate Report")
            {
                return PageBuilder.GenerateReport(project, 0, testingContext.ToList()
                    , new Column("defect_id", 1, true)
                    , new Column("name", 1)
                    , new Column("module.name", 1, "(None)")
                    , new Column("test_run_id", 1, true, "linked test run")
                  , new Column("description", 1, "description")
                 , new Column("found_byNavigation.name", 1, "found by")
                 , new Column("found_date", 1)
                 , new Column("due_date", 1)
                 , new Column("external_ref", 1, label: "External reference")
                 , new Column("last_modified_byNavigation.name", 2, "", "last modified by")
                 , new Column("last_modified_date", 2)
                 , new Column("assigned_toNavigation.name", 1, "(None)", "assigned to")
                 , new Column("defect_status.name", 1, "latest status")
                 , new Column("priority.name", 1, "latest priority")
                 , new Column("severity.name", 1, "latest severity")
                 , new Column("status_comment", 1, "latest status comment")
                 , new Column("effective_date", 1, "effective date of status update")
                 , new Column("defect_type.name", 1, "defect type")
                 , new Column("phase.name", 1, "phase")
                 );
            }


            var has_default_ids = ((name ?? "") == "" && found_by_id == 0 && due_date_status == 0 && assigned_to_id == 0 && defect_status_id == 0 && priority_id == 0 && severity_id == 0 && defect_type_id == 0 && module_id == 0 && phase_id == 0 && closure_reason_id == 0 && ((start_date?.ToString("yyyy-MM-dd") ?? "2010-01-01") == "2010-01-01") && ((end_date?.ToString("yyyy-MM-dd") ?? "2020-01-01") == "2020-01-01") && ((due_start_date?.ToString("yyyy-MM-dd") ?? "2010-01-01") == "2010-01-01") && ((due_end_date?.ToString("yyyy-MM-dd") ?? "2020-01-01") == "2020-01-01"));

            return View("defects_Index",new IndexViewModel<DefectContainer>(role,project,0,Request.QueryString.HasValue,testingContext.ToList(),page,
                 new Filters(has_default_ids,
                        new Textbox("name", name)
                    , new Dropdown("module_id", _context.project_modules(project), module_id, true)
                    , new Dropdown("found_by_id", _context.project_users(project), "user_id", found_by_id)
                    , new Dropdown("assigned_to_id", _context.project_users(project), "user_id", assigned_to_id)
                    , new Dropdown("defect_status_id", _context.defect_status, defect_status_id, true)
                    , new Dropdown("priority_id", _context.priorities, priority_id, true)
                    , new Dropdown("severity_id", _context.severities, severity_id, true)
                    , new Datetime("start_date",start_date?.ToString("yyyy-MM-dd") ?? "2010-01-01", "run on or after")
                    , new Datetime("end_date", end_date?.ToString("yyyy-MM-dd") ?? "2020-01-01", "run on or before")
                     , new Datetime("due_start_date", due_start_date?.ToString("yyyy-MM-dd") ?? "", "due on or after")
                    , new Datetime("due_end_date", due_end_date?.ToString("yyyy-MM-dd") ?? "", "due on or before")
                     , new Dropdown("defect_type_id", _context.defect_types, defect_type_id, true)
                      , new Dropdown("phase_id", _context.phases, phase_id, true)
                      , new Dropdown("due_date_status", null, due_date_status, true)
                    )
                    ,new Column("defect_id", 1, "", "#")
                , new Column("name", 1, true)
                  , new Column("module.name", 1, "(None)", "func. area")
                 , new Column("found_byNavigation.name", 1, "", "found by")
                 , new Column("found_date", 1)
                 , new Column("due_date", 1)
                 , new Column("test_run.test.name", 1)
                 , new Column("test_run.run_date", 1)
                  , new Column("defect_type.name", 1)
                 , new Column("assigned_toNavigation.name", 1, "(None)", "assigned to")
                 , new Column("phase.name",1)
                   , new Column("external_ref", 1)
                 , new Column("defect_status.name", 1)));
        }

        public IActionResult History(int project = 0, int defect_id = 0, int page = 1)
        {
            var testingContext = _context.defect_history
                 .Include(d => d.priority)
                 .Include(d => d.severity)
                 .Include(d => d.assigned_toNavigation)
                 .Include(r => r.last_modified_byNavigation)
                 .Include(d => d.defect_status)
                 .Where(d => d.defect_id == defect_id).OrderByDescending(r => r.effective_date).ToList(); ;

            return View("defects_History", new IndexViewModel<defect_history>(role, project, 0, Request.QueryString.HasValue, testingContext.ToList(), page
                , new Filters()
                , new Column("effective_date", 1)
                 , new Column("last_modified_byNavigation.name", 1, "", "updated by")
                 , new Column("defect_status.name", 1)
                 , new Column("assigned_toNavigation.name", 1, "", "assigned to")
                 , new Column("priority.name", 1)
                 , new Column("severity.name", 1)
                 , new Column("status_comment", 3)));
        }

        public IActionResult Create(int project = 0, int run_id = 0)
        {
            if (!role.IsTesterOrAbove() || project == 0) { return PageBuilder.Error403(project); }

            return View("defects_create", new ViewModel<defects>(project, new defects { project_id = project, test_run_id = (run_id == 0 ? null : (int?)run_id), found_by = role.user_id, found_date = DateTime.Now }
                            , new MultiField("attachment", "defect_attachment_id", new List<object>(), new AttachmentUpload())
            , new Dropdown("module_id",_context.project_modules(project))
                , new Dropdown("found_by",_context.project_users(project), "user_id", -97)
                , new Dropdown("priority_id", _context.priorities)
                , new Dropdown("severity_id", _context.severities, true, @"
<b>Now</b>: Needs to be fixed now e.g. a patch to the current release <br/>
<b>Next</b>: Needs to be fixed in the next release &#10; <br/>
<b>Before</b>: Needs to be fixed before go-live &#10; <br/>
<b>After</b>: Can be fixed after go-live 
"), new Dropdown("defect_type_id", _context.defect_types)
, new Dropdown("phase_id", _context.phases)
 ,   new Dropdown("assigned_to", _context.project_users(project), "user_id", (object)-97)
                ));
        }

        [HttpPost]
        public IActionResult Create(int project, defects defects, int priority_id, int severity_id, int assigned_to = -97)
        {
            encoder.Encode(defects);

            var files = _context.GetFormFilesAsync(HttpContext).Result;

            if (defects == null || !role.IsTesterOrAbove() || project != defects?.project_id || project == 0) { return PageBuilder.Error403(project); }

            if(defects.module_id == -99) { defects.module_id = null; }

            if (ModelState.IsValid)
            {
                database.Add(defects);
                defect_history defect_history;
                if(assigned_to != -97)
                {
                defect_history = new defect_history() { defect_id = defects.defect_id, defect_status_id = 2, closure_reason_id = 1, status_comment = "Defect created", priority_id = priority_id, severity_id = severity_id, effective_date = DateTime.Now, assigned_to = assigned_to };

                    Email.Send(
_context.users.Single(u => u.user_id == assigned_to)
, "Defect assigned to you", "A defect has been assigned to you:" + Environment.NewLine + Environment.NewLine + $"Name: {defects.name}" + Environment.NewLine + $"Due: {defects.due_date?.ToShortDateString() ?? "None"}" + Environment.NewLine + Environment.NewLine + $@"https://tmt.web01.lincoln.ac.uk/defects/AssignOrUpdate/{project}/?defect_id={defects.defect_id}");
                }
                else
                {
                    defect_history = new defect_history() { defect_id = defects.defect_id, defect_status_id = 1, closure_reason_id = 1, status_comment = "Defect created", priority_id = priority_id, severity_id = severity_id, effective_date = DateTime.Now, assigned_to = null };
                }
               

                 database.Add(defect_history);
                ;

                for (int i = 0; i < files.Count(); i++)
                {
                    if(files[i] != null && files[i]?.attachment_content != null)
                    {
                        var an = new defects_attachments()
                        {
                            defect_id = defects.defect_id,
                            attachment_content = files[i]?.attachment_content,
                            attachment_filename = encoder.Encode(files[i]?.attachment_filename),
                            attachment_mimetype = encoder.Encode(files[i]?.attachment_mimetype)
                        };
                        database.Add(an);
                        ;
                    }               
                }

                //Send an email to all test managers, project managers and administrators for this project linking to the created defect
                Email.Send(
                    _context.projects.Include(p => p.users_projects_roles).ThenInclude(upr => upr.user).ToList().SelectMany(p => p.users_projects_roles).Where(upr => (upr.role_id == 4 || upr.user.is_administrator) && upr.project_id == project).Select(upr => upr.user)
                    , "Defect created and pending assignment", "A defect has been created and is ready for assignment:" + Environment.NewLine + Environment.NewLine + $"Name: {defects.name}" + Environment.NewLine + $"Due: {defects.due_date?.ToShortDateString() ?? "None"}" + Environment.NewLine + Environment.NewLine + $@"https://tmt.web01.lincoln.ac.uk/defects/AssignOrUpdate/{project}/?defect_id={defects.defect_id}");


                return RedirectToAction("Index", new { project = project });
            }

            Email.HandleError(ModelState, role, "Defects create");

            return PageBuilder.ModelStateError(project, role, "Defects create", ModelState);
        }

        public IActionResult Edit(int project = 0, int id = 0, bool isdelete = false)
        {
            var defects = _context.defects.Include(d => d.defect_history).Include(d => d.defects_attachments).SingleOrDefault(r => r.project_id == project && r.defect_id == id);

            var defect_history = defects?.defect_history.OrderByDescending(dh => dh.effective_date).FirstOrDefault();

            if (defects == null || defect_history == null || !role.IsAuthenticated() || project == 0)
            {
                return PageBuilder.Error403(project);
            }
            ;
            if(!role.IsSupplierDOrAbove())
            {
                ;
                return RedirectToAction("View", new { project = project, defect_id = id });
            }

            return View("defects_Edit", new ViewModel<defects>(project, defects
                , new MultiField("attachment", "defect_attachment_id", defects.defects_attachments, new AttachmentUpload())
                , new Dropdown("module_id", _context.project_modules(project))
                , new Dropdown("found_by", _context.project_users(project), "user_id")
                , new Dropdown("defect_type_id", _context.defect_types)
, new Dropdown("phase_id", _context.phases)
                ));
        }    

        [HttpPost]
        public IActionResult Edit(int project = 0, defects defects = null, List<int> defect_attachment_id = null, bool delete_screenshot = false)
        {
            encoder.Encode(defects);

            if (defects == null || !role.IsSupplierDOrAbove() || project != defects?.project_id || project == 0) { return PageBuilder.Error403(project); }

            if(defects.module_id == -99) { defects.module_id = null; }

            var files = _context.GetFormFilesAsync(HttpContext).Result;

            if (ModelState.IsValid)
            {
                var existing_defect = _context.defects.AsNoTracking().Single(r => r.defect_id == defects.defect_id);
                database.UpdateLinked<defects_attachments>(_context.defects_attachments.Where(da => da.defect_id == defects.defect_id)
                  , "defect_attachment_id", defect_attachment_id
                  , (i) => new defects_attachments()
                  {
                      defect_id = defects.defect_id
                    ,
                      attachment_content = files[i]?.attachment_content
                    ,
                      attachment_filename = encoder.Encode(files[i]?.attachment_filename)
                    ,
                      attachment_mimetype = encoder.Encode(files[i]?.attachment_mimetype)
                  },
                    (existing, i) =>
                    {
                        if(files[i] != null && files[i]?.attachment_content != null)
                        {
                            existing.attachment_content = files[i]?.attachment_content;
                            existing.attachment_filename = encoder.Encode(files[i]?.attachment_filename);
                            existing.attachment_mimetype = encoder.Encode(files[i]?.attachment_mimetype);
                        }
                    });

                database.Update(defects);
                ;

                    return RedirectToAction("Index", new { project = project });
            }
            return PageBuilder.ModelStateError(project, role,"defects edit", ModelState);
        }

        public IActionResult AssignOrUpdate(int project = 0, int defect_id = 0)
        {
            var defects = _context.defects
                .Include(d => d.module)
                .Include(d => d.found_byNavigation)
                .Include(r => r.last_modified_byNavigation)
                .Include(d => d.defects_attachments)
                .Include(d => d.defect_history).ThenInclude(dha => dha.defect_status)
                .Include(d => d.defect_history).ThenInclude(dha => dha.priority)
                .Include(d => d.defect_history).ThenInclude(dha => dha.severity)
                .Include(d => d.defect_history).ThenInclude(dha => dha.assigned_toNavigation)
                .Include(d => d.defect_history).ThenInclude(dha => dha.closure_reason)
                .Include(d => d.defect_type)
                .Include(d => d.phase).SingleOrDefault(r => r.project_id == project && r.defect_id == defect_id);          

            if (defects == null || !role.IsAuthenticated() || project == 0)
            {
                return PageBuilder.Error403(project);
            }

            var defect_container = new DefectContainer(defects, _context);

            var viewname = "";

            if (role.IsSupplierDOrAbove() )
            {
                viewname = "defects_AssignOrUpdateAuth";
            }
            else if (defect_container.defect_status_id == 2 || defect_container.defect_status_id == 3)
            {
                viewname = "defects_AssignOrUpdate";
            }
            else
            {
                viewname = "defects_AssignOrUpdateViewOnly";
            }

            return View(viewname, new ViewModel<DefectContainer>(project, defect_container
               , new MultiField("attachment", "defect_attachment_id", defects.defects_attachments, new AttachmentUpload())
               , new Dropdown("assigned_to", _context.project_users(project), "user_id", defect_container.assigned_to == null ? (object)-97 : null)
                , new Dropdown("priority_id", _context.priorities)
                , new Dropdown("severity_id", _context.severities, true, @"
<b>Now</b>: Needs to be fixed now e.g. a patch to the current release <br/>
<b>Next</b>: Needs to be fixed in the next release <br/>
<b>Before</b>: Needs to be fixed before go-live <br/>
<b>After</b>: Can be fixed after go-live 
")
               , new Dropdown("defect_status_id", _context.defect_status.Where(d => d.defect_status_id == 2 || d.defect_status_id == 3 || (d.defect_status_id > 3 && role.IsSupplierDOrAbove())))
               , new Dropdown("closure_reason_id", _context.closure_reason)
               , new Dropdown("module_id", _context.project_modules(project))
               , new Dropdown("found_by", _context.project_users(project), "user_id")
               , new Dropdown("defect_type_id", _context.defect_types)
               , new Dropdown("phase_id", _context.phases))
               );


        }

        [HttpPost]
        public IActionResult AssignOrUpdate(int project, defect_history defect_history, List<int> defect_attachment_id = null, bool delete_screenshot = false, string external_ref = null)
        {
            var defects = _context.defects.AsNoTracking().Include(d => d.module)
                .Include(d => d.found_byNavigation)
                .Include(r => r.last_modified_byNavigation)
                .Include(d => d.defects_attachments)
                .Include(d => d.defect_history).ThenInclude(dha => dha.defect_status)
                .Include(d => d.defect_history).ThenInclude(dha => dha.priority)
                .Include(d => d.defect_history).ThenInclude(dha => dha.severity)
                .Include(d => d.defect_history).ThenInclude(dha => dha.assigned_toNavigation)
                .Include(d => d.defect_history).ThenInclude(dha => dha.closure_reason)
                .Include(d => d.defect_type)
                .Include(d => d.phase).SingleOrDefault(d => d.defect_id == defect_history.defect_id);

            if (defects == null || defect_history == null || !role.IsAuthenticated() || project != defects?.project_id || project == 0) { return PageBuilder.Error403(project); }

            if (defect_history.assigned_to == -99) { defect_history.assigned_to = null; }

            if (ModelState.IsValid)
            {
                var dhfirst = defects.defect_history.OrderByDescending(dhf => dhf.effective_date).FirstOrDefault();

                defect_history.effective_date = DateTime.Now;
                defect_history.defect_history_id = 0;

                var files = _context.GetFormFilesAsync(HttpContext).Result;

                database.UpdateLinked<defects_attachments>(_context.defects_attachments.Where(da => da.defect_id == defects.defect_id)
  , "defect_attachment_id", defect_attachment_id
  , (i) => new defects_attachments()
  {
      defect_id = defects.defect_id
    ,
      attachment_content = files[i]?.attachment_content
    ,
      attachment_filename = encoder.Encode(files[i]?.attachment_filename)
    ,
      attachment_mimetype = encoder.Encode(files[i]?.attachment_mimetype)
  },
    (existing, i) =>
    {
        if (files[i] != null && files[i]?.attachment_content != null)
        {
            existing.attachment_content = files[i]?.attachment_content;
            existing.attachment_filename = encoder.Encode(files[i]?.attachment_filename);
            existing.attachment_mimetype = encoder.Encode(files[i]?.attachment_mimetype);
        }
    });

                database.Add(defect_history);

                try
                {
                    _context.Database.ExecuteSqlCommand("UPDATE defects SET external_ref = @externalref WHERE defect_id = @defectid",new SqlParameter("@externalref",external_ref ?? ""), new SqlParameter("@defectid", defect_history.defect_id));
                }
                catch(Exception ex)
                {
                    Email.SendErrorText(ex.Message);
                }
                



                if (defect_history.assigned_to != null && dhfirst?.assigned_to != defect_history?.assigned_to)
                {
                    //Send an email to the assigned user
                    Email.Send(
                    _context.users.Single(u => u.user_id == defect_history.assigned_to)
                    , "Defect assigned to you", "A defect has been assigned to you:" + Environment.NewLine + Environment.NewLine + $"Name: {defects.name}" + Environment.NewLine + $"Due: {defects.due_date?.ToShortDateString() ?? "None"}" + Environment.NewLine + Environment.NewLine + $@"https://tmt.web01.lincoln.ac.uk/defects/AssignOrUpdate/{project}/?defect_id={defects.defect_id}");

                }

                if (defect_history.defect_status_id == 3 && dhfirst?.defect_status_id != 3)
                {
                    //Send an email to the managers
                    Email.Send(
                _context.projects.Include(p => p.users_projects_roles).ThenInclude(upr => upr.user).ToList().SelectMany(p => p.users_projects_roles).Where(upr => (upr.role_id == 4 || upr.user.is_administrator) && upr.project_id == project).Select(upr => upr.user)
                , "Defect marked as resolved", (defect_history.last_modified_byNavigation?.name ?? "A user")+" has marked a defect as resolved:" + Environment.NewLine + Environment.NewLine + $"Name: {defects.name}" + Environment.NewLine + $"Due: {defects.due_date?.ToShortDateString() ?? "None"}" + Environment.NewLine + Environment.NewLine + $@"https://tmt.web01.lincoln.ac.uk/defects/AssignOrUpdate/{project}/?defect_id={defects.defect_id}");

                }

                return RedirectToAction("Index", new { project = project });
            }

            return PageBuilder.ModelStateError(project, role, "Defects assignorupdate", ModelState);
        }

        public IActionResult Delete(int project = 0, int id = 0)
        {
            var defects = _context.defects
                  .Include(d => d.module)
                 .Include(d => d.found_byNavigation)
                 .Include(r => r.last_modified_byNavigation)
                 .Include(d => d.defect_history).Include(d => d.defects_attachments).SingleOrDefault(r => r.project_id == project && r.defect_id == id);

            var defect_history = defects?.defect_history.OrderByDescending(dh => dh.effective_date).FirstOrDefault();

            if (defects == null || defect_history == null || !role.IsAuthenticated() || project == 0)
            {
                return PageBuilder.Error403(project);
            }

            return View("defects_Delete", new ViewModel<defects>(project, defects
                , new MultiField("attachment", "defect_attachment_id", defects.defects_attachments, new AttachmentUpload())
                , new Dropdown("module_id", _context.project_modules(project))
                , new Dropdown("found_by", _context.project_users(project), "user_id")
                ));
        }       

        public IActionResult DeleteConfirmed(int project = 0, int id = 0)
        {
            var defects = _context.defects.Include(d => d.defect_history).Include(d => d.defects_attachments).SingleOrDefault(m => m.defect_id == id);

            if (defects == null || !role.IsSupplierDOrAbove() || project != defects?.project_id || project == 0)
            {
                return PageBuilder.Error403(project);
            }

            database.RemoveRange(defects.defects_attachments.ToList());
            database.RemoveRange(defects.defect_history.ToList());
            database.Remove(defects);

            return RedirectToAction("Index", new { project = project });
        }

        public IActionResult DownloadAttachment(int project, int id)
        {
            var attachment = _context.defects_attachments.SingleOrDefault(t => t.defect_attachment_id == id);

            if (!role.IsAuthenticated() || attachment == null) { return PageBuilder.Error403(project); }

            HttpContext.Response.ContentType = attachment.attachment_mimetype;
            FileContentResult result = new FileContentResult(attachment.attachment_content, attachment.attachment_mimetype)
            {
                FileDownloadName = attachment.attachment_filename
            };

            return result;
        }

        public IActionResult T1Report()
        {
            const int p = 2;

            var t1 = _context.project_defects(p)
                 .Include(d => d.module)
                 .Include(d => d.found_byNavigation)
                 .Include(r => r.last_modified_byNavigation)
                 .Include(d => d.defects_attachments)
                 .Include(d => d.defect_history).ThenInclude(dha => dha.defect_status)
                 .Include(d => d.defect_history).ThenInclude(dha => dha.priority)
                 .Include(d => d.defect_history).ThenInclude(dha => dha.severity)
                 .Include(d => d.defect_history).ThenInclude(dha => dha.assigned_toNavigation)
                 .Include(d => d.defect_history).ThenInclude(dha => dha.closure_reason)
                 .Include(d => d.defect_type)
                 .Include(d => d.phase).ToList();

            var t1a = t1.Select(d => new DefectContainer(d, _context));

            var testingContext = t1a.ToList().Where(d => d.assigned_to == 24 && d.defect_status_id <= 2);

            var testingContext2 = t1a.ToList().Where(d => d.assigned_to == 24 && d.defect_status_id >= 3);

            return PageBuilder.GenerateReport(p, 0, testingContext.ToList(), testingContext2.ToList(), "Assigned", "Resolved or Closed"
                    , new Column("defect_id", 1, true)
                     , new Column("assigned_toNavigation.name", 1, "(None)", "assigned to")
                       , new Column("defect_type.name", 1, "defect type")
                        , new Column("priority.name", 1, "latest priority")
                    , new Column("name", 1)
                                      , new Column("description", 1, "description")
                    , new Column("module.name", 1, "(None)")
                                     , new Column("found_date", 1)
                 , new Column("found_byNavigation.name", 1, "(None)", "found by")
                 , new Column("external_ref", 1, label: "External reference")
                 );
        }

        public IActionResult UploadBulkImport(int project)
        {
            return View("defects_bulkimport", project);
        }

        [HttpPost]
        public IActionResult UploadBulkImport(int project, IFormFile fileuploaded)
        {
            ;
            //Processes uploaded bulk import spreadsheet

            if (fileuploaded == null) { return PageBuilder.Back(role, "File is required. Click Choose File on the previous page.", "", "defects", "UploadBulkImport", project, true); }

            if (!role.IsManager() || project == 0) { return PageBuilder.Error403(project); }

            //If the file uploaded is an Excel spreadsheet
            if (fileuploaded.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            {

                var rs = fileuploaded.OpenReadStream();

                using (var ms = new MemoryStream())
                {
                    rs.CopyTo(ms);

                    //Use DocumentFormat.OpenXml library to extract the data from the uploaded spreadsheet

                    SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(rs, false);

                    var sheet = spreadsheetDocument.WorkbookPart.GetPartsOfType<WorksheetPart>().First();

                    var sheetdata = sheet.Worksheet.GetFirstChild<SheetData>();

                    SharedStringTable sst_ = spreadsheetDocument.WorkbookPart.GetPartsOfType<SharedStringTablePart>().First().SharedStringTable;

                    var sst = sst_.ChildElements.Select(u => u.InnerText);

                    var rows = sheetdata.Descendants<Row>();

                    var newdefect = new List<DefectContainer>();
                    string error_strings = "";
                    string error_string = "";
                    var project_id = project;

                    //For each row in the spreadsheet
                    foreach (var r in rows)
                    {
                        //Put the data in the correct form - an array of strings

                        var allcells2 = r.Elements<Cell>();

                        var allcells = allcells2.Select(c => new ExcelCell(c, sst)).Where(c => c.column < 999);

                        var rownum = allcells.FirstOrDefault()?.row;

                        //If row is not blank
                        if (allcells.Any() && rownum >= 5)
                        {

                            error_string = "";
                            var defect = new DefectContainer();

                        var cells = new string[13];

                            for (int k = 0; k < cells.Length; k++)
                            {
                                cells[k] = "";
                            }

                            foreach (var c in allcells)
                            {
                                cells[Math.Min(c.column - 1, cells.Length - 1)] = c.value;
                            }

                            encoder.Encode(cells);

                            //With a new test cycles test object, populate its fields with the validated input from the sheet

                            defect.name = Validation.ValidateLength("name", cells[0], 0, 255, ref error_string);
                            defect.defect_type_id = Validation.ValidateIDOrDefault("defect_type_id", cells[1], _context.defect_types.AsEnumerable(), ref error_string, 1);

                            defect.phase_id = Validation.ValidateIDOrDefault("phase_id", cells[2], _context.phases.AsEnumerable(), ref error_string, 1);
                            defect.description = Validation.ValidateLength("description", cells[3], 0, 999999, ref error_string);
                            defect.tags = Validation.ValidateLength("tags", cells[4], -1, 255, ref error_string);
                            defect.module_id = Validation.ValidateIDOrNull("module_id", cells[5], _context.project_modules(project).AsEnumerable(), ref error_string);
                            defect.found_by = Validation.ValidateID("user_id", cells[6], _context.project_users(project), ref error_string, "raised_by");
                            defect.assigned_to = Validation.ValidateIDOrNull("user_id", cells[7], _context.project_users(project), ref error_string, "assigned_to");
                            try
                            {
                                defect.found_date = DateTime.FromOADate(Convert.ToInt32(cells[8]));

                                if (defect.found_date < DateTime.Now.AddYears(-1) || defect.found_date > DateTime.Now.AddYears(10))
                                {
                                    error_string += "Raised date out of range; ";
                                }
                            }
                            catch
                            {
                                error_string += "Invalid raised date specified; ";
                            }

                            try
                            {                                                         
                                if(cells[9] != "")
                                {
                                    defect.due_date = DateTime.FromOADate(Convert.ToInt32(cells[9]));

                                    if (defect.due_date < DateTime.Now.AddYears(-1) || defect.due_date > DateTime.Now.AddYears(10))
                                    {
                                        error_string += "Due date out of range; ";
                                    }
                                }
                            }
                            catch
                            {
                                error_string += "Invalid due date specified; ";
                            }

                            defect.external_ref = Validation.ValidateLength("external_ref", cells[10], -1, 255, ref error_string);
                            defect.priority_id = Validation.ValidateIDOrDefault("priority_id", cells[11], _context.priorities, ref error_string, 3);
                            defect.severity_id = Validation.ValidateIDOrDefault("severity_id", cells[12], _context.severities, ref error_string, 5);

                            defect.defect_status_id = (defect.assigned_to == null) ? 1 : 2;

                            //If there's an error, add the row number
                            if (error_string != "")
                            {
                                error_string = "Row " + rownum + ": " + error_string + "<br>";
                                error_strings += error_string;
                            }
                            else { newdefect.Add(defect); }

                        }


                    }

                    //If there's no errors
                    if (ModelState.IsValid && error_strings == "")
                    {
                        try
                        {
                            foreach (var r in newdefect)
                            {
                                //Add the new defect run to the database

                                var defect = new defects();

                                defect.name = r.name;
                                defect.defect_type_id = r.defect_type_id;
                                defect.project_id = project;
                                defect.phase_id = r.phase_id;
                                defect.description = r.description;
                                defect.tags = r.tags;
                                defect.module_id = r.module_id;
                                defect.found_by = r.found_by;
                                defect.found_date = r.found_date;
                                defect.due_date = r.due_date;
                                defect.last_modified_by = role.user_id;
                                defect.last_modified_date = DateTime.Now;

                                database.Add(defect);

                                var defect_history = new defect_history() { defect_id = defect.defect_id, defect_status_id = r.defect_status_id, closure_reason_id = 1, status_comment = "Defect created", priority_id = r.priority_id ?? 3, severity_id = r.severity_id, effective_date = DateTime.Now, assigned_to = r.assigned_to };

                                database.Add(defect_history);
                            }

                            //Return the success page
                            return PageBuilder.Back(role, "Bulk Import Defects", newdefect.Count() + @" records successfully created.", "defects", "Index", project);
                        }
                        catch (Exception e)
                        {
                            //Return the exception page
                            Email.SendError("defects bulk import", e);
                            return PageBuilder.Back(role, "Bulk Import Defects", e.ToString(), "defects", "Index", project);
                        }
                    }
                    else
                    {
                        //Return the page that shows the errors
                        return PageBuilder.Back(role, "Bulk Import Defects", "File contained the following errors and was not loaded: <br><br>" + error_strings, "defects", "Index", project);
                    }
                }


            }
            //Return the exception page
            return PageBuilder.Back(role, "Invalid file", "", "defects", "Index", project, true);
        }

        public IActionResult GenerateBulkImport(int project)
        {
            if (!role.IsManager()) { return PageBuilder.Error403(project); }

            //Create a new file response that's a copy of defects_template.xlsx under wwwroot/template, and send it to the user to download

            HttpContext.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            try
            {
                FileContentResult result = new FileContentResult(System.IO.File.ReadAllBytes("wwwroot/templates/defects_template.xlsx"), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                {
                    FileDownloadName = "defects_template.xlsx"
                };
                return result;
            }
            catch
            {
                return PageBuilder.Back(role, "I/O error", "", "defects", "Index", project, true);
            }

        }

    }
}
