using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApplication6;

namespace WebApplication6.Controllers
{
    public class fieldsController : Controller
    {
        private testing2Context _context;

        public fieldsController(testing2Context context)
        {
            _context = context;    
        }

        // GET: fields

        public IActionResult Index(int? id)
        {
            var testing2Context = _context.fields.Where(f => f.table_id == id).Include(f => f.entity).Include(f => f.table);
            ViewData["source_id"] = id;
            ViewData["source_system_id"] = _context.tables.Where(t => t.table_id == id).First().system_id;
            return View(testing2Context.ToList());
        }

        [HttpPost]
        public IActionResult Index(string sourceid, string name)
        {
            var s_id = System.Convert.ToInt32(sourceid);
            var testing2Context = _context.fields.Include(f => f.table).Include(f => f.entity).Where(f => ( f.table_id == s_id || s_id == -1) && (f.field_name + f.table.table_name + f.entity.entity_name + f.field_type).Contains(name)).Take(50);
            if(s_id != -1)
            { 
            ViewData["source_id"] = s_id;
            ViewData["source_system_id"] =testing2Context.First().table.system_id;
            }
            return View(testing2Context.ToList());

        }

        // GET: fields/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            fields fields = _context.fields.Single(m => m.field_id == id);
            if (fields == null)
            {
                return NotFound();
            }

            return View(fields);
        }

        // GET: fields/Create
        public IActionResult Create(int? id)
        {
            ViewData["entity_id"] = new SelectList(_context.entities, "entity_id", "entity");
            ViewData["table_id"] = id;
            return View();
        }

        // POST: fields/Create
        [HttpPost]
        public IActionResult Create(fields fields)
        {
            if (ModelState.IsValid)
            {
                _context.fields.Add(fields);
                _context.SaveChanges();
                return RedirectToAction("Index", new { id = fields.table_id });
            }
            ViewData["entity_id"] = new SelectList(_context.entities, "entity_id", "entity", fields.entity_id);
            ViewData["table_id"] = fields.table_id;
            return View(fields);
        }

        // GET: fields/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            fields fields = _context.fields.Single(m => m.field_id == id);
            if (fields == null)
            {
                return NotFound();
            }
            ViewData["entity_id"] = new SelectList(_context.entities, "entity_id", "entity_name", fields.entity_id);
            ViewData["table_id"] = fields.table_id;
            return View(fields);
        }

        // POST: fields/Edit/5
        [HttpPost]
        
        public IActionResult Edit(fields fields)
        {
            if (ModelState.IsValid)
            {
                _context.Update(fields);
                _context.SaveChanges();
                return RedirectToAction("Index", new { id = fields.table_id });
            }
            ViewData["entity_id"] = new SelectList(_context.entities, "entity_id", "entity", fields.entity_id);
            ViewData["table_id"] = fields.table_id;
            return View(fields);
        }

        // GET: fields/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            fields fields = _context.fields.Single(m => m.field_id == id);
            if (fields == null)
            {
                return NotFound();
            }

            return View(fields);
        }

        // POST: fields/Delete/5
        [HttpPost, ActionName("Delete")]
        
        public IActionResult DeleteConfirmed(int id)
        {
            fields fields = _context.fields.Single(m => m.field_id == id);
            var return_ = fields.table_id;
            _context.fields.Remove(fields);
            _context.SaveChanges();
            return RedirectToAction("Index",new { id = return_ });
        }
    }
}
