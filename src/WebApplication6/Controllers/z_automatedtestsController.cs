﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;


// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApplication6.Controllers
{

    public class automatedtestsController : Controller
    {
        private testing2Context _context;
        private RoleInfo role;
        private IRoleService _rs;
        private IDatabase database;
        private IHtmlEncoderService encoder;

        public automatedtestsController(testing2Context context, IRoleService rs, IDatabase database, IHtmlEncoderService encoder)
        {
            _context = context;
            _rs = rs;
            role = rs.GetRole();
            this.database = database;
            this.encoder = encoder;
        }

        public IActionResult Index(int project = 0, string name = "", int page = 1)
        {
            name = encoder.Encode(name ?? "");

            if (!role.IsAdministrator()) { return PageBuilder.Error403(project); }

            /*
             var testingContext = _context.project_automatedtests(project).Where(at => (name == "" || at.name.Contains(name))).ToList();

            return PageBuilder.Index("Search Automated Tests", role, "automatedtests", testingContext.ToPagedList(page), project, 0, Request.QueryString.HasValue
                , new Filters(
                     new Textbox("name", name)
                    )
                 , new Links(false, false, new Link(("automatedtests", "Index", "Run Test Now") ))
                  , new Options()
                 , new Column("name", 2)
                 , new Column("requirements.name", 2)
                 , new Column("run_frequency", 2)
                 , new Column("last_run_date", 2)
                 , new Column("last_run_outcome", 2)
                );
            */

            return PageBuilder.Back(role, "Automated Tests", "", "requirements", "Index", project);
        }
    }
}
