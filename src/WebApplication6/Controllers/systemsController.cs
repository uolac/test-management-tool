using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApplication6;

namespace WebApplication6.Controllers
{
    public class systemsController : Controller
    {
        private testing2Context _context;

        public systemsController(testing2Context context)
        {
            _context = context;    
        }

        // GET: systems
        public IActionResult Index()
        {
            return View(_context.systems.ToList());
        }


        //POST
        [HttpPost]
        
        public IActionResult Index(string name)
        {
            return View(_context.systems.Where(s => s.system_name.Contains(name)).ToList());
        }

        // GET: systems/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            systems systems = _context.systems.Single(m => m.system_id == id);
            if (systems == null)
            {
                return NotFound();
            }

            return View(systems);
        }

        // GET: systems/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: systems/Create
        [HttpPost]
        
        public IActionResult Create(systems systems)
        {
            if (ModelState.IsValid)
            {
                _context.systems.Add(systems);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(systems);
        }

        // GET: systems/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            systems systems = _context.systems.Single(m => m.system_id == id);
            if (systems == null)
            {
                return NotFound();
            }
            return View(systems);
        }

        // POST: systems/Edit/5
        [HttpPost]
        
        public IActionResult Edit(systems systems)
        {
            if (ModelState.IsValid)
            {
                _context.Update(systems);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(systems);
        }

        // GET: systems/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            systems systems = _context.systems.Single(m => m.system_id == id);
            if (systems == null)
            {
                return NotFound();
            }

            return View(systems);
        }

        // POST: systems/Delete/5
        [HttpPost, ActionName("Delete")]
        
        public IActionResult DeleteConfirmed(int id)
        {
            systems systems = _context.systems.Single(m => m.system_id == id);
            _context.systems.Remove(systems);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
