﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using Microsoft.AspNetCore.Http;
using System.IO;
using System.Net.Http.Headers;


using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;


namespace WebApplication6.Controllers
{  

    public class scheduletestrunsController : Controller
    {

        private testing2Context _context;
        private RoleInfo role;
        private IRoleService _rs;
        private IDatabase database;
        private IHtmlEncoderService encoder;

        public scheduletestrunsController(testing2Context context, IRoleService rs, IDatabase database, IHtmlEncoderService encoder)
        {
            _context = context;
            _rs = rs;
            role = rs.GetRole();
            this.database = database;
            this.encoder = encoder;
        }

        public IActionResult Index(int project = 0, int scheduled_user_id = 0, int scheduled_status_id = 0, int test_id = 0, int test_cycle_id = 0, DateTime? start_date = null, DateTime? end_date = null, string button_name = "", int page = 1)
        {
            button_name = encoder.Encode(button_name ?? "");

            project = role.project;
            var prj = _context.projects.SingleOrDefault(p => p.project_id == project);

            if (!role.IsAuthenticated() || prj == null) { return PageBuilder.Error403(project); }
            
            if (test_cycle_id == 0) { test_cycle_id = prj.current_test_cycle_id ?? 0; }

            var testingContext = _context.test_cycles_tests
                 .Include(r => r.test_cycle)
                 .Include(r => r.test)
                 .Include(r => r.scheduled_userNavigation)
                 .Include(r => r.last_modified_byNavigation)
                 .Include(r => r.scheduled_status)
                 .Where(r => (r.test.project_id == project && r.test.draft_status_id != 3)
                 && (r.scheduled_userNavigation.user_id == scheduled_user_id || scheduled_user_id == 0)
                 && (r.scheduled_status_id == scheduled_status_id || scheduled_status_id == 0)
                 && (r.test_id == test_id || test_id == 0)
                 && (r.test_cycle_id == test_cycle_id || test_cycle_id == 0)).ToList()
                 .Where(r => (r.scheduled_end_date >= start_date || start_date == null)
                 && (r.scheduled_start_date <= end_date || end_date == null)).OrderByDescending(r => r.scheduled_start_date);

            if (button_name == "Generate Report")
            {
                return PageBuilder.GenerateReport(project, test_cycle_id, testingContext.ToList()
                    , new Column("test_cycle_test_id", 1, true)
                    , new Column("test_id", 1, true)
                    , new Column("test.name", 1)
                    , new Column("test_cycle.name", 1)
                 , new Column("scheduled_userNavigation.name", 1)
                 , new Column("scheduled_start_date", 1)
                 , new Column("scheduled_end_date", 1)
                 , new Column("scheduled_status.name", 1)
                 , new Column("last_modified_byNavigation.name", 2, "", "last modified by")
                 , new Column("last_modified_date", 2));
            }

            return View("scheduletestruns_index", new IndexViewModel<test_cycles_tests>(role, project, test_cycle_id, Request.QueryString.HasValue, testingContext.ToList(), page
                , new Filters(
                        new Dropdown("test_id", _context.project_tests(project), test_id, true)
                     , new Dropdown("test_cycle_id", _context.project_testcycles(project), test_cycle_id, true)
                    , new Dropdown("scheduled_user_id", _context.project_users(project), "user_id", scheduled_user_id)
                    , new Dropdown("scheduled_status_id", _context.scheduled_status, scheduled_status_id, true)
                    )
                  , new Column("test_cycle.name", 1, "Default")
                 , new Column("test.name", 1)
                 , new Column("last_modified_byNavigation.name", 2, "", "scheduled by")
                 , new Column("scheduled_userNavigation.name", 1, "", "to be run by")
                 , new Column("scheduled_start_date", 1)
                 , new Column("scheduled_end_date", 1)
                 , new Column("scheduled_status.name", 1)
                ));
        }

        public IActionResult Create(int project = 0, int test_cycles_test_id = 0, int test_id = 0)
        {
            var project_ = _context.projects.AsNoTracking().SingleOrDefault(t => t.project_id == project);

            if (!role.IsManager() || project == 0 || project_ == null) { return PageBuilder.Error403(project); }

            return View("scheduletestruns_create", new ViewModel<test_cycles_tests>(project, new test_cycles_tests() { test_id = test_id, scheduled_user = role.user_id, scheduled_start_date = DateTime.Now, scheduled_end_date = DateTime.Now, scheduled_status_id = 1, test_cycle_id = project_.current_test_cycle_id ?? 0 }
                , new Dropdown("test_id", _context.project_tests(project).Where(t => t.draft_status_id != 3))
                , new Dropdown("test_cycle_id",_context.project_testcycles(project))
                , new Dropdown("scheduled_user",_context.project_users(project), "user_id", -97)
                ));
        }

        
        [HttpPost]
        public IActionResult Create(int project = 0, test_cycles_tests test_cycles_tests = null)
        {

            var test = _context.tests.SingleOrDefault(t => t.test_id == test_cycles_tests.test_id);

            if (test_cycles_tests == null || test == null || !role.IsManager() || test?.project_id != project || project == 0) { return PageBuilder.Error403(project); }

            if (ModelState.IsValid)
            {
                database.Add(test_cycles_tests);

                return RedirectToAction("Index", new { project = project });
            }

            return PageBuilder.ModelStateError(project, role, "Scheduletestruns create", ModelState);
        }

        public IActionResult Edit(int project = 0, int id = 0 , bool isdelete = false)
        {
            var test_cycles_test_pre = _context.test_cycles_tests
.Include(r => r.test_cycle)
.Include(r => r.test)
.Include(r => r.scheduled_userNavigation)
.Include(r => r.scheduled_status).ToList();

var test_cycles_test = test_cycles_test_pre.SingleOrDefault(r => r.test.project_id == project && r.test_cycle_test_id == id);

            if (test_cycles_test == null || !role.IsAuthenticated() || project == 0)
            {
                return PageBuilder.Error403(project);
            }

            return View("scheduletestruns_Edit", new ViewModel<test_cycles_tests>(project, test_cycles_test
                , new Dropdown("test_cycle_id", _context.project_testcycles(project))
                , new Dropdown("scheduled_user", _context.project_users(project), "user_id")
                ));
        }

        [HttpPost]
        public IActionResult Edit(int project = 0, test_cycles_tests test_cycles_tests = null)
        {
            var tct_t_p = _context.tests.AsNoTracking().SingleOrDefault(tc => tc.test_id == test_cycles_tests.test_id)?.project_id;

            if (test_cycles_tests == null || !role.IsManager() || tct_t_p != project || project == 0) { return PageBuilder.Error403(project); }

            if (ModelState.IsValid)
            {

                database.Update(test_cycles_tests);
                ;

                    return RedirectToAction("Index", new { project = project });
            }
            return PageBuilder.ModelStateError(project, role, "Scheduletestruns edit", ModelState);
        }

        public IActionResult Delete(int project = 0, int id = 0)
        {
            var test_cycles_test_pre = _context.test_cycles_tests
.Include(r => r.test_cycle)
.Include(r => r.test)
.Include(r => r.scheduled_userNavigation)
.Include(r => r.scheduled_status).ToList();

            var test_cycles_test = test_cycles_test_pre.SingleOrDefault(r => r.test.project_id == project && r.test_cycle_test_id == id);

            if (test_cycles_test == null || !role.IsAuthenticated() || project == 0)
            {
                return PageBuilder.Error403(project);
            }

            return View("scheduletestruns_Delete", new ViewModel<test_cycles_tests>(project, test_cycles_test));
        }

        public IActionResult DeleteConfirmed(int project = 0, int id = 0)
        {
            var test_cycles_test = _context.test_cycles_tests
.Include(r => r.test_cycle)
.Include(r => r.test)
.Include(r => r.scheduled_userNavigation)
.Include(r => r.scheduled_status)
.SingleOrDefault(r => r.test.project_id == project && r.test_cycle_test_id == id);

            var tct_t_p = _context.tests.AsNoTracking().SingleOrDefault(tc => tc.test_id == test_cycles_test.test_id)?.project_id;

            if (test_cycles_test == null || !role.IsManager() || tct_t_p != project || project == 0)
            {
                return PageBuilder.Error403(project);
            }

            database.Remove(test_cycles_test);
            ;

            return RedirectToAction("Index", new { project = project });
        }


        public IActionResult UploadBulkImport(int project)
        {
            return View("scheduletestruns_bulkimport", project);
        }

        [HttpPost]
        public IActionResult UploadBulkImport(int project, IFormFile fileuploaded)
        {
            ;
            //Processes uploaded bulk import spreadsheet

            if (fileuploaded == null) { return PageBuilder.Back(role, "File is required. Click Choose File on the previous page.", "", "scheduletestruns", "UploadBulkImport", project, true); }

            if (!role.IsManager() || project == 0) { return PageBuilder.Error403(project); }

            //If the file uploaded is an Excel spreadsheet
            if (fileuploaded.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            {

                var rs = fileuploaded.OpenReadStream();

                using (var ms = new MemoryStream())
                {
                    rs.CopyTo(ms);

                    //Use DocumentFormat.OpenXml library to extract the data from the uploaded spreadsheet

                    SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(rs, false);

                    var sheet = spreadsheetDocument.WorkbookPart.GetPartsOfType<WorksheetPart>().First();

                    var sheetdata = sheet.Worksheet.GetFirstChild<SheetData>();

                    SharedStringTable sst_ = spreadsheetDocument.WorkbookPart.GetPartsOfType<SharedStringTablePart>().First().SharedStringTable;

                    var sst = sst_.ChildElements.Select(u => u.InnerText);

                    var rows = sheetdata.Descendants<Row>();

                    var newscheduled = new List<test_cycles_tests>();
                    string error_strings = "";
                    string error_string = "";
                    var project_id = project;

                    //For each row in the spreadsheet
                    foreach (var r in rows)
                    {
                        //Put the data in the correct form - an array of strings

                        var allcells2 = r.Elements<Cell>();

                        var allcells = allcells2.Select(c => new ExcelCell(c, sst)).Where(c => c.column < 999);

                        var rownum = allcells.FirstOrDefault()?.row;

                        //If row is not blank
                        if (allcells.Any() && rownum >= 5)
                        {

                            error_string = "";
                            var scheduled = new test_cycles_tests();

                            var cells = new string[7];

                            for (int k = 0; k < cells.Length; k++)
                            {
                                cells[k] = "";
                            }

                            foreach (var c in allcells)
                            {
                                cells[Math.Min(c.column - 1, cells.Length - 1)] = c.value;
                            }

                            encoder.Encode(cells);

                            //With a new test cycles test object, populate its fields with the validated input from the sheet

                            scheduled.test_id = Validation.ValidateID("test_id", cells[0], _context.project_tests(project), ref error_string);
                            scheduled.test_cycle_id = Validation.ValidateID("test_cycle_id", cells[1], _context.project_testcycles(project), ref error_string);
                            scheduled.scheduled_user = Validation.ValidateID("user_id", cells[2], _context.project_users(project), ref error_string, "scheduled_user");
                            try
                            {
                                scheduled.scheduled_start_date = DateTime.FromOADate(Convert.ToInt32(cells[3]));

                                if(scheduled.scheduled_start_date < DateTime.Now.AddDays(-1) || scheduled.scheduled_start_date > DateTime.Now.AddYears(10))
                                {
                                    error_string += "Start date out of range; ";
                                }
                            }
                            catch
                            {
                                error_string += "Invalid start date specified; ";
                            }

                            try
                            {
                                scheduled.scheduled_end_date = DateTime.FromOADate(Convert.ToInt32(cells[4]));

                                if (scheduled.scheduled_end_date < DateTime.Now.AddDays(-1) || scheduled.scheduled_end_date > DateTime.Now.AddYears(10))
                                {
                                    error_string += "End date out of range; ";
                                }
                            }
                            catch
                            {
                                error_string += "Invalid end date specified; ";
                            }

                            scheduled.scheduled_status_id = 1;

                            scheduled.last_modified_by = role.user_id;
                            scheduled.last_modified_date = DateTime.Now;

                            //If there's an error, add the row number
                            if (error_string != "")
                            {
                                error_string = "Row " + rownum + ": " + error_string + "<br>";
                                error_strings += error_string;
                            }
                            else { newscheduled.Add(scheduled); }

                        }


                    }

                    //If there's no errors
                    if (ModelState.IsValid && error_strings == "")
                    {
                        try
                        {
                            foreach (var r in newscheduled)
                            {
                                //Add the new scheduled run to the database
                                database.Add(r);
                            }

                            //Return the success page
                            return PageBuilder.Back(role, "Bulk Import Scheduled Test Runs", newscheduled.Count() + @" records successfully created.", "scheduletestruns", "Index", project);
                        }
                        catch (Exception e)
                        {
                            //Return the exception page
                            Email.SendError("scheduled runs bulk import", e);
                            return PageBuilder.Back(role, "Bulk Import Scheduled Test Runs", e.ToString(), "scheduletestruns", "Index", project);
                        }
                    }
                    else
                    {
                        //Return the page that shows the errors
                        return PageBuilder.Back(role, "Bulk Import Scheduled Test Runs", "File contained the following errors and was not loaded: <br><br>" + error_strings, "scheduletestruns", "Index", project);
                    }
                }


            }
            //Return the exception page
            return PageBuilder.Back(role, "Invalid file", "", "scheduletestruns", "Index", project, true);
        }

        public IActionResult GenerateBulkImport(int project)
        {
            if (!role.IsManager()) { return PageBuilder.Error403(project); }

            //Create a new file response that's a copy of scheduletestruns.xlsx under wwwroot/template, and send it to the user to download

            HttpContext.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            try
            {
                FileContentResult result = new FileContentResult(System.IO.File.ReadAllBytes("wwwroot/templates/scheduletestruns_template.xlsx"), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                {
                    FileDownloadName = "scheduletestruns_template.xlsx"
                };
                return result;
            }
            catch
            {
                return PageBuilder.Back(role, "I/O error", "", "scheduletestruns", "Index", project, true);
            }

        }

    }
}
