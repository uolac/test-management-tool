﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;


namespace WebApplication6.Controllers
{
   
    public class projectsController : Controller
    {
        private testing2Context _context;
        private RoleInfo role;
        private IRoleService _rs;
        private IDatabase database;
        private IHtmlEncoderService encoder;

        public projectsController(testing2Context context, IRoleService rs, IDatabase database, IHtmlEncoderService encoder)
        {
            _context = context;
            _rs = rs;
            role = rs.GetRole();
            this.database = database;
            this.encoder = encoder;
        }

        public IActionResult Index(int project=0,string name = "", int owner_id = 0, int draft_status_id = 0, int page = 1, DateTime? start_date = null, DateTime? end_date = null)
        {
            name = encoder.Encode(name ?? "");

            if (!role.IsAdministrator()) { return PageBuilder.Error403(project); }

            var testingContext = _context.projects.AsNoTracking()
                .Include(p => p.current_test_cycle)
                .Include(p => p.owner)
                .Where(p => (p.name.ToLower().Contains(name.ToLower()) || name == "")
                && (p.owner.user_id == owner_id || owner_id == 0)).ToList()
                .Where(p => (p.start_date >= start_date || start_date == null)
                 && (p.start_date <= end_date || end_date == null)
                ).OrderBy(r => r.name, new NaturalSortComparer<string>());

            return View("projects_index", new IndexViewModel<projects>(role, project, 0, Request.QueryString.HasValue, testingContext.ToList(), page
                , new Filters(
                     new Textbox("name", name)
                    , new Dropdown("owner_id", _context.users_, "user_id", owner_id)
                    , new Datetime("start_date", start_date?.ToString("yyyy-MM-dd") ?? "2010-01-01")
                    , new Datetime("end_date", end_date?.ToString("yyyy-MM-dd") ?? DateTime.Now.ToString("yyyy-MM-dd"))
                    )
                 , new Column("name", 3)
                 , new Column("owner.name", 2)
                 , new Column("start_date", 1)
                 , new Column("end_date", 1)
                 , new Column("current_test_cycle.name", 2, "Default")
                ));
        }

        public IActionResult Create(int project = 0,int project_id = 0)
        {
            if (!role.IsAdministrator()) { return PageBuilder.Error403(project); }

            return View("projects_create", new ViewModel<projects>(project, new projects() { start_date = DateTime.Now, end_date = DateTime.Now}

                , new MultiField("test cycle","test_cycle_id", new List<test_cycles>() { new test_cycles() { name = "Default", description = "Default test cycle for projects that only have one cycle"} }, new Textbox("name"))
                , new MultiField("module","module_id", new List<modules>(), new Textbox("name"))
                , new MultiField("users","users_projects_roles_id", new List<users_projects_roles>(), new Dropdown("user_id", _context.users.Where(u => u.user_id != 0)), new Dropdown("role_id", _context.roles.Where(r => r.role_id != 2), true, "<b>Tester</b>: Can create and edit Test Runs and create new Defects <br/><b>Test Manager / Project Manager / Supplier (full access)</b>: Full access to the project <br/><b>Supplier (read-only)</b>: Can only view project information and not edit it <br/><b>Supplier (defects access)</b>: Can create, edit and update Defects, but not anything else"))
                ));
        }
        
        [HttpPost]
        public IActionResult Create(int project, projects projects, List<string> test_cycle_name, List<string> module_name, List<int> users_projects_roles_user_id, List<int> users_projects_roles_role_id)
        {
            encoder.Encode(projects);
            encoder.Encode(test_cycle_name);
            encoder.Encode(module_name);

            if (projects == null || !role.IsAdministrator()) { return PageBuilder.Error403(project); }

            if (ModelState.IsValid)
            {
               projects.start_date = DateTime.Now;

                if(projects.owner_id == -99) { projects.owner_id = null; }
                database.Add(projects);

                for (int i = 0;i< test_cycle_name.Count; i++)
                {
                    var test_cycle = new test_cycles() { name = test_cycle_name[i], project_id = projects.project_id };
                    database.Add(test_cycle);
                }

                for (int i = 0; i < module_name.Count; i++)
                {
                    var module = new modules() { name = module_name[i], project_id = projects.project_id };
                    database.Add(module);
                }

                for (int i = 0; i < users_projects_roles_user_id.Count; i++)
                {
                    var users_projects_roles = new users_projects_roles() { user_id = users_projects_roles_user_id[i], role_id = users_projects_roles_role_id[i], project_id = projects.project_id };
                    database.Add(users_projects_roles);
                }

                projects.current_test_cycle_id = _context.project_testcycles(projects.project_id).First().test_cycle_id;

                database.Update(projects);

                return RedirectToAction("Index", new { project = project });
            }

            return PageBuilder.Back(role, "Error", "Error in model state", "projects", "Create", project);
        }

        public IActionResult Edit(int project=0, int id = 0)
        {
            var projects = _context.projects.AsNoTracking().SingleOrDefault(p => p.project_id == id);
            ;
            if (!role.IsAdministrator())
            {
                return PageBuilder.Error403(project);
            }

            return View("projects_edit", new ViewModel<projects>(project,projects
                  , new MultiField("test cycle", "test_cycle_id", false, _context.test_cycles.Where(tc => tc.project_id == id), new Textbox("name"))
                , new MultiField("functional area", "module_id", _context.modules.Where(tc => tc.project_id == id), new Textbox("name"))
                , new MultiField("user", "users_projects_roles_id", _context.users_projects_roles.Where(upr => upr.project_id == id), new Dropdown("user_id", _context.users.Where(u => u.user_id != 0), false), new Dropdown("role_id", _context.roles.Where(r => r.role_id != 2), true, "<b>Tester</b>: Can create and edit Test Runs and create new Defects <br/><b>Test Manager / Project Manager / Supplier (full access)</b>: Full access to the project <br/><b>Supplier (read-only)</b>: Can only view project information and not edit it <br/><b>Supplier (defects access)</b>: Can create, edit and update Defects, but not anything else"))
              , new Dropdown("owner_id", _context.users_, "user_id")
                , new Dropdown("current_test_cycle_id", _context.test_cycles.Where(tc => tc.project_id == id), "test_cycle_id")          
                ));
        }

        
        [HttpPost]
        public IActionResult Edit(int project, projects projects, List<string> test_cycle_name, List<int> test_cycle_id, List<string> module_name, List<int> module_id, List<int> users_projects_roles_id, List<int> users_projects_roles_user_id, List<int> users_projects_roles_role_id)
        {
            encoder.Encode(projects);
            encoder.Encode(test_cycle_name);
            encoder.Encode(module_name);

            if (projects == null || !role.IsAdministrator())
            {
                return PageBuilder.Error403(project);
            }

            if (projects.owner_id == -99) { projects.owner_id = null; }

            var allexistingp = _context.test_cycles.AsNoTracking().Where(tc => tc.project_id == projects.project_id).ToList();

            foreach (var ae in allexistingp.Where(ae => !test_cycle_id.Contains(ae.test_cycle_id)))
            {
                //Delete
                database.Remove(ae);
            }
            for (int i = 0; i < test_cycle_name.Count; i++)
            {
                var existing = allexistingp.SingleOrDefault(tc => tc.test_cycle_id == test_cycle_id[i]);

                if(existing != null)
                {
                    //Update
                    existing.name = test_cycle_name[i];
                    database.Update(existing);
                }
                else
                {
                    //Insert
                    var test_cycle = new test_cycles() { name = test_cycle_name[i], project_id = projects.project_id };
                    database.Add(test_cycle);
                }

            }

            var allexistingm = _context.modules.AsNoTracking().Where(m => m.project_id == projects.project_id).ToList();

            foreach (var ae in allexistingm.Where(ae => !module_id.Contains(ae.module_id)))
            {
                //Delete
                database.Remove(ae);
            }
            for (int i = 0; i < module_name.Count; i++)
            {
                var existing = allexistingm.SingleOrDefault(m => m.module_id == module_id[i]);

                if (existing != null)
                {
                    //Update
                    existing.name = module_name[i];
                    database.Update(existing);
                }
                else
                {
                    //Insert
                    var module = new modules() { name = module_name[i], project_id = projects.project_id };
                    database.Add(module);
                }

            }
            ;

            var allexistingupr = _context.users_projects_roles.AsNoTracking().Where(upr => upr.project_id == projects.project_id && upr.role_id != 2).ToList();

            foreach (var ae in allexistingupr.Where(ae => !users_projects_roles_id.Contains(ae.users_projects_roles_id)))
            {
                //Delete
                if (ae.role_id != 2)
                {
                    database.Remove(ae);
                }
            }
            for (int i = 0; i < users_projects_roles_user_id.Count; i++)
            {
                var existing = allexistingupr.SingleOrDefault(m => m.users_projects_roles_id == users_projects_roles_id[i]);

                if (existing != null)
                {
                    //Update
                    existing.user_id = users_projects_roles_user_id[i];
                    existing.role_id = users_projects_roles_role_id[i];
                    database.Update(existing);
                }
                else
                {
                    //Insert
                    var users_projects_roles = new users_projects_roles() { user_id = users_projects_roles_user_id[i], role_id= users_projects_roles_role_id[i], project_id = projects.project_id };
                    database.Add(users_projects_roles);
                }

            }
            ;

            if (ModelState.IsValid)
            {
                projects.last_modified_by = role.user_id;
                projects.last_modified_date = DateTime.Now;
                _context.Entry(_context.projects.Single(p => p.project_id == projects.project_id)).State = EntityState.Detached;
                database.Update(projects);
                ;
                return RedirectToAction("Index", new { project = project });
            }
            return PageBuilder.ModelStateError(project, role, "Projects edit", ModelState);
        }
    }
}
