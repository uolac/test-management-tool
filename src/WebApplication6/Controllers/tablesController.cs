using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApplication6;

namespace WebApplication6.Controllers
{
    public class tablesController : Controller
    {
        private testing2Context _context;

        public tablesController(testing2Context context)
        {
            _context = context;    
        }

        // GET: tables
        public IActionResult Index(int? id)
        {
            if (id == null)
            {
                var testing2Context = _context.tables.Take(50).Include(t => t.system);
                ViewData["source_id"] = -1;
                return View(testing2Context.ToList());
            }
            else
            {
                var testing2Context = _context.tables.Where(t => t.system_id == id).Take(50).Include(t => t.system);
                ViewData["source_id"] = id;
                return View(testing2Context.ToList());
            }
           
        }


        [HttpPost]
        
        public IActionResult Index(int sourceid, string name)
        {
            var s_id = System.Convert.ToInt32(sourceid);
            var testing2Context = _context.tables.Include(t => t.system).Where(t => (t.system_id == s_id || s_id == -1) && (t.table_name+t.system.system_name).Contains(name)).Take(50);
            ViewData["source_id"] = sourceid;
            return View(testing2Context.ToList());
        }

        // GET: tables/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            tables tables = _context.tables.Single(m => m.table_id == id);
            if (tables == null)
            {
                return NotFound();
            }

            return View(tables);
        }

        // GET: tables/Create
        public IActionResult Create(int? id)
        {
            ViewData["system_id"] = id;
            return View();
        }

        // POST: tables/Create
        [HttpPost]
        
        public IActionResult Create(tables tables)
        {
            if (ModelState.IsValid)
            {
                _context.tables.Add(tables);
                _context.SaveChanges();
                return RedirectToAction("Index", new { id = tables.system_id });
            }
            ViewData["system_id"] = tables.system_id;
            return View(tables);
        }

        // GET: tables/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            tables tables = _context.tables.Single(m => m.table_id == id);
            if (tables == null)
            {
                return NotFound();
            }
            ViewData["system_id"] = tables.system_id;
            return View(tables);
        }

        // POST: tables/Edit/5
        [HttpPost]
        
        public IActionResult Edit(tables tables)
        {
            if (ModelState.IsValid)
            {
                _context.Update(tables);
                _context.SaveChanges();
                return RedirectToAction("Index", new { id = tables.system_id });
            }
            ViewData["system_id"] = tables.system_id;
            return View(tables);
        }

        // GET: tables/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            tables tables = _context.tables.Single(m => m.table_id == id);
            if (tables == null)
            {
                return NotFound();
            }

            return View(tables);
        }

        // POST: tables/Delete/5
        [HttpPost, ActionName("Delete")]
        
        public IActionResult DeleteConfirmed(int id)
        {
            tables tables = _context.tables.Single(m => m.table_id == id);
            var return_ = tables.system_id;
            _context.tables.Remove(tables);
            _context.SaveChanges();
            return RedirectToAction("Index", new { id = return_ });
        }
    }
}
