﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;


// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApplication6.Controllers
{
   
    public class usersController : Controller
    {
        private testing2Context _context;
        private RoleInfo role;
        private IRoleService _rs;
        private IDatabase database;
        private IHtmlEncoderService encoder;

        public usersController(testing2Context context, IRoleService rs, IDatabase database, IHtmlEncoderService encoder)
        {
            _context = context;
            _rs = rs;
            role = rs.GetRole();
            this.database = database;
            this.encoder = encoder;
        }

        public IActionResult Index(int project=0,string name = "", int page = 1)
        {
            name = encoder.Encode(name ?? "");

            if (!role.IsAdministrator()) { return PageBuilder.Error403(project); }

            var testingContext = _context.users
                .Include(u => u.default_project)
                .Include(u => u.email_preference)
                .Where(u => (u.name.ToLower().Contains(name.ToLower()) || u.email.ToLower().Contains(name.ToLower()) || name == "" || name == null) && u.user_id != 0
                ).ToList().OrderBy(r => r.name, new NaturalSortComparer<string>());

            return View("users_index", new IndexViewModel<users>(role, project, 0, Request.QueryString.HasValue, testingContext.ToList(), page
                , new Filters(
                     new Textbox("name", name)
                    )
                 , new Column("name", 2)
                 , new Column("email", 2)
                 , new Column("ad_identifier", 2, "", "network name")
                 , new Column("default_project.name", 2)
                ));
        }

        public IActionResult Create(int project = 0)
        {
            if (!role.IsAdministrator()) { return PageBuilder.Error403(project); }

            return View("users_create", new ViewModel<users>(project, new users()
                 , new MultiField("project", "users_projects_roles_id", new List<users_projects_roles>(), new Dropdown("project_id", _context.projects, false), new Dropdown("role_id", _context.roles.Where(r => r.role_id != 2), true, "<b>Tester</b>: Can create and edit Test Runs and create new Defects <br/><b>Test Manager / Project Manager / Supplier (full access)</b>: Full access to the project <br/><b>Supplier (read-only)</b>: Can only view project information and not edit it <br/><b>Supplier (defects access)</b>: Can create, edit and update Defects, but not anything else"))
                , new Dropdown("default_project_id", _context.projects, "project_id")
                , new Dropdown("email_preference_id", _context.email_preferences)
                ));

        }
        
        [HttpPost]
        public IActionResult Create(int project, users users, List<int> users_projects_roles_project_id, List<int> users_projects_roles_role_id)
        {
            encoder.Encode(users);

            if (users == null || !role.IsAdministrator()) { return PageBuilder.Error403(project); }

            //Not allowed to change is_administrator via the GUI
            users.is_administrator = false;

            if (ModelState.IsValid)
            {
                users.last_modified_by = role.user_id;
                users.last_modified_date = DateTime.Now;

                database.Add(users);

                for (int i = 0; i < users_projects_roles_project_id.Count; i++)
                {
                    var users_projects_roles = new users_projects_roles() { user_id = users.user_id, role_id = users_projects_roles_role_id[i], project_id = users_projects_roles_project_id[i] };
                    database.Add(users_projects_roles);
                }

                return RedirectToAction("Index", new { project = project });
            }

            return PageBuilder.ModelStateError(project, role, "Users create", ModelState);
        }

        public IActionResult Edit(int project=0, int id = 0)
        {
            var ut = _context.users.Include(u => u.users_projects_roles).ToList();
            var users = _context.users.Include(u => u.users_projects_roles).AsNoTracking().SingleOrDefault(p => p.user_id == id);

            if (!role.IsAdministrator())
            {
                return PageBuilder.Error403(project);
            }

            return View("users_edit",new ViewModel<users>(project, users
                 , new MultiField("project", "users_projects_roles_id", users.users_projects_roles.Where(upr => upr.role_id != 2), new Dropdown("project_id", _context.projects, false), new Dropdown("role_id", _context.roles.Where(r => r.role_id != 2), true, "<b>Tester</b>: Can create and edit Test Runs and create new Defects <br/><b>Test Manager / Project Manager / Supplier (full access)</b>: Full access to the project <br/><b>Supplier (read-only)</b>: Can only view project information and not edit it <br/><b>Supplier (defects access)</b>: Can create, edit and update Defects, but not anything else"))
      , new Dropdown("default_project_id", _context.projects, "project_id")
      , new Dropdown("email_preference_id", _context.email_preferences)
     ));
        }
        
        [HttpPost]
        public IActionResult Edit(int project, users users, List<int> users_projects_roles_id, List<int> users_projects_roles_project_id, List<int> users_projects_roles_role_id)
        {
            encoder.Encode(users);
            if (users == null || !role.IsAdministrator())
            {
                return PageBuilder.Error403(project);
            }

            //Not allowed to change is_administrator via the GUI
            users.is_administrator = _context.users.AsNoTracking().FirstOrDefault(u => u.user_id == users.user_id)?.is_administrator ?? false;
        
            var allexistingupr = _context.users_projects_roles.AsNoTracking().Where(upr => upr.user_id == users.user_id && upr.role_id != 2).ToList();

            foreach (var ae in allexistingupr.Where(ae => !users_projects_roles_id.Contains(ae.users_projects_roles_id)))
            {
                //Delete
                if(ae.role_id != 2)
                {
                    database.Remove(ae);
                }
            }
            for (int i = 0; i < users_projects_roles_project_id.Count; i++)
            {
                var existing = allexistingupr.SingleOrDefault(m => m.users_projects_roles_id == users_projects_roles_id[i]);

                if (existing != null)
                {
                    //Update
                    existing.project_id = users_projects_roles_project_id[i];
                    existing.role_id = users_projects_roles_role_id[i];
                    database.Update(existing);
                }
                else
                {
                    //Insert
                    var users_projects_roles = new users_projects_roles() { user_id = users.user_id, role_id = users_projects_roles_role_id[i], project_id = users_projects_roles_project_id[i] };
                    database.Add(users_projects_roles);
                }

            }
            ;

            if (ModelState.IsValid)
            {
                database.Update(users);
                ;
                return RedirectToAction("Index", new {project = project});
            }
            return PageBuilder.ModelStateError(project, role, "Users create", ModelState);
        }
    }
}
