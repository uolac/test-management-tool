using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApplication6;

namespace WebApplication6.Controllers
{
    public class entitiesController : Controller
    {
        private testing2Context _context;

        public entitiesController(testing2Context context)
        {
            _context = context;    
        }

        // GET: entities
        public IActionResult Index()
        {
            return View(_context.entities.ToList());
        }

        // GET: entities/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            entities entities = _context.entities.Single(m => m.entity_id == id);
            if (entities == null)
            {
                return NotFound();
            }

            return View(entities);
        }

        // GET: entities/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: entities/Create
        [HttpPost]
        
        public IActionResult Create(entities entities)
        {
            if (ModelState.IsValid)
            {
                _context.entities.Add(entities);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(entities);
        }

        // GET: entities/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            entities entities = _context.entities.Single(m => m.entity_id == id);
            if (entities == null)
            {
                return NotFound();
            }
            return View(entities);
        }

        // POST: entities/Edit/5
        [HttpPost]
        
        public IActionResult Edit(entities entities)
        {
            if (ModelState.IsValid)
            {
                _context.Update(entities);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(entities);
        }

        // GET: entities/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            entities entities = _context.entities.Single(m => m.entity_id == id);
            if (entities == null)
            {
                return NotFound();
            }

            return View(entities);
        }

        // POST: entities/Delete/5
        [HttpPost, ActionName("Delete")]
        
        public IActionResult DeleteConfirmed(int id)
        {
            entities entities = _context.entities.Single(m => m.entity_id == id);
            _context.entities.Remove(entities);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
