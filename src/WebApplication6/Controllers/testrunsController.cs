﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using Microsoft.AspNetCore.Http;
using System.IO;
using System.Net.Http.Headers;


using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;


namespace WebApplication6.Controllers
{  

    public class testrunsController : Controller
    {
        private testing2Context _context;
        private RoleInfo role;
        private IRoleService _rs;
        private IDatabase database;
        private IHtmlEncoderService encoder;

        public testrunsController(testing2Context context, IRoleService rs, IDatabase database, IHtmlEncoderService encoder)
        {
            _context = context;
            _rs = rs;
            role = rs.GetRole();
            this.database = database;
            this.encoder = encoder;
        }

        public IActionResult Index(int project = 0, int run_by_id = 0, int outcome_id = 0, int test_id = 0, int test_cycle_id = 0, DateTime? start_date = null, DateTime? end_date = null, string button_name = "", int page = 1)
        {
            project = role.project;
            var prj = _context.projects.SingleOrDefault(p => p.project_id == project);

            button_name = encoder.Encode(button_name ?? "");

            if (!role.IsAuthenticated()) { return PageBuilder.Error403(project); }

            var current_cycle = _context.projects.SingleOrDefault(p => p.project_id == project)?.current_test_cycle_id;

            if (test_cycle_id == 0) { test_cycle_id = prj.current_test_cycle_id ?? 0; }
        

            var testingContext = _context.runs
                 .Include(r => r.outcome)
                 .Include(r => r.test_cycle)
                 .Include(r => r.test)
                 .Include(r => r.last_modified_byNavigation)
                 .Include(r => r.run_byNavigation)
                 .Include(r => r.defects)
                 .Where(r => (r.test.project_id == project && r.test.draft_status_id != 3)
                 && (r.run_byNavigation.user_id == run_by_id || run_by_id == 0)
                 && (r.outcome_id == outcome_id || outcome_id == 0)
                 && (r.test_id == test_id || test_id == 0)
                 && (r.test_cycle_id == test_cycle_id || test_cycle_id == 0)).ToList()
                 .Where(r => (r.run_date >= start_date || start_date == null)
                 && (r.run_date <= end_date || end_date == null)
                 ).OrderByDescending(r => r.run_date).ThenByDescending(r => r.last_modified_date).ToList();

            if (button_name == "Generate Report")
            {
                return PageBuilder.GenerateReport(project, test_cycle_id, testingContext.ToList()
                    , new Column("run_id", 1, true)
                    , new Column("test_id", 1, true)
                    , new Column("test.name", 1)
                    , new Column("test_cycle.name", 1)
                 , new Column("run_byNavigation.name", 1)
                 , new Column("run_date", 1)
                 , new Column("outcome.name", 1)
                 , new Column("last_modified_byNavigation.name", 2, "", "last modified by")
                 , new Column("last_modified_date", 2)
                 , new Column("defect_descriptions"));
            }



            return View("testruns_index", new IndexViewModel<runs>(role, project, test_cycle_id, Request.QueryString.HasValue, testingContext.ToList(), page
                            , new Filters(
                        new Dropdown("test_id", _context.project_tests(project), test_id, true)
                     , new Dropdown("test_cycle_id", _context.project_testcycles(project), test_cycle_id, true)
                    , new Dropdown("run_by_id", _context.project_users(project), "user_id", run_by_id)
                    , new Dropdown("outcome_id", _context.outcomes, outcome_id, true)
                    )
                  , new Column("test_cycle.name", 1, "Default")
                 , new Column("test.name", 1)
                 , new Column("run_byNavigation.name", 1, "", "run by")
                 , new Column("run_date", 1)
                 , new Column("outcome.name", 1)
                 , new Column("defects.name", 1)
                ));
        }

        public IActionResult CreateSelect(int project = 0)
        {

            var content = Html.testselectpage(project, 
                Html.Dropdown("test_id", "", _context.project_tests(project).Where(t => t.draft_status_id != 3).Select(t => Html.DropdownOption(t.test_id.ToString(), t.name, false)).ToList(), false));

            return PageBuilder.GetBase("Execute New Test Run", role, project, "testruns", "CreateSelect", content, "");
        }

        public IActionResult Create(int project = 0, int test_id = 0, int test_cycle_test_id = 0)
        {

            
            var test_cycle_test = _context.project_testcycles_tests(project).Include(t => t.test).SingleOrDefault(t => t.test_cycle_test_id == test_cycle_test_id);
            
            var test = _context.project_tests(project).Include(t => t.test_steps).Include(t => t.project).ToList().SingleOrDefault(t => t.test_id == (test_id == 0 ?  test_cycle_test?.test_id ?? 0 : test_id));

            var tcid = (test_cycle_test_id == 0 ? test?.project?.current_test_cycle_id : test_cycle_test?.test_cycle_id) ?? 0;

            if (!role.IsTesterOrAbove() || test == null) { return PageBuilder.Error403(project); }

            return View("testruns_create", new TestRunViewModel<runs>(project, new runs() { test_id = test?.test_id, run_by = role.user_id, run_date = DateTime.Now, test_cycle_id = tcid }
                            , new TestRunsSteps(new List<test_runs_test_steps>(), test?.test_steps.OrderBy(t => t.seq_no), test)
            , new MultiField("attachment", "run_attachment_id", new List<object>(), new AttachmentUpload())
                , new MultiField("defect", "defect_id", new List<object>(), new Textbox("name"), new Textarea("description"), new Dropdown("priority_id", _context.priorities, 3, true), new Dropdown("severity_id", _context.severities, true, @"<b>Now</b>: Needs to be fixed now e.g. a patch to the current release <br/><b>Next</b>: Needs to be fixed in the next release <br/><b>Before</b>: Needs to be fixed before go-live <br/><b>After</b>: Can be fixed after go-live", 5))            
                , new Dropdown("outcome_id", _context.outcomes.OrderByDescending(o => o.outcome_id), true, @"
<b>Passed</b>: All steps passed without issue <br/>
<b>Failed</b>: One or more of the steps did not pass <br/>
<b>Blocked</b>: Another issue that’s not part of this test prevented you from completing the test, such as website issues <br/>
<b>Still In Progress</b>: Used in case you’re in the middle of a test run and need to stop.Has no effect on the reporting and won’t show up as completed.You can come back later to finish it."
)
                , new Dropdown("run_by", _context.project_users(project), "user_id", -97)
                ));
        }
        
        [HttpPost]
        public IActionResult Create(int project, runs runs, int[] stepid = null, List<string> defect_name = null, List<string> defect_description = null, List<int> defect_priority_id = null, List<int> defect_severity_id = null, int[] stepcheck = null)
        {
            encoder.Encode(runs);
            encoder.Encode(defect_name);
            encoder.Encode(defect_description);

            var test = _context.project_tests(project).Include(t => t.module).SingleOrDefault(t => t.test_id == runs.test_id);

            if (runs == null || !role.IsTesterOrAbove() || test?.project_id != project || project == 0) { return PageBuilder.Error403(project); }

            var files = _context.GetFormFilesAsync(HttpContext).Result;

            if (ModelState.IsValid)
            {
                database.Add(runs);

                if(runs.outcome_id != 4)
                {
                    var scheduled_tests = _context.test_cycles_tests.Where(t => t.test_id == runs.test_id && t.scheduled_status_id == 1).ToList();
                    foreach (var scheduled_test in scheduled_tests)
                    {
                        if (DateTime.Now > (scheduled_test.scheduled_end_date ?? DateTime.Now).AddDays(1))
                        {
                            scheduled_test.scheduled_status_id = 5;
                        }
                        else
                        {
                            scheduled_test.scheduled_status_id = 2;
                        }

                        database.Update(scheduled_test);
                    }
                }


                if(defect_name!=null)
                {
                    for(int i=0;i< defect_name.Count;i++)
                    {
                        try
                        {

                       
                        if(defect_name[i] != "" || defect_description[i] != "")
                        {

                       
                        var defects = new defects() { name = defect_name[i], description = defect_description[i], module_id = test.module_id, found_by = runs.run_by, found_date = runs.run_date, project_id = project, test_run_id = runs.run_id };

                            database.Add(defects);

                            Email.SendErrorText($"Defect {defects.defect_id} created.");

                        var defect_history = new defect_history() { defect_id = defects.defect_id, defect_status_id = 1, closure_reason_id = 1, status_comment = "Defect created", priority_id = defect_priority_id[i], severity_id = defect_severity_id[i], effective_date = DateTime.Now };

                        database.Add(defect_history);

                            Email.SendErrorText($"Defect history for {defects.defect_id} created. {defect_history.defect_history_id} {defect_history.defect_id}");

                            Email.Send(
           _context.projects.Include(p => p.users_projects_roles).ThenInclude(upr => upr.user).ToList().SelectMany(p => p.users_projects_roles).Where(upr => (upr.role_id == 4 || upr.user.is_administrator) && upr.project_id == project).Select(upr => upr.user)
           , "Defect created and pending assignment", "A defect has been created and is ready for assignment:" + Environment.NewLine + Environment.NewLine + $"Name: {defects.name}" + Environment.NewLine + $"Due: {defects.due_date?.ToShortDateString() ?? "None"}" + Environment.NewLine + Environment.NewLine + $@"https://tmt.web01.lincoln.ac.uk/defects/AssignOrUpdate/{project}/?defect_id={defects.defect_id}");
                        }
                        }
                        catch(Exception ex)
                        {
                            Email.SendErrorText($"Defect history error for {runs.run_id} {defect_name[i]} {ex.Message}");

                            _context.Database.ExecuteSqlCommand("sp_fix_history");
                        }
                    }
                }

                for(int i=0;i<stepcheck.Count(); i++)
                {
                    database.Add(new test_runs_test_steps() { run_id = runs.run_id, test_step_id = stepid[i], completed = runs.outcome_id == 1 ? 2 : stepcheck[i] });
                }

                for (int i = 0; i < files.Count(); i++)
                {
                    if (files[i] != null && files[i]?.attachment_content != null)
                    {
                        var an = new runs_attachments()
                        {
                            run_id = runs.run_id,
                            attachment_content = files[i]?.attachment_content,
                            attachment_filename = encoder.Encode(files[i]?.attachment_filename),
                            attachment_mimetype = encoder.Encode(files[i]?.attachment_mimetype)
                        };
                        database.Add(an);
                    }
                }

                return RedirectToAction("Index", new { project = project });
            }

            return PageBuilder.ModelStateError(project, role, "Testruns create", ModelState);
        }

        public IActionResult Edit(int project = 0, int id = 0, bool isdelete = false)
        {
            var runs = _context.runs.AsNoTracking().Include(r => r.test).ThenInclude(r => r.test_steps).Include(r => r.test_runs_test_steps).Include(r => r.test_cycle).Include(r => r.attachments).SingleOrDefault(r => r.test.project_id == project && r.run_id == id);


            if (runs  == null || !role.IsAuthenticated() || project != runs?.test?.project_id || project == 0)
            {
                return PageBuilder.Error403(project);
            }

            return View("testruns_Edit", new TestRunViewModel<runs>(project, runs
                  , new TestRunsSteps(runs.test_runs_test_steps, runs.test.test_steps.OrderBy(t => t.seq_no), runs.test)
                                , new MultiField("attachment", "run_attachment_id", runs.attachments, new AttachmentUpload())
                , new Dropdown("outcome_id", _context.outcomes.OrderByDescending(o => o.outcome_id), true, @"
<b>Passed</b>: All steps passed without issue <br/>
<b>Failed</b>: One or more of the steps did not pass <br/>
<b>Blocked</b>: Another issue that’s not part of this test prevented you from completing the test, such as website issues <br/>
<b>Still In Progress</b>: Used in case you’re in the middle of a test run and need to stop.Has no effect on the reporting and won’t show up as completed.You can come back later to finish it.")
                , new Dropdown("run_by", _context.project_users(project), "user_id")
                ));
        }
        
        [HttpPost]
        public IActionResult Edit(int project = 0, runs runs = null, List<int> run_attachment_id = null, int[] stepcheck = null, int[] stepid = null, int[] runstepid = null)
        {
            encoder.Encode(runs);

            var r_project = _context.runs.AsNoTracking().Include(r => r.test).ThenInclude(r => r.test_steps).Include(r => r.test_runs_test_steps).Include(r => r.attachments).SingleOrDefault(r => r.test.project_id == project && r.run_id == runs.run_id)?.test?.project_id;

            if (runs == null || !role.IsTesterOrAbove() || project != r_project) { return PageBuilder.Error403(project); }

            var files = _context.GetFormFilesAsync(HttpContext).Result;
            if (ModelState.IsValid)
            {

                database.Update(runs);
                ;

                database.UpdateLinked<test_runs_test_steps>(_context.test_runs_test_steps.Where(rts => rts.run_id == runs.run_id), "test_runs_test_steps_id", runstepid.ToList()
                    , (i) => new test_runs_test_steps() { run_id = runs.run_id, test_step_id = stepid[i], completed = runs.outcome_id == 1 ? 2 : stepcheck[i] }
                    , (existing, i) => { existing.completed = runs.outcome_id == 1 ? 2 : stepcheck[i];
                    }
                    );

                database.UpdateLinked<runs_attachments>(_context.runs_attachments.Where(da => da.run_id == runs.run_id)
                  , "run_attachment_id", run_attachment_id
                  , (i) => new runs_attachments()
                  {
                      run_id = runs.run_id
                    ,
                      attachment_content = files[i]?.attachment_content
                    ,
                      attachment_filename = encoder.Encode(files[i]?.attachment_filename)
                    ,
                      attachment_mimetype = encoder.Encode(files[i]?.attachment_mimetype)
                  },
                    (existing, i) =>
                    {
                        if (files[i] != null && files[i]?.attachment_content != null)
                        {
                            existing.attachment_content = files[i]?.attachment_content;
                            existing.attachment_filename = encoder.Encode(files[i]?.attachment_filename);
                            existing.attachment_mimetype = encoder.Encode(files[i]?.attachment_mimetype);
                        }
                    });

                ;

                return RedirectToAction("Index", new { project = project });
            }
            
            return PageBuilder.ModelStateError(project, role, "Testruns create", ModelState);
            
        }

        public IActionResult Delete(int project = 0, int id = 0)
        {
            var runs = _context.runs.AsNoTracking().Include(r => r.test).ThenInclude(r => r.test_steps).Include(r => r.test_runs_test_steps).Include(r => r.test_cycle).Include(r => r.attachments)
                 .Include(r => r.outcome)
                 .Include(r => r.last_modified_byNavigation)
                 .Include(r => r.run_byNavigation)
                 .SingleOrDefault(r => r.test.project_id == project && r.run_id == id);


            if (runs == null || !role.IsAuthenticated() || project != runs?.test?.project_id || project == 0)
            {
                return PageBuilder.Error403(project);
            }

            return View("testruns_Delete", new ViewModel<runs>(project, runs
                                , new MultiField("attachment", "run_attachment_id", runs.attachments, new AttachmentUpload())
              , new Dropdown("test_steps", runs.test.test_steps.OrderBy(t => t.seq_no))
                , new Dropdown("outcome_id", _context.outcomes.OrderByDescending(o => o.outcome_id), true, @"
<b>Passed</b>: All steps passed without issue <br/>
<b>Failed</b>: One or more of the steps did not pass <br/>
<b>Blocked</b>: Another issue that’s not part of this test prevented you from completing the test, such as website issues <br/>
<b>Still In Progress</b>: Used in case you’re in the middle of a test run and need to stop.Has no effect on the reporting and won’t show up as completed.You can come back later to finish it.")
                , new Dropdown("run_by", _context.project_users(project), "user_id")
                ));
        }


        public IActionResult DeleteConfirmed(int project = 0, int id = 0)
        {
            var runs = _context.runs.Include(r => r.test).SingleOrDefault(m => m.run_id == id);

            if (runs == null || !role.IsTesterOrAbove() || runs?.test?.project_id != project || project == 0)
            {
                return PageBuilder.Error403(project);
            }
            var runs_steps = _context.test_runs_test_steps.Where(trts => trts.run_id == id).ToList();
            var runs_attachments = _context.runs_attachments.Where(ra => ra.run_id == id).ToList();
            foreach (var ra in runs_attachments)
            {
                database.Remove(ra);
            }

            foreach (var rs in runs_steps)
            {
                database.Remove(rs);
            }
           
            ;
            database.Remove(runs);
            ;

            return RedirectToAction("Index", new { project = project });
        }

        public IActionResult DownloadAttachment(int project, int id)
        {
            var attachment = _context.runs_attachments.SingleOrDefault(t => t.run_attachment_id == id);

            if (!role.IsAuthenticated() || attachment == null) { return PageBuilder.Error403(project); }

            HttpContext.Response.ContentType = attachment.attachment_mimetype;
            FileContentResult result = new FileContentResult(attachment.attachment_content, attachment.attachment_mimetype)
            {
                FileDownloadName = attachment.attachment_filename
            };

            return result;
        }

        public IActionResult ViewImage(int project, int id)
        {
            var attachment = _context.test_steps.SingleOrDefault(t => t.test_step_id == id);

            if (!role.IsAuthenticated() || attachment == null) { return PageBuilder.Error403(project); }

            if(attachment.attachment_mimetype == "image/png" || attachment.attachment_mimetype == "image/bmp" || attachment.attachment_mimetype == "image/jpg" || attachment.attachment_mimetype == "image/jpeg")
            {
                return PageBuilder.GetBlank($@"<img src=""data:{attachment.attachment_mimetype};base64,{Convert.ToBase64String(attachment.attachment_content)}"">");
            }
            else
            {
                HttpContext.Response.ContentType = attachment.attachment_mimetype;
                FileContentResult result = new FileContentResult(attachment.attachment_content, attachment.attachment_mimetype)
                {
                    FileDownloadName = attachment.attachment_filename
                };

                return result;
            }

        }

    }
}
