﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore.ChangeTracking;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace WebApplication6
{
    public class Database : IDatabase
    {
        private readonly testing2Context _context;
        private readonly RoleInfo role;

        public Database(testing2Context _context, IRoleService role)
        {
            this._context = _context;
            this.role = role.GetRole();
        }
        
        //All database changes call these functions so they can be audit logged

        public void Update<T>(T entity) where T : class
        {
            Change(entity, "Update");
        }

        public void Add<T>(T entity) where T : class
        {
            Change(entity, "Add");
        }


        public void AddNoAudit<T>(T entity) where T : class
        {
            ChangeNoAudit(entity, "Add");
        }

        public void Remove<T>(T entity) where T : class
        {
            Change(entity, "Remove");
        }

        public void RemoveRange<T>(IEnumerable<T> entities) where T : class
        {
            var entities_list = entities.ToList();
            foreach(var entity in entities_list)
            {
                Change(entity, "Remove");
            }
        }

        public void AddRange<T>(IEnumerable<T> entities) where T : class
        {
            var entities_list = entities.ToList();
            foreach (var entity in entities_list)
            {
                Change(entity, "Add");
            }
        }


        public void UpdateRange<T>(IEnumerable<T> entities) where T : class
        {
            var entities_list = entities.ToList();
            foreach (var entity in entities_list)
            {
                Change(entity, "Add");
            }
        }

        //Generate an audit record, then update the database
        private void Change<T>(T entity, string action) where T : class
        {

            EntityEntry<T> ee = null;

            var md = System.DateTime.Now;
            var mb = role.user_id;

            //Set last modified date and last modified by
            var lmd = entity.GetType().GetProperties().SingleOrDefault(p => p.Name == "last_modified_date");
            if (lmd != null) { lmd.SetValue(entity, md); }
            var lmb = entity.GetType().GetProperties().SingleOrDefault(p => p.Name == "last_modified_by");
            if (lmb != null) { lmb.SetValue(entity, mb); }

            //Find object primary key
            var pkname = _context.Model.FindEntityType(entity.GetType().FullName).GetProperties().SingleOrDefault(p => p.IsPrimaryKey())?.Name;
            //Find Database Content to modify
            var specific_context = _context.Set<T>();
            //Find original values to record the change for auditing
            var original_entity = specific_context.AsNoTracking().SingleOrDefault(t => t.Bind(pkname) == entity.Bind(pkname));

            try {
            //Perform the database update
            switch(action)
            {
                case "Add": System.Diagnostics.Debug.WriteLine("----Add " + entity.GetType().Name);  ee = specific_context.Add(entity); break;
                case "Remove": System.Diagnostics.Debug.WriteLine("----Remove " + entity.GetType().Name); ee = specific_context.Remove(entity); break;
                case "Update": System.Diagnostics.Debug.WriteLine("----Update " + entity.GetType().Name); ee = specific_context.Update(entity); break;
            }

            _context.SaveChanges();
            }
            catch(Exception ex)
            {
                Email.SendError($"Database update: {entity.GetType().Name}/{action}/{DateTime.Now.ToLongDateString()}/{DateTime.Now.ToLongTimeString()}/{role.user_id}/{role.user_name}/{role.type.ToEnumString()}",ex);
                throw;
            }

            //Save any changes as audit records
            IEnumerable<Comparison> comparison = null; 
           
            switch (action)
            {
                case "Update": comparison = ee.Metadata.GetProperties().Where(p => p.Name != "last_modified_date" && p.Name != "last_modified_by").Select(p => new Comparison() { name = p.Name, original = original_entity?.GetType().GetProperty(p.Name).GetValue(original_entity)?.ToString(), @new = ee.Property(p.Name).CurrentValue?.ToString() }); break;
                case "Add": comparison = ee.Metadata.GetProperties().Where(p => p.Name != "last_modified_date" && p.Name != "last_modified_by").Select(p => new Comparison() { name = p.Name, original = (string)null, @new = ee.Property(p.Name).CurrentValue?.ToString() }); break;
                case "Remove": comparison = ee.Metadata.GetProperties().Where(p => p.Name != "last_modified_date" && p.Name != "last_modified_by").Select(p => new Comparison() { name = p.Name, original = original_entity?.GetType().GetProperty(p.Name).GetValue(original_entity)?.ToString(), @new = (string)null }); break;
            }

            var changes = comparison.Where(p => p.original != p.@new);
            var pk = entity.Bind(pkname).ToString();


            foreach (var c in changes)
            {
                var audit_record = new audit() { action = action, username = role.user_name ?? "None", entity = ee.Entity.GetType().Name, entity_pk = pk, property = c.name, original = c.original, @new = c.@new, modified_by = mb, modified_date = md };
                _context.audit.Add(audit_record);
                _context.SaveChanges();
            }

        }

        //Generate an audit record, then update the database
        private void ChangeNoAudit<T>(T entity, string action) where T : class
        {
            //Find Database Content to modify
            var specific_context = _context.Set<T>();

            try
            {
                //Perform the database update
                switch (action)
                {
                    case "Add": System.Diagnostics.Debug.WriteLine("----Add " + entity.GetType().Name); specific_context.Add(entity); break;
                    case "Remove": System.Diagnostics.Debug.WriteLine("----Remove " + entity.GetType().Name); specific_context.Remove(entity); break;
                    case "Update": System.Diagnostics.Debug.WriteLine("----Update " + entity.GetType().Name); specific_context.Update(entity); break;
                }

                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                Email.SendError($"Database update: {entity.GetType().Name}/{action}/{DateTime.Now.ToLongDateString()}/{DateTime.Now.ToLongTimeString()}/{role.user_id}/{role.user_name}/{role.type.ToEnumString()}", ex);
                throw;
            }
        }

        //Function to add, remove and update linked entities on a page, e.g. test steps from the Edit Test page
        public void UpdateLinked<T>(IEnumerable<T> model, string idfieldname, List<int> idfield, Func<int, T> createaction, Action<T, int> updateaction) where T : class
        {
            ;
            if (model is IQueryable) { model = ((IQueryable<T>)model).AsNoTracking(); }
            var allexisting = model.ToList();

            foreach (var ae in allexisting.Where(ae => !idfield.Contains(Convert.ToInt32(ae.Bind(idfieldname)))))
            {
                //Delete
                Remove(ae);
            }
            for (int i = 0; i < idfield.Count; i++)
            {
                var existing = allexisting.SingleOrDefault(m => Convert.ToInt32(m.Bind(idfieldname)) == idfield[i]);

                if (existing != null)
                {
                    //Update
                    updateaction(existing, i);
                    Update(existing);
                }
                else
                {
                    //Insert
                    var newobj = createaction(i);
                    Add(newobj);
                }


            }
        }
       
        //Intermediate object for audit comparison
        public class Comparison
        {
            public string name;
            public string original;
            public string @new;
        }

    }


    public interface IDatabase
    { void Update<T>(T entity) where T : class;
        void Add<T>(T entity) where T : class;
        void AddNoAudit<T>(T entity) where T : class;
        void Remove<T>(T entity) where T : class;
        void RemoveRange<T>(IEnumerable<T> entities) where T : class;
        void AddRange<T>(IEnumerable<T> entities) where T : class;
        void UpdateRange<T>(IEnumerable<T> entities) where T : class;
        void UpdateLinked<T>(IEnumerable<T> model, string idfieldname, List<int> idfield, Func<int, T> createaction, Action<T, int> updateaction) where T : class;
    }
    
}
