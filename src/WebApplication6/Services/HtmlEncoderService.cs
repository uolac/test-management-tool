﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.WebEncoders;
using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace WebApplication6
{
       public class HtmlEncoderService : IHtmlEncoderService
    {
        private readonly HtmlEncoder encoder;

        public HtmlEncoderService()
        {
            this.encoder = HtmlEncoder.Default;
        }

        public void Encode(object entity)
        {
            
            if(entity is List<string>)
            {
                //HTML encode array of strings
                var es = entity as List<string>;
                for(int i=0;i<es.Count();i++) { es[i] = encoder.Encode(es[i] ?? ""); }
            }
            else if (entity is string[])
            {
                //HTML encode array of strings
                var es = entity as string[];
                for (int i = 0; i < es.Count(); i++) { es[i] = encoder.Encode(es[i] ?? ""); }
            }
            else if (entity is requirements || entity is tests || entity is defects || entity is defect_history || entity is modules || entity is test_cycles || entity is users || entity is projects || entity is runs || entity is test_steps)
            foreach(var p in entity.GetType().GetProperties().Where(f => (f.PropertyType.Name == "Nullable`1" ? Nullable.GetUnderlyingType(f.PropertyType) : f.PropertyType) == typeof(string)))
            {
                    //HTML encode a model object
                    p.SetValue(entity, encoder.Encode((string)p.GetValue(entity) ?? ""));
            }
            else
            {
                throw new Exception();
            }
        }     

        public string Encode(string entity)
        {
            //Html encode a string
            return encoder.Encode(entity ?? "");
        }

        public static string Decode(string entity)
        {
            return System.Net.WebUtility.HtmlDecode(entity);
        }
    }

    public interface IHtmlEncoderService
    {
        void Encode(object entity);
        string Encode(string entity);
    }


}
