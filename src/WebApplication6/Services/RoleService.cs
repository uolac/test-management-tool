﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace WebApplication6
{
       public class RoleService : IRoleService
        {
        private readonly testing2Context _context;
        private readonly HttpContext httpcontext;
        private readonly IHostingEnvironment env;

        public RoleService(testing2Context _context, IHttpContextAccessor hc, IHostingEnvironment env)
        {
            this._context = _context;
            this.httpcontext = hc.HttpContext;
            this.env = env;
        }

        public int GetDefaultProject()
        {

            if (env.ContentRootPath.Contains("aCoady"))
            {
                return 2;
            }
            else
            {
                //Get the user's default project from the database
                var un = httpcontext.User.Identity.Name?.ToLower() ?? "NULL";
                var user = _context.users.AsNoTracking().SingleOrDefault(u => u.ad_identifier.Replace("\\\\", "\\").ToLower() == un.ToLower());
                return user?.default_project_id ?? 0;
            }
        }

        public RoleInfo GetRole(int project=0)
        {
            var path = httpcontext.Request.Path.Value.Split('/');

            //Get the user's authenticated network name
            var username = httpcontext.User.Identity.Name?.ToLower() ?? "NULL";

            //Get the project number requested
            if (path.Count() > 3)
            {
                int i;
                if (int.TryParse(path[3], out i))
                {
                    project = i;
                }
            }

            if (env.ContentRootPath.Contains("aCoady"))
            {
                RoleType default_role = RoleType.Administrator;
                ;
                return new RoleInfo { type = default_role, user_name = username.ToLower(), user_id = 1, project = (project == 0 ? 2 : project), available_projects = _context.projects.ToList() }; 
            }
            else
            {                     
                
                //Based on their network name and their role(s) in the database, make a RoleInfo object storing those details        
                if (username == null)
                {
                    return new RoleInfo { type = RoleType.None, user_name = username.ToLower(), user_id = -1, user_displayname = "System", project = 0, available_projects = new List<projects>() };
                }
                var ctx = _context.users_projects_roles.AsNoTracking().Include(upr => upr.project).Include(upr => upr.user).ToList();
                var all_roles = ctx.Where(upr => upr.user.ad_identifier?.Replace("\\\\", "\\").ToLower() == username.ToLower());
                var available_projects = all_roles.Select(r => r.project).Distinct().ToList();
                var roles = ctx.Where(upr => upr.project.project_id == project && upr.user.ad_identifier?.Replace("\\\\", "\\").ToLower() == username.ToLower());

                var user_id = roles.FirstOrDefault()?.user_id ?? 0;
                var user_displaynamet = roles.FirstOrDefault()?.user?.name ?? "Unknown";
                var user_is_administrator = _context.users.AsNoTracking().ToList().Where(u => u.is_administrator && u.ad_identifier?.Replace("\\\\", "\\").ToLower() == username.ToLower()).Any();
                var default_project_extra = _context.users.AsNoTracking().ToList().Where(u => u.ad_identifier?.Replace("\\\\", "\\").ToLower() == username.ToLower()).FirstOrDefault()?.default_project_id ?? 0;

                if(project == 0)
                {
                    project = default_project_extra;
                }

                if (user_is_administrator) { return new RoleInfo { type = RoleType.Administrator, user_displayname = user_displaynamet, user_name = username, user_id = user_id, project = project, available_projects = _context.projects.ToList() }; }
                else if (roles.Any(r => r.role_id == 3 || r.role_id == 4 || r.role_id == 1003)) { return new RoleInfo { type = RoleType.Manager, user_displayname = user_displaynamet, user_name = username, user_id = user_id, project = project, available_projects = available_projects }; }
                else if (roles.Any(r => r.role_id == 1002)) { return new RoleInfo { type = RoleType.SupplierD, user_displayname = user_displaynamet, user_id = user_id, user_name = username, project = project, available_projects = available_projects }; }
                else if (roles.Any(r => r.role_id == 1)) { return new RoleInfo { type = RoleType.Tester, user_displayname = user_displaynamet, user_id = user_id, user_name = username, project = project, available_projects = available_projects }; }
                else if (roles.Any(r => r.role_id == 5)) { return new RoleInfo { type = RoleType.SupplierRO, user_displayname = user_displaynamet, user_id = user_id, user_name = username, project = project, available_projects = available_projects }; }

                else { return new RoleInfo { type = RoleType.None, user_id = user_id, user_name = username.ToLower(), project = project, available_projects = available_projects }; }
            }                  
        }
    }

    public interface IRoleService { int GetDefaultProject(); RoleInfo GetRole(int project=0); }

    //Stores authenticated role information
    public class RoleInfo
    {
        public RoleType type;
        public int user_id;
        public int project;
        public string user_name;
        public string user_displayname;
        public List<projects> available_projects = new List<projects>();

        public bool IsAllowed(string controller, string action)
        {
            if(controller == "defects")
            {
                if (action == "Create") { return IsTesterOrAbove(); }
                if (action == "History") { return IsAuthenticated(); }
                if (action == "AssignOrUpdate") { return IsAuthenticated(); }
                else { return IsSupplierDOrAbove(); }
            }
            if (controller == "testruns")
            {
                return IsTesterOrAbove();
            }
            if (controller == "projects" || controller == "users")
            {
                return IsAdministrator();
            }
            else
            {
                return IsManager();
            }


        }

        public bool IsAllowed(string controller, PageType type)
        {
            return IsAllowed(controller, type.ToEnumString());
        }

        public bool IsTester()
        {
            if (type == RoleType.Tester) { return true; }
            return false;
        }

        public bool IsTesterOrAbove()
        {
            if (type == RoleType.Tester || type == RoleType.SupplierD || type == RoleType.Administrator || type == RoleType.Manager) { return true; }
            return false;
        }

        public bool IsSupplierDOrAbove()
        {
            if (type == RoleType.SupplierD || type == RoleType.Administrator || type == RoleType.Manager) { return true; }
            return false;
        }


        public bool IsManager()
        {
            if (type == RoleType.Manager || type == RoleType.Administrator) { return true; }
            return false;
        }

        public bool IsAdministrator()
        {
            if (type == RoleType.Administrator) { return true; }
            return false;
        }

        public bool IsAuthenticated()
        {
            if (type != RoleType.None) { return true; }
            return false;
        }
    }

public enum RoleType { None, Administrator, Tester, Manager, SupplierRO, SupplierD}
}
