﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication6
{
    public class IndexViewModel<T>
    {
        public PagedList<T> entities;
        public PagedList<object> entities_generic;
        public List<Column> columns;
        public List<Dropdown> filters;
        public List<Field> filters_generic;
        public int test_cycle_id = 0;
        public bool querystring = false;
        public int project = 0;
        public RoleInfo role;

        public IndexViewModel(RoleInfo role, int project, int test_cycle_id, bool querystring, List<T> entities, int page, Filters filters, params Column[] columns)
        {
            this.entities = entities.ToPagedListT(page);
            this.entities_generic = entities.ToPagedList(page);
            this.columns = columns.ToList();
            this.project = project;
            this.test_cycle_id = test_cycle_id;
            this.querystring = querystring && !filters.defaultids;
            this.columns = columns.ToList();
            this.filters = filters.filters.OfType<Dropdown>().ToList();
            this.filters_generic = filters.filters.ToList();
            this.role = role;
        }
    }
}
