﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication6
{
    public class HomepageViewModel
    {
        public List<NotificationTagHelper.InboxElement> notifications { get; set; }
        public int project { get; set; }
        public int test_cycle_id { get; set; }
        public int user_id { get; set; }

        public HomepageViewModel(int project,int test_cycle_id,int user_id,params NotificationTagHelper.InboxElement[] notifications)
        {
            this.notifications = notifications.ToList();
            this.project = project;
            this.test_cycle_id = test_cycle_id;
            this.user_id = user_id;
        }
    }
}
