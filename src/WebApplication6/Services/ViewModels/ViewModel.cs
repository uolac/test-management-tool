﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication6
{
    public class ViewModel<T>
    {
        public T entity;
        public List<Dropdown> dropdowns;
        public List<MultiField> mf;
        public RoleInfo role;
        public int project;

        public ViewModel(int project, T entity, MultiField mf, params Dropdown[] dropdowns)
        {
            this.entity = entity;
            this.dropdowns = dropdowns.ToList();
            this.project = project;
            this.mf = new List<MultiField>() { mf };
        }

        public ViewModel(int project, T entity, MultiField mf, MultiField mf2, params Dropdown[] dropdowns)
        {
            this.entity = entity;
            this.dropdowns = dropdowns.ToList();
            this.project = project;
            this.mf = new List<MultiField>() { mf, mf2 };
        }


        public ViewModel(int project, T entity, MultiField mf, MultiField mf2, MultiField mf3, params Dropdown[] dropdowns)
        {
            this.entity = entity;
            this.dropdowns = dropdowns.ToList();
            this.project = project;
            this.mf = new List<MultiField>() { mf, mf2, mf3 };
        }

        public ViewModel(int project, T entity, params Dropdown[] dropdowns)
        {
            this.entity = entity;
            this.dropdowns = dropdowns.ToList();
            this.project = project;
            this.mf = new List<MultiField>();
        }
    }

    public class TestRunViewModel<T>
    {
        public T entity;
        public List<Dropdown> dropdowns;
        public List<MultiField> mf;
        public int project;
        public TestRunsSteps steps;

        public TestRunViewModel(int project, T entity, TestRunsSteps steps, MultiField mf1, MultiField mf2, params Dropdown[] dropdowns)
        {
            this.entity = entity;
            this.dropdowns = dropdowns.ToList();
            this.project = project;
            this.mf = new List<MultiField>() { mf1, mf2 };
            this.steps = steps;
        }

        public TestRunViewModel(int project, T entity, TestRunsSteps steps, MultiField mf, params Dropdown[] dropdowns)
        {
            this.entity = entity;
            this.dropdowns = dropdowns.ToList();
            this.project = project;
            this.mf = new List<MultiField>() { mf };
            this.steps = steps;
        }
    }
}
