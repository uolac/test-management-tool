﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication6
{
    public class DashboardViewModel
    {
        public MetricGroup req_metrics { get; set; }
        public MetricGroup test_metrics { get; set; }
        public MetricGroup defect_metrics { get; set; }
        public List<DashboardElement> charts { get; set; }
        public List<Dropdown> filters { get; set; }
        public BarChartData barchartdata { get; set; }
        public int project { get; set; }

        public DashboardViewModel(int project, BarChartData barchartdata, MetricGroup req_metrics, MetricGroup test_metrics, MetricGroup defect_metrics, Filters filters, params DashboardElement[] charts)
        {
            this.req_metrics = req_metrics;
            this.test_metrics = test_metrics;
            this.defect_metrics = defect_metrics;
            this.filters = filters.filters.OfType<Dropdown>().ToList();
            this.charts = charts.ToList();
            this.project = project;
            this.barchartdata = barchartdata;
        }
    }

    public class BarChartData
    {
        public List<List<string>> columns { get; set; }
        public string type = "bar";
        public string x = "x";
        public List<List<string>> groups { get; set; }
        
        public BarChartData(List<string> names, List<Tuple<string,List<string>>> data)
        {
            ;
            this.groups = new List<List<string>>() { data.Select(d => d.Item1).ToList() };
            this.columns = data.Select(d => d.Item2).ToList();

            for (int i=0;i < data.Count;i++)
            {
                columns[i].Insert(0,data[i].Item1);
            }
            var col = names;
            col.Insert(0, "x");
            columns.Add(col);

            ;
        }
    }

    public class BarChartStack
    {

    }

    /*
    {
            columns: [
                ['data1', -30, 200, 200, 400, -150, 250],
                ['data2', 130, 100, -100, 200, -150, 50],
                ['data3', -230, 200, 200, -300, 250, 250]
            ],
            type: 'bar',
            groups: [
                ['data1', 'data2']
            ]
        }
        */
}
