using System;
using System.Collections.Generic;

namespace WebApplication6
{
    public partial class interfaces
    {
        public interfaces()
        {
            links = new HashSet<links>();
        }

        public int interface_id { get; set; }
        public string argument1 { get; set; }
        public string external_id { get; set; }
        public string interface_name { get; set; }
        public string procedure_name { get; set; }
        public string schedule { get; set; }
        public string script { get; set; }
        public string future_script { get; set; }
        public string servers { get; set; }

        public virtual ICollection<links> links { get; set; }
    }
}
