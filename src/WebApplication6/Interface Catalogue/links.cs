using System;
using System.Collections.Generic;

namespace WebApplication6
{
    public partial class links
    {
        public int link_id { get; set; }
        public string business_logic { get; set; }
        public int? batch { get; set; }
        public int? destination_field_id { get; set; }
        public int? interface_id { get; set; }
        public bool is_future { get; set; }
        public int? link_column_num { get; set; }
        public int? source_field_id { get; set; }
        public string transformation { get; set; }
        public string where_conditions { get; set; }

        public virtual fields destination_field { get; set; }
        public virtual interfaces _interface { get; set; }
        public virtual fields source_field { get; set; }
    }

    public class ExceptionReportViewModel
    {
        public int link_id { get; set; }
        public string interface_name { get; set; }
        public string source_field { get; set; }
        public string dest_field { get; set; }
        public bool is_future { get; set; }
        public string transformation { get; set; }
        public string exception { get; set; }
    }

}
