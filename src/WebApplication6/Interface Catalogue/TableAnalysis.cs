﻿

using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
//using Microsoft.SqlServer.Management.SqlParser;
using System.Reflection;
using Microsoft.SqlServer.TransactSql.ScriptDom;
using System.IO;
using System.Diagnostics;
//using Microsoft.SqlServer.Management.SqlParser.MetadataProvider;


namespace Interfaces
{
    class TableAnalysis : TSqlFragmentVisitor
    {

        int current_col, current_col_absolute, stored_col, stored_col_absolute, scalarsubqueryid;
        //bool is_blank,
            bool update_col_added,select_col_added;
            Query current_query, stored_query;
        string stringliterals;
        //Column current_column;

            

        public List<Tuple<Type, int, int>> fragments = new List<Tuple<Type, int, int>>();
        public List<Tuple<FunctionCall, int, int>> functions = new List<Tuple<FunctionCall, int, int>>();

        public override void Visit(TSqlFragment node)
        {
            Program.Debug2(node.GetType().ToString());
            System.Diagnostics.Debug.WriteLine(node.GetType().Name + " " + GetStringFromTSqlFragment(node).Substring(0, Math.Min(GetStringFromTSqlFragment(node).Length, 100)));
            fragments.Add(new Tuple<Type, int, int>(node.GetType(), node.FirstTokenIndex, node.LastTokenIndex));
            if(node.GetType() == typeof(FunctionCall))
            {
                var n = (FunctionCall)node;
                functions.Add(new Tuple<FunctionCall, int, int>(n, node.FirstTokenIndex, node.LastTokenIndex));

            }
            base.Visit(node);
        }

        public override void Visit(UpdateSpecification node)
        {
            current_query = new Query() {target = new Table(node.Target)};
            Program.queries.Add(current_query);

            foreach (AssignmentSetClause n in node.SetClauses)
            {
                update_col_added = false;
                SelectIterate(n.NewValue,n, new List<string>(), 1);    
                if(!update_col_added)
                {
                    var col = new Column("null") { destinationcolumn = GetStringFromTSqlFragment(n.Column) };

                    current_query.selectcolumns.Add(col);
                }
            }

            if (node.FromClause != null)
            {
                foreach (TableReference t in node.FromClause.TableReferences)
                {
                    JoinIterate(t);


                }
            }
            base.Visit(node);
        }

        public override void Visit(DeclareTableVariableBody node)
        {
            var t = new DistinctTable() { name = node.VariableName.Value.ToLower(), isvartable = true };
            Program.distincttables.Add(t);
            //Console.WriteLine("VARIABLE TABLE: " + t.name);
            //Program.streamwriter_debug.WriteLine("VARIABLE TABLE: " + t.name);

            int i = 1;
            foreach (var c in ((TableDefinition)node.Definition).ColumnDefinitions)
            {
                var pd = ((ParameterizedDataTypeReference)c.DataType).Parameters;
                var col = new Column() { name = c.ColumnIdentifier.Value.ToLower(), datatype = c.DataType.Name.Identifiers.First().Value.ToLower(), datatype_val = !pd.Any() ? 0 : Convert.ToInt32(((ParameterizedDataTypeReference)c.DataType).Parameters[0].Value), ordinal = i };
                t.columns.Add(col);
                //Console.WriteLine("VARIABLE COLUMN: " + col.name + " " + col.datatype);
                i++;
            }

        }

        public override void Visit(CreateTableStatement node)
        {
            var t = new DistinctTable() { name = node.SchemaObjectName.Identifiers.Last().Value.ToLower() };

            Program.distincttables.Add(t);
            //Console.WriteLine("TEMP TABLE: " + t.name);
            //Program.streamwriter_debug.WriteLine("TEMP TABLE: " + t.name);

            int i = 1;
            foreach (var c in ((TableDefinition)node.Definition).ColumnDefinitions)
            {
                var pd = ((ParameterizedDataTypeReference)c.DataType).Parameters;
                var col = new Column() { name = c.ColumnIdentifier.Value.ToLower(), datatype = c.DataType.Name.Identifiers.First().Value.ToLower(), datatype_val = !pd.Any() ? 0 : Convert.ToInt32(((ParameterizedDataTypeReference)c.DataType).Parameters[0].Value), ordinal = i };
                t.columns.Add(col);
                //Console.WriteLine("TEMP TABLE COLUMN: " + col.name + " " + col.datatype);
                i++;
            }
        }

        public override void Visit(InsertStatement node)
        {

            current_query = new Query() { target = new Table((TableReference)node.InsertSpecification.Target) };
            current_col = 1;
            Program.queries.Add(current_query);

            if (node.InsertSpecification.InsertSource is SelectInsertSource)
            {
                var sis = (node.InsertSpecification.InsertSource as SelectInsertSource).Select;

            if (sis is QuerySpecification)
            {
                
                QuerySpec(sis as QuerySpecification);
            }
            else if (sis is BinaryQueryExpression)
            {
                BinaryQueryExp(sis as BinaryQueryExpression, node);
            }
            else
            {
                throw new Exception(sis.GetType().Name);
            }
            }
            else
            {
                throw new Exception();
            }

            foreach (var c in node.InsertSpecification.Columns)
            {
                current_query.insertintocolumns.Add(new Column(c));
            }
            
            
            base.Visit(node);
        }
        public void BinaryQueryExp(BinaryQueryExpression bqe, InsertStatement node)
        {

            if (bqe.FirstQueryExpression is QuerySpecification)
            {
                current_query = new Query() { target = new Table((TableReference)node.InsertSpecification.Target) };
                current_col = 1;
                QuerySpec(bqe.FirstQueryExpression as QuerySpecification);
            }
            else if (bqe.FirstQueryExpression is BinaryQueryExpression)
            {
                BinaryQueryExp(bqe.FirstQueryExpression as BinaryQueryExpression, node);
            }
            else
            {
                throw new Exception(bqe.FirstQueryExpression.GetType().Name);
            }


            if (bqe.SecondQueryExpression is QuerySpecification)
            {
                current_query = new Query() { target = new Table((TableReference)node.InsertSpecification.Target) };
                current_col = 1;
                QuerySpec(bqe.SecondQueryExpression as QuerySpecification);
            }
            else if (bqe.SecondQueryExpression is BinaryQueryExpression)
            {
               BinaryQueryExp(bqe.SecondQueryExpression as BinaryQueryExpression, node);
            }
            else
            {
                throw new Exception(bqe.FirstQueryExpression.GetType().Name);
            }
        }
        public void QuerySpec(QuerySpecification node)
        {
            current_col = 1;
            current_col_absolute = 1;

            current_query.wherecolumns = GetStringFromTSqlFragment(node.WhereClause).Replace("@", "").Replace("'", "\"");

            foreach (var ele in  node.SelectElements)
            {
               
                if (ele is SelectScalarExpression)
                {
                    var element = (SelectScalarExpression)ele;

                    if (element.Expression is Literal)
                    {
                        Program.Debug2("COL " + (GetStringFromTSqlFragment(element.Expression) ?? "None") + " " + current_col_absolute + "/" + current_col);
                        var col = new Column() { name = GetStringFromTSqlFragment(element.Expression), ordinal = current_col, absordinal = current_col_absolute, table_alias = "Fixed", column_alias = element.ColumnName == null ? "" : element.ColumnName.Identifier.Value,  isliteral = true };
                        current_col++;
                        current_query.selectcolumns.Add(col);

                    }
                    else
                    {
                        select_col_added = false;
                        stringliterals = "";
                        SelectIterate((ScalarExpression)element.Expression, element, new List<string>(), 1);    
                        if(!select_col_added)
                        {
                            Program.Debug2("EMPTYCOL " + current_col_absolute + "/" + current_col + (GetStringFromTSqlFragment(element.Expression) ?? "None"));
                            var col = new Column(stringliterals) { };
                            col.absordinal = current_col_absolute;
                            col.ordinal = current_col;
                            current_query.selectcolumns.Add(col);
                            col.isliteral = true;
                            col.column_alias = element.ColumnName == null ? "" : element.ColumnName.Identifier.Value;
                            current_col++;
                           
                        }
                    }
                }
                current_col_absolute++;
            }

            if (node.FromClause != null)
            {
                foreach (TableReference t in node.FromClause.TableReferences)
                {
                    JoinIterate(t);
                }
            }

            base.Visit(node);
        }

        public override void Visit(SelectStatement node)
        {

            
                current_query = new Query() { };
                current_col = 1;

                if (node.Into != null)
                {
                current_query.target = new Table(node.Into);
                }

                var sis = node.QueryExpression;

                if (sis is QuerySpecification)
                {

                    QuerySpec(sis as QuerySpecification);
                }
                else
                {
                    throw new Exception();
                }
                Program.queries.Add(current_query);
            
            
            base.Visit(node);
        }

        public override void Visit(SelectStarExpression node)
        {
            if (IsIn(node, typeof(InsertStatement)))
            {
                current_query.selectcolumns.Add(new Column("star"));
                //Console.WriteLine("SELECT OR INSERT *");
                //Program.streamwriter_debug.WriteLine("SELECT OR INSERT *");
            }
            base.Visit(node);
        }

       

        public void SelectIterate(ScalarExpression s, Object n, List<string> transform_, int depth)
        {
            Program.Debug2("SUBSTR(" + depth +  ")" + (GetStringFromTSqlFragment(s) ?? "None"));
            List<string> transform = transform_.ToList();
            string name__ = "AA";

            if (n is AssignmentSetClause) { name__ = GetStringFromTSqlFragment(((AssignmentSetClause)n).Column); }
            if (n is SelectScalarExpression) { name__ = ((SelectScalarExpression)n).ColumnName == null ? "" : ((SelectScalarExpression)n).ColumnName.Identifier.Value; }

            //Program.Debug2(current_col_absolute,current_col,s.GetType().Name,GetStringFromTSqlFragment(s));

            if (s.GetType() == typeof(ColumnReferenceExpression) && n is AssignmentSetClause)
            {
                var n2 = (AssignmentSetClause)n;
                var col = new Column((ColumnReferenceExpression)s) { destinationcolumn = GetStringFromTSqlFragment(n2.Column) };
                //Program.streamwriter_debug.WriteLine("UPDATE COLUMN " + GetStringFromTSqlFragment((ColumnReferenceExpression)s) + " : " + GetStringFromTSqlFragment(n2.Column));
                current_query.selectcolumns.Add(col);

                update_col_added = true;
                
            }
            else if (s.GetType() == typeof(ColumnReferenceExpression) && n is SelectScalarExpression)
            {
                var n2 = (SelectScalarExpression)n;
                var col = new Column((ColumnReferenceExpression)s) { ordinal = current_col, absordinal = current_col_absolute, column_alias = n2.ColumnName == null ? "" : n2.ColumnName.Identifier.Value, original = GetStringFromTSqlFragment(n2.Expression) };
                current_col++;
                //Program.Debug2("SELECT COLUMN " + GetStringFromTSqlFragment((ColumnReferenceExpression)s) + " : " + current_col);
                if (current_query == null) { throw new Exception(GetStringFromTSqlFragment(s)); }
                col.transform = transform;
                current_query.selectcolumns.Add(col);
                select_col_added = true;

            }
            else if (s.GetType() == typeof(BinaryExpression))
            {
                SelectIterate(((BinaryExpression)s).FirstExpression, n, transform, depth+1);
                SelectIterate(((BinaryExpression)s).SecondExpression, n, transform, depth + 1);
            }
            else if (s.GetType() == typeof(UnaryExpression))
            {
                SelectIterate(((UnaryExpression)s).Expression, n, transform, depth + 1);
            }
            else if (s.GetType() == typeof(ConvertCall))
            {
                SelectIterate(((ConvertCall)s).Parameter, n, transform, depth + 1);
            }
            else if (s.GetType() == typeof(ParenthesisExpression))
            {
                SelectIterate(((ParenthesisExpression)s).Expression, n, transform, depth + 1);
            }
            else if (s.GetType() == typeof(CastCall))
            {
                SelectIterate(((CastCall)s).Parameter, n, transform, depth + 1);
            }
            else if (s.GetType() == typeof(LeftFunctionCall))
            {
                foreach (ScalarExpression ps in ((LeftFunctionCall)s).Parameters)
                {
                    transform.Add("LEFT(" + GetStringFromTSqlFragment(((LeftFunctionCall)s).Parameters.Last())+")");
                    SelectIterate(ps, n, transform, depth + 1);
                }
            }
            else if (s.GetType() == typeof(RightFunctionCall))
            {
                foreach (ScalarExpression ps in ((RightFunctionCall)s).Parameters)
                {
                    transform.Add("RIGHT(" + GetStringFromTSqlFragment(((RightFunctionCall)s).Parameters.Last()) + ")");
                    SelectIterate(ps, n, transform, depth + 1);
                }
            }
            else if (s.GetType() == typeof(FunctionCall))
            {
                var fname = ((FunctionCall)s).FunctionName.Value.ToUpper();
                if (fname == "LTRIM")
                {
                    transform.Add("LTRIM");
                    SelectIterate(((FunctionCall)s).Parameters.First(), n, transform, depth + 1);
                }
                else if (fname == "RTRIM")
                {
                    transform.Add("RTRIM");
                    SelectIterate(((FunctionCall)s).Parameters.First(), n, transform, depth + 1);
                }
                else
                {
                    var args = ((FunctionCall)s).Parameters;
                    if (fname == "DATEADD" || fname == "DATEPART") { args.RemoveAt(0); }
                    foreach (ScalarExpression ps in args)
                    {
                        SelectIterate(ps, n, transform, depth + 1);
                    }
                }

            }
            else if (s.GetType() == typeof(CoalesceExpression))
            {
                foreach (ScalarExpression ps in ((CoalesceExpression)s).Expressions)
                {
                    SelectIterate(ps, n, transform, depth + 1);
                }
            }
            else if (s.GetType() == typeof(SearchedCaseExpression))
            {
                foreach (WhenClause w in ((SearchedCaseExpression)s).WhenClauses)
                {
                    SelectIterate(w.ThenExpression, n, transform, depth + 1);
                }
                if (((SearchedCaseExpression)s).ElseExpression != null) { SelectIterate(((SearchedCaseExpression)s).ElseExpression, n, transform, depth + 1); }
            }
            else if (s.GetType() == typeof(SimpleCaseExpression))
            {
                foreach (WhenClause w in ((SimpleCaseExpression)s).WhenClauses)
                {
                    SelectIterate(w.ThenExpression, n, transform, depth + 1);
                }
            }
            else if (s.GetType() == typeof(StringLiteral))
            {
                stringliterals += GetStringFromTSqlFragment(s) + ", ";
            }
            else if (s is ValueExpression) { stringliterals += GetStringFromTSqlFragment(s) + ", "; }
            else if (s is ScalarSubquery)
            {
                var q = ((ScalarSubquery)s).QueryExpression;

                if (q is QuerySpecification)
                {
                    scalarsubqueryid++;
                    stored_query = current_query;
                    stored_col = current_col;
                    stored_col_absolute = current_col_absolute;
                    current_query = new Query() { target = new Table("scalarsub" + scalarsubqueryid) };

                    current_col = 1;
                    Program.queries.Add(current_query);

                    QuerySpec(q as QuerySpecification);

                    current_query = stored_query;
                    current_col = stored_col;
                    current_col_absolute = stored_col_absolute;

                    var col2 = new Column("scalarsub" + scalarsubqueryid) { name = "scalarsub" + scalarsubqueryid, ordinal = current_col, absordinal = current_col_absolute, column_alias = name__, original = "scalarsubquery" };
                    current_col++;
                    col2.transform = transform;
                    current_query.selectcolumns.Add(col2);
                    select_col_added = true;
                }
                else
                {
                    throw new Exception();
                }
            }
            else { throw new Exception(s.GetType().ToString()); }
        }


        public Boolean IsIn(TSqlFragment node, Type T)
        {
            return fragments.Exists(x => x.Item1 == T && node.FirstTokenIndex >= x.Item2 && node.LastTokenIndex <= x.Item3);
        }

        public Boolean IsInFunction(TSqlFragment node, string functiontype, out FunctionCall func)
        {
            var q = functions.Where(x => x.Item1.FunctionName.Value.ToLower() == functiontype.ToLower() && node.FirstTokenIndex >= x.Item2 && node.LastTokenIndex <= x.Item3).FirstOrDefault();
            if (q == null)
            {
                func = null;
            }
            else
            {
                func = q.Item1;
            }
            return functions.Exists(x => x.Item1.FunctionName.Value.ToLower() == functiontype.ToLower() && node.FirstTokenIndex >= x.Item2 && node.LastTokenIndex <= x.Item3);
        }

        public Boolean IsInFunction(TSqlFragment node, string functiontype)
        {
            return functions.Exists(x => x.Item1.FunctionName.Value.ToLower() == functiontype.ToLower() && node.FirstTokenIndex >= x.Item2 && node.LastTokenIndex <= x.Item3);
        }

        public void Debug(Object s)
        {
            //Program.streamwriter_debug.WriteLine(s.ToString());
        }
        public void Debug(Object s, Object s2)
        {
           // Program.streamwriter_debug.WriteLine(s.ToString() + " / " + s2.ToString());
        }

        public void Debug(Object s, Object s2, Object s3)
        {
           // Program.streamwriter_debug.WriteLine(s.ToString() + " / " + s2.ToString() + " / " + s3.ToString());
        }


        public Boolean IsIn(int token, Type T)
        {
            return fragments.Exists(x => x.Item1 == T && token >= x.Item2 && token <= x.Item3);
        }

        /*public Boolean IsInSub(int token, Type T)
        {
            return fragments.Exists(x => x.Item1.IsSubclassOf(T) && token >= x.Item2 && token <= x.Item3);
        }*/

        public static string GetStringFromTSqlFragment(TSqlFragment fragment)
        {
            if(fragment == null)
                { return ""; }
            else
            {
                return Program.query.Substring(Math.Max(0, fragment.StartOffset), Math.Max(0, fragment.FragmentLength));
            }          
        }


        public void JoinIterate(TableReference node)
        {
            if (node is QueryDerivedTable)
            {
                var qdt = ((QueryDerivedTable)node);
                if (qdt.QueryExpression is QuerySpecification)
                {
                    stored_query = current_query;
                    stored_col = current_col;
                    stored_col_absolute = current_col_absolute;
                    current_query = new Query() { target = new Table(qdt.Alias) };

                    current_query.name = qdt.Alias.Value;

                    

                    current_col = 1;
                    Program.queries.Add(current_query);

                    QuerySpec(qdt.QueryExpression as QuerySpecification);

                    current_query = stored_query;
                    current_col = stored_col;
                    current_col_absolute = stored_col_absolute;
                }
                else
                {
                    throw new Exception();
                }
            }
            else if (node is JoinTableReference)
            {
               // Console.WriteLine("JOIN");
                JoinIterate((node as JoinTableReference).FirstTableReference);
                JoinIterate((node as JoinTableReference).SecondTableReference);
            }
            else if (node is NamedTableReference || node is VariableTableReference)
            {
                    var n = node as TableReferenceWithAlias;
                    if (n.Alias != null)
                    {
                        var t = new Table(n, n.Alias.Value.ToLower());
                        if (t.name == null) {throw new Exception();}
                        current_query.sources.Add(t);
                    }
                    else
                    {
                        var t = new Table(node);
                        if (t.name == null) { throw new Exception(); }
                        current_query.sources.Add(t);

                    }
            }
            
            else
            {
                    throw new Exception(node.GetType().Name);

      
            }
            
        }
    }
}


