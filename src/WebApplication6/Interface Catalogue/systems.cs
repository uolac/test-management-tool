using System;
using System.Collections.Generic;

namespace WebApplication6
{
    public partial class systems
    {
        public systems()
        {
            tables = new HashSet<tables>();
        }

        public int system_id { get; set; }
        public string system_name { get; set; }

        public virtual ICollection<tables> tables { get; set; }
    }
}
