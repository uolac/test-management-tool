﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Data.SqlClient;
using MoreLinq;




namespace T1ETL
{
    class Program
    {
    public static StreamWriter sw, sw2;
    public static Regex squarebracketregex = new Regex("\\[(.*?)\\]");
        public static int intfid;
        public static int ord;
        public static SqlCommand com;
        public static string argument1;
        public static bool current;

    public static void Start(int interfaceid, bool current_, string argument1_)
    {
            intfid = interfaceid;
            argument1 = argument1_;
            ord = 1;
            current = current_;

            //var filestr = "WPM_RCP_ACCOM_RECEIPTS";

            //var f1 = File.ReadAllText("C:\\Users\\aCoady\\XML\\wpm\\"+filestr+".t1etl");

            SqlConnection con2 = new SqlConnection();
            con2.ConnectionString = "Server=ASQL02;Database=testmanagementtool;Integrated Security=true;";
            con2.Open();

            com = new SqlCommand();
            com.Connection = con2;
            com.CommandText = "SELECT script FROM testmanagementtool.dbo.interfaces WHERE interface_id = @param";
            com.Parameters.AddWithValue("@param", interfaceid);

            var f1 = (string)com.ExecuteScalar();

            for (int i = 100; i > 0; i--)
        {
            f1 = f1.Replace("<Column." + i, "<Column id=\"" + i + "\"");
            f1 = f1.Replace("</Column." + i, "</Column");
        }

        //File.WriteAllText("C:\\Users\\aCoady\\XML\\"+filestr+"_2.txt", f1);
        //var f2 = new FileStream("C:\\Users\\aCoady\\XML\\" + filestr + "_2.txt", FileMode.Open);

       // var output = "C:\\Users\\aCoady\\XML\\Output.txt";
       // var output2 = "C:\\Users\\aCoady\\XML\\Output2.txt";
       // var f3 = new FileStream(output, FileMode.Create);
       // var f4 = new FileStream(output2, FileMode.Create);

//sw = new StreamWriter(f3);
      //  sw2 = new StreamWriter(f4);


        var s = new XmlSerializer(typeof(root));
            var ds = (root)s.Deserialize(new StringReader(f1));

        var links = new List<Link>();


        var steps = ds.Steps.items;

        steps.First().Items.classname = ds.Steps.ItemClassName;

        var steps_flat = ds.Steps.items;


        foreach (var step in steps)
        {
            switch (step.Items.classname)
            {
                case "T1.E1.ETL.ETLStepRepeat":
                    {
                        var temp = step.Items.SubSteps.items;
                        temp.First().Items.classname = step.Items.SubSteps.ItemClassName;
                        steps_flat = ArrayMerge<rootStepsItem>(steps_flat, temp);
                        break;
                    }

                case "T1.E1.ETL.ETLStepBranch":
                    {

                        foreach(var branch in step.Items.branches.items)
                        {
                            var temp = branch.Items.steps.items;
                            temp.First().Items.classname = branch.Items.steps.ItemClassName;
                            steps_flat = ArrayMerge<rootStepsItem>(steps_flat, branch.Items.steps.items);
                        }
                        
                        break;
                    }
            }
        }

        steps = steps_flat;

        foreach (var step in steps)
        {
            switch (step.Items.classname)
            {
                case "T1.E1.ETL.ETLStepRepeat":
                    {
                        var temp = step.Items.SubSteps.items;
                        temp.First().Items.classname = step.Items.SubSteps.ItemClassName;
                        steps_flat = ArrayMerge<rootStepsItem>(steps_flat, temp);
                        break;
                    }

                case "T1.E1.ETL.ETLStepBranch":
                    {

                        foreach (var branch in step.Items.branches.items)
                        {
                            var temp = branch.Items.steps.items;
                            temp.First().Items.classname = branch.Items.steps.ItemClassName;
                            steps_flat = ArrayMerge<rootStepsItem>(steps_flat, branch.Items.steps.items);
                        }

                        break;
                    }
            }
        }

        BranchingSteps(steps_flat, links);

        


        foreach (var link in links)
        {
            Echo2(link.stepname);
          Echo2(link.stepname + "#" + link.table1.type + "#" + link.table1.table + "#" + link.table1.column + "#" + link.table2.type + "#" + link.table2.table + "#" + link.table2.column);
        }



        /*foreach (var link in links.Where(l => l.table1.type == "FIXED LENGTH IMPORT"))
        {
            var b = Iterate(link, link, links, link.transform, new List<Link>());
            if (!b)
            {
                if (link.table2.type == "OUTPUT - LINE" || link.table2.type == "OUTPUT - DOCUMENT" || link.table2.type == "OUTPUT - DOCFILE" || lnk.table2.type == "OUTPUT - CHART")
                {
                    Echo(link.table1.table + "#" + link.table1.column + "#" + link.table2.table + "#" + link.table2.column);
                }
                else
                {
                    Echo(link.table1.table + "#" + link.table1.column /*+ " (" + link.stepname + ")");
                }
            }
        }

        Echo("");*/

        Echo("Source File" + "#" + "ETL Source" + "#" + "Source Column Name" + "#" + "Position" + "#" + "Type" + "#" + "Fixed Length Position" + "#" + "Fixed Length" + "# " + "ETL Destination" + "#" + "Destination Column Name" + "#" + "ETL Transformation" + "#" + "Web Service");

            

            foreach (var link in links.Where(l => l.table2.type == "OUTPUT - LINE" || l.table2.type == "OUTPUT - DOCUMENT" || l.table2.type == "OUTPUT - DOCFILE" || l.table2.type == "OUTPUT - CHART"))
            {
            var b = IterateReverse(link, link, links, link.transform, new List<Link>());
            if (!b)
            {
                if ( link.table1.type == "CSV IMPORT" || link.table1.type == "FIXED LENGTH IMPORT" || link.table1.type == "DATASOURCE" || link.table1.type == "CALCULATED")
                {
                        var colstr = "#" + link.table1.table + "#" + link.table1.column + "# # # # #" + link.table2.table + "#" + link.table2.column + "#";
                    Echo(colstr);
                }
                else
                {
                        var colstr = "#CALCULATED # COLUMN NOT IN EXPORTED CSV # # # # #" + link.table2.table + "#" + link.table2.column /*+ " (" + link.stepname + ")"*/;
                    Echo(colstr);

                    
                }
            }
        }



            con2.Close();



            //sw.Close();
        //f3.Close();
       // sw2.Close();
        //f4.Close();

       // Process.Start("notepad.exe", output);
       // Process.Start("notepad.exe", output2);


    }

    static T[] ArrayMerge<T>(T[] r1, T[] r2)
    {
        var l1 = r1.ToList();
        var l2 = r2.ToList();
        l1.AddRange(l2);
        T[] newArray = l1.ToArray();
        return newArray;
    }

    static void BranchingSteps(rootStepsItem[] steps,List<Link> links)
    {
        

        foreach (var step in steps)
        {
            if (step == null)
            {
                Crash(step.GetType().FullName);
            }

            if (step.Items.classname == null)
            {
                var set = "";
                switch (step.Key)
                {
                    case "Get AR Import Table": set = "T1.E1.ETL.ETLStepRunQuery"; break;
                    case "Get SR Import Table": set = "T1.E1.ETL.ETLStepRunQuery"; break;
                    case "Create Bank Lines": set = "T1.E1.ETL.ETLStepRunQuery"; break;
                    case "Create Receipt Lines": set = "T1.E1.ETL.ETLStepRunQuery"; break;
                    case "Create Prnt Lines": set = "T1.E1.ETL.ETLStepRunQuery"; break;
                    case "Create Bank Lines - Cash": set = "T1.E1.ETL.ETLStepRunQuery"; break;
                    case "Create Receipt Lines - Cash": set = "T1.E1.ETL.ETLStepRunQuery"; break;
                    case "Create Bank Lines - Card": set = "T1.E1.ETL.ETLStepRunQuery"; break;
                    case "Create Receipt Line - Card": set = "T1.E1.ETL.ETLStepRunQuery"; break;
                }
                step.Items.classname = set;
            }

            //Echo2(step.Key);

            switch (step.Items.classname)
            {
                case "T1.F1.ETL.ETLStepChartImport":
                    {
                        var chartname = step.Items.ImportSettings.ChartName;
                        var importfunction = step.Items.ImportSettings.ImportFunctionType == null ? "Account Detail" : "Account Address";



                        var outputname = "CHART[" + chartname + "," + importfunction + "]";

                        var memtable = step.Items.InputMemoryTableName;
                        if (memtable == null)
                        {
                            //Match to CSV import

                            var csvstep = steps.Where(s => s.Items.classname == "T1.E1.ETL.ETLStepExportMemoryTableToCSV" && s.Items.FileName == step.Items.ImportSettings.FileName).First();
                            memtable = csvstep.Items.ExportMemoryTableName;
                        }
                        

                            foreach (var item in step.Items.ImportColumnMappings.items)
                            {
                                if (item.Items.ToValueSource != null)
                                {
                                    if (item.Items.ToValueSource.ToString() == "1")
                                    {
                                        links.Add(new Link(new ColumnReference("CALCULATED", "CALCULATED", "([" + item.Items.FromValue + "] = " + item.Items.ToValue + ")"), new ColumnReference("OUTPUT - CHART", outputname, item.Items.FromValue), step.Key));
                                    }
                                    else
                                    {
                                        links.Add(new Link(new ColumnReference("MEMORYTABLE", memtable, item.Items.ToValue), new ColumnReference("OUTPUT - CHART", outputname, item.Items.FromValue), step.Key));
                                    }
                                }
                                else
                                {
                                    links.Add(new Link(new ColumnReference("MEMORYTABLE", memtable, item.Items.ToValue), new ColumnReference("OUTPUT - CHART", outputname, item.Items.FromValue), step.Key));
                                }
                            }
                        
                        break;
                    }

                case "T1.F1.ETL.ETLStepDocumentFileImport":
                    {

                        var format = step.Items.DocFileImportColumnMappings.items.Where(i => i.Key == "BAT_FMT_NAME").FirstOrDefault().Items.ToValue;
                        var doctype = step.Items.DocFileImportColumnMappings.items.Where(i => i.Key == "BAT_DOC_TYPE").FirstOrDefault().Items.ToValue;

                        var outputname = "POST[" + format + "," + doctype + "]";

                        foreach (var item in step.Items.LineImportColumnMappings.items)
                        {
                            if (item.Items.ToValueSource.ToString() == "1")
                            {
                                links.Add(new Link(new ColumnReference("CALCULATED", "CALCULATED", "([" + item.Items.FromValue + "] = " + item.Items.ToValue + ")"), new ColumnReference("OUTPUT - LINE", outputname, item.Items.FromValue), step.Key));
                            }
                            else
                            {
                                
                                links.Add(new Link(new ColumnReference("MEMORYTABLE", step.Items.InputMemoryTableName, item.Items.ToValue), new ColumnReference("OUTPUT - LINE", outputname, item.Items.FromValue), step.Key));
                            }
                        }

                        foreach (var item in step.Items.DocumentImportColumnMappings.items)
                        {
                            if (item.Items.ToValueSource.ToString() == "1")
                            {
                                links.Add(new Link(new ColumnReference("CALCULATED", "CALCULATED", "([" + item.Items.FromValue + "] = " + item.Items.ToValue + ")"), new ColumnReference("OUTPUT - DOCUMENT", outputname, item.Items.FromValue), step.Key));
                            }
                            else
                            {
                                links.Add(new Link(new ColumnReference("MEMORYTABLE", step.Items.InputMemoryTableName, item.Items.ToValue), new ColumnReference("OUTPUT - DOCUMENT", outputname, item.Items.FromValue), step.Key));
                            }
                        }

                        foreach (var item in step.Items.DocFileImportColumnMappings.items)
                        {
                            if (item.Items.ToValueSource.ToString() == "1")
                            {
                                links.Add(new Link(new ColumnReference("CALCULATED", "CALCULATED", "([" + item.Items.FromValue + "] = " + item.Items.ToValue + ")"), new ColumnReference("OUTPUT - DOCFILE", outputname, item.Items.FromValue), step.Key));
                            }
                            else
                            {
                                links.Add(new Link(new ColumnReference("MEMORYTABLE", step.Items.InputMemoryTableName, item.Items.ToValue), new ColumnReference("OUTPUT - DOCFILE", outputname, item.Items.FromValue), step.Key));
                            }
                        }
                        break;
                    }

                case "T1.E1.ETL.ETLStepRunQuery":
                    {
                        foreach (var column in step.Items.ConsolidationDefn.Columns.Column)
                        {

                            if (column.CalculateExpression != null)
                            {
                                var matches = squarebracketregex.Matches(column.CalculateExpression).OfType<Match>().Where(m => column.CalculateExpression.Substring(Math.Max(0, m.Index - 4), 4) != "IIF(" || m.Index - 4 < 0).DistinctBy(m => m.Value);
                                if (matches.Count() == 0)
                                {
                                    links.Add(new Link(new ColumnReference("CALCULATED", "CALCULATED", "([" + column.ColumnName + "] = " + column.CalculateExpression + ")"), new ColumnReference("MEMORYTABLE", step.Items.OutputMemoryTableName, column.ColumnName), step.Key));
                                }
                                foreach (Match match in matches)
                                {

                                    if (step.Items.InputMemoryTableName != null)
                                    {
                                        links.Add(new Link(new ColumnReference("MEMORYTABLE", step.Items.InputMemoryTableName, match.Value.Replace("[", "").Replace("]", "")), new ColumnReference("MEMORYTABLE", step.Items.OutputMemoryTableName, column.ColumnName), step.Key, column.CalculateExpression));
                                    }
                                    else
                                    {
                                        links.Add(new Link(new ColumnReference("MEMORYTABLE", step.Items.DataSourceName, match.Value.Replace("[", "").Replace("]", "")), new ColumnReference("MEMORYTABLE", step.Items.OutputMemoryTableName, column.ColumnName), step.Key, column.CalculateExpression));
                                    }

                                }
                            }


                            if (step.Items.InputMemoryTableName != null)
                            {
                                links.Add(new Link(new ColumnReference("MEMORYTABLE", step.Items.InputMemoryTableName, column.EnquirySetBaseColumnId), new ColumnReference("MEMORYTABLE", step.Items.OutputMemoryTableName, column.ColumnName), step.Key));
                            }
                            else
                            {
                                if (step.Items.DataSourceName == "F1MGT.ChartAccounts")
                                {
                                    if (step.Items.ConsolidationDefn.Criteria.Criterion1.ColumnName != "GLF_CHART_CTL.CHART_NAME" && step.Items.ConsolidationDefn.Criteria.Criterion1.ColumnName != "F1Cha_ChartName")
                                    {
                                        throw new Exception("Chart criterion not found"+step.Key);
                                    }


                                    var chart = step.Items.ConsolidationDefn.Criteria.Criterion1.Value;
                                    links.Add(new Link(new ColumnReference("DATASOURCE", "CHART: "+chart, column.EnquirySetBaseColumnId), new ColumnReference("MEMORYTABLE", step.Items.OutputMemoryTableName, column.ColumnName), step.Key));
                                    var existslookuptable = step.Items.ExistsFilters.items.item.Items.ExistsMemoryTableName;
                                    var existslookupcolumn = step.Items.ExistsFilters.items.item.Items.LinkColumns.items.item.Items.FromValue;
                                    links.Add(new Link(new ColumnReference("MEMORYTABLE", existslookuptable, existslookupcolumn), new ColumnReference("MEMORYTABLE", step.Items.OutputMemoryTableName, column.ColumnName), step.Key));
                                }
                                else
                                {
                                    links.Add(new Link(new ColumnReference("DATASOURCE", step.Items.DataSourceName, column.EnquirySetBaseColumnId), new ColumnReference("MEMORYTABLE", step.Items.OutputMemoryTableName, column.ColumnName), step.Key));
                                }
                            }
                        }
                        break;
                    }

                case "T1.E1.ETL.ETLStepLoadFixedLengthFile":
                    {
                        var colcount = 1;

                        foreach (var column in step.Items.ColumnDefns.items)
                        {
                            var datatype = "String";

                                if (column.Items.DataType.ToString() == "2") { datatype = "Float"; }
                                if (column.Items.DataType.ToString() == "3") { datatype = "Integer"; }

                                links.Add(new Link(new ColumnReference("FIXED LENGTH IMPORT", "IMPORT[" + step.Items.ServerFolder + "/" + step.Items.FileName + "]", column.Items.ColumnName, colcount.ToString(), datatype, column.Items.FileStartPosition.ToString(), column.Items.FieldLength.ToString()), new ColumnReference("MEMORYTABLE", step.Items.OutputMemoryTableName, column.Items.ColumnName), step.Key));
                                colcount++;
                        }
                        break;
                    }

                case "T1.E1.ETL.ETLStepLoadCSVFile":
                    {
                        foreach (var column in step.Items.ColumnDefns.items)
                        {
                            var datatype = "String";

                                if (column.Items.DataType.ToString() == "2") { datatype = "Float"; }
                                if (column.Items.DataType.ToString() == "3") { datatype = "Integer"; }

                                links.Add(new Link(new ColumnReference("CSV IMPORT", "IMPORT[" + step.Items.ServerFolder + "/" + step.Items.FileName + "]", column.Items.ColumnName,column.Items.FileColumnIndex,datatype), new ColumnReference("MEMORYTABLE", step.Items.OutputMemoryTableName, column.Items.ColumnName), step.Key));
                        }
                        break;
                    }

                case "T1.E1.ETL.ETLStepAddColumns":
                    {
                        if (step.Items.CreateKeyColumn != null)
                        {
                            if (step.Items.CreateKeyColumn == "True")
                            {
                                links.Add(new Link(new ColumnReference("CALCULATED", "CALCULATED", "([" + step.Items.KeyColumnName + "] = " + "NEW KEYCOLUMN" + ")"), new ColumnReference("MEMORYTABLE", step.Items.OutputMemoryTableName, step.Items.KeyColumnName), step.Key));
                            }
                        }
                        if (step.Items.Columns != null)
                        {
                        
                        foreach (var column in step.Items.Columns.Column)
                        {


                            if (column.CalculateExpression != null)
                            {
                                var matches = squarebracketregex.Matches(column.CalculateExpression).OfType<Match>().Where(m => column.CalculateExpression.Substring(Math.Max(0, m.Index - 4), 4) != "IIF(" || m.Index - 4 < 0).DistinctBy(m => m.Value);
                                if (matches.Count() == 0)
                                {
                                    links.Add(new Link(new ColumnReference("CALCULATED", "CALCULATED", "([" + column.ColumnName + "] = " + column.CalculateExpression + ")"), new ColumnReference("MEMORYTABLE", step.Items.OutputMemoryTableName, column.ColumnName), step.Key));
                                }
                                foreach (Match match in matches)
                                {

                                    if (step.Items.InputMemoryTableName != null)
                                    {
                                        links.Add(new Link(new ColumnReference("MEMORYTABLE", step.Items.InputMemoryTableName, match.Value.Replace("[", "").Replace("]", "")), new ColumnReference("MEMORYTABLE", step.Items.OutputMemoryTableName, column.ColumnName), step.Key, column.CalculateExpression));
                                    }
                                    else
                                    {
                                        links.Add(new Link(new ColumnReference("MEMORYTABLE", step.Items.OutputMemoryTableName, match.Value.Replace("[", "").Replace("]", "")), new ColumnReference("MEMORYTABLE", step.Items.OutputMemoryTableName, column.ColumnName), step.Key, column.CalculateExpression));
                                    }

                                }
                            }

                            if (column.LookupField != null)
                            {
                                links.Add(new Link(new ColumnReference("LOOKUP", column.LookupName, column.LookupField), new ColumnReference("MEMORYTABLE", step.Items.OutputMemoryTableName, column.ColumnName), step.Key));
                            }


                        }
                        }

                        break;
                    }
            }



        }

        foreach (var step in steps)
        {
            if (step.Items.classname == "T1.E1.ETL.ETLStepAppendMemoryTable" && step.Key != "Append Memory Table1")
            {
                if (step.Items.ImportColumnMappings != null)
                {
                    foreach (var column in step.Items.ImportColumnMappings.items)
                    {
                        links.Add(new Link(new ColumnReference("MEMORYTABLE", step.Items.InputMemoryTableName, column.Items.ToValue), new ColumnReference("MEMORYTABLE", step.Items.OutputMemoryTableName, column.Items.FromValue), step.Key));
                    }
                }
            }
        }

        foreach (var step in steps)
        {
            if (step.Items.classname == "T1.E1.ETL.ETLStepAddColumns")
            {
                if (step.Items.LookupDefns != null)
                {
                    foreach (var lookup in step.Items.LookupDefns.items)
                    {
                        foreach (var l in links.Where(l => l.table1.table == lookup.Items.LookupName && l.stepname == step.Key))
                        {
                            l.table1.table = lookup.Items.LookupMemoryTableName;
                        }
                    }
                }
            }
        }
    }

    static bool Iterate(Link original, Link l, List<Link> links, string transform, List<Link> done)
    {
        bool b = false;
        foreach (Link lnk in links.Where(lk => lk.table1.table == l.table2.table && lk.table1.column == l.table2.column).Except(done))
        {
            string transform_ = transform;
            if (lnk.transform != "") { if (transform != "") { transform_ += " AND "; } transform_ += "( " + lnk.transform + " )"; }

            if (lnk.table2.type == "OUTPUT - LINE" || lnk.table2.type == "OUTPUT - DOCUMENT" || lnk.table2.type == "OUTPUT - DOCFILE" || lnk.table2.type == "OUTPUT - CHART")
            {
                Echo("#" + original.table1.table + "#" + original.table1.column + "# # # # #" + lnk.table2.table + "#" + lnk.table2.column + "#" + (transform_ != "" ? transform_ : ""));
                b = true;
            }

            var b2 = Iterate(original, lnk, links, transform_, done);
            done.Add(lnk);
            b = b2 || b;
        }
        return b;
    }

    static bool IterateReverse(Link original, Link l, List<Link> links, string transform, List<Link> done)
    {
        bool b = false;
        foreach (Link lnk in links.Where(lk => lk.table2.table == l.table1.table && lk.table2.column == l.table1.column).Except(done))
        {
            string transform_ = transform;
            if (lnk.transform != "") { if (transform != "") { transform_ += " AND "; } transform_ += "( " + lnk.transform + " )"; }

                if (lnk.table1.type == "FIXED LENGTH IMPORT" || lnk.table1.type == "CSV IMPORT" || lnk.table1.type == "DATASOURCE" || lnk.table1.type == "CALCULATED")
                {
                    Echo("#" + lnk.table1.table + "#" + lnk.table1.column + "#" + lnk.table1.columnnumber + "#" + lnk.table1.datatype + "#" + lnk.table1.fileposition + "#" + lnk.table1.length + "# " + original.table2.table + "#" + original.table2.column + "#" + (transform_ != "" ? transform_ : ""));
                    b = true;

                    if (lnk.table1.type == "FIXED LENGTH IMPORT" || lnk.table1.type == "CSV IMPORT")
                    {
                        //com.CommandText = "INSERT INTO testmanagementtool.dbo.holding_table VALUES (" + intfid + ",1,'T1.ETL.DOCUMENT " + original.table2.table.Substring(5, original.table2.table.Length-6) + "','" + ord + "','" + lnk.table1.column + "','-','" + original.table2.column + "','-','" + 0 + "','" + lnk.table1.datatype + "','System.Files." + argument1 + "','" + (transform_ != "" ? transform_.Replace("'","\"") : "") + "','" + 0 + "','unknown',NULL,NULL,NULL,'System','Files','"+argument1+"',NULL,'T1','ETL','DOCUMENT " + original.table2.table.Substring(5, original.table2.table.Length-6) + "'," + (current ? 0 : 1) + ",NULL)";
                        com.CommandText = "INSERT INTO testmanagementtool.dbo.holding_table VALUES (@intfid,1,@param1, @ord,@column1,'-',@column2,'-',0,@datatype1,@param2,@transform,0,'unknown',NULL,NULL,NULL,'System','Files',@param3,NULL,'T1','ETL',@tablename,@current,NULL)";
                            com.Parameters.AddWithValue("@intfid", intfid);
                            com.Parameters.AddWithValue("@param1", "T1.ETL.DOCUMENT" + original.table2.table.Substring(5, original.table2.table.Length - 6) ?? "");
                            com.Parameters.AddWithValue("@ord", ord);
                            com.Parameters.AddWithValue("@column1", lnk.table1.column ?? "");
                            com.Parameters.AddWithValue("@column2", original.table2.column ?? "");
                            com.Parameters.AddWithValue("@datatype1", lnk.table1.datatype ?? "");
                            com.Parameters.AddWithValue("@param2", "System.Files." + argument1);
                            com.Parameters.AddWithValue("@transform", (transform_ != "" ? transform_.Replace("'", "\"") : "") ?? "");
                            com.Parameters.AddWithValue("@param3",argument1);
                            com.Parameters.AddWithValue("@tablename", "DOCUMENT " + original.table2.table.Substring(5, original.table2.table.Length - 6) );
                            com.Parameters.AddWithValue("@current", (current ? 0 : 1));
                        com.ExecuteNonQuery();
                        com.Parameters.Clear();
                        ord++;
                    }
                }

            if (original.table2.column == "LNE_ACCNBRI")
            {
                Echo2("#" + lnk.stepname + "#" + lnk.table1.type + "#" + lnk.table1.table + "#" + lnk.table1.column + "# " + lnk.table2.type + "#" + lnk.table2.table + "#" + lnk.table2.column + "#" + (transform_ != "" ? transform_ : "") + "#" + "AAAAAA" + "#");
           }

            var b2 = IterateReverse(original, lnk, links, transform_, done);
            done.Add(lnk);
            b = b2 || b;
        }
        return b;
    }

    static void Echo(Object o)
    {
            var str = o.ToString();
        //sw.WriteLine(str);

    }

    static void Echo2(Object o)
    {
        //sw2.WriteLine(o.ToString());
    }

    static void Crash(Object o)
    {
        throw new Exception(o.ToString());
    }
}

//It's on the appended GLLines


public class ColumnReference
{
    public string table;
    public string column;
    public string type;
    public string columnnumber;
    public string datatype;
    public string fileposition;
    public string length;


    public ColumnReference(string type, string table, string column, string columnnumber = "", string datatype = "", string fileposition = "", string length = "")
    {
        this.table = table;
        this.column = column;
        this.type = type;
        this.columnnumber = columnnumber;
        this.datatype = datatype;
        this.fileposition = fileposition;
        this.length = length;
    }
}

public class Link
{
    public ColumnReference table1;
    public ColumnReference table2;
    public string stepname;
    public string transform;

    public Link(ColumnReference table1, ColumnReference table2, string stepname, string transform = "")
    {
        this.table1 = table1;
        this.table2 = table2;
        this.stepname = stepname;
        this.transform = transform;
    }
}
}


