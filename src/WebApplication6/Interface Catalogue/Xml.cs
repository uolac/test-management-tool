﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.Diagnostics;
using MoreLinq;

namespace T1ETL
{

   

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class root
    {

        private rootVariables variablesField;

        private rootSteps stepsField;

        private string processTypeField;

        private string processNameField;

        private string descriptionField;

        private string narrationField;

        private string categoryField;

        private string createUserField;

        private string createDateField;

        private System.DateTime createTimeField;

        private string createNodeField;

        private string createWindowField;

        private string lastModUserField;

        private string lastModDateField;

        private System.DateTime lastModTimeField;

        private string lastModNodeField;

        private string lastModWindowField;

        private string windowNameField;

        private string uiField;

        /// <remarks/>
        public rootVariables Variables
        {
            get
            {
                return this.variablesField;
            }
            set
            {
                this.variablesField = value;
            }
        }

        /// <remarks/>
        public rootSteps Steps
        {
            get
            {
                return this.stepsField;
            }
            set
            {
                this.stepsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ProcessType
        {
            get
            {
                return this.processTypeField;
            }
            set
            {
                this.processTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ProcessName
        {
            get
            {
                return this.processNameField;
            }
            set
            {
                this.processNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Narration
        {
            get
            {
                return this.narrationField;
            }
            set
            {
                this.narrationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Category
        {
            get
            {
                return this.categoryField;
            }
            set
            {
                this.categoryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CreateUser
        {
            get
            {
                return this.createUserField;
            }
            set
            {
                this.createUserField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CreateDate
        {
            get
            {
                return this.createDateField;
            }
            set
            {
                this.createDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "time")]
        public System.DateTime CreateTime
        {
            get
            {
                return this.createTimeField;
            }
            set
            {
                this.createTimeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CreateNode
        {
            get
            {
                return this.createNodeField;
            }
            set
            {
                this.createNodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CreateWindow
        {
            get
            {
                return this.createWindowField;
            }
            set
            {
                this.createWindowField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string LastModUser
        {
            get
            {
                return this.lastModUserField;
            }
            set
            {
                this.lastModUserField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string LastModDate
        {
            get
            {
                return this.lastModDateField;
            }
            set
            {
                this.lastModDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "time")]
        public System.DateTime LastModTime
        {
            get
            {
                return this.lastModTimeField;
            }
            set
            {
                this.lastModTimeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string LastModNode
        {
            get
            {
                return this.lastModNodeField;
            }
            set
            {
                this.lastModNodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string LastModWindow
        {
            get
            {
                return this.lastModWindowField;
            }
            set
            {
                this.lastModWindowField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string WindowName
        {
            get
            {
                return this.windowNameField;
            }
            set
            {
                this.windowNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string UI
        {
            get
            {
                return this.uiField;
            }
            set
            {
                this.uiField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootVariables
    {

        private rootVariablesItem[] itemsField;

        private rootVariablesErrors errorsField;

        private string classnameField;

        private string assemblyField;

        private string collectionNameField;

        private byte collectionCountField;

        private string itemClassNameField;

        private string itemAssemblyNameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("item", IsNullable = false)]
        public rootVariablesItem[] items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        public rootVariablesErrors Errors
        {
            get
            {
                return this.errorsField;
            }
            set
            {
                this.errorsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CollectionName
        {
            get
            {
                return this.collectionNameField;
            }
            set
            {
                this.collectionNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte CollectionCount
        {
            get
            {
                return this.collectionCountField;
            }
            set
            {
                this.collectionCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ItemClassName
        {
            get
            {
                return this.itemClassNameField;
            }
            set
            {
                this.itemClassNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ItemAssemblyName
        {
            get
            {
                return this.itemAssemblyNameField;
            }
            set
            {
                this.itemAssemblyNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootVariablesItem
    {

        private rootVariablesItemItems itemsField;

        private string keyField;

        /// <remarks/>
        public rootVariablesItemItems Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Key
        {
            get
            {
                return this.keyField;
            }
            set
            {
                this.keyField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootVariablesItemItems
    {

        private string nameField;

        private string descriptionField;

        private string dataTypeField;

        private string isEditableField;

        private string mandatoryField;

        private string displayField;

        private byte decimalPlacesField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DataType
        {
            get
            {
                return this.dataTypeField;
            }
            set
            {
                this.dataTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string IsEditable
        {
            get
            {
                return this.isEditableField;
            }
            set
            {
                this.isEditableField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Mandatory
        {
            get
            {
                return this.mandatoryField;
            }
            set
            {
                this.mandatoryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte DecimalPlaces
        {
            get
            {
                return this.decimalPlacesField;
            }
            set
            {
                this.decimalPlacesField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootVariablesErrors
    {

        private rootVariablesErrorsErrors errorsField;

        private string classnameField;

        private string assemblyField;

        /// <remarks/>
        public rootVariablesErrorsErrors Errors
        {
            get
            {
                return this.errorsField;
            }
            set
            {
                this.errorsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootVariablesErrorsErrors
    {

        private object itemsField;

        private string classnameField;

        private string assemblyField;

        private string collectionNameField;

        /// <remarks/>
        public object items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CollectionName
        {
            get
            {
                return this.collectionNameField;
            }
            set
            {
                this.collectionNameField = value;
            }
        }
    }

    
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootSteps
    {

        private rootStepsItem[] itemsField;

        private string classnameField;

        private string assemblyField;

        private string collectionNameField;

        private byte collectionCountField;

        private string itemClassNameField;

        private string itemAssemblyNameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("item", IsNullable = false)]
        public rootStepsItem[] items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CollectionName
        {
            get
            {
                return this.collectionNameField;
            }
            set
            {
                this.collectionNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte CollectionCount
        {
            get
            {
                return this.collectionCountField;
            }
            set
            {
                this.collectionCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ItemClassName
        {
            get
            {
                return this.itemClassNameField;
            }
            set
            {
                this.itemClassNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ItemAssemblyName
        {
            get
            {
                return this.itemAssemblyNameField;
            }
            set
            {
                this.itemAssemblyNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItem
    {

        private rootStepsItemItems itemsField;

        private string keyField;

        /// <remarks/>
        public rootStepsItemItems Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Key
        {
            get
            {
                return this.keyField;
            }
            set
            {
                this.keyField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItems
    {

        private rootStepsItemItemsDocFileImportColumnMappings docFileImportColumnMappingsField;

        private rootStepsItemItemsDocumentImportColumnMappings documentImportColumnMappingsField;

        private rootStepsItemItemsLineImportColumnMappings lineImportColumnMappingsField;

        private rootStepsItemItemsImportSettings importSettingsField;

        private rootStepsItemItemsOffsetDetails offsetDetailsField;

        private rootStepsItemItemsSubSteps subStepsField;

        private rootStepsItemItemsbranches branchesField;

        private rootStepsItemItemsImportColumnMappings importColumnMappingsField;

        private rootStepsItemItemsLookupDefns lookupDefnsField;

        private rootStepsItemItemsColumns columnsField;

        private rootStepsItemItemsLookupColumns lookupColumnsField;

        private rootStepsItemItemsConsolidationDefn consolidationDefnField;

        private rootStepsItemItemsExistsFilters existsFiltersField;

        private rootStepsItemItemsColumnDefns columnDefnsField;



        private string stepNameField;

        private string variableNameField;

        private string expressionField;

        private string classnameField;

        private string assemblyField;

        private string outputMemoryTableNameField;

        private string exportMemoryTableNameField;

        private string serverFolderField;

        private string fileNameField;

        private byte noOfHeaderRowsField;

        private bool noOfHeaderRowsFieldSpecified;

        private byte columnHeaderRowField;

        private bool columnHeaderRowFieldSpecified;

        private string dataSourceNameField;

        private byte sourceField;

        private bool sourceFieldSpecified;

        private string inputMemoryTableNameField;

        private string createKeyColumnField;

        private string KeyColumnNameField;

        private string memoryTableField;

        private string operationField;

        private string sourceDirField;

        private string sourceFileField;

        private string destDirField;

        private string destFileField;

        private string proceedOnErrorField;

        private string emailToField;

        private string emailSubjectField;

        private string emailMessageField;

        private string emailIsFromAddressOverriddenField;

        private string emailFromField;

        private string emailFromDisplayNameField;

        /// <remarks/>
        public rootStepsItemItemsDocFileImportColumnMappings DocFileImportColumnMappings
        {
            get
            {
                return this.docFileImportColumnMappingsField;
            }
            set
            {
                this.docFileImportColumnMappingsField = value;
            }
        }

        /// <remarks/>
        public rootStepsItemItemsDocumentImportColumnMappings DocumentImportColumnMappings
        {
            get
            {
                return this.documentImportColumnMappingsField;
            }
            set
            {
                this.documentImportColumnMappingsField = value;
            }
        }

        /// <remarks/>
        public rootStepsItemItemsLineImportColumnMappings LineImportColumnMappings
        {
            get
            {
                return this.lineImportColumnMappingsField;
            }
            set
            {
                this.lineImportColumnMappingsField = value;
            }
        }

        /// <remarks/>
        public rootStepsItemItemsImportSettings ImportSettings
        {
            get
            {
                return this.importSettingsField;
            }
            set
            {
                this.importSettingsField = value;
            }
        }

        /// <remarks/>
        public rootStepsItemItemsOffsetDetails OffsetDetails
        {
            get
            {
                return this.offsetDetailsField;
            }
            set
            {
                this.offsetDetailsField = value;
            }
        }

        /// <remarks/>
        public rootStepsItemItemsSubSteps SubSteps
        {
            get
            {
                return this.subStepsField;
            }
            set
            {
                this.subStepsField = value;
            }
        }

        /// <remarks/>
        public rootStepsItemItemsbranches branches
        {
            get
            {
                return this.branchesField;
            }
            set
            {
                this.branchesField = value;
            }
        }

        /// <remarks/>
        public rootStepsItemItemsImportColumnMappings ImportColumnMappings
        {
            get
            {
                return this.importColumnMappingsField;
            }
            set
            {
                this.importColumnMappingsField = value;
            }
        }

        /// <remarks/>
        public rootStepsItemItemsLookupDefns LookupDefns
        {
            get
            {
                return this.lookupDefnsField;
            }
            set
            {
                this.lookupDefnsField = value;
            }
        }

        /// <remarks/>
        public rootStepsItemItemsColumns Columns
        {
            get
            {
                return this.columnsField;
            }
            set
            {
                this.columnsField = value;
            }
        }

        /// <remarks/>
        public rootStepsItemItemsLookupColumns LookupColumns
        {
            get
            {
                return this.lookupColumnsField;
            }
            set
            {
                this.lookupColumnsField = value;
            }
        }

        /// <remarks/>
        public rootStepsItemItemsConsolidationDefn ConsolidationDefn
        {
            get
            {
                return this.consolidationDefnField;
            }
            set
            {
                this.consolidationDefnField = value;
            }
        }

        /// <remarks/>
        public rootStepsItemItemsExistsFilters ExistsFilters
        {
            get
            {
                return this.existsFiltersField;
            }
            set
            {
                this.existsFiltersField = value;
            }
        }

        /// <remarks/>
        public rootStepsItemItemsColumnDefns ColumnDefns
        {
            get
            {
                return this.columnDefnsField;
            }
            set
            {
                this.columnDefnsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string StepName
        {
            get
            {
                return this.stepNameField;
            }
            set
            {
                this.stepNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string VariableName
        {
            get
            {
                return this.variableNameField;
            }
            set
            {
                this.variableNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Expression
        {
            get
            {
                return this.expressionField;
            }
            set
            {
                this.expressionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string OutputMemoryTableName
        {
            get
            {
                return this.outputMemoryTableNameField;
            }
            set
            {
                this.outputMemoryTableNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ExportMemoryTableName
        {
            get
            {
                return this.exportMemoryTableNameField;
            }
            set
            {
                this.exportMemoryTableNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ServerFolder
        {
            get
            {
                return this.serverFolderField;
            }
            set
            {
                this.serverFolderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string FileName
        {
            get
            {
                return this.fileNameField;
            }
            set
            {
                this.fileNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte NoOfHeaderRows
        {
            get
            {
                return this.noOfHeaderRowsField;
            }
            set
            {
                this.noOfHeaderRowsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool NoOfHeaderRowsSpecified
        {
            get
            {
                return this.noOfHeaderRowsFieldSpecified;
            }
            set
            {
                this.noOfHeaderRowsFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte ColumnHeaderRow
        {
            get
            {
                return this.columnHeaderRowField;
            }
            set
            {
                this.columnHeaderRowField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ColumnHeaderRowSpecified
        {
            get
            {
                return this.columnHeaderRowFieldSpecified;
            }
            set
            {
                this.columnHeaderRowFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DataSourceName
        {
            get
            {
                return this.dataSourceNameField;
            }
            set
            {
                this.dataSourceNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte Source
        {
            get
            {
                return this.sourceField;
            }
            set
            {
                this.sourceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SourceSpecified
        {
            get
            {
                return this.sourceFieldSpecified;
            }
            set
            {
                this.sourceFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string InputMemoryTableName
        {
            get
            {
                return this.inputMemoryTableNameField;
            }
            set
            {
                this.inputMemoryTableNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CreateKeyColumn
        {
            get
            {
                return this.createKeyColumnField;
            }
            set
            {
                this.createKeyColumnField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string KeyColumnName
        {
            get
            {
                return this.KeyColumnNameField;
            }
            set
            {
                this.KeyColumnNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string MemoryTable
        {
            get
            {
                return this.memoryTableField;
            }
            set
            {
                this.memoryTableField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Operation
        {
            get
            {
                return this.operationField;
            }
            set
            {
                this.operationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string SourceDir
        {
            get
            {
                return this.sourceDirField;
            }
            set
            {
                this.sourceDirField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string SourceFile
        {
            get
            {
                return this.sourceFileField;
            }
            set
            {
                this.sourceFileField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DestDir
        {
            get
            {
                return this.destDirField;
            }
            set
            {
                this.destDirField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DestFile
        {
            get
            {
                return this.destFileField;
            }
            set
            {
                this.destFileField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ProceedOnError
        {
            get
            {
                return this.proceedOnErrorField;
            }
            set
            {
                this.proceedOnErrorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string EmailTo
        {
            get
            {
                return this.emailToField;
            }
            set
            {
                this.emailToField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string EmailSubject
        {
            get
            {
                return this.emailSubjectField;
            }
            set
            {
                this.emailSubjectField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string EmailMessage
        {
            get
            {
                return this.emailMessageField;
            }
            set
            {
                this.emailMessageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string EmailIsFromAddressOverridden
        {
            get
            {
                return this.emailIsFromAddressOverriddenField;
            }
            set
            {
                this.emailIsFromAddressOverriddenField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string EmailFrom
        {
            get
            {
                return this.emailFromField;
            }
            set
            {
                this.emailFromField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string EmailFromDisplayName
        {
            get
            {
                return this.emailFromDisplayNameField;
            }
            set
            {
                this.emailFromDisplayNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsDocFileImportColumnMappings
    {

        private rootStepsItemItemsDocFileImportColumnMappingsItem[] itemsField;

        private string classnameField;

        private string assemblyField;

        private string collectionNameField;

        private byte collectionCountField;

        private string itemClassNameField;

        private string itemAssemblyNameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("item", IsNullable = false)]
        public rootStepsItemItemsDocFileImportColumnMappingsItem[] items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CollectionName
        {
            get
            {
                return this.collectionNameField;
            }
            set
            {
                this.collectionNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte CollectionCount
        {
            get
            {
                return this.collectionCountField;
            }
            set
            {
                this.collectionCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ItemClassName
        {
            get
            {
                return this.itemClassNameField;
            }
            set
            {
                this.itemClassNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ItemAssemblyName
        {
            get
            {
                return this.itemAssemblyNameField;
            }
            set
            {
                this.itemAssemblyNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsDocFileImportColumnMappingsItem
    {

        private rootStepsItemItemsDocFileImportColumnMappingsItemItems itemsField;

        private string keyField;

        /// <remarks/>
        public rootStepsItemItemsDocFileImportColumnMappingsItemItems Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Key
        {
            get
            {
                return this.keyField;
            }
            set
            {
                this.keyField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsDocFileImportColumnMappingsItemItems
    {

        private string fromValueField;

        private string toValueField;

        private byte toValueSourceField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string FromValue
        {
            get
            {
                return this.fromValueField;
            }
            set
            {
                this.fromValueField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ToValue
        {
            get
            {
                return this.toValueField;
            }
            set
            {
                this.toValueField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte ToValueSource
        {
            get
            {
                return this.toValueSourceField;
            }
            set
            {
                this.toValueSourceField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsDocumentImportColumnMappings
    {

        private rootStepsItemItemsDocumentImportColumnMappingsItem[] itemsField;

        private string classnameField;

        private string assemblyField;

        private string collectionNameField;

        private byte collectionCountField;

        private string itemClassNameField;

        private string itemAssemblyNameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("item", IsNullable = false)]
        public rootStepsItemItemsDocumentImportColumnMappingsItem[] items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CollectionName
        {
            get
            {
                return this.collectionNameField;
            }
            set
            {
                this.collectionNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte CollectionCount
        {
            get
            {
                return this.collectionCountField;
            }
            set
            {
                this.collectionCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ItemClassName
        {
            get
            {
                return this.itemClassNameField;
            }
            set
            {
                this.itemClassNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ItemAssemblyName
        {
            get
            {
                return this.itemAssemblyNameField;
            }
            set
            {
                this.itemAssemblyNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsDocumentImportColumnMappingsItem
    {

        private rootStepsItemItemsDocumentImportColumnMappingsItemItems itemsField;

        private string keyField;

        /// <remarks/>
        public rootStepsItemItemsDocumentImportColumnMappingsItemItems Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Key
        {
            get
            {
                return this.keyField;
            }
            set
            {
                this.keyField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsDocumentImportColumnMappingsItemItems
    {

        private string fromValueField;

        private byte toValueSourceField;

        private bool toValueSourceFieldSpecified;

        private string toValueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string FromValue
        {
            get
            {
                return this.fromValueField;
            }
            set
            {
                this.fromValueField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte ToValueSource
        {
            get
            {
                return this.toValueSourceField;
            }
            set
            {
                this.toValueSourceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ToValueSourceSpecified
        {
            get
            {
                return this.toValueSourceFieldSpecified;
            }
            set
            {
                this.toValueSourceFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ToValue
        {
            get
            {
                return this.toValueField;
            }
            set
            {
                this.toValueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsLineImportColumnMappings
    {

        private rootStepsItemItemsLineImportColumnMappingsItem[] itemsField;

        private string classnameField;

        private string assemblyField;

        private string collectionNameField;

        private byte collectionCountField;

        private string itemClassNameField;

        private string itemAssemblyNameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("item", IsNullable = false)]
        public rootStepsItemItemsLineImportColumnMappingsItem[] items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CollectionName
        {
            get
            {
                return this.collectionNameField;
            }
            set
            {
                this.collectionNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte CollectionCount
        {
            get
            {
                return this.collectionCountField;
            }
            set
            {
                this.collectionCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ItemClassName
        {
            get
            {
                return this.itemClassNameField;
            }
            set
            {
                this.itemClassNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ItemAssemblyName
        {
            get
            {
                return this.itemAssemblyNameField;
            }
            set
            {
                this.itemAssemblyNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsLineImportColumnMappingsItem
    {

        private rootStepsItemItemsLineImportColumnMappingsItemItems itemsField;

        private string keyField;

        /// <remarks/>
        public rootStepsItemItemsLineImportColumnMappingsItemItems Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Key
        {
            get
            {
                return this.keyField;
            }
            set
            {
                this.keyField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsLineImportColumnMappingsItemItems
    {

        private string fromValueField;

        private string toValueField;

        private byte toValueSourceField;

        private bool toValueSourceFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string FromValue
        {
            get
            {
                return this.fromValueField;
            }
            set
            {
                this.fromValueField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ToValue
        {
            get
            {
                return this.toValueField;
            }
            set
            {
                this.toValueField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte ToValueSource
        {
            get
            {
                return this.toValueSourceField;
            }
            set
            {
                this.toValueSourceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ToValueSourceSpecified
        {
            get
            {
                return this.toValueSourceFieldSpecified;
            }
            set
            {
                this.toValueSourceFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsImportSettings
    {

        private string classnameField;

        private string assemblyField;

        private string chartNameField;

        private string importFunctionTypeField;

        private string fileNameField;

        private byte documentModeField;

        private byte docSingleModeField;

        private string workflowDecisionField;

        private string workflowUserField;

        private string useSuspenseInvalidAccountField;

        private string useSuspenseInactiveAccountField;

        private byte invalidAccountStorageNumberField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ChartName
        {
            get
            {
                return this.chartNameField;
            }
            set
            {
                this.chartNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ImportFunctionType
        {
            get
            {
                return this.importFunctionTypeField;
            }
            set
            {
                this.importFunctionTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string FileName
        {
            get
            {
                return this.fileNameField;
            }
            set
            {
                this.fileNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte DocumentMode
        {
            get
            {
                return this.documentModeField;
            }
            set
            {
                this.documentModeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte DocSingleMode
        {
            get
            {
                return this.docSingleModeField;
            }
            set
            {
                this.docSingleModeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string WorkflowDecision
        {
            get
            {
                return this.workflowDecisionField;
            }
            set
            {
                this.workflowDecisionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string WorkflowUser
        {
            get
            {
                return this.workflowUserField;
            }
            set
            {
                this.workflowUserField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string UseSuspenseInvalidAccount
        {
            get
            {
                return this.useSuspenseInvalidAccountField;
            }
            set
            {
                this.useSuspenseInvalidAccountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string UseSuspenseInactiveAccount
        {
            get
            {
                return this.useSuspenseInactiveAccountField;
            }
            set
            {
                this.useSuspenseInactiveAccountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte InvalidAccountStorageNumber
        {
            get
            {
                return this.invalidAccountStorageNumberField;
            }
            set
            {
                this.invalidAccountStorageNumberField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsOffsetDetails
    {

        private rootStepsItemItemsOffsetDetailsOffsetColumnMappings offsetColumnMappingsField;

        private string classnameField;

        private string assemblyField;

        /// <remarks/>
        public rootStepsItemItemsOffsetDetailsOffsetColumnMappings OffsetColumnMappings
        {
            get
            {
                return this.offsetColumnMappingsField;
            }
            set
            {
                this.offsetColumnMappingsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsOffsetDetailsOffsetColumnMappings
    {

        private object itemsField;

        private string classnameField;

        private string assemblyField;

        private string collectionNameField;

        /// <remarks/>
        public object items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CollectionName
        {
            get
            {
                return this.collectionNameField;
            }
            set
            {
                this.collectionNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    /*public partial class rootStepsItemItemsSubSteps
    {

        private rootStepsItemItemsSubStepsItem[] itemsField;

        private string classnameField;

        private string assemblyField;

        private string collectionNameField;

        private byte collectionCountField;

        private string itemClassNameField;

        private string itemAssemblyNameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("item", IsNullable = false)]
        public rootStepsItemItemsSubStepsItem[] items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CollectionName
        {
            get
            {
                return this.collectionNameField;
            }
            set
            {
                this.collectionNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte CollectionCount
        {
            get
            {
                return this.collectionCountField;
            }
            set
            {
                this.collectionCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ItemClassName
        {
            get
            {
                return this.itemClassNameField;
            }
            set
            {
                this.itemClassNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ItemAssemblyName
        {
            get
            {
                return this.itemAssemblyNameField;
            }
            set
            {
                this.itemAssemblyNameField = value;
            }
        }
    }*/

    public partial class rootStepsItemItemsbranches //New
    {

        private rootStepsItemItemsbranchesitem[] itemsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("item", IsNullable = false)]
        public rootStepsItemItemsbranchesitem[] items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }
    }

    public partial class rootStepsItemItemsbranchesitem //New
    {

        private rootStepsItemItemsbranchesitemItems itemsField;

        /// <remarks/>
        public rootStepsItemItemsbranchesitemItems Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }
    }

    public partial class rootStepsItemItemsbranchesitemItems //New
    {

        private rootStepsItemItemsbranchesitemsItemssteps stepsField;

        /// <remarks/>
        public rootStepsItemItemsbranchesitemsItemssteps steps
        {
            get
            {
                return this.stepsField;
            }
            set
            {
                this.stepsField = value;
            }
        }
    }

    public partial class rootStepsItemItemsbranchesitemsItemssteps //New
    {

        private rootStepsItem[] itemsField;

        private string itemClassNameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ItemClassName
        {
            get
            {
                return this.itemClassNameField;
            }
            set
            {
                this.itemClassNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("item", IsNullable = false)]
        public rootStepsItem[] items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }
    }

    public partial class rootStepsItemItemsSubSteps //Modified
    {

        private rootStepsItem[] itemsField;

        private string classnameField;

        private string assemblyField;

        private string collectionNameField;

        private byte collectionCountField;

        private string itemClassNameField;

        private string itemAssemblyNameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("item", IsNullable = false)]
        public rootStepsItem[] items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CollectionName
        {
            get
            {
                return this.collectionNameField;
            }
            set
            {
                this.collectionNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte CollectionCount
        {
            get
            {
                return this.collectionCountField;
            }
            set
            {
                this.collectionCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ItemClassName
        {
            get
            {
                return this.itemClassNameField;
            }
            set
            {
                this.itemClassNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ItemAssemblyName
        {
            get
            {
                return this.itemAssemblyNameField;
            }
            set
            {
                this.itemAssemblyNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsSubStepsItem
    {

        private rootStepsItemItemsSubStepsItemItems itemsField;

        private string keyField;

        /// <remarks/>
        public rootStepsItemItemsSubStepsItemItems Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Key
        {
            get
            {
                return this.keyField;
            }
            set
            {
                this.keyField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsSubStepsItemItems
    {

        private rootStepsItemItemsSubStepsItemItemsImportColumnMappings importColumnMappingsField;

        private rootStepsItemItemsSubStepsItemItemsColumns columnsField;

        private rootStepsItemItemsSubStepsItemItemsConsolidationDefn consolidationDefnField;

        private string stepNameField;

        private string outputMemoryTableNameField;

        private byte sourceField;

        private bool sourceFieldSpecified;

        private string inputMemoryTableNameField;

        private string classnameField;

        private string assemblyField;

        private string createKeyColumnField;

        private string keyColumnNameField;

        /// <remarks/>
        public rootStepsItemItemsSubStepsItemItemsImportColumnMappings ImportColumnMappings
        {
            get
            {
                return this.importColumnMappingsField;
            }
            set
            {
                this.importColumnMappingsField = value;
            }
        }

        /// <remarks/>
        public rootStepsItemItemsSubStepsItemItemsColumns Columns
        {
            get
            {
                return this.columnsField;
            }
            set
            {
                this.columnsField = value;
            }
        }

        /// <remarks/>
        public rootStepsItemItemsSubStepsItemItemsConsolidationDefn ConsolidationDefn
        {
            get
            {
                return this.consolidationDefnField;
            }
            set
            {
                this.consolidationDefnField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string StepName
        {
            get
            {
                return this.stepNameField;
            }
            set
            {
                this.stepNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string OutputMemoryTableName
        {
            get
            {
                return this.outputMemoryTableNameField;
            }
            set
            {
                this.outputMemoryTableNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte Source
        {
            get
            {
                return this.sourceField;
            }
            set
            {
                this.sourceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SourceSpecified
        {
            get
            {
                return this.sourceFieldSpecified;
            }
            set
            {
                this.sourceFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string InputMemoryTableName
        {
            get
            {
                return this.inputMemoryTableNameField;
            }
            set
            {
                this.inputMemoryTableNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CreateKeyColumn
        {
            get
            {
                return this.createKeyColumnField;
            }
            set
            {
                this.createKeyColumnField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string KeyColumnName
        {
            get
            {
                return this.keyColumnNameField;
            }
            set
            {
                this.keyColumnNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsSubStepsItemItemsImportColumnMappings
    {

        private rootStepsItemItemsSubStepsItemItemsImportColumnMappingsItem[] itemsField;

        private string classnameField;

        private string assemblyField;

        private string collectionNameField;

        private byte collectionCountField;

        private string itemClassNameField;

        private string itemAssemblyNameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("item", IsNullable = false)]
        public rootStepsItemItemsSubStepsItemItemsImportColumnMappingsItem[] items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CollectionName
        {
            get
            {
                return this.collectionNameField;
            }
            set
            {
                this.collectionNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte CollectionCount
        {
            get
            {
                return this.collectionCountField;
            }
            set
            {
                this.collectionCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ItemClassName
        {
            get
            {
                return this.itemClassNameField;
            }
            set
            {
                this.itemClassNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ItemAssemblyName
        {
            get
            {
                return this.itemAssemblyNameField;
            }
            set
            {
                this.itemAssemblyNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsSubStepsItemItemsImportColumnMappingsItem
    {

        private rootStepsItemItemsSubStepsItemItemsImportColumnMappingsItemItems itemsField;

        private string keyField;

        /// <remarks/>
        public rootStepsItemItemsSubStepsItemItemsImportColumnMappingsItemItems Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Key
        {
            get
            {
                return this.keyField;
            }
            set
            {
                this.keyField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsSubStepsItemItemsImportColumnMappingsItemItems
    {

        private string fromValueField;

        private string toValueField;

        private byte toValueSourceField;

        private bool toValueSourceFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string FromValue
        {
            get
            {
                return this.fromValueField;
            }
            set
            {
                this.fromValueField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ToValue
        {
            get
            {
                return this.toValueField;
            }
            set
            {
                this.toValueField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte ToValueSource
        {
            get
            {
                return this.toValueSourceField;
            }
            set
            {
                this.toValueSourceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ToValueSourceSpecified
        {
            get
            {
                return this.toValueSourceFieldSpecified;
            }
            set
            {
                this.toValueSourceFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsSubStepsItemItemsColumns
    {

        private rootStepsItemItemsSubStepsItemItemsColumnsColumn[] columnField;

        private string classnameField;

        private string assemblyField;

        private byte columnCountField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Column")]
        public rootStepsItemItemsSubStepsItemItemsColumnsColumn[] Column
        {
            get
            {
                return this.columnField;
            }
            set
            {
                this.columnField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte ColumnCount
        {
            get
            {
                return this.columnCountField;
            }
            set
            {
                this.columnCountField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsSubStepsItemItemsColumnsColumn
    {

        private byte idField;

        private string classnameField;

        private string assemblyField;

        private string columnNameField;

        private byte actionField;

        private string fieldField;

        private string calculateExpressionField;

        private byte calculateDataTypeField;

        private bool calculateDataTypeFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ColumnName
        {
            get
            {
                return this.columnNameField;
            }
            set
            {
                this.columnNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Field
        {
            get
            {
                return this.fieldField;
            }
            set
            {
                this.fieldField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CalculateExpression
        {
            get
            {
                return this.calculateExpressionField;
            }
            set
            {
                this.calculateExpressionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte CalculateDataType
        {
            get
            {
                return this.calculateDataTypeField;
            }
            set
            {
                this.calculateDataTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CalculateDataTypeSpecified
        {
            get
            {
                return this.calculateDataTypeFieldSpecified;
            }
            set
            {
                this.calculateDataTypeFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsSubStepsItemItemsConsolidationDefn
    {

        private rootStepsItemItemsSubStepsItemItemsConsolidationDefnColumns columnsField;

        private rootStepsItemItemsSubStepsItemItemsConsolidationDefnCriteria criteriaField;

        private string classnameField;

        private string assemblyField;

        /// <remarks/>
        public rootStepsItemItemsSubStepsItemItemsConsolidationDefnColumns Columns
        {
            get
            {
                return this.columnsField;
            }
            set
            {
                this.columnsField = value;
            }
        }

        /// <remarks/>
        public rootStepsItemItemsSubStepsItemItemsConsolidationDefnCriteria Criteria
        {
            get
            {
                return this.criteriaField;
            }
            set
            {
                this.criteriaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsSubStepsItemItemsConsolidationDefnColumns
    {

        private rootStepsItemItemsSubStepsItemItemsConsolidationDefnColumnsColumn[] columnField;

        private string classnameField;

        private string assemblyField;

        private byte columnCountField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Column")]
        public rootStepsItemItemsSubStepsItemItemsConsolidationDefnColumnsColumn[] Column
        {
            get
            {
                return this.columnField;
            }
            set
            {
                this.columnField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte ColumnCount
        {
            get
            {
                return this.columnCountField;
            }
            set
            {
                this.columnCountField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsSubStepsItemItemsConsolidationDefnColumnsColumn
    {

        private byte idField;

        private string classnameField;

        private string assemblyField;

        private string enquirySetBaseColumnIdField;

        private string columnNameField;

        private string fieldField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string EnquirySetBaseColumnId
        {
            get
            {
                return this.enquirySetBaseColumnIdField;
            }
            set
            {
                this.enquirySetBaseColumnIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ColumnName
        {
            get
            {
                return this.columnNameField;
            }
            set
            {
                this.columnNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Field
        {
            get
            {
                return this.fieldField;
            }
            set
            {
                this.fieldField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsSubStepsItemItemsConsolidationDefnCriteria
    {

        private rootStepsItemItemsSubStepsItemItemsConsolidationDefnCriteriaCriterion1 criterion1Field;

        private rootStepsItemItemsSubStepsItemItemsConsolidationDefnCriteriaCriteriaSet criteriaSetField;

        private string classnameField;

        private string assemblyField;

        private byte criterionCountField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Criterion.1")]
        public rootStepsItemItemsSubStepsItemItemsConsolidationDefnCriteriaCriterion1 Criterion1
        {
            get
            {
                return this.criterion1Field;
            }
            set
            {
                this.criterion1Field = value;
            }
        }

        /// <remarks/>
        public rootStepsItemItemsSubStepsItemItemsConsolidationDefnCriteriaCriteriaSet CriteriaSet
        {
            get
            {
                return this.criteriaSetField;
            }
            set
            {
                this.criteriaSetField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte CriterionCount
        {
            get
            {
                return this.criterionCountField;
            }
            set
            {
                this.criterionCountField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsSubStepsItemItemsConsolidationDefnCriteriaCriterion1
    {

        private string classnameField;

        private string assemblyField;

        private string columnNameField;

        private string operatorField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ColumnName
        {
            get
            {
                return this.columnNameField;
            }
            set
            {
                this.columnNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Operator
        {
            get
            {
                return this.operatorField;
            }
            set
            {
                this.operatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsSubStepsItemItemsConsolidationDefnCriteriaCriteriaSet
    {

        private string classnameField;

        private string assemblyField;

        private byte criteriaCountField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte CriteriaCount
        {
            get
            {
                return this.criteriaCountField;
            }
            set
            {
                this.criteriaCountField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsImportColumnMappings
    {

        private rootStepsItemItemsImportColumnMappingsItem[] itemsField;

        private string classnameField;

        private string assemblyField;

        private string collectionNameField;

        private byte collectionCountField;

        private string itemClassNameField;

        private string itemAssemblyNameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("item", IsNullable = false)]
        public rootStepsItemItemsImportColumnMappingsItem[] items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CollectionName
        {
            get
            {
                return this.collectionNameField;
            }
            set
            {
                this.collectionNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte CollectionCount
        {
            get
            {
                return this.collectionCountField;
            }
            set
            {
                this.collectionCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ItemClassName
        {
            get
            {
                return this.itemClassNameField;
            }
            set
            {
                this.itemClassNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ItemAssemblyName
        {
            get
            {
                return this.itemAssemblyNameField;
            }
            set
            {
                this.itemAssemblyNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsImportColumnMappingsItem
    {

        private rootStepsItemItemsImportColumnMappingsItemItems itemsField;

        private string keyField;

        /// <remarks/>
        public rootStepsItemItemsImportColumnMappingsItemItems Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Key
        {
            get
            {
                return this.keyField;
            }
            set
            {
                this.keyField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsImportColumnMappingsItemItems
    {

        private string fromValueField;

        private string toValueField;

        private string toValueSourceField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string FromValue
        {
            get
            {
                return this.fromValueField;
            }
            set
            {
                this.fromValueField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ToValueSource
        {
            get
            {
                return this.toValueSourceField;
            }
            set
            {
                this.toValueSourceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ToValue
        {
            get
            {
                return this.toValueField;
            }
            set
            {
                this.toValueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsLookupDefns
    {

        private rootStepsItemItemsLookupDefnsItem[] itemsField;

        private string classnameField;

        private string assemblyField;

        private string collectionNameField;

        private byte collectionCountField;

        private string itemClassNameField;

        private string itemAssemblyNameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("item", IsNullable = false)]
        public rootStepsItemItemsLookupDefnsItem[] items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CollectionName
        {
            get
            {
                return this.collectionNameField;
            }
            set
            {
                this.collectionNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte CollectionCount
        {
            get
            {
                return this.collectionCountField;
            }
            set
            {
                this.collectionCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ItemClassName
        {
            get
            {
                return this.itemClassNameField;
            }
            set
            {
                this.itemClassNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ItemAssemblyName
        {
            get
            {
                return this.itemAssemblyNameField;
            }
            set
            {
                this.itemAssemblyNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsLookupDefnsItem
    {

        private rootStepsItemItemsLookupDefnsItemItems itemsField;

        private string keyField;

        /// <remarks/>
        public rootStepsItemItemsLookupDefnsItemItems Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Key
        {
            get
            {
                return this.keyField;
            }
            set
            {
                this.keyField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsLookupDefnsItemItems
    {

        private rootStepsItemItemsLookupDefnsItemItemsLinkColumns linkColumnsField;

        private string lookupNameField;

        private string lookupMemoryTableNameField;

        /// <remarks/>
        public rootStepsItemItemsLookupDefnsItemItemsLinkColumns LinkColumns
        {
            get
            {
                return this.linkColumnsField;
            }
            set
            {
                this.linkColumnsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string LookupName
        {
            get
            {
                return this.lookupNameField;
            }
            set
            {
                this.lookupNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string LookupMemoryTableName
        {
            get
            {
                return this.lookupMemoryTableNameField;
            }
            set
            {
                this.lookupMemoryTableNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsLookupDefnsItemItemsLinkColumns
    {

        private rootStepsItemItemsLookupDefnsItemItemsLinkColumnsItems itemsField;

        private string classnameField;

        private string assemblyField;

        private string collectionNameField;

        private byte collectionCountField;

        private string itemClassNameField;

        private string itemAssemblyNameField;

        /// <remarks/>
        public rootStepsItemItemsLookupDefnsItemItemsLinkColumnsItems items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CollectionName
        {
            get
            {
                return this.collectionNameField;
            }
            set
            {
                this.collectionNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte CollectionCount
        {
            get
            {
                return this.collectionCountField;
            }
            set
            {
                this.collectionCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ItemClassName
        {
            get
            {
                return this.itemClassNameField;
            }
            set
            {
                this.itemClassNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ItemAssemblyName
        {
            get
            {
                return this.itemAssemblyNameField;
            }
            set
            {
                this.itemAssemblyNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsLookupDefnsItemItemsLinkColumnsItems
    {

        private rootStepsItemItemsLookupDefnsItemItemsLinkColumnsItemsItem itemField;

        /// <remarks/>
        public rootStepsItemItemsLookupDefnsItemItemsLinkColumnsItemsItem item
        {
            get
            {
                return this.itemField;
            }
            set
            {
                this.itemField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsLookupDefnsItemItemsLinkColumnsItemsItem
    {

        private rootStepsItemItemsLookupDefnsItemItemsLinkColumnsItemsItemItems itemsField;

        private string keyField;

        /// <remarks/>
        public rootStepsItemItemsLookupDefnsItemItemsLinkColumnsItemsItemItems Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Key
        {
            get
            {
                return this.keyField;
            }
            set
            {
                this.keyField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsLookupDefnsItemItemsLinkColumnsItemsItemItems
    {

        private string fromValueField;

        private string toValueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string FromValue
        {
            get
            {
                return this.fromValueField;
            }
            set
            {
                this.fromValueField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ToValue
        {
            get
            {
                return this.toValueField;
            }
            set
            {
                this.toValueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsColumns
    {

        private rootStepsItemItemsColumnsColumn[] columnField;

        private string classnameField;

        private string assemblyField;

        private byte columnCountField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Column")]
        public rootStepsItemItemsColumnsColumn[] Column
        {
            get
            {
                return this.columnField;
            }
            set
            {
                this.columnField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte ColumnCount
        {
            get
            {
                return this.columnCountField;
            }
            set
            {
                this.columnCountField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsColumnsColumn
    {

        private byte idField;

        private string classnameField;

        private string assemblyField;

        private string columnNameField;

        private byte actionField;

        private string fieldField;

        private byte calculateDecimalPlacesField;

        private bool calculateDecimalPlacesFieldSpecified;

        private string lookupNameField;

        private string lookupFieldField;

        private byte lookupDecimalPlacesField;

        private bool lookupDecimalPlacesFieldSpecified;

        private string calculateExpressionField;

        private byte calculateDataTypeField;

        private bool calculateDataTypeFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ColumnName
        {
            get
            {
                return this.columnNameField;
            }
            set
            {
                this.columnNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Field
        {
            get
            {
                return this.fieldField;
            }
            set
            {
                this.fieldField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte CalculateDecimalPlaces
        {
            get
            {
                return this.calculateDecimalPlacesField;
            }
            set
            {
                this.calculateDecimalPlacesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CalculateDecimalPlacesSpecified
        {
            get
            {
                return this.calculateDecimalPlacesFieldSpecified;
            }
            set
            {
                this.calculateDecimalPlacesFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string LookupName
        {
            get
            {
                return this.lookupNameField;
            }
            set
            {
                this.lookupNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string LookupField
        {
            get
            {
                return this.lookupFieldField;
            }
            set
            {
                this.lookupFieldField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte LookupDecimalPlaces
        {
            get
            {
                return this.lookupDecimalPlacesField;
            }
            set
            {
                this.lookupDecimalPlacesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool LookupDecimalPlacesSpecified
        {
            get
            {
                return this.lookupDecimalPlacesFieldSpecified;
            }
            set
            {
                this.lookupDecimalPlacesFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CalculateExpression
        {
            get
            {
                return this.calculateExpressionField;
            }
            set
            {
                this.calculateExpressionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte CalculateDataType
        {
            get
            {
                return this.calculateDataTypeField;
            }
            set
            {
                this.calculateDataTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CalculateDataTypeSpecified
        {
            get
            {
                return this.calculateDataTypeFieldSpecified;
            }
            set
            {
                this.calculateDataTypeFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsLookupColumns
    {

        private rootStepsItemItemsLookupColumnsItem[] itemsField;

        private string classnameField;

        private string assemblyField;

        private string collectionNameField;

        private byte collectionCountField;

        private string itemClassNameField;

        private string itemAssemblyNameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("item", IsNullable = false)]
        public rootStepsItemItemsLookupColumnsItem[] items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CollectionName
        {
            get
            {
                return this.collectionNameField;
            }
            set
            {
                this.collectionNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte CollectionCount
        {
            get
            {
                return this.collectionCountField;
            }
            set
            {
                this.collectionCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ItemClassName
        {
            get
            {
                return this.itemClassNameField;
            }
            set
            {
                this.itemClassNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ItemAssemblyName
        {
            get
            {
                return this.itemAssemblyNameField;
            }
            set
            {
                this.itemAssemblyNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsLookupColumnsItem
    {

        private rootStepsItemItemsLookupColumnsItemColumns columnsField;

        private string keyField;

        /// <remarks/>
        public rootStepsItemItemsLookupColumnsItemColumns Columns
        {
            get
            {
                return this.columnsField;
            }
            set
            {
                this.columnsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Key
        {
            get
            {
                return this.keyField;
            }
            set
            {
                this.keyField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsLookupColumnsItemColumns
    {

        private string nameField;

        private string defaultValueField;

        private byte forceCaseField;

        private byte minLenField;

        private byte maxLenField;

        private string fieldNameField;

        private string imageURLField;

        private string hintTextField;

        private string signedNumberField;

        private byte decimalPlacesField;

        private string outputMaskField;

        private string inputMaskField;

        private string codeTypeField;

        private string codeTypeSysIDField;

        private string pickKeyField;

        private string pickObjectField;

        private string syncKeyField;

        private string syncObjectField;

        private string valKeyField;

        private string valObjectField;

        private string customTypeNameField;

        private string fieldDictCodeField;

        private string mandatoryField;

        private string applyIntellisenseField;

        private string canSearchInactiveStandardCodesField;

        private string descriptionField;

        private string columnIdField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DefaultValue
        {
            get
            {
                return this.defaultValueField;
            }
            set
            {
                this.defaultValueField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte ForceCase
        {
            get
            {
                return this.forceCaseField;
            }
            set
            {
                this.forceCaseField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte MinLen
        {
            get
            {
                return this.minLenField;
            }
            set
            {
                this.minLenField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte MaxLen
        {
            get
            {
                return this.maxLenField;
            }
            set
            {
                this.maxLenField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string FieldName
        {
            get
            {
                return this.fieldNameField;
            }
            set
            {
                this.fieldNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ImageURL
        {
            get
            {
                return this.imageURLField;
            }
            set
            {
                this.imageURLField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string HintText
        {
            get
            {
                return this.hintTextField;
            }
            set
            {
                this.hintTextField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string SignedNumber
        {
            get
            {
                return this.signedNumberField;
            }
            set
            {
                this.signedNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte DecimalPlaces
        {
            get
            {
                return this.decimalPlacesField;
            }
            set
            {
                this.decimalPlacesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string OutputMask
        {
            get
            {
                return this.outputMaskField;
            }
            set
            {
                this.outputMaskField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string InputMask
        {
            get
            {
                return this.inputMaskField;
            }
            set
            {
                this.inputMaskField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CodeType
        {
            get
            {
                return this.codeTypeField;
            }
            set
            {
                this.codeTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CodeTypeSysID
        {
            get
            {
                return this.codeTypeSysIDField;
            }
            set
            {
                this.codeTypeSysIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PickKey
        {
            get
            {
                return this.pickKeyField;
            }
            set
            {
                this.pickKeyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PickObject
        {
            get
            {
                return this.pickObjectField;
            }
            set
            {
                this.pickObjectField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string SyncKey
        {
            get
            {
                return this.syncKeyField;
            }
            set
            {
                this.syncKeyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string SyncObject
        {
            get
            {
                return this.syncObjectField;
            }
            set
            {
                this.syncObjectField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ValKey
        {
            get
            {
                return this.valKeyField;
            }
            set
            {
                this.valKeyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ValObject
        {
            get
            {
                return this.valObjectField;
            }
            set
            {
                this.valObjectField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CustomTypeName
        {
            get
            {
                return this.customTypeNameField;
            }
            set
            {
                this.customTypeNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string FieldDictCode
        {
            get
            {
                return this.fieldDictCodeField;
            }
            set
            {
                this.fieldDictCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Mandatory
        {
            get
            {
                return this.mandatoryField;
            }
            set
            {
                this.mandatoryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ApplyIntellisense
        {
            get
            {
                return this.applyIntellisenseField;
            }
            set
            {
                this.applyIntellisenseField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CanSearchInactiveStandardCodes
        {
            get
            {
                return this.canSearchInactiveStandardCodesField;
            }
            set
            {
                this.canSearchInactiveStandardCodesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ColumnId
        {
            get
            {
                return this.columnIdField;
            }
            set
            {
                this.columnIdField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsConsolidationDefn
    {

        private rootStepsItemItemsConsolidationDefnColumns columnsField;

        private rootStepsItemItemsConsolidationDefnCriteria criteriaField;

        private rootStepsItemItemsConsolidationDefnOrderBys orderBysField;

        private string classnameField;

        private string assemblyField;

        /// <remarks/>
        public rootStepsItemItemsConsolidationDefnColumns Columns
        {
            get
            {
                return this.columnsField;
            }
            set
            {
                this.columnsField = value;
            }
        }

        /// <remarks/>
        public rootStepsItemItemsConsolidationDefnCriteria Criteria
        {
            get
            {
                return this.criteriaField;
            }
            set
            {
                this.criteriaField = value;
            }
        }

        /// <remarks/>
        public rootStepsItemItemsConsolidationDefnOrderBys OrderBys
        {
            get
            {
                return this.orderBysField;
            }
            set
            {
                this.orderBysField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsConsolidationDefnColumns
    {

        private rootStepsItemItemsConsolidationDefnColumnsColumn[] columnField;

        private string classnameField;

        private string assemblyField;

        private byte columnCountField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Column")]
        public rootStepsItemItemsConsolidationDefnColumnsColumn[] Column
        {
            get
            {
                return this.columnField;
            }
            set
            {
                this.columnField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte ColumnCount
        {
            get
            {
                return this.columnCountField;
            }
            set
            {
                this.columnCountField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsConsolidationDefnColumnsColumn
    {

        private byte idField;

        private string classnameField;

        private string assemblyField;

        private string enquirySetBaseColumnIdField;

        private string columnNameField;

        private string fieldField;

        private byte calculateDecimalPlacesField;

        private bool calculateDecimalPlacesFieldSpecified;

        private byte descriptionFormatField;

        private bool descriptionFormatFieldSpecified;

        private byte actionField;

        private bool actionFieldSpecified;

        private string calculateExpressionField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string EnquirySetBaseColumnId
        {
            get
            {
                return this.enquirySetBaseColumnIdField;
            }
            set
            {
                this.enquirySetBaseColumnIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ColumnName
        {
            get
            {
                return this.columnNameField;
            }
            set
            {
                this.columnNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Field
        {
            get
            {
                return this.fieldField;
            }
            set
            {
                this.fieldField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte CalculateDecimalPlaces
        {
            get
            {
                return this.calculateDecimalPlacesField;
            }
            set
            {
                this.calculateDecimalPlacesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CalculateDecimalPlacesSpecified
        {
            get
            {
                return this.calculateDecimalPlacesFieldSpecified;
            }
            set
            {
                this.calculateDecimalPlacesFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte DescriptionFormat
        {
            get
            {
                return this.descriptionFormatField;
            }
            set
            {
                this.descriptionFormatField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DescriptionFormatSpecified
        {
            get
            {
                return this.descriptionFormatFieldSpecified;
            }
            set
            {
                this.descriptionFormatFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CalculateExpression
        {
            get
            {
                return this.calculateExpressionField;
            }
            set
            {
                this.calculateExpressionField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsConsolidationDefnCriteria
    {

        private rootStepsItemItemsConsolidationDefnCriteriaCriterion1 criterion1Field;

        private rootStepsItemItemsConsolidationDefnCriteriaCriteriaSet criteriaSetField;

        private string classnameField;

        private string assemblyField;

        private byte criterionCountField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Criterion.1")]
        public rootStepsItemItemsConsolidationDefnCriteriaCriterion1 Criterion1
        {
            get
            {
                return this.criterion1Field;
            }
            set
            {
                this.criterion1Field = value;
            }
        }

        /// <remarks/>
        public rootStepsItemItemsConsolidationDefnCriteriaCriteriaSet CriteriaSet
        {
            get
            {
                return this.criteriaSetField;
            }
            set
            {
                this.criteriaSetField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte CriterionCount
        {
            get
            {
                return this.criterionCountField;
            }
            set
            {
                this.criterionCountField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsConsolidationDefnCriteriaCriterion1
    {

        private string classnameField;

        private string assemblyField;

        private string columnNameField;

        private string operatorField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ColumnName
        {
            get
            {
                return this.columnNameField;
            }
            set
            {
                this.columnNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Operator
        {
            get
            {
                return this.operatorField;
            }
            set
            {
                this.operatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsConsolidationDefnCriteriaCriteriaSet
    {

        private string classnameField;

        private string assemblyField;

        private byte criteriaCountField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte CriteriaCount
        {
            get
            {
                return this.criteriaCountField;
            }
            set
            {
                this.criteriaCountField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsConsolidationDefnOrderBys
    {

        private rootStepsItemItemsConsolidationDefnOrderBysOrderBy1 orderBy1Field;

        private rootStepsItemItemsConsolidationDefnOrderBysOrderBy2 orderBy2Field;

        private string classnameField;

        private string assemblyField;

        private byte orderByCountField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("OrderBy.1")]
        public rootStepsItemItemsConsolidationDefnOrderBysOrderBy1 OrderBy1
        {
            get
            {
                return this.orderBy1Field;
            }
            set
            {
                this.orderBy1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("OrderBy.2")]
        public rootStepsItemItemsConsolidationDefnOrderBysOrderBy2 OrderBy2
        {
            get
            {
                return this.orderBy2Field;
            }
            set
            {
                this.orderBy2Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte OrderByCount
        {
            get
            {
                return this.orderByCountField;
            }
            set
            {
                this.orderByCountField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsConsolidationDefnOrderBysOrderBy1
    {

        private string classnameField;

        private string assemblyField;

        private string columnNameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ColumnName
        {
            get
            {
                return this.columnNameField;
            }
            set
            {
                this.columnNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsConsolidationDefnOrderBysOrderBy2
    {

        private string classnameField;

        private string assemblyField;

        private string columnNameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ColumnName
        {
            get
            {
                return this.columnNameField;
            }
            set
            {
                this.columnNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsExistsFilters
    {

        private rootStepsItemItemsExistsFiltersItems itemsField;

        private string classnameField;

        private string assemblyField;

        private string collectionNameField;

        private byte collectionCountField;

        private string itemClassNameField;

        private string itemAssemblyNameField;

        /// <remarks/>
        public rootStepsItemItemsExistsFiltersItems items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CollectionName
        {
            get
            {
                return this.collectionNameField;
            }
            set
            {
                this.collectionNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte CollectionCount
        {
            get
            {
                return this.collectionCountField;
            }
            set
            {
                this.collectionCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ItemClassName
        {
            get
            {
                return this.itemClassNameField;
            }
            set
            {
                this.itemClassNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ItemAssemblyName
        {
            get
            {
                return this.itemAssemblyNameField;
            }
            set
            {
                this.itemAssemblyNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsExistsFiltersItems
    {

        private rootStepsItemItemsExistsFiltersItemsItem itemField;

        /// <remarks/>
        public rootStepsItemItemsExistsFiltersItemsItem item
        {
            get
            {
                return this.itemField;
            }
            set
            {
                this.itemField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsExistsFiltersItemsItem
    {

        private rootStepsItemItemsExistsFiltersItemsItemItems itemsField;

        /// <remarks/>
        public rootStepsItemItemsExistsFiltersItemsItemItems Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsExistsFiltersItemsItemItems
    {

        private rootStepsItemItemsExistsFiltersItemsItemItemsLinkColumns linkColumnsField;

        private string existsMemoryTableNameField;

        /// <remarks/>
        public rootStepsItemItemsExistsFiltersItemsItemItemsLinkColumns LinkColumns
        {
            get
            {
                return this.linkColumnsField;
            }
            set
            {
                this.linkColumnsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ExistsMemoryTableName
        {
            get
            {
                return this.existsMemoryTableNameField;
            }
            set
            {
                this.existsMemoryTableNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsExistsFiltersItemsItemItemsLinkColumns
    {

        private rootStepsItemItemsExistsFiltersItemsItemItemsLinkColumnsItems itemsField;

        private string classnameField;

        private string assemblyField;

        private string collectionNameField;

        private byte collectionCountField;

        private string itemClassNameField;

        private string itemAssemblyNameField;

        /// <remarks/>
        public rootStepsItemItemsExistsFiltersItemsItemItemsLinkColumnsItems items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CollectionName
        {
            get
            {
                return this.collectionNameField;
            }
            set
            {
                this.collectionNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte CollectionCount
        {
            get
            {
                return this.collectionCountField;
            }
            set
            {
                this.collectionCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ItemClassName
        {
            get
            {
                return this.itemClassNameField;
            }
            set
            {
                this.itemClassNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ItemAssemblyName
        {
            get
            {
                return this.itemAssemblyNameField;
            }
            set
            {
                this.itemAssemblyNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsExistsFiltersItemsItemItemsLinkColumnsItems
    {

        private rootStepsItemItemsExistsFiltersItemsItemItemsLinkColumnsItemsItem itemField;

        /// <remarks/>
        public rootStepsItemItemsExistsFiltersItemsItemItemsLinkColumnsItemsItem item
        {
            get
            {
                return this.itemField;
            }
            set
            {
                this.itemField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsExistsFiltersItemsItemItemsLinkColumnsItemsItem
    {

        private rootStepsItemItemsExistsFiltersItemsItemItemsLinkColumnsItemsItemItems itemsField;

        private string keyField;

        /// <remarks/>
        public rootStepsItemItemsExistsFiltersItemsItemItemsLinkColumnsItemsItemItems Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Key
        {
            get
            {
                return this.keyField;
            }
            set
            {
                this.keyField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsExistsFiltersItemsItemItemsLinkColumnsItemsItemItems
    {

        private string fromValueField;

        private string toValueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string FromValue
        {
            get
            {
                return this.fromValueField;
            }
            set
            {
                this.fromValueField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ToValue
        {
            get
            {
                return this.toValueField;
            }
            set
            {
                this.toValueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsColumnDefns
    {

        private rootStepsItemItemsColumnDefnsItem[] itemsField;

        private string classnameField;

        private string assemblyField;

        private string collectionNameField;

        private byte collectionCountField;

        private string itemClassNameField;

        private string itemAssemblyNameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("item", IsNullable = false)]
        public rootStepsItemItemsColumnDefnsItem[] items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string assembly
        {
            get
            {
                return this.assemblyField;
            }
            set
            {
                this.assemblyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CollectionName
        {
            get
            {
                return this.collectionNameField;
            }
            set
            {
                this.collectionNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte CollectionCount
        {
            get
            {
                return this.collectionCountField;
            }
            set
            {
                this.collectionCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ItemClassName
        {
            get
            {
                return this.itemClassNameField;
            }
            set
            {
                this.itemClassNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ItemAssemblyName
        {
            get
            {
                return this.itemAssemblyNameField;
            }
            set
            {
                this.itemAssemblyNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsColumnDefnsItem
    {

        private rootStepsItemItemsColumnDefnsItemItems itemsField;

        private string keyField;

        /// <remarks/>
        public rootStepsItemItemsColumnDefnsItemItems Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Key
        {
            get
            {
                return this.keyField;
            }
            set
            {
                this.keyField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootStepsItemItemsColumnDefnsItemItems
    {

        private string columnNameField;

        private ushort fileStartPositionField;

        private byte fieldLengthField;

        private string idField;

        private string FileColumnIndexField;

        private string padCharacterField;

        private byte dataTypeField;

        private bool dataTypeFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ColumnName
        {
            get
            {
                return this.columnNameField;
            }
            set
            {
                this.columnNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string FileColumnIndex
        {
            get
            {
                return this.FileColumnIndexField;
            }
            set
            {
                this.FileColumnIndexField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ushort FileStartPosition
        {
            get
            {
                return this.fileStartPositionField;
            }
            set
            {
                this.fileStartPositionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte FieldLength
        {
            get
            {
                return this.fieldLengthField;
            }
            set
            {
                this.fieldLengthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PadCharacter
        {
            get
            {
                return this.padCharacterField;
            }
            set
            {
                this.padCharacterField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte DataType
        {
            get
            {
                return this.dataTypeField;
            }
            set
            {
                this.dataTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DataTypeSpecified
        {
            get
            {
                return this.dataTypeFieldSpecified;
            }
            set
            {
                this.dataTypeFieldSpecified = value;
            }
        }
    }


}


