using System;
using System.Collections.Generic;

namespace WebApplication6
{
    public partial class tables
    {
        public tables()
        {
            fields = new HashSet<fields>();
        }

        public int table_id { get; set; }
        public int? source_interface_id { get; set; }
        public int? system_id { get; set; }
        public string table_name { get; set; }

        public virtual ICollection<fields> fields { get; set; }
        public virtual systems system { get; set; }
    }
}
