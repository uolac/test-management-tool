﻿

using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
//using Microsoft.SqlServer.Management.SqlParser;
using System.Reflection;
using Microsoft.SqlServer.TransactSql.ScriptDom;
using System.IO;
using System.Diagnostics;



namespace Interfaces
{
    class Program
    {
        public static string defaultdatabase;
        public static string query;
        public static TSqlFragment fragment;
        public static List<Query> queries = new List<Query>();
        public static List<Table> tables = new List<Table>();
        public static List<DistinctTable> distincttables = new List<DistinctTable>();
        //public static StreamWriter streamwriter_debug;
        //public static StreamWriter streamwriter_debug_2;

        public static void Start(int interfaceid, bool current)
        {
            queries.Clear();
            tables.Clear();
            distincttables.Clear();
                         

            defaultdatabase = "StagingLive";

            //string FileName = "C:\\Users\\aCoady\\Temp\\QUERY.txt";

            //Console.BufferHeight = 20000;      

            string listconcat = "";


            SqlConnection con2 = new SqlConnection();
            con2.ConnectionString = "Server=ASQL02;Database=testmanagementtool;Integrated Security=true;";

            //con.ConnectionString = "Server=AREP01;Database=WebPayments;Integrated Security=true;";
            con2.Open();

            //string Output = "C:\\Users\\aCoady\\Temp\\Output.txt";

                //var sr1 = new StreamReader(FileName);
                //listconcat = sr1.ReadToEnd();

                var com = new SqlCommand();
                com.Connection = con2;
                if(current)
                {
                    com.CommandText = "SELECT script FROM testmanagementtool.dbo.interfaces WHERE interface_id = @param";
                }
                else
                {
                    com.CommandText = "SELECT future_script FROM testmanagementtool.dbo.interfaces WHERE interface_id = @param";
                }
               
                com.Parameters.AddWithValue("@param", interfaceid);

                listconcat  = (string)com.ExecuteScalar();
            com.Parameters.Clear();

            //var com2 = new SqlCommand();
            //com2.Connection = con;
            //com2.CommandText = "SELECT OBJECT_DEFINITION(OBJECT_ID('"+ script +"')) ";

            //var proc = ((string)com2.ExecuteScalar());


            //listconcat = proc.Substring(proc.IndexOf("AS")+2);

            //throw new Exception();


            var sr2 = new StringReader(listconcat);
                     

            query = listconcat; 


            
            //string Output = "C:\\Users\\aCoady\\Temp\\Output.txt";
            //string OutputDebug = "C:\\Users\\aCoady\\Temp\\Output_Debug.txt";
            //string OutputDebug2 = "C:\\Users\\aCoady\\Temp\\Output_Debug2.txt";
            //StreamWriter streamwriter = new StreamWriter(Output);
            //streamwriter_debug = new StreamWriter(OutputDebug);
            //streamwriter_debug_2 = new StreamWriter(OutputDebug2);


           

            var parser = new TSql120Parser(true);
            IList<ParseError> errors;


            fragment = parser.Parse(sr2, out errors);

            var tyy = errors;
            var fyy = fragment.ScriptTokenStream;

            if (errors.Count() > 0)
            { throw new Exception(); }
            
            

            TableAnalysis tableanalysis = new TableAnalysis();

           fragment.Accept(tableanalysis);



            Debug2(listconcat);

         

            var k2 = queries.SelectMany(q => q.sources.Union(new List<Table>() { q.target }));


            var ttt = k2.Where(x => x.alias == "");
           var k = queries.SelectMany(q => q.sources.Union(new List<Table>() { q.target })).Where(x => x == null ? false : !x.name.Contains("@")).Select(x => (x.server == null ? "ASMSDB01" : x.server) + "." + (x.database == null ? defaultdatabase : x.database) + "." + (x.schema == null ? "dbo" : x.schema) + "." + x.name);
           foreach (string t in k.Distinct())
           {
               var split = t.Split('.');


               //if (split[1].ToLower() == "qlsdat") { split[0] = "asmsdev01"; split[1] = "c_qlsdat"; }
               //if (split[1].ToLower() == "qlfdat") { split[0] = "asmsdev01"; split[1] = "c_qlfdat"; }
               //if (split[1].ToLower() == "ulusertables") { split[0] = "asmsdev01"; split[1] = "ulusertables"; }

               //Program.streamwriter_debug.WriteLine("DISTINCT_TABLE" + t);

               //Console.WriteLine("DISTINCT_TABLE" + t);

               //SqlCommand com_2 = new SqlCommand("SELECT ORDINAL_POSITION,COLUMN_NAME,DATA_TYPE,ISNULL(CHARACTER_MAXIMUM_LENGTH,0) AS CHARACTER_MAXIMUM_LENGTH FROM " + split[0] + "." + split[1] + ".INFORMATION_SCHEMA.columns WHERE TABLE_SCHEMA = @schema AND TABLE_NAME = @name", con);
               SqlCommand com_2 = new SqlCommand("SELECT column_position AS ORDINAL_POSITION, field_name AS COLUMN_NAME,field_type AS DATA_TYPE,ISNULL(field_length,0) AS CHARACTER_MAXIMUM_LENGTH FROM testmanagementtool.dbo.tables INNER JOIN testmanagementtool.dbo.fields ON tables.table_id = fields.table_id WHERE table_name = @tbl", con2);


                var tblp = split[1] + "." + split[2] + "." + split[3];

                com_2.Parameters.AddWithValue("@tbl", tblp);

               List<Column> col_list = new List<Column>();

               try
               {
                   var reader2 = com_2.ExecuteReader();
                    com_2.Parameters.Clear();


                    DataTable dt = new DataTable();
                   dt.Load(reader2);
                   reader2.Close();
                   com_2.Dispose();

                   

                  // Console.WriteLine("ROWS:" + dt.Rows.Count);
                   foreach (var dr in dt.AsEnumerable())
                   {
                       var col = new Column() { ordinal = (int)dr["ORDINAL_POSITION"], name = ((string)dr["COLUMN_NAME"]).ToLower(), datatype = (string)dr["DATA_TYPE"], datatype_val = (int)dr["CHARACTER_MAXIMUM_LENGTH"] };
                      // Console.WriteLine(col.name + " " + col.datatype);
                       col_list.Add(col);
                      




                   }
               }
               catch { }

               distincttables.Add(new DistinctTable() {server = split[0],database = split[1],schema = split[2],name = split[3], columns = col_list});
              
           }

            int querynum = 0;

            var batch = Guid.NewGuid().ToString();

            foreach (Query q in queries)
            {
                querynum++;
                var dsc = q.selectcolumns.GroupBy(x => new { x.name, x.ordinal }).Select(x => x.First());
                q.selectcolumns = dsc.ToList();

                //streamwriter.WriteLine();
               
                var targetstr = "n.n.n.n";

                if(q.target !=null) {targetstr = q.target.server + "." + q.target.database + "." + q.target.schema + "." + q.target.name;}

                //streamwriter.WriteLine("Destination: " + targetstr);
//streamwriter.WriteLine("Ordinal#SourceFieldName#Description#KnownAs#ExampleValue#Size#DataType#SourceTable#Transform#DestinationSize#DestinationDataType#DestinationTable#Original");
                foreach (Table t in q.sources)
                {
                    //Console.WriteLine("SOURCE TABLE: " + t.database + "; " + t.schema + "; " + t.name + "; " + t.alias);
                }

                foreach (Column w in q.selectcolumns)
                {
                    if (w.table_alias == null)
                    {
                        var matched_table = q.sources;
                        if (matched_table.Count() > 0)
                        {



                            var distinct_match_cols = distincttables.Where(x => matched_table.Select(y => y.name).Contains(x.name)).Where(x => x.columns.Exists(y => y.name == w.name));

                            if (distinct_match_cols.Count() == 1)
                            {
                                var t = q.sources.Where(x => x.name == distinct_match_cols.First().name).First();
                                w.table_alias = t.alias;
                                //sw.WriteLine("FOUNDTHEALIAS" + t.alias);
                            }
                            else if (distinct_match_cols.Count() > 1)
                            {
                                //Console.WriteLine("NULLAMBIG");
                            }
                            else
                            {
                                //Console.WriteLine("NULLNF2");
                            }
                        }
                        else
                        {

                           // Console.WriteLine("NULLNF3");
                        }

                    }
                }

                foreach (Column w in q.selectcolumns.ToList())
                {
                    if (w.name == "star")
                    {
                        StarCol(q,w);
                        if (w.sourcetable.columns.Count == 0)
                        {
                            w.name = "SelectStarUnreachableColumns";
                        }
                        else
                        {
                            q.selectcolumns.Remove(w);
                        }
                    }
                 }

                 foreach (Column w in q.selectcolumns)
                {
                    var colstring = new string[13];
                    colstring[9] = ""; colstring[10] = ""; colstring[11] = "";

                    colstring[1] = w.name;
                    colstring[2] = "-";

                   if (w.destinationcolumn != null)
                   {
                       colstring[3] = w.destinationcolumn;
                   }                   
                   else if (q.insertintocolumns.Count>0)
                   {

                       if (w.absordinal-1 < q.insertintocolumns.Count && w.absordinal-1 >= 0)
                       {
                           colstring[3] = q.insertintocolumns[w.absordinal - 1].name;
                           colstring[11] = q.target.name + q.insertintocolumns[w.absordinal - 1].name;
                       }
                       else
                       {

                           colstring[3] = "DESTORDINSERTINTO";
   
                       }
                   } 
                   else
                   {
                        IEnumerable<DistinctTable> destination_match = new List<DistinctTable>();

                       if (q.target != null)
                       {
                            destination_match = distincttables.Where(x => x.name == q.target.name);
                       }
                       

                        if (destination_match.Count() > 0)
                        {
                            var destination_column_match = destination_match.First().columns.Where(x => x.ordinal == w.absordinal);
                            if (destination_column_match.Count() > 0)
                            {
                                var dc = destination_column_match.First();
                                colstring[3] = dc.name; colstring[10] = dc.datatype; colstring[9] = dc.datatype_val.ToString();

                                colstring[11] = (q.target.name ?? "") + "." + dc.name;
                            }
                            else
                            {
                                if (w.column_alias != null)
                                {
                                    colstring[3] = w.column_alias;
                                    colstring[11] = (q?.target?.name ?? "Unknown") + "." + w.column_alias;
                                }
                                else
                                {

                                    if (destination_match.First().columns.Count() == 0)
                                    {
                                        colstring[3] += w.ordinal + "SOURCETNOCOLSFOUND";
                                    }
                                    else
                                    {

                                        colstring[3] = w.ordinal + "DESTORDNOTFOUND";
                                    }
                                }

                            }
                        }
                        else
                        {
                            if (w.column_alias != null)
                            {
                                colstring[3] = w.column_alias;
                                colstring[11] = (q?.target?.name ?? "Unknown") + "." + w.column_alias;
                            }
                            else
                            {
                                colstring[3] = "DESTORDNOTFOUND";
                            }
                        }
                   }

                   colstring[4] = " - ";


                        if (w.table_alias != null)
                        {
                            //streamwriter.WriteLine(w.table_alias);
                            var matched_table = q.sources.Where(x => x.alias == w.table_alias);
                            if (matched_table.Count() > 0)
                            {
                                var distinct_match = distincttables.Where(x => x.name == matched_table.First().name);
                                if (distinct_match.Count() > 0)
                                {
                                    var t = distinct_match.First();
                                    w.sourcetable = t;

                                    //Program.streamwriter_debug.WriteLine(w.name+" "+w.sourcetable.name+" "+w.sourcetable.columns.Count);

                                    var column_match = w.sourcetable.columns.Where(x => x.name == w.name);
                                    if (column_match.Count() > 0)
                                    {
                                        var c = column_match.First();
                                        colstring[5] = (c.datatype_val == 0 ? " - " : c.datatype_val.ToString()); colstring[6] = c.datatype; colstring[7] = t.server + "." + t.database + "." + t.schema + "." + t.name;
                                    }
                                    else
                                    {
                                        colstring[5] = "SOURCECOLNOTFOUND"; colstring[6]="-";colstring[7]="-";colstring[8]="-";
                                    }



                                }
                                else
                                {
                                    colstring[5] = "SOURCETBLNOTFOUND"; colstring[6]="-";colstring[7]="-";colstring[8]="-";
                                }
                            }
                            else
                            {

                                if (w.table_alias == "Fixed")
                                {
                                    colstring[5] = "-"; colstring[6]="-";colstring[7]="-";colstring[8]="-";
                                }
                                else
                                {
                                    colstring[5] = "See subquery "+w.table_alias+"."+w.name; colstring[6] = "-"; colstring[7] = "-";
                                }
                            }
                        }
                        else
                        {

                            if (w.name == "null" || w.isliteral)
                            {
                                colstring[5] = "-"; colstring[6]="-";colstring[7]="-";colstring[8]="-";
                            }
                            else
                            {
                                colstring[5] = "SOURCETBNOALIAS"; colstring[6]=q.target.name+"."+w.name;colstring[7]="-";colstring[8]="-";
                            }
                        }
                    
                   
                                

                                string woriginal = "";
                                if (w.original != null) { woriginal = RemoveWhitespace(w.original); }
                                colstring[12] = woriginal;
                     colstring[8] = w.transform.Any() ? w.transform.Aggregate((x, y) => x + "," + y) : "" ;
                     colstring[0] = w.absordinal.ToString();

                     //streamwriter.WriteLine(colstring.Aggregate((x,y) => x + "#"+y));

                    var com2 = new SqlCommand();
                    com2.Connection = con2;



                   
                    if (colstring[7]=="-")
                    {
                        //com2.CommandText = "INSERT INTO testmanagementtool.dbo.holding_table VALUES ('" + interfaceid + "'," + querynum + ",'" + targetstr + "','" + colstring.Aggregate((x, y) => x + "~" + y).Replace("'", "").Replace("~","','")+ "','-','-','-','-','" + (targetstr.Split('.')[0] == "" ? "ASMSDB01" : targetstr.Split('.')[0]) + "','" + (targetstr.Split('.')[1] == "" ? "StagingLive" : targetstr.Split('.')[1]) + "','" + (targetstr.Split('.')[2] == "" ? "dbo" : targetstr.Split('.')[2]) + "','" + targetstr.Split('.')[3] + "'," + (current ? 0 : 1)+",'" + q.wherecolumns + "')";
                        
                        com2.CommandText = "INSERT INTO testmanagementtool.dbo.holding_table VALUES (@interface_id,@querynum ,@targetstr,@colstring0,@colstring1,@colstring2,@colstring3,@colstring4,@colstring5,@colstring6,@colstring7,@colstring8,@colstring9,@colstring10,@colstring11,@colstring12,'-','-','-','-', @serverstr,@dbstr,@schemastr,@tablestr,@current,@wherecols)";

 
                        com2.Parameters.AddWithValue("@interface_id", interfaceid);
                        com2.Parameters.AddWithValue("@querynum", querynum);
                        com2.Parameters.AddWithValue("@targetstr", targetstr ?? "");
                        com2.Parameters.AddWithValue("@colstring0", colstring[0] ?? "");
                        com2.Parameters.AddWithValue("@colstring1", colstring[1] ?? "");
                        com2.Parameters.AddWithValue("@colstring2", colstring[2] ?? "");
                        com2.Parameters.AddWithValue("@colstring3", colstring[3] ?? "");
                        com2.Parameters.AddWithValue("@colstring4", colstring[4] ?? "");
                        com2.Parameters.AddWithValue("@colstring5", colstring[5] ?? "");
                        com2.Parameters.AddWithValue("@colstring6", colstring[6] ?? "");
                        com2.Parameters.AddWithValue("@colstring7", colstring[7] ?? "");
                        com2.Parameters.AddWithValue("@colstring8", colstring[8] ?? "");
                        com2.Parameters.AddWithValue("@colstring9", colstring[9] ?? "");
                        com2.Parameters.AddWithValue("@colstring10", colstring[10] ?? "");
                        com2.Parameters.AddWithValue("@colstring11", colstring[11] ?? "");
                        com2.Parameters.AddWithValue("@colstring12", colstring[12] ?? "");
                        com2.Parameters.AddWithValue("@tablestr", targetstr.Split('.')[3]);
                        com2.Parameters.AddWithValue("@serverstr", (targetstr.Split('.')[0] == "" ? "ASMSDB01" : targetstr.Split('.')[0]));
                        com2.Parameters.AddWithValue("@dbstr", (targetstr.Split('.')[1] == "" ? "StagingLive" : targetstr.Split('.')[1]));
                        com2.Parameters.AddWithValue("@schemastr", (targetstr.Split('.')[2] == "" ? "dbo" : targetstr.Split('.')[2]));
                        com2.Parameters.AddWithValue("@current", (current ? 0 : 1));
                        com2.Parameters.AddWithValue("@wherecols", q.wherecolumns ?? "");
                        

                    }
                    else
                    {
                        //com2.CommandText = "INSERT INTO testmanagementtool.dbo.holding_table VALUES ('" + interfaceid + "'," + querynum + ",'" + targetstr + "','" + colstring.Aggregate((x, y) => x + "~" + y).Replace("'", "").Replace("~", "','") + "','" + (colstring[7].Split('.')[0] == "" ? "ASMSDB01" : colstring[7].Split('.')[0]) + "','" + (colstring[7].Split('.')[1] == "" ? "StagingLive" : colstring[7].Split('.')[1]) + "','" + (colstring[7].Split('.')[2] == "" ? "dbo" : colstring[7].Split('.')[2]) + "','" + colstring[7].Split('.')[3] + "','" + (targetstr.Split('.')[0] == "" ? "ASMSDB01" : targetstr.Split('.')[0]) + "','" + (targetstr.Split('.')[1] == "" ? "StagingLive" : targetstr.Split('.')[1]) + "','" + (targetstr.Split('.')[2] == "" ? "dbo" : targetstr.Split('.')[2]) + "','" + targetstr.Split('.')[3] + "'," + (current ? 0 : 1) + ",'" + q.wherecolumns+"')"; 

                        com2.CommandText = "INSERT INTO testmanagementtool.dbo.holding_table VALUES (@interface_id,@querynum ,@targetstr,@colstring0,@colstring1,@colstring2,@colstring3,@colstring4,@colstring5,@colstring6,@colstring7,@colstring8,@colstring9,@colstring10,@colstring11,@colstring12,@sourceserver,@sourcedb,@sourceschema,@sourcetable, @serverstr,@dbstr,@schemastr,@tablestr,@current,@wherecols)";


                        com2.Parameters.AddWithValue("@interface_id", interfaceid);
                        com2.Parameters.AddWithValue("@querynum", querynum);
                        com2.Parameters.AddWithValue("@targetstr", targetstr ?? "");
                        com2.Parameters.AddWithValue("@colstring0", colstring[0] ?? "");
                        com2.Parameters.AddWithValue("@colstring1", colstring[1] ?? "");
                        com2.Parameters.AddWithValue("@colstring2", colstring[2] ?? "");
                        com2.Parameters.AddWithValue("@colstring3", colstring[3] ?? "");
                        com2.Parameters.AddWithValue("@colstring4", colstring[4] ?? "");
                        com2.Parameters.AddWithValue("@colstring5", colstring[5] ?? "");
                        com2.Parameters.AddWithValue("@colstring6", colstring[6] ?? "");
                        com2.Parameters.AddWithValue("@colstring7", colstring[7] ?? "");
                        com2.Parameters.AddWithValue("@colstring8", colstring[8] ?? "");
                        com2.Parameters.AddWithValue("@colstring9", colstring[9] ?? "");
                        com2.Parameters.AddWithValue("@colstring10", colstring[10] ?? "");
                        com2.Parameters.AddWithValue("@colstring11", colstring[11] ?? "");
                        com2.Parameters.AddWithValue("@colstring12", colstring[12] ?? "");
                        com2.Parameters.AddWithValue("@sourcetable", colstring[7].Split('.')[3]);
                        com2.Parameters.AddWithValue("@sourceserver", (colstring[7].Split('.')[0] == "" ? "ASMSDB01" : colstring[7].Split('.')[0]));
                        com2.Parameters.AddWithValue("@sourcedb", (colstring[7].Split('.')[1] == "" ? "StagingLive" : colstring[7].Split('.')[1]));
                        com2.Parameters.AddWithValue("@sourceschema", (colstring[7].Split('.')[2] == "" ? "dbo" : colstring[7].Split('.')[2]));
                        com2.Parameters.AddWithValue("@tablestr", targetstr.Split('.')[3]);
                        com2.Parameters.AddWithValue("@serverstr", (targetstr.Split('.')[0] == "" ? "ASMSDB01" : targetstr.Split('.')[0]));
                        com2.Parameters.AddWithValue("@dbstr", (targetstr.Split('.')[1] == "" ? "StagingLive" : targetstr.Split('.')[1]));
                        com2.Parameters.AddWithValue("@schemastr", (targetstr.Split('.')[2] == "" ? "dbo" : targetstr.Split('.')[2]));
                        com2.Parameters.AddWithValue("@current", (current ? 0 : 1));
                        com2.Parameters.AddWithValue("@wherecols", q.wherecolumns ?? "");
                    }
                    
                    com2.ExecuteNonQuery();
                    com2.Parameters.Clear();

                }
                 foreach (Column w in q.fromcolumns)
                 {
                     //Console.WriteLine("FROM COLUMN: " + w.table_alias + "; " + w.name);

                     
                 }
            }

            var com3 = new SqlCommand("", con2);

            com3.CommandText = "DELETE FROM testmanagementtool.dbo.links WHERE interface_id = @interfaceid AND is_future =  @isfuture";
            com3.Parameters.AddWithValue("@interfaceid", interfaceid);
            com3.Parameters.AddWithValue("@isfuture", (current ? 0 : 1));
            
            com3.ExecuteNonQuery();
            com3.Parameters.Clear();

            com3.CommandText = "DELETE testmanagementtool.dbo.fields FROM testmanagementtool.dbo.fields INNER JOIN testmanagementtool.dbo.tables ON fields.table_id = tables.table_id WHERE source_interface_id = @sintfid";
            com3.Parameters.AddWithValue("@sintfid", interfaceid * (current ? 1 : -1));
            com3.ExecuteNonQuery();
            com3.Parameters.Clear();

            com3.CommandText = "DELETE FROM testmanagementtool.dbo.tables WHERE source_interface_id = @sintfid";
            com3.Parameters.AddWithValue("@sintfid", interfaceid * (current ? 1 : -1));

            com3.ExecuteNonQuery();
            com3.Parameters.Clear();

            foreach (var dtbl in distincttables.Where(dt => dt.isvartable))
            {
                com3.CommandText = "INSERT INTO testmanagementtool.dbo.tables VALUES ('-1',@sintfid,@dtblname)";
                com3.Parameters.AddWithValue("@sintfid", (interfaceid * (current ? 1 : -1)));
                com3.Parameters.AddWithValue("@dtblname", dtbl.name ?? "");
                com3.ExecuteNonQuery();
                com3.Parameters.Clear();
                com3.CommandText = "SELECT MAX(table_id) FROM testmanagementtool.dbo.tables";
                var dtblid = (int)com3.ExecuteScalar();
                com3.Parameters.Clear();
                foreach (var dcol in dtbl.columns)
                {
                    //com3.CommandText = "INSERT INTO testmanagementtool.dbo.fields VALUES ('"+dtblid+"','" + dcol.ordinal + "','" + dcol.name + "','" + dcol.datatype+ "','" + dcol.datatype_val + "', NULL, NULL, 0, NULL, NULL)";
                    com3.CommandText = "INSERT INTO testmanagementtool.dbo.fields VALUES (@dtblid,@dcolordinal,@dcolname,@dcoldatatype,@dcoldatatype_val, NULL, NULL, 0, NULL, NULL)";
                    com3.Parameters.AddWithValue("@dtblid", dtblid);
                    com3.Parameters.AddWithValue("@dcolordinal", dcol.ordinal);
                    com3.Parameters.AddWithValue("@dcolname", dcol.name ?? "");
                    com3.Parameters.AddWithValue("@dcoldatatype", dcol.datatype ?? "");
                    com3.Parameters.AddWithValue("@dcoldatatype_val", dcol.datatype_val);

                    com3.ExecuteNonQuery();
                    com3.Parameters.Clear();
                }
            }
            con2.Close();
            //streamwriter.Close();
            //streamwriter_debug.Close();
            //streamwriter_debug_2.Close();

            //Process.Start("notepad.exe", OutputDebug2);
            //Process.Start("notepad.exe", Output);
            

            //Console.ReadKey();



        }

        public static void StarCol(Query q, Column w)
        {

  
                            var matched_table = q.sources;
                            if (matched_table.Count() > 0)
                            {
                                var m = matched_table.First();
                                var distinct_match = distincttables.Where(x => x.name == m.name);
                                if (distinct_match.Count() > 0)
                                {
                                    var t = distinct_match.First();
                                    w.sourcetable = t;

                                    int i = 1;                                   
                                 
                                    foreach (Column c in t.columns)
                                    {
                                        
                                        var col = new Column() { ordinal = i, name = c.name.ToLower(), table_alias = m.alias };
                                        Debug(col.name, w.sourcetable.name, w.sourcetable.alias ?? "NULL");
                                       
                                        q.selectcolumns.Add(col);
                                        i++;
                                    }




                                }
                                else
                                {
                                    //Console.WriteLine("STARNF2");
                                }
                            }
                            else
                            {

                               // Console.WriteLine("STARNF3");
                            }
                        

        }

        public static void Debug(Object s)
        {
            //Program.streamwriter_debug.WriteLine(s.ToString());
        }
        public static void Debug(Object s, Object s2)
        {
            //Program.streamwriter_debug.WriteLine(s.ToString() + " / " + s2.ToString());
        }

        public static void Debug(Object s, Object s2, Object s3)
        {
            //Program.streamwriter_debug.WriteLine(s.ToString() + " / " + s2.ToString() + " / " + s3.ToString());
        }

        public static void Debug2(Object s)
        {
            //Program.streamwriter_debug_2.WriteLine(s?.ToString());
        }
        public static void Debug2(Object s, Object s2)
        {
            //Program.streamwriter_debug_2.WriteLine(s.ToString() + " / " + s2.ToString());
        }

        public static void Debug2(Object s, Object s2, Object s3)
        {
            //Program.streamwriter_debug_2.WriteLine(s.ToString() + " / " + s2.ToString() + " / " + s3.ToString());
        }

        public static void Debug2(Object s, Object s2, Object s3, Object s4)
        {
           // Program.streamwriter_debug_2.WriteLine(s.ToString() + " / " + s2.ToString() + " / " + s3.ToString() + " / " + s4.ToString());
        }

        public static string RemoveWhitespace(string input)
        {
            return new string(input.ToCharArray()
                .Where(c => !Char.IsWhiteSpace(c))
                .ToArray());
        }
        
    }

    

    public class Query
    {
        public int id = -1;
        public static int curr_id = 0;
        public Table target;
        public List<Column> selectcolumns = new List<Column>();
        public string wherecolumns;
        public List<Column> fromcolumns = new List<Column>();
        public List<Column> insertintocolumns = new List<Column>();
        public List<Table> sources = new List<Table>();
        public string name = "";

        public Query()
        {
            id = curr_id;
            curr_id++;
        }



    }

    public class DistinctTable
    {
        public string server;
        public string database;
        public string schema;
        public string name;
        public string alias;
        public bool isvartable = false;
        public List<Column> columns = new List<Column>();
    }

    public class Table
    {
        public Table(TSqlFragment name_)
        {
            var s = TableAnalysis.GetStringFromTSqlFragment(name_);

            string u;

            var sp_ = s.IndexOf(" ");

            if (sp_ != -1)
            {
                u = s.Substring(0, sp_).ToLower();
            }
            else
            {
                u = s.ToLower();
            }

            string[] t = u.Split('.');

            if (t.Count() == 4) { server = t[0]; database = t[1]; schema = t[2]; name = t[3]; alias = t[3]; }
            else if (t.Count() == 3) { database = t[0]; schema = t[1]; name = t[2]; alias = t[2]; }
            else if (t.Count() == 2) { schema = t[0]; name = t[1]; alias = t[1]; }
            else if (t.Count() == 1) { name = t[0]; alias = t[0]; }
            else { name = "OTHER"; }



        }
        public Table(TableReference name_, string alias_) : this(name_)
        {
            alias = alias_;

        }

        public Table(string alias_)
        {
            name = alias = alias_;

        }

        public string server;
        public string database;
        public string schema;
        public string name;
        public string alias;
    }

    public class Column
    {
        public Column(ColumnReferenceExpression name_)
        {
            var s = TableAnalysis.GetStringFromTSqlFragment(name_);
            var t = s.ToLower().Split('.');
            if (t.Count() == 2) { table_alias = t[0]; name = t[1]; }
            else if (t.Count() == 1) { name = t[0]; }
            else { name = "OTHER"; }
        }

        public Column(string name_)
        {
            name = name_.ToLower();
        }


        public Column()
        {
        }
        public int ordinal;
        public int absordinal;
        public string table_alias;
        public string column_alias;
        public string name;
        public string datatype;
        public int datatype_val;
        public DistinctTable sourcetable;
        public string destinationcolumn;
        public string original;
        public List<string> transform = new List<string>();
        public bool isliteral = false;

    }



}





