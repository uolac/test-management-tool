using System;
using System.Collections.Generic;

namespace WebApplication6
{
    public partial class entities
    {
        public entities()
        {
            fields = new HashSet<fields>();
        }

        public int entity_id { get; set; }
        public string entity_name { get; set; }

        public virtual ICollection<fields> fields { get; set; }
    }
}
