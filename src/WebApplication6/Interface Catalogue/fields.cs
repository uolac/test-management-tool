using System;
using System.Collections.Generic;

namespace WebApplication6
{
    public partial class fields
    {
        public fields()
        {
            links = new HashSet<links>();
            linksNavigation = new HashSet<links>();
        }

        public int field_id { get; set; }
        public int? column_position { get; set; }
        public string description { get; set; }
        public int? entity_id { get; set; }
        public int? field_length { get; set; }
        public string field_name { get; set; }
        public string field_type { get; set; }
        public int? fixed_position { get; set; }
        public string front_end_field { get; set; }
        public bool is_entity_master { get; set; }
        public int? table_id { get; set; }

        public virtual ICollection<links> links { get; set; }
        public virtual ICollection<links> linksNavigation { get; set; }
        public virtual entities entity { get; set; }
        public virtual tables table { get; set; }
    }
}
