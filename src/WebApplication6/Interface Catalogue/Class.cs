﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication6
{
    public class CanvasViewModel
    {
        public CVMTable maintable;
        public List<CVMTable> tables;
        public List<CVMLink> links;
        public CVMLink link;
        public CVMField field;
        public int num_field_selected;
        public List<int> included_interfaces;
        public List<int> all_interfaces;
        public List<string> all_interfaces_names;
    }

    public class CanvasInitialViewModel
    {
        public int initial_id;
        public string initial_type;
    }

    public class CanvasRecieveParams
    {
        public int original_id { get; set; }
        public string original_type { get; set; }
        public int new_id { get; set; }
        public string new_type { get; set; }
        public int original_x { get; set; }
        public int original_y { get; set; }
        public int new_x { get; set; }
        public int new_y { get; set; }
        public List<int> including_interface_ids { get; set; }
        public bool is_current { get; set; }
    }


    public class CVMTable
    {
        public int table_id, x, y;
        public string table_name;
        public List<CVMField> fields;
        public bool isforward;
    }

    public class CVMField
    {
        public int field_id;
        public string field_name;
        public string table;
        public string system;
        public string entity;
        public string datatype;
        public string column_order;
    }

    public class CVMLink
    {
        public int link_id;
        public int interface_id;
        public string interface_name;
        public int x1, y1, x2, y2;
        public int source_field_id;
        public int dest_field_id;
        public string source_field;
        public string dest_field;
        public string source_field_table;
        public string dest_field_table;
        public string source_field_type;
        public string dest_field_type;
        public bool isforward;
    }
}
