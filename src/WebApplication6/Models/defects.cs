using System;
using System.Collections.Generic;

namespace WebApplication6
{
    public partial class defects
    {
        public defects()
        {
            defect_history = new HashSet<defect_history>();
            defects_attachments = new HashSet<defects_attachments>();
            defect_type_id = 1;
            phase_id = 1;
        }

        public int defect_id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int found_by { get; set; }
        public DateTime found_date { get; set; }
        public int last_modified_by { get; set; }
        public DateTime last_modified_date { get; set; }
        public DateTime? due_date { get; set; }
        public int? module_id { get; set; }
        public string external_ref { get; set; }
        public int project_id { get; set; }
        public string tags { get; set; }
        public int? test_run_id { get; set; }
        public int defect_type_id { get; set; }       
        public int phase_id { get; set; }      

        public virtual ICollection<defect_history> defect_history { get; set; }
        public virtual users found_byNavigation { get; set; }
        public virtual runs test_run { get; set; }
        public virtual users last_modified_byNavigation { get; set; }
        public virtual modules module { get; set; }
        public virtual projects project { get; set; }
        public virtual ICollection<defects_attachments> defects_attachments { get; set; }
        public defect_types defect_type { get; set; }
        public phases phase { get; set; }
    }
}