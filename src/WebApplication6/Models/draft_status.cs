using System;
using System.Collections.Generic;

namespace WebApplication6
{
    public partial class draft_status
    {
        public draft_status()
        {
            requirements = new HashSet<requirements>();
            tests = new HashSet<tests>();
        }

        public int draft_status_id { get; set; }
        public string name { get; set; }

        public virtual ICollection<requirements> requirements { get; set; }
        public virtual ICollection<tests> tests { get; set; }
    }
}
