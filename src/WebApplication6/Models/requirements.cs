using System;
using System.Collections.Generic;

namespace WebApplication6
{
    public partial class requirements
    {
        public requirements()
        {
            requirements_tests = new HashSet<requirements_tests>();
        }

        public int requirement_id { get; set; }
        public string cross_reference { get; set; }
        public string description { get; set; }
        public int draft_status_id { get; set; }
        public DateTime effective_date { get; set; }
        public int last_modified_by { get; set; }
        public DateTime last_modified_date { get; set; }
        public int? lineage_id { get; set; }
        public int? module_id { get; set; }
        public string name { get; set; }
        public int? owner_id { get; set; }
        public int project_id { get; set; }
        public string tags { get; set; }
        public bool accepted_will_not_be_met { get; set; }
        public bool compulsory_yn { get; set; }
        public DateTime? date_made_live { get; set; }

        public virtual draft_status draft_status { get; set; }
        public virtual users last_modified_byNavigation { get; set; }
        public virtual modules module { get; set; }
        public virtual users owner { get; set; }
        public virtual projects project { get; set; }
        public virtual ICollection<requirements_tests> requirements_tests { get; set; }
    }
}
