using System;
using System.Collections.Generic;

namespace WebApplication6
{
    public partial class defect_history
    {
        public int defect_history_id { get; set; }
        public int? assigned_to { get; set; }
        public int defect_id { get; set; }
        public int defect_status_id { get; set; }
        public DateTime effective_date { get; set; }
        public int last_modified_by { get; set; }
        public DateTime last_modified_date { get; set; }
        public int priority_id { get; set; }
        public int closure_reason_id { get; set; }
        public int? severity_id { get; set; }
        public string status_comment { get; set; }

        public virtual users assigned_toNavigation { get; set; }
        public virtual defects defect { get; set; }
        public virtual defect_status defect_status { get; set; }
        public virtual closure_reason closure_reason { get; set; }
        public virtual users last_modified_byNavigation { get; set; }
        public virtual priorities priority { get; set; }
        public virtual severities severity { get; set; }
    }
}
