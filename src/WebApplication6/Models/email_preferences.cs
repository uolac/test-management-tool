using System;
using System.Collections.Generic;

namespace WebApplication6
{
    public partial class email_preferences
    {
        public email_preferences()
        {
            users = new HashSet<users>();
        }
        public int email_preference_id { get; set; }
        public string name { get; set; }

        public virtual ICollection<users> users { get; set; }
    }
}
