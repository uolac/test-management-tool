using System;
using System.Collections.Generic;

namespace WebApplication6
{
    public partial class defect_status
    {
        public defect_status()
        {
            defect_history = new HashSet<defect_history>();
        }

        public int defect_status_id { get; set; }
        public string name { get; set; }

        public virtual ICollection<defect_history> defect_history { get; set; }
    }
}
