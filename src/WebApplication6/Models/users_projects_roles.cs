using System;
using System.Collections.Generic;

namespace WebApplication6
{
    public partial class users_projects_roles
    {
        public int users_projects_roles_id { get; set; }
        public int last_modified_by { get; set; }
        public DateTime last_modified_date { get; set; }
        public int project_id { get; set; }
        public int role_id { get; set; }
        public int user_id { get; set; }

        public virtual users last_modified_byNavigation { get; set; }
        public virtual projects project { get; set; }
        public virtual roles role { get; set; }
        public virtual users user { get; set; }
    }
}
