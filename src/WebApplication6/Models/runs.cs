using System;
using System.Collections.Generic;

namespace WebApplication6
{
    public partial class runs
    {
        public runs()
        {
            defects = new HashSet<defects>();
            test_runs_test_steps = new HashSet<test_runs_test_steps>();
            attachments = new HashSet<runs_attachments>();
        }
        public int run_id { get; set; }
        public string description { get; set; }
        public int last_modified_by { get; set; }
        public DateTime last_modified_date { get; set; }
        public int outcome_id { get; set; }
        public int run_by { get; set; }
        public DateTime run_date { get; set; }
        public int test_cycle_id { get; set; }
        public int? test_id { get; set; }

        public virtual users last_modified_byNavigation { get; set; }
        public virtual outcomes outcome { get; set; }
        public virtual users run_byNavigation { get; set; }
        public virtual ICollection<runs_attachments> attachments { get; set; }
        public virtual test_cycles test_cycle { get; set; }
        public virtual tests test { get; set; }
        public virtual ICollection<defects> defects { get; set; }
        public virtual ICollection<test_runs_test_steps> test_runs_test_steps { get; set; }
    }
}
