using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace WebApplication6
{
    public partial class tests
    {
        public tests()
        {
            runs = new HashSet<runs>();
            test_steps = new HashSet<test_steps>();
            requirements_tests = new HashSet<requirements_tests>();
            test_cycles_tests = new HashSet<test_cycles_tests>();
        }

        public int test_id { get; set; }
        public int project_id { get; set; }
        public int? module_id { get; set; }
        public string description { get; set; }
        public int draft_status_id { get; set; }
        public DateTime effective_date { get; set; }
        public int last_modified_by { get; set; }
        public DateTime last_modified_date { get; set; }
        public int? lineage_id { get; set; }
        public string name { get; set; }
        public int? owner_id { get; set; }
        public int priority_id { get; set; }
        public string tags { get; set; }
        public string test_item { get; set; }
        public DateTime? date_made_live { get; set; }

        public virtual ICollection<runs> runs { get; set; }
        public virtual ICollection<test_steps> test_steps { get; set; }
        public virtual ICollection<test_cycles_tests> test_cycles_tests { get; set; }
        public virtual draft_status draft_status { get; set; }
        public virtual users last_modified_byNavigation { get; set; }
        public virtual users owner { get; set; }
        public virtual projects project { get; set; }
        public virtual modules module { get; set; }
        public virtual priorities priority { get; set; }
        public virtual ICollection<requirements_tests> requirements_tests { get; set; }
    }
}
