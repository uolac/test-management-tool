using System;
using System.Collections.Generic;

namespace WebApplication6
{
    public partial class closure_reason
    {
        public closure_reason()
        {
            defect_history = new HashSet<defect_history>();
        }

        public int closure_reason_id { get; set; }
        public string name { get; set; }

        public virtual ICollection<defect_history> defect_history { get; set; }
    }
}
