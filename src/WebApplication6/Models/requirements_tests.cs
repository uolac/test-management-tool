using System;
using System.Collections.Generic;

namespace WebApplication6
{
    public partial class requirements_tests
    {
        public int requirements_tests_id { get; set; }
        public int? requirement_id { get; set; }
        public int? test_id { get; set; }

        public virtual requirements requirement { get; set; }
        public virtual tests test { get; set; }
    }
}
