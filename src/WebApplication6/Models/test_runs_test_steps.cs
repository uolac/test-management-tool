using System;
using System.Collections.Generic;

namespace WebApplication6
{
    public partial class test_runs_test_steps
    {

        public int test_runs_test_steps_id { get; set; }
        public int run_id { get; set; }
        public int test_step_id { get; set; }
        public int completed { get; set; }

        public virtual runs run { get; set; }
        public virtual test_steps test_steps { get; set; }
    }
}
