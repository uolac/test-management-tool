using System;
using System.Collections.Generic;

namespace WebApplication6
{
    public partial class projects
    {
        public projects()
        {
            modules = new HashSet<modules>();
            tests = new HashSet<tests>();
            requirements = new HashSet<requirements>();
            test_cycles = new HashSet<test_cycles>();
            users = new HashSet<users>();
            defects = new HashSet<defects>();
            users_projects_roles = new HashSet<users_projects_roles>();
        }

        public int project_id { get; set; }
        public int? current_test_cycle_id { get; set; }
        public string description { get; set; }
        public int last_modified_by { get; set; }
        public DateTime last_modified_date { get; set; }
        public DateTime start_date { get; set; }
        public DateTime? end_date { get; set; }
        public string name { get; set; }
        public int? owner_id { get; set; }

        public virtual ICollection<tests> tests { get; set; }
        public virtual ICollection<modules> modules { get; set; }
        public virtual ICollection<requirements> requirements { get; set; }
        public virtual ICollection<test_cycles> test_cycles { get; set; }
        public virtual ICollection<users> users { get; set; }
        public virtual ICollection<defects> defects { get; set; }
        public virtual ICollection<users_projects_roles> users_projects_roles { get; set; }
        public virtual test_cycles current_test_cycle { get; set; }
        public virtual users last_modified_byNavigation { get; set; }
        public virtual users owner { get; set; }
    }
}
