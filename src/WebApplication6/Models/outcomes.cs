using System;
using System.Collections.Generic;

namespace WebApplication6
{
    public partial class outcomes
    {
        public outcomes()
        {
            runs = new HashSet<runs>();
        }

        public int outcome_id { get; set; }
        public string name { get; set; }

        public virtual ICollection<runs> runs { get; set; }
    }
}
