using System;
using System.Collections.Generic;

namespace WebApplication6
{
    public partial class roles
    {
        public roles()
        {
            users_projects_roles = new HashSet<users_projects_roles>();
        }

        public int role_id { get; set; }
        public string name { get; set; }

        public virtual ICollection<users_projects_roles> users_projects_roles { get; set; }
    }
}
