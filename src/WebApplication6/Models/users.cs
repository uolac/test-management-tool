using System;
using System.Collections.Generic;

namespace WebApplication6
{
    public partial class users
    {
        public users()
        {
            runs_attachments = new HashSet<runs_attachments>();
            defects_attachments = new HashSet<defects_attachments>();
            defect_history = new HashSet<defect_history>();
            defect_historyNavigation = new HashSet<defect_history>();
            defects = new HashSet<defects>();
            defectsNavigation = new HashSet<defects>();
            modules = new HashSet<modules>();
            projects = new HashSet<projects>();
            projectsNavigation = new HashSet<projects>();
            requirements = new HashSet<requirements>();
            requirementsNavigation = new HashSet<requirements>();
            runs = new HashSet<runs>();
            runsNavigation = new HashSet<runs>();
            test_cycles = new HashSet<test_cycles>();
            test_cycles_tests = new HashSet<test_cycles_tests>();
            test_cycles_testsNavigation = new HashSet<test_cycles_tests>();
            test_steps = new HashSet<test_steps>();
            tests = new HashSet<tests>();
            testsNavigation = new HashSet<tests>();
            users_projects_roles = new HashSet<users_projects_roles>();
            users_projects_roles_lmb_Navigation = new HashSet<users_projects_roles>();
        }

        public int user_id { get; set; }
        public string ad_identifier { get; set; }
        public int? default_project_id { get; set; }
        public string email { get; set; }
        public int? email_preference_id { get; set; }
        public int last_modified_by { get; set; }
        public DateTime last_modified_date { get; set; }
        public string name { get; set; }
        public bool is_administrator { get; set; }

        public virtual ICollection<defects_attachments> defects_attachments { get; set; }
        public virtual ICollection<runs_attachments> runs_attachments { get; set; }
        public virtual ICollection<defect_history> defect_history { get; set; }
        public virtual ICollection<defect_history> defect_historyNavigation { get; set; }
        public virtual ICollection<defects> defects { get; set; }
        public virtual ICollection<defects> defectsNavigation { get; set; }

        public virtual ICollection<modules> modules { get; set; }
        public virtual ICollection<projects> projects { get; set; }
        public virtual ICollection<projects> projectsNavigation { get; set; }
        public virtual ICollection<requirements> requirements { get; set; }
        public virtual ICollection<requirements> requirementsNavigation { get; set; }
        public virtual ICollection<runs> runs { get; set; }
        public virtual ICollection<runs> runsNavigation { get; set; }
        public virtual ICollection<test_cycles> test_cycles { get; set; }
        public virtual ICollection<test_cycles_tests> test_cycles_tests { get; set; }
        public virtual ICollection<test_cycles_tests> test_cycles_testsNavigation { get; set; }
        public virtual ICollection<test_steps> test_steps { get; set; }
        public virtual ICollection<tests> tests { get; set; }
        public virtual ICollection<tests> testsNavigation { get; set; }
        public virtual ICollection<users_projects_roles> users_projects_roles { get; set; }
        public virtual ICollection<users_projects_roles> users_projects_roles_lmb_Navigation { get; set; }
        public virtual email_preferences email_preference { get; set; }
        public virtual projects default_project { get; set; }
        public virtual users last_modified_byNavigation { get; set; }
        public virtual ICollection<users> Inverselast_modified_byNavigation { get; set; }
    }
}
