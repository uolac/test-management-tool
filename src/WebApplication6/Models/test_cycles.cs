using System;
using System.Collections.Generic;

namespace WebApplication6
{
    public partial class test_cycles
    {
        public test_cycles()
        {
            projects = new HashSet<projects>();
            runs = new HashSet<runs>();
        }

        public int test_cycle_id { get; set; }
        public string description { get; set; }
        public int last_modified_by { get; set; }
        public DateTime last_modified_date { get; set; }
        public string name { get; set; }
        public int project_id { get; set; }

        public virtual ICollection<projects> projects { get; set; }
        public virtual ICollection<runs> runs { get; set; }
        public virtual ICollection<test_cycles_tests> test_cycles_tests { get; set; }
        public virtual users last_modified_byNavigation { get; set; }
        public virtual projects project { get; set; }
    }
}
