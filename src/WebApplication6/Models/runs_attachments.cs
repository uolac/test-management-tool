using System;
using System.Collections.Generic;

namespace WebApplication6
{
    public partial class runs_attachments
    {
        public runs_attachments()
        {

        }

        public int run_attachment_id { get; set; }
        public int run_id { get; set; }
        public byte[] attachment_content { get; set; }
        public string attachment_mimetype { get; set; }
        public string attachment_filename { get; set; }
        public int last_modified_by { get; set; }
        public DateTime last_modified_date { get; set; }

        public virtual runs runs { get; set; }
        public virtual users last_modified_byNavigation { get; set; }
    }
}
