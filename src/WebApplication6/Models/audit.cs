using System;
using System.Collections.Generic;

namespace WebApplication6
{
    public partial class audit
    {
        public int audit_id { get; set; }
        public string username { get; set; }
        public string entity { get; set; }
        public string entity_pk { get; set; }
        public string property { get; set; }
        public string original { get; set; }
        public string @new { get; set; }
        public string action { get; set; }
        public int modified_by { get; set; }
        public DateTime modified_date { get; set; }
    }
}
