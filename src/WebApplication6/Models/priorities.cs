using System;
using System.Collections.Generic;

namespace WebApplication6
{
    public partial class priorities
    {
        public priorities()
        {
            defect_history = new HashSet<defect_history>();
            tests = new HashSet<tests>();
        }

        public int priority_id { get; set; }
        public string name { get; set; }

        public virtual ICollection<defect_history> defect_history { get; set; }
        public virtual ICollection<tests> tests { get; set; }
    }
}
