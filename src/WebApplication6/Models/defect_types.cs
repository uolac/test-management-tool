using System;
using System.Collections.Generic;

namespace WebApplication6
{
    public partial class defect_types
    {
        public defect_types()
        {
            defects = new HashSet<defects>();
        }

        public int defect_type_id { get; set; }
        public string name { get; set; }

        public virtual ICollection<defects> defects { get; set; }
    }
}
