using System;
using System.Collections.Generic;

namespace WebApplication6
{
    public partial class severities
    {
        public severities()
        {
            defect_history = new HashSet<defect_history>();
        }

        public int severity_id { get; set; }
        public string name { get; set; }

        public virtual ICollection<defect_history> defect_history { get; set; }
    }
}
