using System;
using System.Collections.Generic;

namespace WebApplication6
{
    public partial class phases
    {
        public phases()
        {
            defects = new HashSet<defects>();
        }

        public int phase_id { get; set; }
        public string name { get; set; }

        public virtual ICollection<defects> defects { get; set; }
    }
}
