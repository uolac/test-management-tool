using System;
using System.Collections.Generic;

namespace WebApplication6
{
    public partial class scheduled_status
    {
        public scheduled_status()
        {
            test_cycles_tests = new HashSet<test_cycles_tests>();
        }

        public int scheduled_status_id { get; set; }
        public string name { get; set; }

        public virtual ICollection<test_cycles_tests> test_cycles_tests { get; set; }
    }
}
