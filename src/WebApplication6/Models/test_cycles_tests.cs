using System;
using System.Collections.Generic;

namespace WebApplication6
{
    public partial class test_cycles_tests
    {
        public int test_cycle_test_id { get; set; }
        public int last_modified_by { get; set; }
        public DateTime last_modified_date { get; set; }
        public DateTime? scheduled_end_date { get; set; }
        public DateTime? scheduled_start_date { get; set; }
        public int scheduled_status_id { get; set; }
        public int? scheduled_user { get; set; }
        public int test_cycle_id { get; set; }
        public int test_id { get; set; }

        public virtual users last_modified_byNavigation { get; set; }
        public virtual scheduled_status scheduled_status { get; set; }
        public virtual users scheduled_userNavigation { get; set; }
        public virtual test_cycles test_cycle { get; set; }
        public virtual tests test { get; set; }
    }
}
