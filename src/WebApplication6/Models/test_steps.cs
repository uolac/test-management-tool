using System;
using System.Collections.Generic;

namespace WebApplication6
{
    public partial class test_steps
    {
        public test_steps()
            {
            test_runs_test_steps = new HashSet<test_runs_test_steps>();
            }
        public int test_step_id { get; set; }
        public string description { get; set; }
        public string expected_result { get; set; }
        public int last_modified_by { get; set; }
        public DateTime last_modified_date { get; set; }
        public byte[] attachment_content { get; set; }
        public string attachment_mimetype { get; set; }
        public string attachment_filename { get; set; }
        public int seq_no { get; set; }
        public int? test_id { get; set; }

        public virtual users last_modified_byNavigation { get; set; }
        public virtual tests test { get; set; }
        public virtual ICollection<test_runs_test_steps> test_runs_test_steps { get; set; }
    }
}
