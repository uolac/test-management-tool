using System;
using System.Collections.Generic;

namespace WebApplication6
{
    public partial class modules
    {
        public modules()
        {
            defects = new HashSet<defects>();
            requirements = new HashSet<requirements>();
            tests = new HashSet<tests>();
        }

        public int module_id { get; set; }
        public string description { get; set; }
        public int last_modified_by { get; set; }
        public DateTime last_modified_date { get; set; }
        public string name { get; set; }
        public int project_id { get; set; }

        public virtual ICollection<defects> defects { get; set; }
        public virtual ICollection<requirements> requirements { get; set; }
        public virtual ICollection<tests> tests { get; set; }
        public virtual users last_modified_byNavigation { get; set; }
        public virtual projects project { get; set; }
    }
}
